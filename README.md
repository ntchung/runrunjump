## KIWI RUN!
Codename: SEA-OTTERS

Please find a list of the latest release notes for Kiwi Run below:

### 1.4.1 "Served" (Monday, October 8, 2012)
- Platform Versions:
	- basil tag 0.0.17-warden-4 (custom buffered textViews + currentUser.js)
	- android tag 2.3.6
- Release Notes:
	- server-side storage
	- moved to managed item product IDs
	- fixes for users who purchased levels / avatars twice
	- new Korean translations
	- new experiment on hiding Trainer Kiwi
	- fixed leaderboard scrolling



### 1.4.0 "Chatty Kiwi" (Friday, October 5, 2012)
- Platform versions:
    - basil tag 0.0.17-beta-3 (custom buffered textViews + currentUser.js)
    - android tag 2.3.1 (custom GCSocial.java for local notifs)
- Release Notes:
    - Social leaderboards
    - Friend callouts
    - Firefly level
    - Halloween level
    - New Kiwis



### 1.3.4 "No More Bibimbap" (Friday, September 21, 2012)
- Platform Versions:
	- basil tag 0.0.16-social-20
	- android tag 2.2.6
- Release Notes:
	- Terms of Service
	- Angel Save modal can save you when you die
	- Equip modal to encourage more consumable purchases
	- ABCD-test for Angel Save / Equip modals
	- Korean translation fixes
	- new analytics events
	- sound on/off toggle button
	- new purchasable ($0.99) baby kiwis: Lilly and Kai
	- basil changes



### 1.3.3 "Hawtfix" (Wednesday, September 12, 2012)
- Platform Versions:
	- gc_sdk tag kiwi-korea-beta-1
	- android tag 2.1.7-beta-8
- Release Notes:
	- rebuilt APK using hot-fixed social data analytics



### 1.3.2 "Image of Success" (Thursday, September 6, 2012)
- Platform Versions:
	- gc_sdk tag 0.0.7-beta-5
	- android tag 2.1.7-beta-6 + cherry-picks, thanks Jared!
- Release Notes:
	- converted GameTextViews to ScoreViews
		- ScoreViews use Images and ImageViews instead of bitmap fonts and offscreen canvases to avoid redraw issues
		- gameplay is now 100% images with no text rendering
	- Ace Kiwi now purchasable for $0.99; fixed a bug where the store was not preventing multiple purchases
	- removed input event cancellation from ButtonViews which made scrolling seem broken on some ScrollViews
	- Nestables screen re-worked with new art
	- sprite sheet improvements
- Known Bugs:
	- we should add a lock on purchasing Ace that releases if a purchase doesn't happen within a time limit or on purchase callback



### Release 1.3.1

**Nickname:** i_refuse_to_create_a_nickname_for_every_release

- Korean release
- GameTextView to ImageViews instead of offscreen buffers
- Boost icons for $0.99 and Ace Kiwi
- Achievements, Callouts, and Ready Go converted to images
- Localization for images and text
- Reduced object instantiation, PlayerViews cleaned up, and more preloading
- Turtle and AngelFish powerup displays

**sdkVersion:** 0.0.7-beta-5

lib/runtimeNative has a cherry-pick `5f1d5ac260b45596fb6dd0f05050b5cfab0b870b` from `devsdk-bugfix-stroke-text` branch

**androidVersion:** 2.1.7-beta-7

<pre>
in `TeaLeaf/jni/core` change line 168 in `draw_textures.c`

diff --git a/draw_textures.c b/draw_textures.c
index 615bed7..fcbe3a9 100644
--- a/draw_textures.c
+++ b/draw_textures.c
@@ -168,7 +168,7 @@ void draw_textures_flush() {
	                                glUniform4f(global_shaders[current_shader].draw_color, lastOpacity, lastOpacity, lastOpacity, lastOpacity);
									                        } else if (last_filter_type == FILTER_MULTIPLY) {
																                                tealeaf_shaders_bind(PRIMARY_SHADER);
																								-                               glUniform4f(global_shaders[current_shader].draw_color, last_add_color.r, last_add_color.g, last_add_color.b, lastOpacity);
																								+                               glUniform4f(global_shaders[current_shader].draw_color, last_add_color.r* lastOpacity, last_add_color.g* lastOpacity, last_add_color.b * lastOpacity, lastOpacity);
																								                        }
																														                }
</pre>

**Known bugs:** slight lag first time achievement modal comes up.



### 1.2.0 "Over-Achiever" (Thursday, August 30, 2012)
- Platform Versions:
	- gc_sdk tag 0.0.7-beta-5
	- android tag 2.1.7-beta-2
- Release Notes:
	- added Achievement system and UI
		- 50/50 experiment on new users; all old users will have this system
	- rating modal now displays based on games played instead of app sessions
		- 50/50 experiment to determine how many games (20 or 50) should be completed before asking for a rating
	- gameplay changes
		- fixes a bug where quitting a run while gliding would cause the game to run backwards on next run
		- dropping off of gliders now causes the kiwi to jump
	- increased in-app purchase berry values
		- updated in-app store icons to reflect these changes as "sales"
		- added a "Berry Sale" modal that redirects old users to see the new prices
	- new Loading screens
		- we display a loading screen on all screen transitions
			- the loading screen before gameplay includes "Trainer Tips", and displays for a minimum of 3 seconds
		- preloading and transitioning flow reworked
			- preloading starts after loading screen is visible
			- screens that are not active are removed from the view hierarchy
		- IcePop Beat splash screen is no longer stretched on app resume
		- IcePop Beat splash screen is preloaded first and separately from bitmap fonts on resume
	- new art, animations, and music
		- new animations for "work-out" kiwis on the Main Menu
		- new sprite sheets for kiwi gameplay animations, including darkened strokes etc.
		- new updated Title screen
		- new nighttime ambiance music for Achievements and Stats screens
		- new store music
		- updated in-game music
	- native fixes
		- low-end phones no longer flicker and glitch when transitioning between screens
		- sleeping a newer phone with its power button no longer restarts the app
	- work-arounds
		- off-screen canvases are now being redrawn when users start a new run
- Known Bugs:
	- pausing and resuming the app on low-end phones can occasionally lead to graphical issues
		- on rare occasions, low-end phones flicker on resume
		- on rare occasions, low-end phones lose textures on resume
		- the aforementioned work-around redraws off-screen canvases when a new run begins because we found that exiting the app while browsing the menus of the game could cause gameplay text to redraw without strokes
