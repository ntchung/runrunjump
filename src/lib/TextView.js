import ui.TextView as TextView;

exports = Class(TextView, function (supr) {
  this.init = function (opts) {
    opts.y = opts.y - 7;
    if (opts.strokeColor) {
      opts.strokeWidth = 4;
    }
    supr(this, 'init', [opts]);
  };
});
