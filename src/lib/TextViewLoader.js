import ui.View as View;
import ui.TextView as TextView;

exports = Class(View, function(supr) {

	var NUM_BUFFERS = 5;

	this.init = function(opts) {

		this.buffers = [];
		this.config = GC.app.controller.config;

		supr(this, 'init', opts);		

		this.designView();
	}

	this.designView = function() {
		var i;
		
		for ( i = 0; i < NUM_BUFFERS; i++ ) {
			this.buffers[i] = new TextView({
				parent: this,
				x: 0,
				y: 0,
				width: 10,
				height: 10,
				size: this.config.fonts.callout,
				color: this.config.colors.calloutFill,
				strokeColor: this.config.colors.calloutStroke,
				buffer: true,
				visible: false
			});	
		}
	};

	this.preload = function(textPreloads, cb, iRoot, jRoot, delay) {
		var 
			bufferGroup,
			buffer,
			bufferIndex = 0,
			textList,
			i, len,
			j, jLen,
			iStart = iRoot !== undefined ? iRoot : 0,
			jStart = jRoot !== undefined ? jRoot : 0,
			delayPast = delay !== undefined ? delay : 0;

		if ( textPreloads == null ) { 
			for ( i = 0; i < NUM_BUFFERS; i++ ) {
				buffer = this.buffers[i];
				buffer.setText("");		
				buffer.style.visible = false;
			}
			cb(); return; 
		}

		for ( i = iStart, len = textPreloads.length; i < len; i++ ) {
			bufferGroup = textPreloads[i];	
			textList = bufferGroup.text;

			for ( j = jStart, jLen = textList.length; j < jLen; j++ ) {
				if ( bufferIndex !== NUM_BUFFERS ) {
					buffer = this.buffers[bufferIndex++];
					buffer.updateOpts({
						size: bufferGroup.font,
						color: bufferGroup.color,
						strokeColor: bufferGroup.strokeStyle,
						multiline: bufferGroup.multiline,
						text: textList[j]
					});
					buffer.style.width = bufferGroup.width;
					buffer.style.height = bufferGroup.height;
					buffer.style.visible = true;
				} else {
					setTimeout(bind(this, this.preload, textPreloads, cb, i, j + 1, delayPast + 1000), 1000);	
					return;
				}
			}
		}

		setTimeout(bind(this, cb, delayPast + 500), 500);
	};

});
