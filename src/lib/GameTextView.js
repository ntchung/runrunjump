import device;
import ui.View as View;

var FontRenderer = device.get('FontRenderer');

exports = Class(View, function(supr) {

	var CANVAS_WIDTH = 1024,
		CANVAS_HEIGHT = 1024,
		BASE_TEXT_WIDTH = 16;

	var uid = 0;
	this.init = function(opts) {
		// block input events before super init
		opts.blockEvents = true;
		opts.canHandleEvents = false;
		supr(this, 'init', [opts]);

		this.opts = opts;
		this.uid = uid++;
		this.renderReady = false;

		// text options
		this.font = opts.font;
		this.color = opts.color || 'rgb(0, 0, 0)';
		this.strokeStyle = opts.strokeStyle;
		this.textAlign = opts.textAlign || 'center';

		// sizing and spacing
		this.srcWidth = opts.srcWidth;
		this.srcHeight = opts.srcHeight;
		this.destWidth = opts.destWidth;
		this.destHeight = opts.destHeight;
		this.spacing = opts.spacing || 0;
		this.monospaced = opts.monospaced || false;
		this.specialCharacters = opts.specialCharacters || {};

		// available characters
		this.characters = opts.characters;

		// characters that should be rendered
		this.activeCharacters = [];

		// calculate size of a space
		this.fontSize = parseInt(this.font.split("p")[0]);
		this.spaceSize = this.fontSize / 2;

		// character data
		this.characterMap = {};

		// set up canvas size
		var x = 0, y = 0, cols = 1, rows = 1;
		for (var i = 0; i < this.characters.length; i++) {
			var character = this.characters[i];
			if (character == " ") {
				continue;
			}

			x += this.srcWidth;
			if (y == 0) {
				cols++;
			}

			if (x + this.srcWidth >= CANVAS_WIDTH) {
				rows++;
				x = 0;
				y += this.srcHeight;
				if (y + this.srcHeight >= CANVAS_HEIGHT) {
					logger.log("WARNING: ScoreView ran out of space for characters!");
					break;
				}
			}
		}

		// initialize offscreen canvas
		this.canvWidth = cols * this.srcWidth;
		this.canvHeight = rows * this.srcHeight;
		this.canvas = new GCCanvas({ width: this.canvWidth, height: this.canvHeight });
		this.ctx = this.canvas.getContext('2d');
		this.ctx.clear();
		this.ctx.loadIdentity();
		this.ctx.font = this.font;
		this.ctx.textAlign = 'center';
		this.ctx.textBaseline = 'middle';
		this.ctx.fillStyle = this.color;
		if (this.strokeStyle) {
			this.ctx.strokeStyle = this.strokeStyle;
		}

		// pull font data from FontRenderer
		this.fontInfo = FontRenderer.findFontInfo(this.ctx);
		this.charData = this.fontInfo.customFont.dimensions;

		// render the characters we want to use offscreen
		this.textToSet = opts.text;
		this.renderTextOffscreen();

		// offscreen buffer needs to be redrawn on app resume
		GC.app.subscribe('gameLoaded', this, 'restoreCanvas');
	};

	this.restoreCanvas = function() {
		logger.log("RESTORE CANVAS CALLED");
		this.renderTextOffscreen();
	};

	// draw characters to our off-screen canvas
	this.renderTextOffscreen = function() {
		this.ctx.clear();
		this.ctx.loadIdentity();
		this.ctx.font = this.font;
		this.ctx.textAlign = 'center';
		this.ctx.textBaseline = 'middle';
		this.ctx.fillStyle = this.color;
		if (this.strokeStyle) {
			this.ctx.strokeStyle = this.strokeStyle;
		}

		var x = 0, y = 0, halfWidth = this.srcWidth / 2, halfHeight = this.srcHeight / 2;
		for (var i = 0; i < this.characters.length; i++) {
			var character = this.characters[i];
			if (character == " ") {
				continue;
			}

			if (this.strokeStyle) {
				this.ctx.strokeText(character, x + halfWidth, y + halfHeight);
			}
			this.ctx.fillText(character, x + halfWidth, y + halfHeight);

			this.characterMap[character] = {
				x: x,
				y: y
			};

			x += this.srcWidth;
			if (x + this.srcWidth >= CANVAS_WIDTH) {
				x = 0;
				y += this.srcHeight;
				if (y + this.srcHeight >= CANVAS_HEIGHT) {
					logger.log("WARNING: ScoreView ran out of space for characters!");
					break;
				}
			}
		}

		this.renderReady = true;

		if (this.textToSet) {
			this.setText(this.textToSet);
			this.textToSet = false;
		}
	};

	this.setText = function(text) {
		if (!this.renderReady) {
			return;
		}

		text = text + "";
		this.textWidth = BASE_TEXT_WIDTH;

		var i = 0, data;
		while (i < text.length) {
			var character = text.charAt(i);
			var index = this.characters.indexOf(character);
			if (index >= 0) {
				if (i >= this.activeCharacters.length) {
					data = {};
				} else {
					data = this.activeCharacters[i];
				}

				var map = this.characterMap[character];
				data.character = character;
				data.srcX = map ? map.x : -1;
				data.srcY = map ? map.y : -1;

				// special x offsets to fix text kerning only affect text width if it's first or last char
				var special = this.specialCharacters[character];
				if (special && (i == 0 || i == text.length - 1)) {
					this.textWidth += special.x;
				}

				if (character == " ") {
					this.textWidth += this.spaceSize;
				} else if (this.monospaced) {
					this.textWidth += this.spacing;
				} else {
					this.textWidth += this.charData[character.charCodeAt(0)].ow + this.spacing;
				}

				this.activeCharacters[i] = data;
			}

			i++;
		}

		if (this.textAlign == 'center') {
			this.offset = (this.style.width - this.textWidth) / 2;
		} else if (this.textAlign == 'right') {
			this.offset = this.style.width - this.textWidth;
		} else {
			this.offset = 0;
		}

		// trims excess characters, TODO save objects
		this.activeCharacters.length = text.length;
	};

	this.render = function(ctx) {
		if (!this.renderReady) {
			return;
		}

		var x = this.offset, y = 0;
		for (var i = 0; i < this.activeCharacters.length; i++) {
			var data = this.activeCharacters[i];

			// special x offsets to fix text kerning
			var special = this.specialCharacters[data.character];
			if (special) {
				x += special.x;
			}

			if (data.character != " ") {
				ctx.drawImage(this.canvas, data.srcX, data.srcY, this.srcWidth, this.srcHeight, x, y, this.destWidth, this.destHeight);
			}

			if (data.character == " ") {
				x += this.spaceSize;
			} else if (this.monospaced) {
				x += this.spacing;
			} else {
				x += this.charData[data.character.charCodeAt(0)].ow + this.spacing - (special ? special.x : 0);
			}
		}
	};
});
