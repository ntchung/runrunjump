import ui.View;
import src.vendor.localize as vendor.localize;

from base import logging;


exports = (function(){

	var
		instance,
		DEFAULT_LOCALE = 'en';


	/**
	 * Constructor
	 */
	function LocaleManager() {
		if (window.NATIVE && NATIVE.locale) {
			if (NATIVE.locale.language === 'ko') {
				this.locale = 'ko-kr';
			} else if (NATIVE.locale.language === 'ja') {
				this.locale = 'ja-jp';
			} else {
				this.locale = DEFAULT_LOCALE;
			}
		} else {
			this.locale = DEFAULT_LOCALE;
		}
	}

	/**
	 * Singleton
	 */
	function getInstance() {
		if ( instance == null ) {
			instance = new LocaleManager();
		}

		return instance;
	}


	/**
	 * Inheritance
	 */
	LocaleManager.prototype.constructor = LocaleManager;
	LocaleManager.prototype.logger = logging.get(LocaleManager.name);

	/**
	 * Class methods
	 */

	/**
	 * getLocalizeInstance
	 */
	LocaleManager.prototype.getLocalizeInstance = function() {
		var localePath = 'resources/data/locales/' + this.locale + '.json',
			localeObj,
			nodeObj = {},
			i;

		if ( this.localizeInst == null ) {
			if ( this.locale !== DEFAULT_LOCALE ) {
				try {
					localeObj = JSON.parse(window.CACHE[localePath]);
				} catch (e) {
					logger.error('Locale not found. defaulting to English.');
					localeObj = {};
				}
			}

			// put into format for node-localize
			for ( i in localeObj ) {
				nodeObj[i] = {};
				nodeObj[i][this.locale] = localeObj[i];
			}

			this.localizeInst = new vendor.localize(nodeObj);
			this.localizeInst.setLocale(this.locale);
		}

		return this.localizeInst;
	};

	//Iterates through ticker arguments, finds any that also have their own args, translates everything
	LocaleManager.prototype.translateTickerArgs = function(args){
		var argsNew = [];

		for(var i = 0; i < args.length; i++){
			if(Object.prototype.toString.apply(args[i]) === '[object Array]'){
				argsNew.push(GC.app.l.translate.apply(this, args[i]));
			}
			else{
				argsNew.push(GC.app.l.translate(args[i]));
			}
		}

		return argsNew;
	}


	/**
	 * getLocale
	 */
	LocaleManager.prototype.getLocale = function() {
		return this.locale;
	};


	return getInstance();

}());
