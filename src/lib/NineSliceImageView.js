import ui.View as View;
import ui.ImageView as ImageView;
import event.Callback;

from ui.filter import LinearAddFilter;

exports = Class("NineSliceImageView", View, function (logger, supr) {

	this.defaults = {
		image: null,
		postfix: ["_01", "_02", "_03", "_04", "_05", "_06", "_07", "_08", "_09"],
		extension: ".png",
	}
	
	this.init = function(opts) {
		
		opts.width = ~~opts.width;
		opts.height = ~~opts.height;

		this._opts = opts = merge(opts, this.defaults);

		// NineSliceImageView expects 9 images that all have
		// the same prefix and then have the postfixes specified
		// in the defaults at the top of this file. this.imageOvverides
		// allows you to override the index name for a specific image.
		this.imageOverrides = {};

		supr(this, 'init', [opts]);
		this.builder();
	};


	/**
	 * Rebuilds imageView. If you pass an index
	 * as the second argument only that index will be rebuilt.
	 */
	this.setImage = function(img, index) {
		var
			i,
			startIndex,
			endIndex;
	

		if (this.isBuilding) {
			this.resetImage = img;
		}
		else {
			this._opts.image = img;

			startIndex = index !== undefined ? index : 0;
			endIndex = index !== undefined ? index + 1 : this.imageViews.length;

			for (i = startIndex; i < endIndex; i++) {
				this.imageViews[i].removeFromSuperview();
			}

			this.builder(index);
		}
	};


	/**
	 * Expects an index between 1-9 inclusive.
	 */
	this.overrideImage = function(index, img) {
		var 
			zeroIndexed = index - 1;
	
		if ( index < 1 || index > 9 ) {
			throw new Error('NineSliceImageView.overrideImage expects an index between 1 and 9 inclusive.');
		}
		
		this.imageOverrides[zeroIndexed] = img;
		this.setImage(this._opts.image, zeroIndexed);
	};

	
	this.builder = function (index) {
		var 
			i, view,
			howMany = this._opts.firstThree || this._opts.verticalThree ? 3 : 9,
			loadedCb = new event.Callback(),
			imageName,
			startIndex,
			endIndex;
		
		this.resetImage = null;
		this.isBuilding = true;
		this.style.visible = false;
		this.imageViews = index !== undefined ? this.imageViews : [];

		startIndex = index !== undefined ? index : 0;
		endIndex = index !== undefined ? index + 1 : howMany;

		for (i = startIndex; i < endIndex; i++) {
			imageName = this.imageOverrides[i] != null ? this.imageOverrides[i] : this._opts.image;

			view = this.imageViews[i] = new ImageView({
				parent: this,
				image: imageName + this._opts.postfix[i] + this._opts.extension,
				canHandleEvents: false,
				autoSize: true
			});
			
			view.doOnLoad(loadedCb.chain());
		}
		
		loadedCb.run(this.layout.bind(this));
		loadedCb.run(function () {
			this.isBuilding = false;
			if (this.resetImage) {
				this.setImage(this.resetImage);
			}
			else {
				this.style.visible = ((this._opts.visible != undefined) ? this._opts.visible : true);
			}
		}.bind(this));
	}
	
	this._moveView = function(idx, x, y, w, h) {
		var s = this.imageViews[idx].style;
		s.x = x;
		s.y = y;
		s.width = w;
		s.height = h;
	}
	
	this.layout = function() {
		var w = this.style.width;
		var h = this.style.height;
		var
			views = this.imageViews;

		for( var v in views ){
			views[v].style.width = ~~(views[v].style.width);
			views[v].style.height = ~~(views[v].style.height);
		}
		
		var
			tlWidth = views[0].style.width,
			tlHeight = views[0].style.height,
			trWidth = views[2].style.width,
			trHeight = views[2].style.height,
			blWidth,
			blHeight,
			brWidth,
			brHeight;

		this._sizeHash = w + 'x' + h;

		if ( this._opts.verticalThree ) {
			this._moveView(0, 0, 0, tlWidth, tlHeight);
			this._moveView(1, 0, tlHeight, tlWidth, h - tlHeight - trHeight);
			this._moveView(2, 0, h - trHeight, tlWidth, trHeight);
		} else {

			if (!this._opts.firstThree) {
				blWidth = views[6].style.width;
				blHeight = views[6].style.height;
				brWidth = views[8].style.width;
				brHeight = views[8].style.height;
			}
			
			// top row
			this._moveView(0, 0, 0, tlWidth, tlHeight);
			this._moveView(1, tlWidth, 0, w - tlWidth - trWidth, tlHeight);
			this._moveView(2, w - trWidth, 0, trWidth, trHeight);
			if (!this._opts.firstThree) {
				// middle row
				this._moveView(3, 0, tlHeight, tlWidth, h - tlHeight - blHeight);
				this._moveView(4, tlWidth, tlHeight, w - tlWidth - trWidth, h - tlHeight - blHeight);
				this._moveView(5, w - trWidth, trHeight, trWidth, h - trHeight - brHeight);
				// bottom row
				this._moveView(6, 0, h - blHeight, blWidth, blHeight);
				this._moveView(7, blWidth, h - blHeight, w - blWidth - brWidth, blHeight);
				this._moveView(8, w - brWidth, h - brHeight, brWidth, brHeight);
			}
		}
	}


	this.subscribeToRefresh = function() {
		this._refreshTick();
		GC.app.engine.subscribe('Tick', this, '_refreshTick');
	};


	this.unsubscribeToRefresh = function() {
		this._refreshTick();
		GC.app.engine.unsubscribe('Tick', this, '_refreshTick');	
	};

	
	this._refreshTick = function () {
		// TODO: replace with tricks from timestep's ui branch
		if (this.imageViews && (this.style.width + 'x' + this.style.height) != this._sizeHash) {
			this.layout();
		}
	}
	
});

exports.prototype.addFilter = function(filter) {
	var fopts = filter.get();
	for (var i = 0; i < this.imageViews.length; i++) {
		this.imageViews[i].filter = new LinearAddFilter(fopts);
		this.imageViews[i].addFilter(this.imageViews[i].filter);
	}
	filter.update = bind(this, function(opts) {
		for (var i = 0; i < this.imageViews.length; i++) {
			this.imageViews[i].filter.update(opts);
		}
	});
};
