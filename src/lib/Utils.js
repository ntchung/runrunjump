exports = {
	choose: function(arr){
		return arr[~~(Math.random() * arr.length)];
	},
	randomize: function(arr) {
		arr.sort(function() { return 0.5 - Math.random(); });
		return arr;
	}
};

//getRandomObjectProperty
// iterates through an object with any number of properties, returns a random result
exports.getRandomObjectProperty = function(obj){
	var result,
	count = 0, prop;
	for (prop in obj){
		if (Math.random() < 1/++count){
			result = prop;
		}
	}
	return obj[result];
};

//getRandomObjectKey
// iterates through an object with any number of properties, returns a random key
exports.getRandomObjectKey = function(obj){
	var result,
	count = 0, prop;
	for (prop in obj){
		if (Math.random() < 1/++count){
			result = prop;
		}
	}
	return result;
};

//getWeightedRandomObject
exports.getWeightedRandomObject = function(obj){
	var result, prop, totalWeight = 0, runningWeight = 0, randValue;

	for(prop in obj) {
		totalWeight += obj[prop].weight;
	}

	randValue = Math.random() * totalWeight;

	for(prop in obj){
		runningWeight += obj[prop].weight;
		if( runningWeight >= randValue ) {
			return obj[prop];
		}
	}
};

// Pass in a string representing an object hierarchy (ie: controller.upgradeView.buyButton),
// selectEvalFromApp returns the final object referenced
// Store string refrences to objects that may or may not exist yet,
// then access those objects once you know they exist
exports.selectEvalFromApp = function(string){
	var i, parts = string.split('.'), ret = GC.app;

	for( i in parts ){
		if( ret ){
			ret = ret[parts[i]];
		}
		else{
			logger.log('Object - ' + string + ' does not exist. Returning exisiting views.');
			break;
		}
	}

	return ret;
};