import ui.resource.Image as Image;
import device;
import ui.View as View;
import ui.ImageView as ImageView;

exports = Class(View, function(supr) {

	this.init = function(opts) {
		// block input events before super init
		opts.blockEvents = true;
		opts.canHandleEvents = false;
		supr(this, 'init', [opts]);

		// get our image data and set up Images
		this.characterData = opts.characterData;
		for (var i in this.characterData) {
			this.characterData[i].img = new Image({ url: this.characterData[i].image });
		}

		// text options
		this.textAlign = opts.textAlign || 'center';
		this.spacing = opts.spacing || 0;

		// characters that should be rendered
		this.activeCharacters = [];
		this.imageViews = [];

		var initCount = opts.initCount || 1;
		for (var i = 0; i < initCount; i++) {
			this.imageViews.push(new ImageView({
				parent: this,
				x: 0,
				y: 0,
				width: 1,
				height: 1,
				visible: false,
				canHandleEvents: false
			}));
		}
	};

	this.setText = function(text) {
		text = text + "";
		this.textWidth = 0;

		var i = 0, data;
		while (i < text.length) {
			var character = text.charAt(i);
			var data = this.characterData[character];
			if (data) {
				this.activeCharacters[i] = data;
				this.textWidth += data.width + this.spacing;
				// special x offsets to fix text kerning only affect text width if it's first or last char
				if (data.offset && (i == 0 || i == text.length - 1)) {
					this.textWidth += data.offset;
				}
			}
			i++;
		}

		if (this.textAlign == 'center') {
			this.offset = (this.style.width - this.textWidth) / 2;
		} else if (this.textAlign == 'right') {
			this.offset = this.style.width - this.textWidth;
		} else {
			this.offset = 0;
		}

		while (text.length > this.imageViews.length) {
			this.imageViews.push(new ImageView({
				parent: this,
				x: 0,
				y: 0,
				width: 1,
				height: 1,
				canHandleEvents: false
			}));
		}

		// trims excess characters, TODO save objects
		this.activeCharacters.length = text.length;

		var x = this.offset, y = 0;
		for (i = 0; i < this.activeCharacters.length; i++) {
			var data = this.activeCharacters[i];
			var view = this.imageViews[i];

			if (!data) { continue; }

			// special x offsets to fix text kerning
			if (data.offset) {
				x += data.offset;
			}

			view.style.x = x;
			view.style.y = y;
			view.style.width = data.width;
			view.style.height = this.style.height; // all characters should have the same height
			view.style.visible = true;
			view.setImage(data.img);

			// remove special offset
			if (data.offset) {
				x -= data.offset;
			}

			x += data.width + this.spacing;
		}

		while(i < this.imageViews.length) {
			this.imageViews[i].style.visible = false;
			i++;
		}
	};
});
