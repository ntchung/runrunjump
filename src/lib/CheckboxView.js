import ui.ImageView as ImageView;

exports = Class(ImageView, function(supr) {

	this.init = function(opts) {

		this.checked = false;
		this.soundOnStart = opts.soundOnStart || function() {};

		supr(this, 'init', [opts]);	

		this.setImage(this._opts.imageUnchecked);
	};

	this.onInputStart = function() {
		this.checked = !this.checked;

		this.setImage(this.checked ? this._opts.imageChecked : this._opts.imageUnchecked);
		this.soundOnStart();
	};

	this.isChecked = function() {
		return this.checked;		
	};

});
