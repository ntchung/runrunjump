import animate;
from ui.filter import LinearAddFilter;


exports = (function(logger, supr) {

	var exports = {};

	/*
		-- addVisualPulse --

		Adds a pulsing linearAddFilter to a view.
			- view: the view to attach your filter to
			- color: an object containing {r: ,g: ,b: ,a: } values
			- maxAlpha: (0.0-1.0) the maximum intensity reached by the filter overlay
			- frequency: how quickly to pulse (time in milliseconds for one full pulse of zero -> maxA -> zero)
	*/

	exports.addVisualPulse = function(view, color, maxAlpha, frequency){
		if(!view.doPulse){
			var filter = new LinearAddFilter(color);
			view.addFilter(filter);
			view.doPulse = true;
			this.pulseFilter( view, filter, maxAlpha, frequency);
		}
	};

	exports.removeVisualPulse = function(view){
		var
			anims,
			animGroup,
			i;

		view.doPulse = false;
		view.removeFilter('LinearAdd');

		animGroup = animate.getGroup('pulseFilter' + view.uid);
		anims = animGroup ? animGroup._anims : null;

		if( anims != null ){
			for ( i in anims ) {
				anims[i].commit();
			}
		}
	};

	exports.pulseFilter = function(view, filter, maxAlpha, frequency){
		var that = this, curTime, newA;

		if(view.doPulse){
			animate(view, 'pulseFilter' + view.uid).then(function(){

				curTime = Date.now()%frequency,
				newA = maxAlpha - Math.abs( (curTime - frequency/2)/(frequency/2) )*maxAlpha ;

				filter.update({ a: newA});

			}).wait(20).then(function(){ that.pulseFilter( view, filter, maxAlpha, frequency); });
		}
		else{
			filter.update( { a: 0 } );
		}
	};

	exports.flicker = function(opts){
		var view = opts.view,
		minTime = opts.minTime || 100,
		maxTime = opts.maxTime || 200,
		minOp = opts.minOpacity || 0,
		maxOp = opts.maxOpacity || 1,
		randDur = minTime + Math.random()*(maxTime-minTime),
		randOp = minOp + Math.random()*(maxOp - minOp),
		delay = opts.delay || 0,
		that = this,
		anim;
		animate(view, opts.tag || 'flicker'+view.uid).wait(delay).then({opacity: randOp}, randDur, animate.linear).then(function(){
			that.flicker(opts);
		});
	};

	exports.stopFlicker = function(view, tag){
		var animTag = tag || 'flicker'+view.uid,
			anims,
			animGroup,
			i;

		animGroup = animate.getGroup(animTag);
		anims = animGroup ? animGroup._anims : null;

		if( anims != null ){
			for ( i in anims ) {
				anims[i].clear();
			}
		}

		view.style.opacity = 0;
	}

	/*
		-- blipIn --


						      ---------
						     |         |
		 o	>> ---------  >> |         |
						     |         |
						      ---------


		Animates a view in as if it were a TV turning on. See example keyframes above  : D

		- view: the view to animate
		- duration: how long do you want this to take
		- wait: how long to pause before starting animation
		- tag: tag for all animations, useful for pausing and resuming animations
		- cb: callback at end of animation
	*/

	exports.blipIn = function(view, duration, wait, tag, cb){
		var wStart = view.style.width, hStart = view.style.height, xStart = view.style.x, yStart = view.style.y, filter = new LinearAddFilter({r:255, g:255, b:255, a: 1}),
		sv, subs, sub, subWStart, subHStart;

		tag = tag || 'anim';

		wait = wait || 0;

		view.style.x = xStart + view.style.width/2;
		view.style.y = yStart + view.style.height/2;

		view.style.height = 2;
		view.style.width = 0;

		view.style.visible = true;

		subs = view.getSubviews();

		for( sub in subs){

			sv = subs[sub];

			subWStart = sv.style.width;
			subHStart = sv.style.height;

			sv.style.height = 2;
			sv.style.width = 0;

			sv.style.visible = true;

			animate(sv, tag)
			.wait(wait)
			.then({width: subWStart}, duration*0.5, animate.easeIn)
			.then({height: subHStart}, duration*0.5, animate.linear);

		}

		animate(view, tag)
		.wait(wait)
		.then({width: wStart, x: xStart}, duration*0.5, animate.easeIn)
		.then({height: hStart, y: yStart}, duration*0.5, animate.linear)
		.then(function(){
			//view.removeFilter('LinearAdd');
			if(cb){
				cb();
			}
		});




	};

	/*
		-- blipOut --


		---------
	   |         |
	   |         |	>> --------- >> o
	   |         |
		---------

		Animates a view out as if it were a TV turning off. See example keyframes above : D
			- view: the view to animate
			- duration: how long do you want this to take
			- wait: how long to pause before starting animation
			- tag: tag for all animations, useful for pausing and resuming animations
			- cb: callback at end of animation
	*/
	exports.blipOut = function(view, duration, wait, tag, cb){
		var wStart = view.style.width, hStart = view.style.height, xStart = view.style.x, yStart = view.style.y, filter = new LinearAddFilter({r:255, g:255, b:255, a: 1}),
		sv, subs, sub, subWStart, subHStart;

		tag = tag || 'anim';

		wait = wait || 0;

		view.style.centerHorizontalAnchor = true;
		view.style.centerVerticalAnchor = true;

		xEnd = xStart + view.style.width/2;
		yEnd = yStart + view.style.height/2;

		//view.addFilter(filter);

		subs = view.getSubviews();

		for( sub in subs){

			sv = subs[sub];

			subWStart = sv.style.width;
			subHStart = sv.style.height;

			sv.style.visible = true;

			animate(sv, tag)
			.wait(wait)
			.then({height: 2}, duration*0.5, animate.easeOut)
			.then({width: 0}, duration*0.5, animate.easeOut)
			.then(function(){
				sv.style.visible = false;

				sv.style.height = subHStart;
				sv.style.width = subWStart;
			});

		}


		animate(view, tag)
		.wait(wait)
		.then({height: 2, y: yEnd}, duration*0.5, animate.easeOut)
		.then({width: 0, x: xEnd}, duration*0.5, animate.easeOut)
		.then(function(){
			//view.removeFilter('LinearAdd');
			if(cb){
				cb();
			}

			view.style.visible = false;
			view.style.x = xStart;
			view.style.y = yStart;

			view.style.height = hStart;
			view.style.width = wStart;
		});


	};


	/*
		-- bulgeIn --
	 *	Bulges an element in (scales past 1, then returns to 1 at end of animation).
	 	- item: the element to bulge in
	 	- animopts: additional animation options if any
	 	- tag: tag for this animation's group (used to stop and start animations within a group at will)
	 	- soundUrl: sound to play at start of animation
	 	- soundTag: tag for this animation's sound
	 	- wait: how long to delay the animation
	 	- startScale: initial scale
	 	- maxScale: scale the element bulges to (ends at 1.0 scale)
		- inDur: time for first half of bulge
		- outDur: time for second half of bulge
	 *
	 */
	exports.bulgeIn = function(item, animopts, animTag, soundUrl, soundTag, wait, startScale, maxScale, inDur, outDur){

		item.style.scale = startScale != undefined ? startScale : 0.1;
		var anim = animate(item, animTag);
		var that = this;

		if(wait){
			anim.wait(wait);
		}
		anim.then(function(){
					if(soundUrl && !that.animationFinished){
						GC.app.playSound(soundUrl, soundTag);
					}
				});
		anim.then( merge({scale:maxScale||1.5}, animopts || {}), inDur || 750, animate.easeInOut);
		anim.then({scale:1.0}, outDur || 500, animate.easeInOut);
		return anim;
	};

	/*
		-- popIn --
	 *	Pops an element in (scales from startScale to endScale over dur).
	 	- item: the element to bulge in
	 	- animopts: additional animation options if any
	 	- tag: tag for this animation's group (used to stop and start animations within a group at will)
	 	- soundUrl: sound to play at start of animation
	 	- soundTag: tag for this animation's sound
	 	- wait: how long to delay the animation
	 	- startScale: initial scale
	 	- endScale: scale the element will end at
		- dur: hot long the animation will take
	 *
	 */
	exports.popIn = function(item, animopts, animTag, soundUrl, soundTag, wait, startScale, endScale, dur){

		var anim = animate(item, animTag);
		var that = this;

		if(wait){
			anim.wait(wait);
		}
		anim.then(function(){
					item.style.scale = startScale || 0.1;
					item.style.visible=true;
					if(soundUrl && !that.animationFinished){
						GC.app.playSound(soundUrl, soundTag);
					}
				});
		anim.then(merge({scale:endScale || 1.0}, animopts || {}), dur || 500, animate.easeInOut);

		return anim;
	};


	/**
	 * squish
	 * Happens in three phases.
	 * 	Flatten squish phase.
	 * 	Squish upwards phase.
	 * 	Return to regular phase.
	 * @param x1 - flatten squish width ratio
	 * @param y1 - flatten squish height ratio
	 * @param x2 - squish upwards width ratio
	 * @param y2 - squish upwards height ratio
	 */
	exports.squish = function(item, x1, y1, x2, y2, cb) {
		var
			ow = item.style.width,
			oh = item.style.height,
			anim = animate(item, 'squish');

		item.style.x += (ow - ow * x1) / 2;
		item.style.y += (oh - oh * y1);
		item.style.width = ow * x1;
		item.style.height = oh * y1;

			anim
				.then({
					width: ow * x2,
					height: oh * y2,
					dx: (ow * x1 - ow * x2) / 2,
					dy: oh * y1 - oh * y2
				}, 250, animate.easeInOut)
				.then({
					width: ow,
					height: oh,
					dx: (ow * x2 - ow) / 2,
					dy: oh * y2 - oh
				}, 500, animate.easeInOut)

		if ( typeof cb === 'function' ) {
			anim.then(cb);
		}

		return anim;
	};

	exports.bounceForever = function( view, minScale, maxScale, frequency){
		if(!view.doBounce){
			view.doBounce = true;
			this.doBounce( view, minScale, maxScale, frequency);
		}
	};

	exports.doBounce = function( view, minScale, maxScale, frequency){
		var that = this;
		if(view.doBounce){
			view.style.scale = minScale;
			animate(view, 'bounceForever'+view.uid)
			.then({scale:maxScale}, frequency/2, animate.easeInOut)
			.then({scale:minScale}, frequency/2, animate.easeInOut)
			.then(function(){
				that.doBounce(view, minScale,maxScale, frequency);
			})
		}
		else{
			view.style.scale = 1.0;
		}
	};

	exports.stopBouncing = function( view ){
		var
			anims,
			animGroup,
			i;

		view.doBounce = false;

		animGroup = animate.getGroup('bounceForever' + view.uid);
		anims = animGroup ? animGroup._anims : null;

		if( anims != null ){
			for ( i in anims ) {
				anims[i].commit();
			}
		}
	}


	return exports;
}());
