import animate;
import device;
import ui.View as View;
import ui.ScrollView as ScrollView;

exports = Class(View, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		// node stuff
		this.nodeCtor = opts.nodeCtor; // node constructor
		this.nodeHeight = opts.nodeHeight;
		this.updateNode = opts.updateNode; // fn to update node data etc; takes node, data
		this.nodeData = opts.nodeData || [];

		// optional
		this.sortFunc = opts.sortFunc;

		this.scrollView = new ScrollView({
			parent: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			fullWidth: this.style.width,
			fullHeight: this.nodeData.length * this.nodeHeight,
			scrollX: false,
			scrollY: true,
			clip: false,
			bounce: true,
			scrollBounds: {
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: 0
			},
			dragRadius: 15
		});

		this.lastY = 0;
		this.topIndex = 0;
		this.bottomIndex = 0;
		this.topHeight = 0;
		this.totalHeight = 0;
		this.bufferHeight = 0;
		this.fixedTop = [];
		this.fixedBottom = [];
		this.buffer = [];
		this.bufferSize = ~~(this.style.height / this.nodeHeight) + 6;

		for (var i = 0; i < this.nodeData.length; i++) {
			this.nodeData[i].index = i;
			this.addNode(this.nodeData[i], true);
		}
	};

	this.addFixedTopNode = function(node) {
		var index = this.fixedTop.length,
			ctor = node.ctor;

		node.y = index * this.nodeHeight;
		node.parent = this.scrollView;
		node.x = node.width ? (this.style.width - node.width) / 2 : 0;
		node.width = node.width || this.style.width;
		node.height = node.height || this.nodeHeight;
		node.index = index;

		this.topHeight += node.height;
		this.totalHeight += node.height;
		if (this.totalHeight > this.style.height) {
			this.scrollView.setScrollBounds({
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: this.totalHeight
			});
		}

		var newNode = new ctor(node);
		this.fixedTop[index] = newNode;

		return newNode;
	};

	this.addFixedBottomNode = function(node) {
		var index = this.fixedBottom.length,
			ctor = node.ctor;

		node.y = this.totalHeight;
		node.parent = this.scrollView;
		node.x = node.width ? (this.style.width - node.width) / 2 : 0;
		node.width = node.width || this.style.width;
		node.height = node.height || this.nodeHeight;
		node.index = index;

		this.totalHeight += node.height;
		if (this.totalHeight > this.style.height) {
			this.scrollView.setScrollBounds({
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: this.totalHeight
			});
		}

		var newNode = new ctor(node);
		this.fixedBottom[index] = newNode;

		return newNode;
	};

	this.addNode = function(node, fromInit) {
		var y = this.totalHeight,
			height = node.height || this.nodeHeight,
			ctor = this.nodeCtor;

		for (var i in this.fixedBottom) {
			y -= this.fixedBottom[i].style.height;
			this.fixedBottom[i].style.y += this.nodeHeight;
		}

		node.y = y;
		this.totalHeight += height;

		if (this.totalHeight > this.style.height) {
			this.scrollView.setScrollBounds({
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: this.totalHeight
			});
		}

		node.parent = this.scrollView;
		node.x = node.width ? (this.style.width - node.width) / 2 : 0;
		node.width = node.width || this.style.width;
		node.height = height;

		if (this.buffer.length < this.bufferSize) {
			this.bottomIndex = this.buffer.length - 1;

			var newNode = new ctor(node);
			this.updateNode(newNode, node);
			this.buffer[this.buffer.length] = newNode;
			this.bufferHeight += height;
		}

		if (!fromInit) {
			node.index = this.nodeData.length;
			this.nodeData[this.nodeData.length] = node;
		}
	};

	this.removeNode = function(index) {
		var node = this.nodeData[index],
			height = node.height || this.nodeHeight;

		this.totalHeight -= height;

		if (this.totalHeight > this.style.height) {
			this.scrollView.setScrollBounds({
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: this.totalHeight
			});
		} else {
			this.scrollView.setScrollBounds({
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: 0
			});
		}

		if (this.nodeData.length <= this.bufferSize) {
			this.bottomIndex--;
			this.bufferHeight -= height;
			this.buffer[index].removeFromSuperview();
			this.buffer.splice(index, 1);
		}

		this.nodeData.splice(index, 1);

		this.refreshNodes();
	};

	this.removeAllNodes = function() {
		var i;
		for (i in this.fixedTop) {
			this.fixedTop[i].removeFromSuperview();
		}
		for (i in this.fixedBottom) {
			this.fixedBottom[i].removeFromSuperview();
		}
		for (i in this.buffer) {
			this.buffer[i].removeFromSuperview();
		}

		this.fixedTop = [];
		this.fixedBottom = [];
		this.buffer = [];
		this.nodeData = [];
		this.topHeight = 0;
		this.totalHeight = 0;
		this.bufferHeight = 0;
		this.topIndex = 0;
		this.bottomIndex = 0;
		this.lastY = 0;
	};

	// updates according to data difference as opposed to a complete repopulate
	// all nodes must have a UID property to use this fn
	this.updateData = function(nodeData, refresh) {
		var i, j, newNode, index,
			oldUIDs = [],
			newUIDs = [];

		// grab all of our current UIDs
		for (i = 0; i < this.nodeData.length; i++) {
			oldUIDs.push(this.nodeData[i].uid);
		}

		// iterate through our new data, adding or updating nodes
		for (i = 0; i < nodeData.length; i++) {
			newNode = nodeData[i];
			newUIDs.push(newNode.uid); // grab all of our new UIDs

			// does our UID already exist?
			index = oldUIDs.indexOf(newNode.uid);
			if (index === -1) { // add a new node
				this.addNode(newNode);
			} else { // merge the matching node data
				this.nodeData[index] = merge(newNode, this.nodeData[index]);
			}
		}

		// remove old nodes that weren't in the update
		for (i = 0; i < this.nodeData.length; i++) {
			index = newUIDs.indexOf(this.nodeData[i].uid);
			if (index === -1) { // remove old node
				this.removeNode(i);
			}
		}

		if (refresh) {
			this.refreshNodes();
		}
	};

	this.sortList = function(sortFunc) {
		var fnSort = sortFunc || this.sortFunc;

		this.nodeData.sort(fnSort);
		this.refreshNodes();
	};

	this.refreshNodes = function() {
		var i, node, data;

		this.totalHeight = 0;

		for (i = 0; i < this.fixedTop.length; i++) {
			node = this.fixedTop[i];
			node.style.y = this.totalHeight;
			this.totalHeight += node.style.height;
		}

		for (i = 0; i < this.nodeData.length; i++) {
			node = this.nodeData[i];
			node.index = i;
			node.y = this.totalHeight;
			this.totalHeight += node.height || this.nodeHeight;
		}

		for (i = 0; i < this.buffer.length; i++) {
			node = this.buffer[i];
			data = this.nodeData[this.topIndex + i];
			if (data) {
				node.style.y = data.y;
				this.updateNode(node, data);
			}
		}

		for (i = 0; i < this.fixedBottom.length; i++) {
			node = this.fixedBottom[i];
			node.style.y = this.totalHeight;
			this.totalHeight += node.style.height;
		}
	};

	this.setOffset = function(offset) {
		this.scrollView.setOffset(0, offset);
	};

	this.getOffset = function() {
		return this.scrollView._contentView.style.y;
	};

	this.getBounds = function() {
		return this.scrollView.getStyleBounds();
	};

	this.scrollTo = function(y, time, trans, cb) {
		animate(this.scrollView._contentView).now({ y: y }, time, trans || animate.linear)
		.then(bind(this, function() {
			cb && cb();
		}));
	};

	this.cycleBufferDown = function() {
		var swapNode, data;

		if (this.bottomIndex < this.nodeData.length - 1
			&& this.buffer.length === this.bufferSize)
		{
			this.topIndex++;
			this.bottomIndex++;

			swapNode = this.buffer.shift();
			this.bufferHeight -= swapNode.style.height;
			data = this.nodeData[this.bottomIndex];

			swapNode.style.y = data.y;

			this.updateNode(swapNode, data);
			this.bufferHeight += swapNode.style.height;
			this.buffer.push(swapNode);
		}
	};

	this.cycleBufferUp = function() {
		var swapNode, data;

		if (this.topIndex > 0
			&& this.buffer.length === this.bufferSize)
		{
			this.topIndex--;
			this.bottomIndex--;

			swapNode = this.buffer.pop();
			this.bufferHeight -= swapNode.style.height;
			data = this.nodeData[this.topIndex];

			swapNode.style.y = data.y;

			this.updateNode(swapNode, data);
			this.bufferHeight += swapNode.style.height;
			this.buffer.unshift(swapNode);
		}
	};

	this.tick = function(dt) {
		// calling ScrollView's getOffset creates a new math2DPoint, avoid that
		var y = -this.scrollView._contentView.style.y - this.topHeight,
			avgHeight = (this.bufferHeight / this.bufferSize) || this.nodeHeight,
			top, bottom, swapCount, i;

		top = (y - 3 * avgHeight) / avgHeight;

		if (this.lastY > y && top < this.topIndex - 1) {
			swapCount = (this.topIndex - 1) - top;

			for (i = 0; i < swapCount; i++) {
				this.cycleBufferUp();
			}

			this.lastY = y;
			return;
		}

		bottom = (y + this.style.height + 2 * avgHeight) / avgHeight;

		if (this.lastY < y && bottom > this.bottomIndex + 1) {
			swapCount = bottom - (this.bottomIndex + 1);

			for (i = 0; i < swapCount; i++) {
				this.cycleBufferDown();
			}

			this.lastY = y;
			return;
		}

		this.lastY = y;
	};
});