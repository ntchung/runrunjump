import ui.View as View;

exports = Class(View, function(supr) {

	var realView;

	this.init = function(opts) {
		//opts.backgroundColor = 'rgba(0, 0, 0, 0.75)';
		supr(this, 'init', [opts]);

		realView = this.realView = opts.realView;
	};

	this.setRealView = function(view) {
		realView = this.realView = view;
	};

	this.onInputStart = function() {
		if (realView.onInputStart) {
			realView.onInputStart.apply(realView, arguments);
		}
	};

	this.onInputSelect = function() {
		if (realView.onInputSelect) {
			realView.onInputSelect.apply(realView, arguments);
		}
	};

	this.onInputMove = function() {
		if (realView.onInputMove) {
			realView.onInputMove.apply(realView, arguments);
		}
	};

	this.onDrag = function() {
		if (realView.onDrag) {
			realView.onDrag.apply(realView, arguments);
		}
	};

	this.onDragStart = function() {
		if (realView.onDragStart) {
			realView.onDragStart.apply(realView, arguments);
		}
	};

	this.onDragStop = function() {
		if (realView.onDragStop) {
			realView.onDragStop.apply(realView, arguments);
		}
	};

	this.onInputOut = function() {
		if (realView.onInputOut) {
			realView.onInputOut.apply(realView, arguments);
		}
	};

	this.onInputOver = function() {
		if (realView.onInputOver) {
			realView.onInputOver.apply(realView, arguments);
		}
	};
});
