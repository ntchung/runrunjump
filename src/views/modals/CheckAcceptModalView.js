import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.CheckboxView as CheckboxView;
import src.lib.ButtonView as ButtonView;
import src.views.modals.ModalView as ModalView;

exports = Class(ModalView, function(supr) {

	var
		MODAL_WIDTH = 795,
		MODAL_HEIGHT = 455,
		CLOSE_WIDTH = 92,
		CLOSE_HEIGHT = 92,
		BUTTON_WIDTH = 171,
		BUTTON_HEIGHT = 99,
		MESSAGE_WIDTH = 520,
		MESSAGE_HEIGHT = 100,
		TEXT_OFFSET_Y = 10,
		OFFSET_X = 25,
		TEXT_SPACING = 90,
		BUTTON_SPACING = 190,
		CHECKBOX_SPACING = 175,
		CHECKBOX_WIDTH = 75,
		CHECKBOX_HEIGHT = 75;

	this.init = function(opts) {

		opts.x = (opts.parent.style.width - MODAL_WIDTH) / 2;
		opts.y = (opts.parent.style.height - MODAL_HEIGHT) / 2;
		opts.width = MODAL_WIDTH;
		opts.height = MODAL_HEIGHT;
		opts.image = "resources/images/Modals/frameModal.png";

		this.controller = GC.app.controller;
		this.message = opts.message;
		this.acceptFn = opts.accept;

		supr(this, 'init', [opts]);	

		this.designView();
	};

	this.designView = function() {
		var config = this.controller.config;
		
		this.txtMessage = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: TEXT_SPACING,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: config.fonts.modalMessage,
			color: config.colors.modalMessage,
			text: this.message,
			textAlign: 'center',
			canHandleEvents: false
		});

		this.btnCancel = new ButtonView({
			parent: this,
			x: MODAL_WIDTH - CLOSE_WIDTH,
			y: 0,
			width: CLOSE_WIDTH,
			height: CLOSE_HEIGHT,
			image: "resources/images/Modals/btnClose.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.closeModal();
			})
		});

		this.checkbox = new CheckboxView({
			parent: this,
			x: OFFSET_X + (MODAL_WIDTH / 3 - CHECKBOX_WIDTH / 2),
			y: this.style.height - CHECKBOX_SPACING,
			width: CHECKBOX_WIDTH,
			height: CHECKBOX_HEIGHT,
			imageChecked: "resources/images/Leaderboard/checkboxChecked.png",
			imageUnchecked: "resources/images/Leaderboard/checkboxUnchecked.png",
			soundOnStart: bind(this.controller, "playSound", "ui_click")
		});

		this.btnAccept = new ButtonView({
			parent: this,
			x: OFFSET_X + (MODAL_WIDTH * 2 / 3 - BUTTON_WIDTH / 2),
			y: this.style.height - BUTTON_SPACING,
			anchorX: BUTTON_WIDTH / 2,
			anchorY: BUTTON_HEIGHT / 2,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnGreen.png",
			imagePressed: "resources/images/Modals/btnGreenPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, this.onAccept)
		});

		this.txtAccept = new TextView({
			parent: this.btnAccept,
			x: 0,
			y: TEXT_OFFSET_Y,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT - TEXT_OFFSET_Y,
			size: config.fonts.modalButton,
			color: config.colors.modalButton,
			text: GC.app.l.translate("OK"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});
	};

	this.onAccept = function() {
		if ( this.checkbox.isChecked() ) {
			this.controller.setData("hideInviteModal", true);	
		}

		this.acceptFn();		
		this.closeModal();
	};

});
