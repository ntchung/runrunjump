import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;

import src.views.modals.FramedModal as FramedModal;
import src.views.modals.BinaryModalView as BinaryModalView;

import src.views.helpers.KiwiEquipSlot as KiwiEquipSlot;
import src.views.helpers.BoostEquipSlot as BoostEquipSlot;

var ID_ANGEL_FISH = "angelFish",
	ID_MEGA_GLIDER = "megaGlider",
	ID_SUPER_GLIDER = "superGlider",
	TYPE_CONSUMABLE = "CONSUMABLE",
	IMG_NOT_EQUIPPED = "resources/images/Achievements/iconMystery.png",

	controller,
	config,
	avatarConfig,
	babyConfig,
	upgradesConfig;

exports = Class(FramedModal, function(supr) {

	// class vars
	var MARGIN = 5,
		BOTTOM_MARGIN = -40,
		KIWI_SLOT_WIDTH = 210,
		KIWI_SLOT_HEIGHT = 189,
		SLOT_COUNT = 4,
		SLOT_PADDING = 0,
		BOOST_SLOT_X = 6,
		BOOST_SLOT_Y = 265,
		BOOST_SLOT_WIDTH = 190,
		BOOST_SLOT_HEIGHT = 225,
		BOOST_SLOT_PADDING = 15,
		BUTTON_WIDTH = 254,
		BUTTON_HEIGHT = 100,
		BUTTON_INSET = 65;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		controller = this.controller;
		config = controller.config;
		avatarConfig = controller.avatarConfig;
		babyConfig = controller.babyConfig;
		upgradesConfig = controller.upgradesConfig;

		this.babyDataArray = [];
		for (var b in babyConfig) {
			this.babyDataArray.push(babyConfig[b]);
		}

		this.kiwiSlots = [];
		this.boostSlots = [];

		this.buildOnView();

		this.controller.subscribe('babySlotPurchase', this, 'buildAndRefreshKiwis');
	};

	this.refresh = function(){
		this.buildAndRefreshKiwis();
		this.buildAndRefreshBoosts();
	}

	this.buildAndRefreshKiwis = function() {
		var that = this;

		// lay slots down right to left so our main kiwi is slot 1

		var availWidth = this.content.style.width;
		var slotX = -14;
		var availSlots = this.controller.getData("babySlots");

		for (var i = 0; i < SLOT_COUNT; i++) {

			if( !this.kiwiSlots[i] ){
				this.kiwiSlots[i] = new KiwiEquipSlot({
					parent: this.content,
					x: slotX,
					y: this.kiwisHeader.style.y + this.kiwisHeader.style.height - 10,
					width: KIWI_SLOT_WIDTH,
					height: KIWI_SLOT_HEIGHT
				});
			}

			var key = undefined,
				keys = undefined,
				data = undefined;

			if (i === SLOT_COUNT - 1) {
				key = controller.getData("selectedAvatar");
				data = avatarConfig[key];
				this.kiwiSlots[i].bg.style.visible = false;
			} else {
				var babyIndex = SLOT_COUNT - i - 2;

				if (babyIndex < availSlots) {
					this.kiwiSlots[i].setPurchasedState(true);
				} else {
					this.kiwiSlots[i].setPurchasedState(false);
					this.kiwiSlots[i].setPrice(50 * babyIndex);
				}

				this.kiwiSlots[i].babyIndex = babyIndex;
				this.kiwiSlots[i].onInputStart = bind(this.controller, this.controller.showBabyKiwiModal, babyIndex);

				keys = controller.getData("selectedBabies");
				var index = SLOT_COUNT - ( i + 2 );
				if ( keys[index] ) {
					data = babyConfig[keys[index]];
				}
			}

			this.kiwiSlots[i].setKiwi(data);

			slotX += (KIWI_SLOT_WIDTH + SLOT_PADDING);
		}
	}

	this.buildAndRefreshBoosts = function() {
		var consumables = {},
		consumableCount = 0,
		purchased = controller.getData("upgradesPurchased"),
		count = 0;

		for (var u in upgradesConfig) {
			var item = upgradesConfig[u];
			owned = (purchased[u] && purchased[u].level) || 0;

			if ( item.type === TYPE_CONSUMABLE && item.id !== 'angelFish' )
			{
				consumables[u] = item;
				consumableCount++;
			}
		}

		for (var c in consumables) {
			var item = consumables[c];
			if( !this.boostSlots[count] ){

				var itemX = BOOST_SLOT_X + (BOOST_SLOT_WIDTH + BOOST_SLOT_PADDING) * count;

				var boostSlot = new BoostEquipSlot({
					parent: this.content,
					x: itemX,
					y: BOOST_SLOT_Y,
					width: BOOST_SLOT_WIDTH,
					height: BOOST_SLOT_HEIGHT,
					item: item
				});

				this.boostSlots[count] = boostSlot;
			}
			else{
				this.boostSlots[count].setBoost(item);
			}

			count ++
		}
	}

	this.buildOnView = function() {
		this.kiwisHeader = new TextView({
			parent: this.content,
			x: 0,
			y: MARGIN,
			width: this.content.style.width,
			height: 45,
			text: GC.app.l.translate('KIWIS'),
			size: config.fonts.modalEquipCount,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.buildAndRefreshKiwis();

		this.boostsHeader = new TextView({
			parent: this.content,
			x: 0,
			y: MARGIN + this.kiwisHeader.style.height + KIWI_SLOT_HEIGHT - 15,
			width: this.content.style.width,
			height: 40,
			text: GC.app.l.translate('BOOSTS'),
			size: config.fonts.modalEquipCount,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.buildAndRefreshBoosts();

		this.playButton = new ButtonView({
			parent: this,
			x: 2 * this.style.width / 3 - BUTTON_WIDTH / 2,
			y: this.frame.style.y + this.frame.style.height - BUTTON_INSET,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Header/button_rect_green.png",
			imagePressed: "resources/images/Header/button_rect_green_pressed.png",
			pressedOffsetY: 2,
			onClick: bind(this, 'transitionToGame'),
			zIndex: 5
		});

		this.playText = new TextView({
			parent: this.playButton,
			x: BUTTON_WIDTH * 0.1,
			y: BUTTON_HEIGHT * 0.1,
			width: BUTTON_WIDTH * 0.8,
			height: BUTTON_HEIGHT * 0.8,
			text: GC.app.l.translate("PLAY!"),
			size: 54,
			color: config.colors.buttonPlayFill,
			strokeColor: config.colors.buttonPlayStroke,
			multiline: false,
			canHandleEvents: false
		});

		this.heartView = new View({
			superview: this.playButton,
			x: this.playButton.style.width/2 - 50,
			y: this.playButton.style.height/2,
			width: 100,
			height: 50,
			anchorX: 50,
			anchorY: 25,
			opacity: 0,
			canHandleEvents: false
		});

		this.heartIcon = new ImageView({
			superview: this.heartView,
			x: 0,
			y: 0,
			width: 50,
			height: 50,
			image: 'resources/images/Header/icon_heart.png',
			canHandleEvents: false
		});

		this.heartText = new TextView({
			superview: this.heartView,
			x: 30,
			y: 0,
			anchorX: 25,
			anchorY: 25,
			width: 50,
			height: 50,
			text: ' -1',
			size: config.fonts.heartFont,
			color: "rgb(255, 255, 255)",
			strokeColor: "rgb(0,0,0)",
			multiline: false,
			canHandleEvents: false
		});

		this.storeButton = new ButtonView({
			parent: this,
			x: this.style.width / 3 - BUTTON_WIDTH / 2,
			y: this.frame.style.y + this.frame.style.height - BUTTON_INSET,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Header/button_rect_green.png",
			imagePressed: "resources/images/Header/button_rect_green_pressed.png",
			pressedOffsetY: 2,
			onClick: bind(this, 'transitionToStore'),
			zIndex: 5
		});
		this.storeText = new TextView({
			parent: this.storeButton,
			x: BUTTON_WIDTH * 0.1,
			y: BUTTON_HEIGHT * 0.1,
			width: BUTTON_WIDTH * 0.8,
			height: BUTTON_HEIGHT * 0.8,
			text: GC.app.l.translate("STORE!"),
			size: 54,
			color: config.colors.buttonPlayFill,
			strokeColor: config.colors.buttonPlayStroke,
			multiline: false,
			canHandleEvents: false
		});
	};

	this.equipKiwi = function(slot, data) {
		var babies = controller.getData("selectedBabies");
		babies[slot.babyIndex] = data.id;
		controller.setData("selectedBabies", babies);

		slot.setKiwi(data);
	};

	this.transitionToGame = function() {
		if (controller.getData('unlimitedHearts')) {
			controller.blockInput();
			controller.closeActiveModal(false);
			controller.activeView.deconstructView(bind(controller, 'transitionToGame'));
		} else if (controller.spendHearts(1, true)) {
			controller.blockInput();
			controller.header.refresh();
			animate(this.heartView)
			.now({dy: -250, scale: 2.5, opacity: 1}, 750, animate.easeOut)
			.then(bind(this, function(){
				controller.closeActiveModal(false, bind(this, function(){
					controller.activeView.deconstructView(bind(controller, 'transitionToGame'));
					this.heartView.style.scale = 1;
					this.heartView.style.y = this.playButton.style.height/2;
					this.heartView.style.opacity = 0;
				}));
			}));
		}
	};

	this.transitionToStore = function() {
		//this.closeModal();
		this.controller.closeActiveModal(false, bind(this, function(){
		 	controller.activeView.deconstructView(bind(controller, 'transitionToStore'));
		 }));
	};
});
