import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.views.modals.FramedModal as FramedModal;

import src.views.helpers.SixProductGrid as SixProductGrid;

import src.lib.ButtonView as ButtonView;

exports = Class(FramedModal, function(supr) {
	var FRAME_SPACING = 40,
		BOTTOM_PADDING = 15,
		TOP_SPACING = 15,
		BUTTON_WIDTH = 254,
		BUTTON_HEIGHT = 100,
		BUTTON_PADDING = 15,
		BUTTON_INNER_PADDING = 20,
		BUTTON_ICON_Y = -10,
		BUTTON_ICON_WIDTH = 100,
		config,
		productConfig;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		config = this.controller.config;
		productConfig = this.controller.modalStoreConfig;
		this.controller.setHardwareBackButton(this.close, true);

		this.buildOnView();
	};

	this.setCategory = function(cat){
		this.productGrid.setCategory(cat);

		var category = productConfig.products[cat],
			currency = category.currency,
			icon = productConfig.currencies[currency].icon,
			name = productConfig.currencies[currency].name;

		this.storeButtonIcon.setImage(icon);
		this.storeButtonText.setText(GC.app.l.translate("GET " + name + "!"));
	}

	this.buildOnView = function() {

		this.productGrid = new SixProductGrid({
			superview: this.content,
			x: 0,
			y: 0,
			width: this.content.style.width,
			height: this.content.style.height
		});

		this.offerWallButton = new ButtonView({
			superview: this,
			x: this.frame.style.x + this.frame.style.width/2 + BUTTON_PADDING,
			y: this.frame.style.y + this.frame.style.height - 60,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: 'resources/images/Header/button_rect_green.png',
			imagePressed: "resources/images/Header/button_rect_green_pressed.png",
			pressedOffsetY: 2,
			onClick: bind(this.controller, this.controller.showOfferWall),
			zIndex: 5
		});

		this.offerWallText = new TextView({
			superview: this.offerWallButton,
			x: BUTTON_INNER_PADDING,
			y: BUTTON_INNER_PADDING / 2 - 5,
			width: this.offerWallButton.style.width - BUTTON_ICON_WIDTH - BUTTON_INNER_PADDING + 10,
			height: this.offerWallButton.style.height - BUTTON_INNER_PADDING,
			text: GC.app.l.translate("GET FREE GEMS!"),
			horizontalAlign: 'center',
			verticalAlign: 'middle',
			lineHeight: 0.9,
			size: config.fonts.modalStoreButtons,
			color: config.colors.modalStoreCost,
			wrap: true,
			canHandleEvents: false
		});

		this.offerWallIcon = new ImageView({
			superview: this.offerWallButton,
			x: this.offerWallText.style.x + this.offerWallText.style.width,
			y: BUTTON_ICON_Y,
			width: BUTTON_ICON_WIDTH,
			height: BUTTON_ICON_WIDTH,
			image:'resources/images/Header/icon_gem.png',
			canHandleEvents: false
		});

		this.storeButton = new ButtonView({
			superview: this,
			x: this.frame.style.x + this.frame.style.width/2 - (BUTTON_WIDTH + BUTTON_PADDING),
			y: this.frame.style.y + this.frame.style.height - 60,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: 'resources/images/Header/button_rect_green.png',
			imagePressed: "resources/images/Header/button_rect_green_pressed.png",
			pressedOffsetY: 2,
			onClick: bind(this.controller, this.controller.showGemStore),
			zIndex: 5
		});

		this.storeButtonText = new TextView({
			superview: this.storeButton,
			x: BUTTON_INNER_PADDING,
			y: -5,
			width: this.storeButton.style.width - BUTTON_ICON_WIDTH - BUTTON_INNER_PADDING + 10,
			height: this.storeButton.style.height,
			text: "GET GEMS",
			horizontalAlign: 'center',
			verticalAlign: 'middle',
			size: config.fonts.modalStoreButtons,
			color: config.colors.modalStoreCost,
			canHandleEvents: false
		});

		this.storeButtonIcon = new ImageView({
			superview: this.storeButton,
			x: this.storeButtonText.style.x + this.storeButtonText.style.width,
			y: BUTTON_ICON_Y,
			width: BUTTON_ICON_WIDTH,
			height: BUTTON_ICON_WIDTH,
			image:'',
			canHandleEvents: false
		});
	};
});
