import ui.View as View;

import src.views.modals.StoreFrameModal as StoreFrameModal;

exports = Class(StoreFrameModal, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.decorateView();
		this.setCategory("hearts");
	};

	this.decorateView = function() {
		if (!GC.app.enableOfferwalls) {
			this.offerWallButton.style.visible = false;
			this.storeButton.style.x = (this.style.width - this.storeButton.style.width) * 0.5;
		}
	};
});