import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.views.modals.ModalView as ModalView;
import src.lib.ButtonView as ButtonView;

exports = Class(ModalView, function(supr) {

	var MODAL_WIDTH = 795,
		MODAL_HEIGHT = 455,
		CANCEL_WIDTH = 92,
		CANCEL_HEIGHT = 92,
		PREVIEW_WIDTH = 340,
		PREVIEW_HEIGHT = 250,
		PREVIEW_OFFSET = 150,
		BUTTON_WIDTH = 171,
		BUTTON_HEIGHT = 99,
		COST_X = 540,
		COST_WIDTH = 180,
		COST_HEIGHT = 60,
		OFFSET_X = 25,
		COL_TWO_PADDING = 35,
		COL_TWO_OFFSET = OFFSET_X + PREVIEW_WIDTH + PREVIEW_OFFSET,
		COL_TWO_WIDTH = MODAL_WIDTH - COL_TWO_OFFSET - 40;

	/**
	 * @method init
	 */
	this.init = function(opts) {
		opts.x = (opts.parent.style.width - MODAL_WIDTH) / 2;
		opts.y = (opts.parent.style.height - MODAL_HEIGHT) / 2;
		opts.width = MODAL_WIDTH;
		opts.height = MODAL_HEIGHT;
		opts.image = "resources/images/Modals/frameModal.png";

		this.onAccept = opts.accept;

		supr(this, 'init', [opts]);

		this.designView();
	};

	/**
	 * @method designView
	 */
	this.designView = function() {
		var config = GC.app.controller.config;

		this.btnCancel = new ButtonView({
			parent: this,
			x: this.style.width - CANCEL_WIDTH,
			y: 0,
			width: CANCEL_WIDTH,
			height: CANCEL_HEIGHT,
			image: "resources/images/Modals/btnClose.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.closeModal();
			}),
			visible: true
		});

		this.previewImage = new ImageView({
			parent: this,
			x: OFFSET_X + PREVIEW_OFFSET,
			y: (this.style.height - PREVIEW_HEIGHT) / 2,
			width: PREVIEW_WIDTH,
			height: PREVIEW_HEIGHT
		});

		this.costTxt = new TextView({
			parent: this,
			x: COL_TWO_OFFSET + (COL_TWO_WIDTH - COST_WIDTH) / 2,
			y: (this.style.height - PREVIEW_HEIGHT) / 2 + COL_TWO_PADDING,
			width: COST_WIDTH,
			height: COST_HEIGHT,
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.buyBtn = new ButtonView({
			parent: this,
			x: COL_TWO_OFFSET + (COL_TWO_WIDTH - BUTTON_WIDTH) / 2,
			y: ((this.style.height - PREVIEW_HEIGHT) / 2) + PREVIEW_HEIGHT - BUTTON_HEIGHT - COL_TWO_PADDING,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnGreen.png",
			imagePressed: "resources/images/Modals/btnGreenPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.onAccept(this.data);
			}),
			pressedOffsetY: 2
		});

		this.buyTxt = new TextView({
			parent: this.buyBtn,
			x: 0,
			y: 0,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			size: config.fonts.modalButton,
			color: '#000000',
			text: GC.app.l.translate("BUY"),
      verticalAlign: 'middle',
      wrap: false,
			canHandleEvents: false
		});
	};

	/**
	 * Sets preview image and price
	 * @method setData
	 */
	this.setData = function(data) {
		this.data = data;

		if (data.money) {
			this.costTxt.setText("$" + data.cost.toFixed(2));
		}
		else {
			this.costTxt.setText(data.cost + " " + GC.app.l.translate("Gems"));
		}

		this.previewImage.setImage(data.preview);
	};

});
