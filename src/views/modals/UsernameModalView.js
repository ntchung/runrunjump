import ui.View as View;
import ui.TextInputView as TextInputView;
import src.views.modals.BinaryModalView as BinaryModalView;

exports = Class(BinaryModalView, function(supr) {

	var ERROR_MESSAGE_WIDTH = 490,
		ERROR_MESSAGE_HEIGHT = 50,
		ERROR_SPACING = 72,
		MESSAGE_WIDTH = 490,
		MESSAGE_HEIGHT = 90,
		SPACING = 125,
		OFFSET_X = 25,
		TXT_BORDER_WIDTH = 3,
		PLACEHOLDER_TXT = "Enter a username",
		config;

	this.init = function(opts) {
		opts.message = "";

		supr(this, 'init', [opts]);	

		config = this.controller.config;

		this.designView();
	};

	this.designView = function() {
		var 
			that = this;
		
		this.txtInput = new TextInputView({
			parent: this,
			backgroundColor: "#ffffff",
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: SPACING,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: config.fonts.modalMessage,
			color: config.colors.placeholder,
			prompt: GC.app.l.translate(PLACEHOLDER_TXT),
			autoShowKeyboard: true,
			text: GC.app.l.translate(PLACEHOLDER_TXT),
			textAlign: 'center',
			multiline: true,
			zIndex: 3
		}).subscribe("Change", this, "onTextChange");

		this.txtInputBorder = new View({
			parent: this,
			backgroundColor: "#000000",
			x: this.txtInput.style.x - TXT_BORDER_WIDTH,
			y: this.txtInput.style.y - TXT_BORDER_WIDTH,
			width: this.txtInput.style.width + (TXT_BORDER_WIDTH * 2),
			height: this.txtInput.style.height + (TXT_BORDER_WIDTH * 2),
			zIndex: 2
		});

		this.txtMessage.style.visible = false;
		this.txtMessage.style.x = OFFSET_X + (this.style.width - ERROR_MESSAGE_WIDTH) / 2;
		this.txtMessage.style.y = ERROR_SPACING;
		this.txtMessage.style.width = ERROR_MESSAGE_WIDTH;
		this.txtMessage.style.height = ERROR_MESSAGE_HEIGHT;
		this.txtMessage.updateOpts({
			size: config.fonts.error,
			color: config.colors.error,
			text: GC.app.l.translate("You must enter a username!")
		});

		this.txtCancel.setText(GC.app.l.translate("Cancel"));
		this.txtAccept.setText(GC.app.l.translate("Save"));

		this.btnAccept.onClick = function() {
			if (that.textInputTxt == null || that.textInputTxt === PLACEHOLDER_TXT) {
				that.txtMessage.style.visible = true;
			} else {
				that.txtMessage.style.visible = false;
				GC.app.controller.socialController.setUsername(that.textInputTxt);		
				that.onAccept();		
				that.closeModal();
			}
		};
	};

	this.onTextChange = function(name) {
		this.textInputTxt = name;	
		this.txtInput.updateOpts({ color: config.colors.modalMessage });
	};

});
