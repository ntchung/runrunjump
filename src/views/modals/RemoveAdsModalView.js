import animate;
import ui.View as View;
import ...lib.TextView as TextView
import ui.ImageView as ImageView;

import src.lib.ButtonView as ButtonView;
import src.views.modals.ModalView as ModalView;

exports = Class(ModalView, function(supr) {

	var MESSAGE_WIDTH = 520,
		MESSAGE_HEIGHT = 50,
		BUTTON_WIDTH = 171,
		BUTTON_HEIGHT = 99,
		CANCEL_WIDTH = 92,
		CANCEL_HEIGHT = 92,
		SPACING = 100,
		MODAL_WIDTH = 795,
		MODAL_HEIGHT = 455,
		TOON_WIDTH = 201,
		TOON_HEIGHT = 276,
		OFFSET_X = 25,
		TEXT_OFFSET_Y = 10,
		STAR_X = 200,
		STAR_Y = 220,
		STAR_WIDTH = 75,
		STAR_HEIGHT = 72,
		STAR_SPACING = 5,
		STAR_DELAY = 500,
		POP_TIME = 300;

	this.init = function(opts) {
		opts.x = (opts.parent.style.width - MODAL_WIDTH) / 2;
		opts.y = (opts.parent.style.height - MODAL_HEIGHT) / 2;
		opts.width = MODAL_WIDTH;
		opts.height = MODAL_HEIGHT;
		opts.image = "resources/images/Modals/frameModal.png";
		supr(this, 'init', [opts]);

		var config = this.controller.config;

		// disables back button, but saves old back button fn
		this.controller.setHardwareBackButton(false, true);

		this.txtMessage = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: SPACING,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: config.fonts.modalMessage,
			color: config.colors.modalMessage,
			text: GC.app.l.translate("Gem purchases remove ads. Remove ads now?"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.btnAccept = new ButtonView({
			parent: this,
			x: OFFSET_X + (this.style.width - BUTTON_WIDTH) / 2,
			y: this.style.height - BUTTON_HEIGHT - SPACING,
			anchorX: BUTTON_WIDTH / 2,
			anchorY: BUTTON_HEIGHT / 2,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnGreen.png",
			imagePressed: "resources/images/Modals/btnGreenPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'onAccept'),
			visible: true
		});
		this.txtAccept = new TextView({
			parent: this.btnAccept,
			x: 0,
			y: TEXT_OFFSET_Y,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT - TEXT_OFFSET_Y,
			size: config.fonts.modalButton,
			color: config.colors.modalButton,
			text: GC.app.l.translate("OK"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.btnCancel = new ButtonView({
			parent: this,
			x: this.style.width - CANCEL_WIDTH,
			y: 0,
			width: CANCEL_WIDTH,
			height: CANCEL_HEIGHT,
			image: "resources/images/Modals/btnClose.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.closeModal();
			}),
			visible: true
		});

		this.toon = new ImageView({
			parent: this,
			x: this.style.width - 3 * TOON_WIDTH / 4,
			y: this.style.height - TOON_HEIGHT,
			width: TOON_WIDTH,
			height: TOON_HEIGHT,
			image: "resources/images/Modals/toonTrainer.png",
			canHandleEvents: false
		});
	};

	this.onAccept = function() {
		var gamesPlayed = GC.app.controller.getData("gamesPlayed") || 0;
		var strGamesPlayed = gamesPlayed + ""; // convert to string
		this.controller.trackEvent("RemoveAdsModalClick", {gamesPlayed: strGamesPlayed});
		this.closeModal();
		GC.app.controller.storeMode = 'BANK';
		GC.app.controller.showGemStore();
	};
});
