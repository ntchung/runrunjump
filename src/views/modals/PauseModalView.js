import animate;
import ui.View as View;
import ...lib.TextView as TextView;
import ui.ImageView as ImageView;

import src.controllers.Controller as Controller;

import src.views.modals.ModalView as ModalView;

exports = Class(ModalView, function(supr) {

	// CLASS CONSTANTS
	var BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		SPACING = 40,
		PAUSED_WIDTH = 420,
		PAUSED_HEIGHT = 135,
		SIGN_SPACING = 26,
		SIGN_WIDTH = 274,
		SIGN_HEIGHT = 555,
		LABEL_X = 50,
		BUTTON_WIDTH = SIGN_WIDTH,
		BUTTON_HEIGHT = 125,
		RESUME_R = 0,
		RESTART_R = -Math.PI / 40,
		MENU_R = Math.PI / 60,

		gameView,
		controller,
		config;

	// initialization
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		gameView = this.parent;
		controller = Controller.get();
		config = controller.config;

		this.designView();
	};

	this.designView = function() {
		this.txtPaused = new ImageView({
			parent: this,
			x: (this.style.width - PAUSED_WIDTH) / 2,
			y: -PAUSED_HEIGHT,
			width: PAUSED_WIDTH,
			height: PAUSED_HEIGHT,
			image: "resources/images/Game/txtPaused.png",
			canHandleEvents: false
		});

		this.pauseSign = new ImageView({
			parent: this,
			x: (this.style.width - SIGN_WIDTH) / 2,
			y: BG_HEIGHT,
			width: SIGN_WIDTH,
			height: SIGN_HEIGHT,
			image: "resources/images/Game/pauseSign.png",
			canHandleEvents: false
		});

		this.txtResume = new TextView({
			parent: this.pauseSign,
			x: LABEL_X,
			y: SPACING,
			r: RESUME_R,
			width: BUTTON_WIDTH - 2 * LABEL_X,
			height: BUTTON_HEIGHT,
			text: GC.app.l.translate("RESUME"),
			textAlign: 'center',
			size: config.fonts.pauseSign,
			color: config.colors.pauseSign,
			multiline: false
		});
		this.txtResume.onInputStart = bind(this, function(evt) {
			evt && evt.cancel();
			controller.playSound("ui_click");

			this.closeModal(bind(this, function() {
				gameView.unpause();
			}));
		});

		this.txtRestart = new TextView({
			parent: this.pauseSign,
			x: LABEL_X - 8,
			y: SPACING + 1.5 * SIGN_SPACING + BUTTON_HEIGHT,
			r: RESTART_R,
			width: BUTTON_WIDTH - 2 * LABEL_X,
			height: BUTTON_HEIGHT,
			text: GC.app.l.translate("RESTART"),
			textAlign: 'center',
			size: config.fonts.pauseSign,
			color: config.colors.pauseSign,
			multiline: false
		});
		this.txtRestart.onInputStart = bind(this, function(evt) {
			evt && evt.cancel();
			controller.playSound("ui_click");

			this.closeModal(bind(this, function() {
				gameView.resetView();
				gameView.constructView();
			}));
		});

		this.txtMenu = new TextView({
			parent: this.pauseSign,
			x: LABEL_X - 5,
			y: SPACING + 2 * SIGN_SPACING + 2 * BUTTON_HEIGHT,
			r: MENU_R,
			width: BUTTON_WIDTH - 2 * LABEL_X,
			height: BUTTON_HEIGHT,
			text: GC.app.l.translate("MENU"),
			textAlign: 'center',
			size: config.fonts.pauseSign,
			color: config.colors.pauseSign,
			multiline: false
		});
		this.txtMenu.onInputStart = bind(this, function(evt) {
			evt && evt.cancel();
			controller.playSound("ui_click");

			this.closeModal(bind(this, function() {
				gameView.model.finished = true;
				gameView.deconstructView(bind(controller, 'transitionToMainMenu'));
			}));
		});
	};

	this.openModal = function() {
		this.controller.enableBackButton(false);
		this.controller.setHardwareBackButton(this.txtResume.onInputStart, true);

		this.txtPaused.style.y = -PAUSED_HEIGHT;
		this.pauseSign.style.y = BG_HEIGHT;
		supr(this, 'openModal', arguments);

		controller.blockInput();
		animate(this.txtPaused).now({ y: SPACING }, config.ui.modalSlideTime, animate.easeOut);
		animate(this.pauseSign).now({ y: BG_HEIGHT - SIGN_HEIGHT }, config.ui.modalSlideTime, animate.easeOut)
		.then(bind(this, function() {
			this.controller.enableBackButton(true);
			controller.unblockInput();
		}));
	};

	this.closeModal = function() {
		this.controller.enableBackButton(false);
		supr(this, 'closeModal', arguments);
		animate(this.txtPaused).now({ y: -PAUSED_HEIGHT }, config.ui.modalSlideTime, animate.easeIn);
		animate(this.pauseSign).now({ y: BG_HEIGHT }, config.ui.modalSlideTime, animate.easeIn);
	};
});
