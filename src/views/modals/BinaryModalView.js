import animate;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;
import src.views.modals.ModalView as ModalView;

exports = Class(ModalView, function(supr) {

	var MESSAGE_WIDTH = 520,
		MESSAGE_HEIGHT = 160,
		BUTTON_WIDTH = 171,
		BUTTON_HEIGHT = 99,
		SPACING = 80,
		MODAL_WIDTH = 795,
		MODAL_HEIGHT = 455,
		TOON_WIDTH = 201,
		TOON_HEIGHT = 276,
		OFFSET_X = 25,
		TEXT_OFFSET_Y = 10;

	this.init = function(opts) {
		opts.x = (opts.parent.style.width - MODAL_WIDTH) / 2;
		opts.y = (opts.parent.style.height - MODAL_HEIGHT) / 2;
		opts.width = MODAL_WIDTH;
		opts.height = MODAL_HEIGHT;
		opts.image = "resources/images/Modals/frameModal.png";
		opts.zIndex = 102;
		supr(this, 'init', [opts]);

		this.acceptText = opts.acceptButtonText || "YES";

		this.onAccept = opts.onAccept || function() {};
		this.onCancel = opts.onCancel || function() {};

		var config = this.controller.config;

		this.txtMessage = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: SPACING,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: config.fonts.modalMessage,
			color: config.colors.modalMessage,
			text: GC.app.l.translate(opts.message),
			textAlign: 'center',
			multiline: true,
			canHandleEvents: false
		});

		this.btnCancel = new ButtonView({
			parent: this,
			x: OFFSET_X + this.style.width / 3 - BUTTON_WIDTH / 2,
			y: this.style.height - BUTTON_HEIGHT - SPACING,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnRed.png",
			imagePressed: "resources/images/Modals/btnRedPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.closeModal(bind(this, 'onCancel'));
			}),
			pressedOffsetY: 2
		});
		this.txtCancel = new TextView({
			parent: this.btnCancel,
			x: 0,
			y: 0,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			size: config.fonts.modalButton,
			color: config.colors.modalButton,
			text: GC.app.l.translate("NO"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.controller.setHardwareBackButton(this.btnCancel.onClick, true);

		this.btnAccept = new ButtonView({
			parent: this,
			x: OFFSET_X + 2 * this.style.width / 3 - BUTTON_WIDTH / 2,
			y: this.style.height - BUTTON_HEIGHT - SPACING,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnGreen.png",
			imagePressed: "resources/images/Modals/btnGreenPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.onAccept();
				this.closeModal();
			}),
			pressedOffsetY: 2
		});
		this.txtAccept = new TextView({
			parent: this.btnAccept,
			x: 0,
			y: 0,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			size: config.fonts.modalButton,
			color: config.colors.modalButton,
			text: GC.app.l.translate(this.acceptText),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.toon = new ImageView({
			parent: this,
			x: this.style.width - 3 * TOON_WIDTH / 4,
			y: this.style.height - TOON_HEIGHT,
			width: TOON_WIDTH,
			height: TOON_HEIGHT,
			image: "resources/images/Modals/toonTrainer.png",
			canHandleEvents: false
		});
	};
});
