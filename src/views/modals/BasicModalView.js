import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;
import src.views.modals.ModalView as ModalView;

exports = Class(ModalView, function(supr) {

	var MESSAGE_WIDTH = 520,
		MESSAGE_HEIGHT = 160,
		BUTTON_WIDTH = 171,
		BUTTON_HEIGHT = 99,
		SPACING = 100,
		MODAL_WIDTH = 795,
		MODAL_HEIGHT = 455,
		TOON_WIDTH = 201,
		TOON_HEIGHT = 276,
		OFFSET_X = 25,
		TEXT_OFFSET_Y = 10,
		CANCEL_WIDTH = 92,
		CANCEL_HEIGHT = 92;

	this.init = function(opts) {
		opts.x = (opts.parent.style.width - MODAL_WIDTH) / 2;
		opts.y = (opts.parent.style.height - MODAL_HEIGHT) / 2;
		opts.width = MODAL_WIDTH;
		opts.height = MODAL_HEIGHT;
		opts.image = "resources/images/Modals/frameModal.png";
		opts.zIndex = 102;
		supr(this, 'init', [opts]);

		this.onAccept = opts.onAccept || function() {};

		var config = this.controller.config;

		this.txtMessage = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: SPACING,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: opts.font || config.fonts.modalMessage,
			color: config.colors.modalMessage,
			text: GC.app.l.translate(opts.message),
			textAlign: 'center',
			multiline: true,
			canHandleEvents: false
		});

		this.btnAccept = new ButtonView({
			parent: this,
			x: OFFSET_X + (this.style.width - BUTTON_WIDTH) / 2,
			y: this.style.height - BUTTON_HEIGHT - SPACING,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnGreen.png",
			imagePressed: "resources/images/Modals/btnGreenPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.onAccept();
				this.closeModal();
			}),
			pressedOffsetY: 2
		});

		this.txtAccept = new TextView({
			parent: this.btnAccept,
			x: 0,
			y: 0,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			size: config.fonts.modalButton,
			color: config.colors.modalButton,
			text: GC.app.l.translate("OK"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.btnCancel = new ButtonView({
			parent: this,
			x: this.style.width - CANCEL_WIDTH,
			y: 0,
			width: CANCEL_WIDTH,
			height: CANCEL_HEIGHT,
			image: "resources/images/Modals/btnClose.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.closeModal();
			}),
			visible: opts.showCancel || false
		});

		this.toon = new ImageView({
			parent: this,
			x: this.style.width - 3 * TOON_WIDTH / 4,
			y: this.style.height - TOON_HEIGHT,
			width: TOON_WIDTH,
			height: TOON_HEIGHT,
			image: "resources/images/Modals/toonTrainer.png",
			canHandleEvents: false
		});

		this.controller.setHardwareBackButton(this.btnAccept.onClick, true);
	};
});
