import animate;
import ui.View as View;
import ui.ImageView as ImageView;

exports = Class(View, function(supr) {

	var Z_OVERLAY,
		Z_MODAL = 101,

		config;

	this.init = function(opts) {
		opts.zIndex = opts.zIndex || Z_MODAL;
		Z_OVERLAY = opts.zIndex - 1;

		// don't add as subview until the modal is opened
		var parent = this.parent = opts.parent;
		opts.visible = false;
		opts.parent = undefined;

		this.controller = GC.app.controller;
		config = this.controller.config;

		this.darkOverlay = new View({
			x: 0,
			y: 0,
			width: parent.style.width,
			height: parent.style.height,
			backgroundColor: config.colors.darkOverlay,
			opacity: 0,
			zIndex: Z_OVERLAY,
			visible: false
		});

		this.slideToY = opts.y;
		this.slideDistance = opts.noSlide ? 0 : -parent.style.height;
		this.startY = opts.y = opts.y - this.slideDistance;

		supr(this, 'init', [opts]);

		if (opts.image) {
			this.background = new ImageView({
				parent: this,
				x: 0,
				y: 0,
				width: this.style.width,
				height: this.style.height,
				image: opts.image
			});
		}

		if (!opts.noAutoOpen) {
			this.openModal();
		}

		this.onFinish = opts.onFinish;
	};

	// fade in dark overlay and slide modal in
	this.openModal = function() {
		this.controller.blockInput();

		this.darkOverlay.style.visible = true;
		this.style.visible = true;
		this.parent.addSubview(this.darkOverlay);

		this.style.y = this.startY;
		this.parent.addSubview(this);

		animate(this.darkOverlay).now({ opacity: 1 }, config.ui.overlayFadeTime, animate.linear);
		animate(this).now({ y: this.slideToY }, config.ui.modalSlideTime, animate.easeOut)
		.then(bind(this, function() {
			this.controller.unblockInput();
		}));
	};

	this.closeModal = function(onFinish) {
		if (!this.closing) {
			this.closing = true;
			this.controller.blockInput();
			// fade out dark overlay and slide modal out
			animate(this.darkOverlay).now({ opacity: 0 }, config.ui.overlayFadeTime, animate.linear);
			animate(this).now({ dy: this.slideDistance }, config.ui.modalSlideTime, animate.easeIn)
			.then(bind(this, function() {
				this.closing = false;
				this.style.visible = false;
				this.controller.unblockInput();
				this.controller.updateModalBackButton();
				this.darkOverlay.removeFromSuperview();
				this.removeFromSuperview();

				onFinish = onFinish || this.onFinish;
				onFinish && typeof onFinish === 'function' && onFinish();
			}));
		}
	};
});
