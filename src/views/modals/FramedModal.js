import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.views.modals.ModalView as ModalView;

import src.lib.ButtonView as ButtonView;

exports = Class(ModalView, function(supr) {
	var FRAME_SPACING = 40,
		BOTTOM_PADDING = 15,
		TOP_SPACING = 15,
		MODAL_WIDTH = 893,
		MODAL_HEIGHT = 599,
		MODAL_Y_DROP = 20,
		config;

	this.init = function(opts) {
		opts.x = (opts.parent.style.width - opts.width) / 2;
		opts.y = (opts.parent.style.height - opts.height) / 2;

		supr(this, 'init', [opts]);

		this.close = bind(this, function(){
			this.controller.closeActiveModal();
		});

		config = this.controller.config;
		this.controller.setHardwareBackButton(this.close, true);

		this.designView();
	};

	this.designView = function() {
		this.frame = new ImageView({
			superview: this,
			x: (this.style.width - MODAL_WIDTH)/2,
			y: (this.style.height - MODAL_HEIGHT)/2 + MODAL_Y_DROP,
			width: MODAL_WIDTH,
			height: MODAL_HEIGHT,
			image: 'resources/images/Header/modal_shop.png'
		});

		this.content = new View({
			superview: this.frame,
			x: FRAME_SPACING,
			y: FRAME_SPACING,
			width: this.frame.style.width - (FRAME_SPACING * 2),
			height: this.frame.style.height - (FRAME_SPACING * 2) - BOTTOM_PADDING
		});

		this.closeButton = new ButtonView({
			superview: this,
			x: this.frame.style.x + this.frame.style.width - 55,
			y: this.frame.style.y - 15,
			width: 75,
			height: 75,
			onClick: this.close,
			image: 'resources/images/Header/modal_x.png'
		});
	};
});
