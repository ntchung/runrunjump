import animate;
import ui.View as View;
import ...lib.TextView as TextView
import ui.ImageView as ImageView;

import src.lib.ButtonView as ButtonView;
import src.views.modals.ModalView as ModalView;

exports = Class(ModalView, function(supr) {

	var MESSAGE_WIDTH = 520,
		MESSAGE_HEIGHT = 50,
		BUTTON_WIDTH = 171,
		BUTTON_HEIGHT = 99,
		CANCEL_WIDTH = 92,
		CANCEL_HEIGHT = 92,
		SPACING = 100,
		MODAL_WIDTH = 795,
		MODAL_HEIGHT = 455,
		TOON_WIDTH = 201,
		TOON_HEIGHT = 276,
		OFFSET_X = 25,
		TEXT_OFFSET_Y = 10,
		STAR_X = 200,
		STAR_Y = 220,
		STAR_WIDTH = 75,
		STAR_HEIGHT = 72,
		STAR_SPACING = 5,
		STAR_DELAY = 500,
		POP_TIME = 300;

	this.init = function(opts) {
		opts.x = (opts.parent.style.width - MODAL_WIDTH) / 2;
		opts.y = (opts.parent.style.height - MODAL_HEIGHT) / 2;
		opts.width = MODAL_WIDTH;
		opts.height = MODAL_HEIGHT;
		opts.image = "resources/images/Modals/frameModal.png";
		supr(this, 'init', [opts]);

		var config = this.controller.config;

		// disables back button, but saves old back button fn
		this.controller.setHardwareBackButton(false, true);

		this.txtMessage = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: SPACING,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: config.fonts.modalMessage,
			color: config.colors.modalMessage,
			text: GC.app.l.translate("RATE THIS GAME?"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.txtMessage2 = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: SPACING + MESSAGE_HEIGHT,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			size: config.fonts.modalMessage,
			color: config.colors.modalMessage,
			text: GC.app.l.translate("RATE US ON THE APP STORE?"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false,
			visible: false
		});

		// create star views
		this.stars = [];
		var x = STAR_X + OFFSET_X;
		for (var i = 0; i < 5; i++) {
			var star = new ImageView({
				parent: this,
				x: x,
				y: STAR_Y,
				width: STAR_WIDTH,
				height: STAR_HEIGHT,
				image: "resources/images/Modals/starEmpty.png"
			});
			star.index = i;

			var that = this;
			star.onInputStart = bind(star, function() {
				that.controller.playSound("ui_click");
				that.starsChosen(this.index);
			});

			this.stars.push(star);
			x += STAR_WIDTH + STAR_SPACING;
		}

		this.btnAccept = new ButtonView({
			parent: this,
			x: OFFSET_X + (this.style.width - BUTTON_WIDTH) / 2,
			y: this.style.height - BUTTON_HEIGHT - SPACING,
			anchorX: BUTTON_WIDTH / 2,
			anchorY: BUTTON_HEIGHT / 2,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Modals/btnGreen.png",
			imagePressed: "resources/images/Modals/btnGreenPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'onAccept'),
			visible: false
		});
		this.txtAccept = new TextView({
			parent: this.btnAccept,
			x: 0,
			y: TEXT_OFFSET_Y,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT - TEXT_OFFSET_Y,
			size: config.fonts.modalButton,
			color: config.colors.modalButton,
			text: GC.app.l.translate("OK"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.btnCancel = new ButtonView({
			parent: this,
			x: this.style.width - CANCEL_WIDTH,
			y: 0,
			width: CANCEL_WIDTH,
			height: CANCEL_HEIGHT,
			image: "resources/images/Modals/btnClose.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.closeModal();
			}),
			visible: false
		});

		this.toon = new ImageView({
			parent: this,
			x: this.style.width - 3 * TOON_WIDTH / 4,
			y: this.style.height - TOON_HEIGHT,
			width: TOON_WIDTH,
			height: TOON_HEIGHT,
			image: "resources/images/Modals/toonTrainer.png",
			canHandleEvents: false
		});
	};

	this.starsChosen = function(starIndex) {
		for (var i = 0; i < 5; i++) {
			var star = this.stars[i];
			star.onInputStart = function() {};

			if (i <= starIndex) {
				star.setImage("resources/images/Modals/starFull.png");
			}
		}

		// only log star rating once so they don't get summed
		if (!this.controller.getData("starRatingLogged")) {
			this.controller.setData("starRatingLogged", true);
			this.controller.trackEvent("StarRating", { rating: (starIndex + 1) + "" });
		}

		if (starIndex == 4) { // 5-star rating
			this.fiveStar = true;
			setTimeout(bind(this, 'showAppStoreRating'), STAR_DELAY);
		} else {
			this.fiveStar = false;
			setTimeout(bind(this, 'showThankYou'), STAR_DELAY);
		}
	};

	this.hideStars = function() {
		for (var i = 0; i < 5; i++) {
			var star = this.stars[i];
			star.style.visible = false;
		}
	};

	this.showAppStoreRating = function() {
		this.hideStars();

		this.txtMessage.setText(GC.app.l.translate("AWESOME! YOU ROCK!"));

		this.btnCancel.style.visible = true;
		this.btnAccept.style.visible = true;
		this.txtMessage2.style.visible = true;

		animate(this.btnAccept).now({ scale: 1.5 }, POP_TIME, animate.easeOut)
		.then({ scale: 1 }, POP_TIME, animate.easeIn);
	};

	this.showThankYou = function() {
		if (!this.fiveStar) {
			this.hideStars();

			this.controller.setData("appRated", true);

			this.btnAccept.style.visible = true;

			animate(this.btnAccept).now({ scale: 1.5 }, POP_TIME, animate.easeOut)
			.then({ scale: 1 }, POP_TIME, animate.easeIn);
		}

		this.txtMessage2.style.visible = false;
		this.txtMessage.setText(GC.app.l.translate("THANKS FOR RATING!"));
	};

	this.onAccept = function() {
		if (this.fiveStar && !this.ratingComplete) {
			this.controller.setData("appRated", true);

			GLOBAL.setLocation && GLOBAL.setLocation(NATIVE.market.url);
			this.ratingComplete = true;

			this.showThankYou();
		} else {
			this.closeModal();
		}
	};
});
