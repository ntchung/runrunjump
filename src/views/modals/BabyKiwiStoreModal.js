import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;
import ui.ScrollView as ScrollView;

import src.views.modals.FramedModal as FramedModal;
import src.views.helpers.KiwiEquipSlot as KiwiEquipSlot;

import src.lib.ButtonView as ButtonView;
import src.lib.NineSliceImageView as NineSliceImageView;

exports = Class(FramedModal, function(supr) {
	var HEADER_HEIGHT = 30,
		PADDING = 10,
		SHELF_WIDTH = 445,
		DETAIL_WIDTH = 340,
		SLOT_WIDTH = 200,
		SLOT_HEIGHT = 180,
		EMPTY_SLOTS = 2,
		config,
		productConfig;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		config = this.controller.config;

		this.controller.setHardwareBackButton(this.close, true);

		this.babyData = this.controller.babyConfig;

		this.index = opts.index || 0;
		this.selectedKiwi = undefined;
		this.selectedSlot = undefined;
		this.slots = [];

		this.purchaseSelected = bind(this, this.purchaseSelected);
		this.equipSelected = bind(this, this.equipSelected);
		this.unequipSelected = bind(this, this.unequipSelected);

		this.buildOnView();

		this.setIndex(this.index);
	};

	this.setIndex = function(index) {
		this.index = index;

		var keys = this.controller.getData("selectedBabies");

		if ( keys[index] ) {
			this.selectedKiwi = this.babyData[keys[index]];
		}
		else{
			keys = Object.keys(this.babyData);
			this.selectedKiwi = this.babyData[keys[0]];
		}

		this.refresh();
		this.kiwiShelfScroll.scrollTo(0, 0, 1, undefined);


		//this.kiwiShelfScroll.scrollTo(0, this.selectedSlot.style.y, 1, undefined);
	};

	this.selectKiwi = function( kiwi ){
		if( kiwi && this.babyData[kiwi] ){
			this.selectedKiwi = this.babyData[kiwi];
		}
		this.refresh();
	};

	this.purchaseSelected = function(){
		//If successful purchase
		if( this.controller.productController.purchaseBabyKiwi(this.selectedKiwi) ){

			var selected = this.controller.getData("selectedBabies");
			for( var i = 0; i<this.controller.getData("babySlots"); i++ ){
				if( !selected[i] ){
					selected[i] = this.selectedKiwi.id;
					this.controller.closeActiveModal();
					break;
				}
			}
			this.controller.setData("selectedBabies", selected);

			this.controller.header.refresh();
			this.refresh();
		}
		else{
			this.controller.showGemStore();
		}
	};

	this.equipSelected = function(){
		var keys = this.controller.getData("selectedBabies"),
			currentIndex = keys.indexOf(this.selectedKiwi.id);

		if (keys[this.index]) {
			//Same baby already Equipped Here
			if (this.index === currentIndex) {
				return;
			} else if (currentIndex >= 0) { // swap the two
				keys[currentIndex] = keys[this.index];
				keys[this.index] = this.selectedKiwi.id;
				this.controller.setData("selectedBabies", keys);
				this.controller.closeActiveModal();
				return;
			}
			else{
				keys[this.index] = this.selectedKiwi.id;
				this.controller.setData("selectedBabies", keys);
				this.controller.closeActiveModal();
				return;
			}
		} else {
			if (currentIndex >= 0) {
				keys[currentIndex] = undefined;
			}

			keys[this.index] = this.selectedKiwi.id;
			this.controller.closeActiveModal();
		}

		this.controller.setData("selectedBabies", keys);
		this.refresh();
	};

	this.unequipSelected = function(){
		var keys = this.controller.getData("selectedBabies"),
			currentIndex = keys.indexOf(this.selectedKiwi.id);

		if (keys[this.index] === keys[currentIndex]) {
			keys[this.index] = undefined;
		}

		this.controller.setData("selectedBabies", keys);
		this.refresh();
	};

	this.refresh = function() {
		if( !this.selectedKiwi ){
			var keys = Object.keys(this.babyData);
			this.selectedKiwi = this.babyData[keys[0]];
		}

		this.buildAndRefreshSlots();

		this.detailsHeader.setText(GC.app.l.translate(this.selectedKiwi.name));
		this.kiwiPicture.setImage(this.selectedKiwi.selectionIcon);

		var isPurchased = this.controller.getData("babies").indexOf(this.selectedKiwi.id) !== -1;

		//this.descriptionText.updateOpts({height: this.descriptionBox.style.height - 20});
		this.descriptionText._opts.font = GC.app.l.locale !== 'en'
			? config.fonts.kiwiDescriptionKR
			: config.fonts.kiwiDescription;

		if( this.selectedKiwi.lockedPromo && !isPurchased){
			this.descriptionText.setText(GC.app.l.translate(this.selectedKiwi.promoDescription));
			this.rankBG.style.visible = false;
			this.costBG.style.visible = false;
		}
		else{
			this.descriptionText.setText(GC.app.l.translate(this.selectedKiwi.description, this.selectedKiwi.boost.value * 100));
			this.rankBG.style.visible = true;
			this.costBG.style.visible = true;
			this.rankText.setText(this.selectedKiwi.unlock);
			this.costText.setText(this.selectedKiwi.cost);
		}

		this.selectionArea.style.x = this.selectedSlot.style.x;
		this.selectionArea.style.y = this.selectedSlot.style.y;

		this.updateButton();
	};

	this.updateButton = function(){
		var kiwi = this.selectedKiwi;

		var selected = this.controller.getData('selectedBabies');

		var rank = this.controller.getData('level');

		var isPurchased = this.controller.getData("babies").indexOf(kiwi.id) !== -1;

		this.buyButton.style.visible = true;
		this.buyButton.canHandleEvents(true);

		if( kiwi.lockedPromo && !isPurchased ){
			this.buyButton.style.visible = false;
			this.buyButton.canHandleEvents(false);
		}
		else if( kiwi.id === selected[this.index] ){
			this.buttonText.setText(GC.app.l.translate('UN-EQUIP!'));
			this.buyButton.onClick = this.unequipSelected;
		}
		else if( selected.indexOf(kiwi.id) !== -1 || isPurchased){
			this.buttonText.setText(GC.app.l.translate('EQUIP!'));
			this.buyButton.onClick = this.equipSelected;
		}
		else if( kiwi.unlock <= rank ){
			this.buttonText.setText(GC.app.l.translate('BUY!'));
			this.buyButton.onClick = this.purchaseSelected;
		}
		else{
			this.buttonText.setText(GC.app.l.translate('RANK $[1]', kiwi.unlock));
			this.buyButton.onClick = function() {};
		}
	};

	this.buildAndRefreshSlots = function() {
		var baby,
			keys = this.controller.babyOrder;

		for( var i = 0; i < keys.length; i ++ ){
			baby = this.babyData[keys[i]];
			if( !this.slots[i] ){
				var slot = new KiwiEquipSlot({
					parent: this.kiwiShelfScroll,
					x: i%2 * SLOT_WIDTH,
					y: ~~(i/2) * SLOT_HEIGHT + 10,
					width: SLOT_WIDTH,
					height: SLOT_HEIGHT
				});

				this.slots[i] = slot;

				this.slots[i].on('InputStart', bind(this, this.selectKiwi, keys[i]));
			}
			this.slots[i].setKiwi(baby);

			if( baby === this.selectedKiwi ){
				this.selectedSlot = this.slots[i];
			}
		}

		//Enticing Empty Slots
		for( i; i < keys.length + EMPTY_SLOTS; i++){
			if( !this.slots[i] ){
				var slot = new KiwiEquipSlot({
					parent: this.kiwiShelfScroll,
					x: i%2 * SLOT_WIDTH,
					y: ~~(i/2) * SLOT_HEIGHT + 10,
					width: SLOT_WIDTH,
					height: SLOT_HEIGHT
				});

				this.slots[i] = slot;

				//TODO: what to do with empty slots?
				//this.slots[i].on('InputStart', bind(this, this.selectKiwi, keys[j]));
			}
			this.slots[i].setKiwi('empty');
		}

		this.kiwiShelfScroll.setScrollBounds( { minY: 0, maxY: Math.ceil((keys.length + EMPTY_SLOTS)/2 * SLOT_HEIGHT)  + 60 } );
	};

	this.buildOnView = function() {
		this.kiwisHeader = new TextView({
			parent: this.content,
			x: 0,
			y: -3,
			width: this.content.style.width,
			height: 45,
			text: GC.app.l.translate('Baby Kiwi Shop'),
			size: config.fonts.modalEquipCount,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.kiwiShelfBG = new NineSliceImageView({
			parent: this.content,
			x: PADDING,
			y: HEADER_HEIGHT + PADDING*2,
			width: SHELF_WIDTH,
			height: this.content.style.height - HEADER_HEIGHT - PADDING,
			image: "resources/images/Modals/box_section"
		});

		this.kiwiShelfScroll = new ScrollView({
			parent: this.kiwiShelfBG,
			width: SHELF_WIDTH - 40,
			height: this.kiwiShelfBG.style.height - 4,
			x: 20,
			y: 2,
			scrollX: false,
			scrollY: true,
			clip: true
		});

		this.selectionArea = new ImageView({
			superview: this.kiwiShelfScroll,
			x: 0,
			y: 0,
			width: SLOT_WIDTH,
			height: SLOT_HEIGHT,
			image: 'resources/images/Modals/selection_babyShopItem.png'
		});

		this.buildAndRefreshSlots();

		this.kiwiDetails = new NineSliceImageView({
			parent: this.content,
			x: this.kiwiShelfBG.style.x + SHELF_WIDTH + PADDING*2,
			y: HEADER_HEIGHT + PADDING*2,
			width: DETAIL_WIDTH,
			height: this.content.style.height - HEADER_HEIGHT - PADDING,
			image: "resources/images/Modals/box_section"
		});

		this.detailsHeader = new TextView({
			parent: this.kiwiDetails,
			x: 0,
			y: PADDING,
			width: this.kiwiDetails.style.width,
			height: 40,
			text: 'KIWI NAME',
			textAlign: 'center',
			size: config.fonts.button,
			color: config.colors.button,
			multiline: false,
			canHandleEvents: false
		});

		this.kiwiPicture = new ImageView({
			parent: this.kiwiDetails,
			x: 95,
			y: this.detailsHeader.style.y + this.detailsHeader.style.height,
			width: 150,
			height: 150,
			image: ''
		});

		this.costSection = new View({
			superview: this.kiwiDetails,
			x: 0,
			y: this.kiwiPicture.style.y + this.kiwiPicture.style.height,
			width: this.kiwiDetails.style.width,
			height: 60
		});

		this.rankBG = new ImageView({
			superview: this.costSection,
			x: 50,
			y: 0,
			width: 100,
			height: 60,
			image: 'resources/images/Modals/counter_xp.png'
		});

		this.rankText = new TextView({
			superview: this.rankBG,
			x: 50,
			y: 0,
			width: 40,
			height: this.rankBG.style.height,
			text: '00',
			size: config.fonts.modalEquipCount,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.costBG = new ImageView({
			superview: this.costSection,
			x: 170,
			y: 0,
			width: 100,
			height: 60,
			image: 'resources/images/Modals/counter_gems.png'
		});

		this.costText = new TextView({
			superview: this.costBG,
			x: 50,
			y: 0,
			width: 40,
			height: this.rankBG.style.height,
			text: '00',
			size: config.fonts.modalEquipCount,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.descriptionBox = new NineSliceImageView({
			superview: this.kiwiDetails,
			x: 8,
			y: this.costSection.style.y + this.costSection.style.height,
			width: this.kiwiDetails.style.width - 16,
			height:  100,
			image: 'resources/images/Modals/box_textArea'
		});

		this.descriptionText = new TextView({
			superview: this.descriptionBox,
			x: 8,
			y: 10,
			width: this.descriptionBox.style.width - 16,
			height: this.descriptionBox.style.height - 20,
			textAlign: 'center',
			text: '',
			size: GC.app.l.locale !== 'en'
				? config.fonts.kiwiDescriptionKR
				: config.fonts.kiwiDescription,
			color: "rgb(0, 0, 0)",
			wrap: true,
			canHandleEvents: false
		});

		this.buyButton = new ButtonView({
			superview: this.kiwiDetails,
			x: 43,
			y: this.descriptionBox.style.y + this.descriptionBox.style.height - 1,
			width: 254,
			height: 100,
			image: "resources/images/Header/button_rect_green.png",
			imagePressed: "resources/images/Header/button_rect_green_pressed.png",
			pressedOffsetY: 2
		});

		this.buttonText = new TextView({
			superview: this.buyButton,
			x: 0,
			y: 0,
			width: this.buyButton.style.width,
			height: this.buyButton.style.height,
			text: 'EQUIP!',
			size: config.fonts.modalEquipCount,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		})

	};
});
