import ui.View as View;

import src.views.modals.StoreFrameModal as StoreFrameModal;

exports = Class(StoreFrameModal, function(supr) {
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.decorateView();
		this.setCategory("gems");
	};

	this.decorateView = function() {
		this.storeButton.style.visible = false;

		if (!GC.app.enableOfferwalls) {
			this.offerWallButton.style.visible = false;
		} else {
			this.offerWallButton.style.x = (this.style.width - this.offerWallButton.style.width) / 2;
		}
	};

	this.closeModal = function() {
		supr(this, 'closeModal', arguments);

		var gemModal = this.controller.gemAngelModal;
		if (gemModal) {
			this.controller.gemAngelModal = undefined;
			gemModal.closeModal();
		}

		// handle angel save mid game
		if (this.controller.midAngelSave
			&& !this.controller.basicAngelModal)
		{
			var gems = this.controller.getData("gems");
			if (gems >= 3) {
				this.controller.publish('acceptAngelPurchase');
			} else {
				this.controller.publish('cancelAngelPurchase');
			}
		}
	};
});