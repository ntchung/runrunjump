import ui.ImageView as ImageView;
import ui.SpriteView as Sprite;

import src.controllers.Controller as Controller;
import src.lib.ParticleEngine as ParticleEngine;

import src.views.helpers.PlayerView as PlayerView;

var ANIM_STATES = {
	DIVING: "DIVING",
	FALLING: "FALLING",
	FLOATING: "FLOATING",
	GLIDING: "GLIDING",
	JUMPING: "JUMPING",
	LANDING: "LANDING",
	ROLLING: "ROLLING",
	RUNNING: "RUNNING"
};

exports = Class(Sprite, function(supr) {

/* ~ ~ ~ CLASS CONSTANTS / GLOBALS ~ ~ ~ */

	var controller = Controller.get(),
		config = controller.config,
		babyConfig = controller.babyConfig,
		model,

		// animation names
		ANIM_NAME_BLINK = "blink",
		ANIM_NAME_DIVE = "dive",
		ANIM_NAME_FLOAT = "float",
		ANIM_NAME_FOCUSED = "focused",
		ANIM_NAME_JUMP = "jump",
		ANIM_NAME_LAND = "land",
		ANIM_NAME_ROLL = "roll",
		ANIM_NAME_RUN = "run",

		TRANSITION_EASE_OUT = "easeOut",

		// mechanic constants
		BG_WIDTH = config.ui.maxWidth,
		BG_HEIGHT = config.ui.maxHeight,
		BLINK_CHANCE = 0.28,
		ROLL_CHANCE = 0.01,
		FOCUSED_START_SPEED = 1200,
		ROLL_AMOUNT = 8 * Math.PI,
		ROLL_TIME = 500;

/* ~ ~ ~ INITIALIZATION / RESET ~ ~ ~ */

	this.init = function(opts) {
		this.babyID = opts.babyID;
		this.babyData = babyConfig[this.babyID];
		opts.width = this.babyData.width;
		opts.height = this.babyData.height;
		opts.canHandleEvents = false;
		opts.defaultAnimation = ANIM_NAME_RUN;
		opts.loop = false;
		opts.autoStart = false;
		supr(this, 'init', [opts]);

		this.particleEngine = new ParticleEngine({
			parent: this,
			x: 0,
			y: 0,
			anchorX: opts.width / 2,
			anchorY: opts.height / 2,
			width: opts.width,
			height: opts.height
		});

		this.boundStartNextAnim = bind(this, "startNextAnimation");
		this.boundFinishRoll = bind(this, "setRolling", false);
		this.boundStartFloating = bind(this, "setFloating", true);
		this.boundStraightUp = bind(this, function() {
			this.style.r = 0;
		});

		this.resetView(opts);
	};

	this.resetView = function(opts) {
		this.resetAllAnimations(opts);

		this.babyID = opts.babyID;
		this.babyData = babyConfig[this.babyID];
		this.style.width = opts.width = this.babyData.width;
		this.style.height = opts.height = this.babyData.height;

		model = controller.getGameModel();

		// animation state flags
		this.animState = ANIM_STATES.RUNNING;
		this.focused = false;
		this.flyingAway = false;
		this.runOnly = false;

		// still anim frame variables
		this.jumpFrame = this.babyData.jumpFrame;
		this.startFrame = this.babyData.startFrame;

		this.startAnimation("run", { randomFrame: true, iterations: 1, callback: this.boundStartNextAnim });
		this.pause();
		this.setImage(this.startFrame);
	};

	this.setArrayIndex = function(i) {
		this.arrayIndex = i;
	};

/* ~ ~ ~ ANIMATION ~ ~ ~ */

	this.startNextAnimation = function() {
		if (this.flyingAway) { return; }

		if (this.isPaused) {
			this.resume();
		}

		this.animState = ANIM_STATES.RUNNING;

		if (this.focused) {
			this.startAnimation(ANIM_NAME_FOCUSED, { iterations: 1, callback: this.boundStartNextAnim });
		} else {
			var roll = Math.random();
			if (roll < BLINK_CHANCE && !this.runOnly) {
				this.startAnimation(ANIM_NAME_BLINK, { iterations: 1, callback: this.boundStartNextAnim });
			} else if (roll < BLINK_CHANCE + ROLL_CHANCE && !this.runOnly) {
				this.setRolling(true);
			} else {
				this.startAnimation(ANIM_NAME_RUN, { iterations: 1, callback: this.boundStartNextAnim });
			}
		}
	};

	this.pause = function() {
		this.isPaused = true;
		supr(this, 'pause', arguments);
		this.style.visible = true;
	};

	this.resume = function() {
		this.isPaused = false;
		supr(this, 'resume', arguments);
		this.style.visible = true;
	};

/* ~ ~ ~ PLAYER STATES ~ ~ ~ */

	this.setDiving = function(diving) {
		this.animState = ANIM_STATES.DIVING;

		this.resume();
		this.startAnimation(ANIM_NAME_DIVE, { iterations: 1 });
		this.pause();
	};

	this.setFalling = function(falling) {
		this.animState = ANIM_STATES.FALLING;

		this.pause();
		this.setImage(this.jumpFrame);
	};

	this.setFloating = function(floating) {
		this.animState = ANIM_STATES.FLOATING;

		this.resume();
		this.startAnimation(ANIM_NAME_FLOAT, { iterations: 999999 }); // no way to specify loop for one anim
	};

	this.setFlyAway = function(flyAway) {
		this.setFloating(true);
		this.flyingAway = flyAway;
	};

	this.setFocused = function(focused) {
		this.focused = focused;
	};

	this.setGliding = function(gliding) {
		this.animState = ANIM_STATES.GLIDING;

		this.resume();
		this.startAnimation(ANIM_NAME_FLOAT, { iterations: 999999 }); // no way to specify loop for one anim
	};

	this.setJumping = function(jumping) {
		this.animState = ANIM_STATES.JUMPING;

		this.startAnimation(ANIM_NAME_JUMP, { iterations: 1, callback: this.boundStartFloating });
	};

	this.setLanding = function(landing) {
		this.animState = ANIM_STATES.LANDING;

		this.resume();
		this.startAnimation(ANIM_NAME_LAND, { iterations: 1, callback: this.boundStartNextAnim });
	};

	this.setRolling = function(rolling) {
		if (rolling) {
			this.animState = ANIM_STATES.ROLLING;

			this.resume();
			this.startAnimation(ANIM_NAME_ROLL, { iterations: 1, callback: this.boundStartNextAnim });
			this.pause();

			// pass this view in as an external particle
			var rollData = this.particleEngine.obtainObject();
			rollData.dr = ROLL_AMOUNT;
			rollData.ttl = ROLL_TIME;
			rollData.transition = TRANSITION_EASE_OUT;
			rollData.onDeath = this.boundFinishRoll;
			this.particleEngine.addExternalParticle(this, rollData);
		} else {
			this.style.r = 0;
			this.startNextAnimation();
		}
	};

/* ~ ~ ~ TICK / UPDATING ~ ~ ~ */

	this.updateView = function(dt, cameraOffset) {
		var player = model.player;

		this.particleEngine.runTick(dt);

		if (model.gameoverCountdown >= 0 || model.finished || this.flyingAway) {
			return;
		}

		// update babies based on player history
		var history = model.playerHistory;
		var index = model.playerHistoryIndex;
		var baby = model.babies[this.arrayIndex];
		var babyIndex = baby.babyIndex;
		if (babyIndex < 0) { babyIndex += history.length; }

		if (baby.diving && this.animState !== ANIM_STATES.DIVING) {
			this.setDiving(true);
			this.style.r = baby.rot;
			setTimeout(this.boundStraightUp, 500);
		}

		// account for anim state changes
		var data = history[babyIndex];
		// only update anim state if the index is moving forward
		if (data.animState && this.animState !== ANIM_STATES.ROLLING && this.lastBabyIndex < babyIndex) {
			if (this.animState !== data.animState) {
				switch (data.animState) {
					case ANIM_STATES.DIVING:
						this.setDiving(true);
						break;

					case ANIM_STATES.FALLING:
						this.setFalling(true);
						break;

					case ANIM_STATES.FLOATING:
						this.setFloating(true);
						break;

					case ANIM_STATES.GLIDING:
						this.setGliding(true);
						break;

					case ANIM_STATES.JUMPING:
						this.setJumping(true);
						break;

					case ANIM_STATES.LANDING:
						this.setLanding(true);
						break;

					case ANIM_STATES.ROLLING:
						this.setRolling(true);
						break;

					case ANIM_STATES.RUNNING:
						this.startNextAnimation();
						break;
				}
			}
		}

		if (!this.focused && !this.isPaused && !model.vxOriginal && player.vx >= FOCUSED_START_SPEED) {
			this.setFocused(true);
		}

		this.lastBabyIndex = babyIndex;

		var diffX = baby.x + baby.horzOffset - this.style.x;
		if (this.animState === ANIM_STATES.DIVING) {
			this.style.x += 2 * diffX * dt / 1000;
		} else {
			this.style.x += diffX / 2 * dt / 1000;
		}

		var offsetY = this.animState === ANIM_STATES.ROLLING ? this.babyData.rollOffset : this.babyData.offsetY;
		this.style.y = baby.y - cameraOffset + offsetY;

		if (model.countdownActive && !this.isPaused) {
			this.pause();
		}
	};
});

exports.ANIM_STATES = ANIM_STATES;