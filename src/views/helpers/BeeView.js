import ui.SpriteView as Sprite;

import src.controllers.Controller as Controller;

var ANIM_STATES = {
	DEAD: "DEAD",
	FLYING: "FLYING"
};

exports = Class(Sprite, function(supr) {

/* ~ ~ ~ CLASS CONSTANTS / GLOBALS ~ ~ ~ */

	var controller = Controller.get(),
		config = controller.config,
		soundConfig = controller.soundConfig,
		model,

		// animation names
		ANIM_NAME_FLYING = "flying",

		// image names
		DEAD_FRAME = "resources/images/Enemies/Bee/bee_flying_0001.png",

		// various constants
		BG_WIDTH = 1280,
		BEE_WIDTH = 64,
		BEE_HEIGHT = 64;

/* ~ ~ ~ INITIALIZATION / RESET ~ ~ ~ */

	this.init = function(opts) {
		opts.flipX = true;
		opts.anchorX = BEE_WIDTH / 2;
		opts.anchorY = BEE_HEIGHT / 2;
		opts.width = BEE_WIDTH;
		opts.height = BEE_HEIGHT;
		opts.canHandleEvents = false;
		opts.defaultAnimation = ANIM_NAME_FLYING;
		opts.loop = true;
		opts.autoStart = true;
		supr(this, 'init', [opts]);

		this.resetView(opts);
	};

	this.resetView = function(opts) {
		this.resetAllAnimations(opts);

		this.style.r = 0;
		this.style.anchorX = BEE_WIDTH / 2;
		this.style.anchorY = BEE_HEIGHT / 2;
		this.style.width = BEE_WIDTH;
		this.style.height = BEE_HEIGHT;

		model = controller.getGameModel();

		// animation state flags
		this.animState = ANIM_STATES.FLYING;
	};

/* ~ ~ ~ ANIMATION ~ ~ ~ */

	this.pause = function() {
		this.isPaused = true;
		supr(this, 'pause', arguments);
		this.style.visible = true;
	};

	this.resume = function() {
		this.isPaused = false;
		supr(this, 'resume', arguments);
		this.style.visible = true;
	};

/* ~ ~ ~ BEE STATES ~ ~ ~ */

	this.setDead = function(dead) {
		controller.pauseSound("bee_buzz");
		this.animState = ANIM_STATES.DEAD;
		this.pause();
		this.setImage(DEAD_FRAME);
	};

	this.setFlying = function(flying) {
		this.buzzing = false;
		this.animState = ANIM_STATES.FLYING;
		this.resume();
		this.startAnimation(ANIM_NAME_FLYING, { loop: true });
	};

/* ~ ~ ~ TICK / UPDATING ~ ~ ~ */

	this.updateView = function(dt, cameraOffset) {
		var player = model.player;
		if (model.finished) {
			return;
		}

		var beeSpawn = model.beeSpawn;
		this.style.x = beeSpawn.x - (model.player.x - model.player.screenX);
		this.style.y = beeSpawn.y - cameraOffset;

		if (!this.buzzing && this.style.x < 2 * BG_WIDTH) {
			this.buzzing = true;
			controller.playSound("bee_buzz");
		}

		if (model.countdownActive && !this.isPaused) {
			this.pause();
		}
	};
});

exports.ANIM_STATES = ANIM_STATES;