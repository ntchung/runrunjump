import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.controllers.Controller as Controller;

exports = Class(ImageView, function(supr) {

	var BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		ICON_WIDTH = 100,
		ICON_HEIGHT = 100,
		SPACING = 6,
		SPACING_Y = 12,
		OFFSET_X = 94,
		TITLE_WIDTH = 414,
		TITLE_HEIGHT = 50,
		BAR_WIDTH = 400,
		BAR_HEIGHT = 52,
		LIST_OFFSET = 2,
		OUTLINE_WIDTH = 112,
		OUTLINE_HEIGHT = 112,

		config;

	this.init = function(opts) {
		opts.x = opts.x + LIST_OFFSET;
		opts.blockEvents = true;
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;

		this.node1 = "resources/images/Achievements/treeNodeLight.png";
		this.node2 = "resources/images/Achievements/treeNodeDark.png";

		this.iconOutline = new ImageView({
			parent: this,
			x: OFFSET_X + (ICON_WIDTH - OUTLINE_WIDTH) / 2,
			y: (this.style.height - OUTLINE_HEIGHT) / 2,
			width: OUTLINE_WIDTH,
			height: OUTLINE_HEIGHT,
			image: "resources/images/Achievements/iconOutline.png",
			canHandleEvents: false
		});

		this.icon = new ImageView({
			parent: this,
			x: OFFSET_X,
			y: (this.style.height - ICON_HEIGHT) / 2,
			width: ICON_WIDTH,
			height: ICON_HEIGHT,
			canHandleEvents: false
		});

		this.title = new TextView({
			parent: this,
			x: OFFSET_X + SPACING + ICON_WIDTH,
			y: SPACING_Y,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: "",
			textAlign: 'center',
			size: config.fonts.achievementTitle,
			color: config.colors.achievementTitle,
			multiline: false,
			canHandleEvents: false
		});

		this.info = new TextView({
			parent: this,
			x: OFFSET_X + SPACING + ICON_WIDTH,
			y: (this.style.height - TITLE_HEIGHT) / 2 + 3,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: "",
			textAlign: 'center',
			size: config.fonts.achievementInfo,
			color: config.fonts.achievementInfo,
			multiline: true,
			canHandleEvents: false
		});

		this.barText = new TextView({
			parent: this,
			x: OFFSET_X + SPACING + ICON_WIDTH,
			y: this.style.height - TITLE_HEIGHT - 8,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: "",
			textAlign: 'center',
			size: config.fonts.achievementProgress,
			color: config.colors.achievementProgressFill,
			strokeColor: config.colors.achievementProgressStroke,
			multiline: false,
			canHandleEvents: false
		});
	};

	this.updateData = function(data) {
		// grab our data
		this.data = data;
		this._uid = data.uid;
		this.index = data.index;
		this.name = data.name;

		this.title.setText(GC.app.l.translate(data.name));
		this.info.setText(GC.app.l.translate(data.info));
		this.icon.setImage(data.icon);

		if (data.earned) {
			this.icon.style.opacity = 1;
		} else {
			this.icon.style.opacity = 0.75;
		}

		if (data.barText) {
			this.barText.style.visible = true;
			this.barText.setText(data.barText);
		} else {
			this.barText.style.visible = false;
		}

		// update background node color
		this.setImage(data.index % 2 ? this.node1 : this.node2);
	};
});
