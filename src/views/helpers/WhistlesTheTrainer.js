import ui.SpriteView as Sprite;

exports = Class(Sprite, function(supr) {

	var TRAINER_WIDTH = 210,
		TRAINER_HEIGHT = 200;

	// animation chances / distribution
	var WINK_CHANCE = 0.1,
		IDLE_CHANCE = 0.9,
		IDLE_WAIT = 1500,
		IDLE_WAIT_MIN = 500;

	// animation names
	var ANIM_NAME_IDLE = "idle",
		ANIM_NAME_WINK = "wink",
		ANIM_NAME_SWITCH = "switch",
		FRAME_NAME_IDLE = "resources/images/Characters/characterTrainer/trainer_idle_0001.png";

	this.init = function(opts) {
		opts.width = TRAINER_WIDTH;
		opts.height = TRAINER_HEIGHT;
		opts.url = "resources/images/Characters/characterTrainer/trainer";
		opts.defaultAnimation = ANIM_NAME_IDLE;
		opts.loop = false;
		opts.autoStart = false;
		opts.canHandleEvents = false;
		supr(this, 'init', [opts]);

		this.boundNextAnimation = bind(this, 'nextAnimation');
		this.startAnimation(ANIM_NAME_SWITCH, { iterations: 1, callback: this.boundNextAnimation });
	};

	this.stopAnimation = function() {
		if (this.currentTimeout) {
			clearTimeout(this.currentTimeout);
			this.currentTimeout = false;
		}

		supr(this, 'stopAnimation', arguments);
	};

	this.nextAnimation = function() {
		var roll = Math.random();
		if (roll < IDLE_CHANCE) {
			this.pause();
			this.setImage(FRAME_NAME_IDLE);
			this.style.visible = true;
			this.currentTimeout = setTimeout(bind(this, function() {
				this.currentTimeout = false;
				this.resume();
				this.startAnimation(ANIM_NAME_IDLE, { iterations: 1, callback: this.boundNextAnimation });
			}), Math.random() * IDLE_WAIT + IDLE_WAIT_MIN);
		} else {
			this.startAnimation(ANIM_NAME_WINK, { iterations: 1, callback: this.boundNextAnimation });
		}
	};

	this.storeSwitch = function() {
		if (this.currentTimeout) {
			clearTimeout(this.currentTimeout);
			this.currentTimeout = false;
			this.resume();
		}

		this.startAnimation(ANIM_NAME_SWITCH, { iterations: 1, callback: this.boundNextAnimation });
	};
});
