import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import animate;

exports = Class(ImageView, function(supr) {

	var WIDTH = 384,
		HEIGHT = 266,
		TEXT_WIDTH = 90,
		TEXT_HEIGHT = 75,
		ANIM_TIME = 600,
		WAIT_TIME = 3000,
		ANGEL_FISH_ID = "angelFish";

	this.init = function(opts) {
		opts.image = GC.app.l.translate("resources/images/Locales/en") + "/Game/fishsave.png";
		opts.x = (opts.parent.style.width - WIDTH) / 2,
		opts.y = opts.parent.style.height,
		opts.width = WIDTH;
		opts.height = HEIGHT;
		opts.visible = false;

		this.controller = GC.app.controller;
		this.offscreenY = opts.y;
		this.finalY = opts.y - 1.25 * HEIGHT;
		this.gameView = opts.gameView;

		supr(this, 'init', [opts]);

		// bindings
		this.hideModal = bind(this, this.hideModal);

		// subscriptions
		this.subscribe("InputStart", this, "accept");

		this.designView();
	};

	this.designView = function() {
		this.gemCost = new TextView({
			superview: this,
			x: this.style.width/2 - TEXT_WIDTH,
			y: this.style.height - TEXT_HEIGHT - 10,
			width: TEXT_WIDTH,
			height: TEXT_HEIGHT,
			color: this.controller.config.colors.angelSaveInner,
			strokeColor: this.controller.config.colors.angelSaveStroke,
			size: this.controller.config.fonts.angelSave,
			textAlign: 'right',
			text: ""
		});

		this.gemIcon = new ImageView({
			superview: this,
			x: this.style.width/2 + 10,
			y: this.style.height - 100,
			width: 85,
			height: 85,
			horiztonalAnchor: 42.5,
			verticalAnchor: 42.5,
			r: Math.PI * 0.15,
			image: 'resources/images/Header/icon_gem.png'
		});
	};

	this.showModal = function() {
		var that = this;

		this.gemCost.setText(this.controller.getAngelSaveCost() + 'x');
		this.style.y = this.offscreenY;
		this.style.visible = true;

		animate(this).then({ y: this.finalY }, ANIM_TIME, animate.easeOut)
		.then(function() {
			that.controller.gameInput.registerInput(this);
			that.startTimer();
		});
	};

	this.hideModal = function(accept) {
		if (this.cancelTimeout && !accept) {
			return;
		}

		var that = this;

		if (this.controller.gameInput != null) {
			this.controller.gameInput.unregisterInput(this);
		}

		var cost = this.controller.getAngelSaveCost(),
			status = accept ? "YES" : "NO";
		this.controller.trackEvent("AngelSaveModal", {
			saved: status,
			cost: cost + "",
			savedCost: status + "_" + cost
		});

		animate(this).then({ y: this.offscreenY }, ANIM_TIME, animate.easeIn)
		.then(function() {
			that.style.visible = false;
			that.gameView.model.angelSaveResolve(accept);
		});
	};

	this.startTimer = function() {
		this.cancelTimeout = false;
		setTimeout(this.hideModal, WAIT_TIME);
	};

	this.accept = function() {
		this.cancelTimeout = true;

		// link users to berry store if they don't have enough
		var gems = this.controller.getData("gems");
		if (gems < this.controller.getAngelSaveCost()) {
			this.gameView.model.pauseGameover();
			this.gameView.canHandleEvents(false);
			this.controller.midAngelSave = true;
			this.controller.deactivateGameInput();
			this.controller.showGemStore();
			this.controller.subscribeOnce('cancelAngelPurchase', this, 'onCancelPurchase');
			this.controller.subscribeOnce('acceptAngelPurchase', this, 'onAcceptPurchase');
		} else {
			this.hideModal(true);
		}
	};

	this.onCancelPurchase = function() {
		this.cancelTimeout = false;
		this.controller.midAngelSave = false;
		this.hideModal(false);
		this.controller.activateGameInput(this.gameView);
		this.gameView.canHandleEvents(true);
		this.controller.unsubscribe('cancelAngelPurchase', this);
		this.controller.unsubscribe('acceptAngelPurchase', this);
	};

	this.onAcceptPurchase = function() {
		this.cancelTimeout = false;
		this.controller.midAngelSave = false;
		this.hideModal(true);
		this.controller.activateGameInput(this.gameView);
		this.gameView.canHandleEvents(true);
		this.controller.unsubscribe('cancelAngelPurchase', this);
		this.controller.unsubscribe('acceptAngelPurchase', this);
	};
});
