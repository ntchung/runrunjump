import ui.View as View;
import ...lib.TextView as TextView;
import ui.ImageView as ImageView;

import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	var BASE_WIDTH = 1280,
		BASE_HEIGHT = 346,
		OVERFLOW_SIZE = 50,
		TITLE_WIDTH = 500,
		TITLE_HEIGHT = 136;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		var config = this.controller.config;

		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BASE_WIDTH) / 2,
			y: 0,
			width: BASE_WIDTH,
			height: BASE_HEIGHT,
			image: "resources/images/Achievements/treeBase.png",
			canHandleEvents: false
		});

		this.title = new TextView({
			parent: this,
			x: (this.style.width - TITLE_WIDTH) / 2,
			y: 0,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: GC.app.l.translate("Achievements"),
			textAlign: 'center',
			size: config.fonts.achievementTitleMain,
			color: config.colors.achievementTitleMain,
			multiline: false,
			canHandleEvents: false
		});

		this.overflow = new ImageView({
			parent: this,
			x: (this.style.width - BASE_WIDTH) / 2,
			y: this.style.height,
			width: BASE_WIDTH,
			height: OVERFLOW_SIZE,
			image: "resources/images/Achievements/treeOverflowBottom.png",
			canHandleEvents: false
		});
	};
});
