import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.controllers.Controller as Controller;

exports = Class(ImageView, function(supr) {

	var BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		OFFSET_X = -94,
		WORKABLE_WIDTH = 960,
		NODE_HEIGHT = 195,
		TITLE_OFFSET_Y = 15,
		TITLE_WIDTH = WORKABLE_WIDTH,
		TITLE_HEIGHT = 65,
		NESTABLE_SIZE = 112,
		REWARD_OFFSET_X = 278,
		REWARD_OFFSET_Y = 5,
		REWARD_WIDTH = 168,
		REWARD_HEIGHT = 172,
		REWARD_TYPE_BERRIES = "Berries";

	this.init = function(opts) {
		opts.blockEvents = true;
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		var config = this.controller.config;

		this.node1 = "resources/images/Nestables/nodeNestablesLight.png";
		this.node2 = "resources/images/Nestables/nodeNestablesDark.png";

		this.title = new TextView({
			parent: this,
			x: OFFSET_X + (BG_WIDTH - TITLE_WIDTH) / 2,
			y: TITLE_OFFSET_Y,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.nestables,
			color: config.colors.nestablesFill,
			strokeColor: config.colors.nestablesStroke,
			text: "",
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.rewardFrame = new ImageView({
			parent: this,
			x: OFFSET_X + BG_WIDTH / 2 + REWARD_OFFSET_X,
			y: REWARD_OFFSET_Y,
			width: REWARD_WIDTH,
			height: REWARD_HEIGHT,
			image: "resources/images/Nestables/frameBadge.png",
			canHandleEvents: false
		});

		this.rewardLock = new ImageView({
			parent: this.rewardFrame,
			x: 0,
			y: 0,
			width: REWARD_WIDTH,
			height: REWARD_HEIGHT,
			image: "resources/images/Nestables/frameBadgeLock.png",
			canHandleEvents: false
		});

		this.rewardView = new ImageView({
			parent: this.rewardFrame,
			x: 0,
			y: 0,
			width: REWARD_WIDTH,
			height: REWARD_HEIGHT,
			image: "resources/images/Nestables/frameBadgeLock.png", // placeholder
			canHandleEvents: false
		});

		this.rewardLabel = new TextView({
			parent: this.rewardFrame,
			x: 0,
			y: 0,
			width: REWARD_WIDTH,
			height: REWARD_HEIGHT,
			text: "",
			textAlign: "center",
			size: config.fonts.rewardValue,
			color: config.colors.rewardValueFill,
			strokeColor: config.colors.rewardValueStroke,
			multiline: false,
			canHandleEvents: false
		});

		this.nestableImages = [];
	};

	this.updateData = function(data) {
		// grab our data
		this.data = data;
		this._uid = data.uid;
		this.index = data.index;
		this.name = data.name;
		this.reward = data.reward;
		this.pieces = data.pieces;

		this.title.setText(GC.app.l.translate(this.name));

		var count = this.pieces.length;
		var imageCount = this.nestableImages.length;

		// ensure we have enough nestable image views
		if (imageCount < count) {
			var newImageCount = count - imageCount;
			for (var i = 0; i < newImageCount; i++) {
				this.nestableImages.push(new ImageView({
					parent: this,
					x: 0,
					y: 0,
					width: NESTABLE_SIZE,
					height: NESTABLE_SIZE,
					image: "resources/images/Game/itemRadial.png", // placeholder
					canHandleEvents: false
				}));
			}

			imageCount = newImageCount;
		}

		var x = OFFSET_X + (BG_WIDTH - count * NESTABLE_SIZE) / 2;
		var y = TITLE_HEIGHT;

		// position and center nestable views
		var index = 0;
		var ownedCount = 0;
		for (var i in this.pieces) {
			var piece = this.pieces[i];
			var image = this.nestableImages[index++];

			image.style.x = x;
			image.style.y = y;
			image.style.visible = true;

			if (piece.owned) {
				image.setImage(piece.image);
				ownedCount++;
			} else {
				image.setImage(piece.shadow);
			}

			x += NESTABLE_SIZE;
		}

		// is the set complete ?
		if (ownedCount != count) {
			this.rewardLock.style.visible = true;
			this.rewardView.style.visible = false;
			this.rewardLabel.style.visible = false;
		} else {
			this.rewardLock.style.visible = false;
			this.rewardView.style.visible = true;
			this.rewardView.setImage(this.reward.image);
			this.rewardView.style.x = (this.rewardFrame.style.width - this.reward.width) / 2;
			this.rewardView.style.y = (this.rewardFrame.style.height - this.reward.height) / 2;
			this.rewardView.style.width = this.reward.width;
			this.rewardView.style.height = this.reward.height;

			// show reward quantity
			if (this.reward.type == REWARD_TYPE_BERRIES) {
				this.rewardLabel.style.visible = true;
				this.rewardLabel.setText(this.reward.value);
			} else {
				this.rewardLabel.style.visible = false;
			}
		}

		// hide any extra views
		while (index < imageCount) {
			this.nestableImages[index++].style.visible = false;
		}

		// update background node color
		this.setImage(data.index % 2 ? this.node1 : this.node2);
	};
});
