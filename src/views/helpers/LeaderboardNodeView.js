import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

exports = Class(View, function(supr) {

	var
		PLANK_EVEN = "resources/images/Leaderboard/boardBlue.png",
		PLANK_ODD = "resources/images/Leaderboard/boardPurple.png",
		PLANK_EVEN_PHT = "resources/images/Leaderboard/boardBluePhoto.png",
		PLANK_ODD_PHT = "resources/images/Leaderboard/boardPurplePhoto.png",
		PLANK_FACEBOOK = "resources/images/Leaderboard/boardFacebook.png",
		PADDING_LEFT = 80,
		PADDING_RIGHT = 80,
		WIDTH = 616,
		HEIGHT = 124,
		NAME_WIDTH = 330,
		PIC_DEFAULT = "resources/images/Leaderboard/leaderboard_pic_unknown.png",
		PIC_X = 154,
		PIC_Y = 25,
		PIC_WIDTH = 76,
		PIC_HEIGHT = 76,
		DEFAULT_MSG = GC.app.l.translate("Loading") + "...",

		controller;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		controller = GC.app.controller;

		this.designView();
	};

	this.designView = function() {
		var config = controller.config;

		this.defaultPhoto = new ImageView({
			parent: this,
			image: PIC_DEFAULT,
			x: PIC_X,
			y: PIC_Y,
			width: PIC_WIDTH,
			height: PIC_HEIGHT,
			canHandleEvents: false
		});

		this.contactPhoto = new ImageView({
			parent: this,
			x: PIC_X,
			y: PIC_Y,
			width: PIC_WIDTH,
			height: PIC_HEIGHT,
			canHandleEvents: false
		});

		this.background = new ImageView({
			parent: this,
			x: 0,
			y: 0,
			width: WIDTH,
			height: HEIGHT,
			canHandleEvents: false
		});

		this.loadingTxt = new TextView({
			parent: this,
			x: (this.style.width - WIDTH) / 2,
			y: 0,
			width: WIDTH,
			height: 124,
			text: DEFAULT_MSG,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			multiline: false,
			canHandleEvents: false,
			visible: false
		});

		this.wrapper = new View({
			parent: this,
			x: 0,
			y: 0,
			width: WIDTH,
			height: HEIGHT,
			canHandleEvents: false
		});

		this.rankTxt = new TextView({
			parent: this.wrapper,
			x: PADDING_LEFT,
			y: 0,
			width: 60,
			height: HEIGHT,
			size: config.fonts.leaderboardRank,
			strokeColor: config.colors.leaderboardRank,
			color: config.colors.leaderboardRank,
			multiline: false
		});

		this.nameTxt = new TextView({
			parent: this.wrapper,
			x: WIDTH - PADDING_RIGHT - NAME_WIDTH,
			y: 5,
			width: NAME_WIDTH,
			height: HEIGHT / 2,
			size: 42,
			fontFamily: "Patrick",
			color: config.colors.leaderboardRank,
			textAlign: "right",
			multiline: false
		});

		this.scoreTxt = new TextView({
			parent: this.wrapper,
			x: WIDTH - PADDING_RIGHT - NAME_WIDTH,
			y: 55,
			width: NAME_WIDTH,
			height: HEIGHT / 2,
			size: config.fonts.leaderboardScore,
			color: config.colors.leaderboardScore,
			textAlign: "right",
			multiline: false
		});
	};

	this.updateData = function(data) {
		var config = GC.app.controller.config,
			that = this;

		if (data.loading === true) {
			this.background.setImage(data.index % 2 === 0 ? PLANK_EVEN : PLANK_ODD);
			this.loadingTxt.setText(data.message != null ? data.message : DEFAULT_MSG);
			this.loadingTxt.style.x = (this.style.width - WIDTH) / 2;
			this.loadingTxt.style.visible = true;
			this.wrapper.style.visible = false;
			this.onInputStart = undefined;
		} else if (data.facebook === true) {
			this.background.setImage(PLANK_FACEBOOK);
			this.loadingTxt.setText(GC.app.l.translate("Log in to Facebook!"));
			this.loadingTxt.style.x = (this.style.width - WIDTH) / 2 + 40;
			this.loadingTxt.style.visible = true;
			this.wrapper.style.visible = false;
			this.onInputStart = bind(this, function() {
				controller.fb.login();
			});
		} else {
			data.rank = data.index + 1;
			if (data.uid === controller.myFacebookID) {
				controller.myCurrentRank = data.rank;
			}

			this.background.setImage(data.index % 2 === 0 ? PLANK_EVEN_PHT : PLANK_ODD_PHT);
			this.loadingTxt.style.visible = false;
			this.wrapper.style.visible = true;
			this.onInputStart = undefined;

			if (this.textTimeout) {
				clearTimeout(this.textTimeout);
			}
			this.textTimeout = setTimeout(function() {
				this.textTimeout = undefined;
				that.rankTxt.setText(data.rank);
				that.nameTxt._opts.fontSize = 42;
				//that.nameTxt._opts.font = config.fonts.leaderboardName;
				that.nameTxt.setText(data.name);
				that.scoreTxt.setText(data.score);
			}, 100);

			if (this.picTimeout) {
				clearTimeout(this.picTimeout);
			}
			this.contactPhoto.style.visible = false;
			this.picTimeout = setTimeout(function() {
				that.contactPhoto.style.visible = true;
				data.pic_small && that.contactPhoto.setImage(data.pic_small);
			}, 1000);
		}
	}
});
