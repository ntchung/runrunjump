import ui.View as View;
import ui.ImageView as ImageView;

exports = Class(View, function(supr) {

	var 
		NUM_ICONS = 2,
		ICON_WIDTH = 69,
		ICON_HEIGHT = 55,
		ICON_SPACING = 20,

		FISH = "fish",
		TURTLE = "turtle",

		IMAGE_HASH = {
			"fish": "resources/images/Game/equip_fish.png",
			"turtle": "resources/images/Game/equip_turtle.png"
		};

	this.init = function(opts) {

		this.powerupIcons = [];
		this.powerupQueue = [];
		this.powerupHash = {};

		opts.width = ICON_WIDTH;
		opts.height = (ICON_HEIGHT + ICON_SPACING) * NUM_ICONS;

		supr(this, 'init', [opts]);	

		this.designView();
	};

	this.designView = function() {
		var
			i;

		for ( i = 0; i < NUM_ICONS; i++ ) {
			this.powerupIcons[i] = new ImageView({
				parent: this,
				x: ICON_SPACING / 2,
				y: i * (ICON_HEIGHT + ICON_SPACING),
				width: ICON_WIDTH,
				height: ICON_HEIGHT
			});	
		}
	};

	this.update = function() {
		var i, len;	
		
		for ( i = 0, len = this.powerupIcons.length; i < len; i++ ) {
			if ( i < this.powerupQueue.length ) {
				this.powerupIcons[i].setImage(IMAGE_HASH[this.powerupQueue[i]]);		
				this.powerupIcons[i].style.visible = true;
			} else {
				this.powerupIcons[i].style.visible = false;		
			}
		}
	};

	this.activateTurtle = function() {
		if ( !this.powerupHash[TURTLE] ) {
			this.powerupQueue.push(TURTLE);		
			this.powerupHash[TURTLE] = true;
			this.update();
		}
	};

	this.activateFish = function() {
		if ( !this.powerupHash[FISH] ) {
			this.powerupQueue.push(FISH);		
			this.powerupHash[FISH] = true;	
			this.update();
		}
	};

	this.deactivateTurtle = function() {
		var i, len;

		this.powerupHash[TURTLE] = false;	

		for ( i = 0, len = this.powerupQueue.length; i < len; i++ ) {
			if ( this.powerupQueue[i] === TURTLE ) {
				this.powerupQueue.splice(i, 1);
				break;
			}		
		}

		this.update();
	};

	this.deactivateFish = function() {
		var i, len;

		this.powerupHash[FISH] = false;	

		for ( i = 0, len = this.powerupQueue.length; i < len; i++ ) {
			if ( this.powerupQueue[i] === FISH ) {
				this.powerupQueue.splice(i, 1);
				break;
			}		
		}

		this.update();	
	};

	this.deactivateAll = function() {
		this.powerupQueue = [];
		this.powerupHash = {};

		this.update();
	};
});
