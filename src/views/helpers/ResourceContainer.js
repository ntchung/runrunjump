import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;

import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

    var config;

    this.init = function(opts) {
        supr(this, 'init', [opts]);

        this.background_middle = 'resources/images/Header/bar_counter_mid.png';
        this.background_cap_up = 'resources/images/Header/button_counter_up.png';
        this.background_cap_down = 'resources/images/Header/button_counter_down.png';

        this.icon = opts.icon || 'resources/images/Header/icon_berry.png';

        this.onInputStart = opts.onClick || function(){};

        this.controller = opts.controller || Controller.get();
        config = this.controller.config;

        this.designView();
    };

    this.setResourceAmount = function(amount){
        if (amount !== this.lastAmount) {
        	this.resourceAmount.setText(amount);
        	this.lastAmount = amount;
        }
    };

    this.designView = function(){
        var barMidX = this.style.height/2,
            capWidth = this.style.height*0.75;

        this.icon_image = new ImageView({
            superview: this,
            x: 0,
            y: 0,
            width: this.style.height,
            height: this.style.height,
            image: this.icon,
            zIndex: 5,
            canHandleEvents: false
        });

        this.bar_middle = new ImageView({
            superview: this,
            x: barMidX,
            y: this.style.height * 0.125,
            width: this.style.width - capWidth - barMidX,
            height: this.style.height * 0.75,
            image: this.background_middle,
            zIndex: 4,
            canHandleEvents: false
        });

        this.cap_button = new ButtonView({
            superview: this,
            x: this.style.width - capWidth,
            y: this.style.height * 0.125,
            width: capWidth,
            height: capWidth,
            image: this.background_cap_up,
            imagePressed: this.background_cap_down,
            onClick: this.onClick,
            zIndex: 4
        });

        var textX = barMidX;

        this.resourceAmount = new TextView({
            superview: this.bar_middle,
            x: textX,
            y: this.bar_middle.style.height * 0.14,
            width: this.bar_middle.style.width - textX,
            height: this.bar_middle.style.height * 0.68,
            text: '0000',
            textAlign: 'right',
            verticalAlign: 'middle',
            size: config.fonts.resourceContainer,
            color: 'black',
            multiline: false,
            canHandleEvents: false,
            zIndex: 4
        })
    }


});
