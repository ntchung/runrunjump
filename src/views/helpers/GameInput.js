import device;

import src.lib.InputExpander as InputExpander;

exports = Class(InputExpander, function(supr) {

	var PAUSE_WIDTH = 103,
		PAUSE_HEIGHT = 103,
		BG_HEIGHT = 720;

	// see InputExpander init
	this.init = function(opts) {

		this.registeredHash = {};
		this.registeredOrdered = [];

		supr(this, 'init', [opts]);
		this.scale = opts.scale;
	};

	this.onInputStart = function(evt, pt) {
		var 
			view,
			position,
			i, len;

		if (pt.x >= this.style.width - PAUSE_WIDTH && pt.y <= PAUSE_HEIGHT) {
			this.realView.pauseButton.onClick(evt, pt);
		} else if (this.registeredOrdered.length > 0) {
			
			for ( i = 0, len = this.registeredOrdered.length; i < len; i++ ) {
				view = this.registeredOrdered[i];
				position = view.getPosition(this);

				if (pt.x >= (position.x / position.scale) && 
					pt.x <= (position.x + position.width) / position.scale && 
					pt.y >= (position.y / position.scale) && 
					pt.y <= (position.y + position.height) / position.scale ) {

					view.publish('InputStart', evt, pt);
					
					if ( typeof view.onInputStart === 'function' ) {
						this.registeredOrdered[i].onInputStart(evt, pt);
					}

					return;
				}
			}	

			supr(this, 'onInputStart', arguments);
		} else {
			supr(this, 'onInputStart', arguments);
		}
	};

	this.onInputSelect = function(evt, pt) {
		var view, position, i, len;

		if (this.registeredOrdered.length > 0) {
			for (i = 0, len = this.registeredOrdered.length; i < len; i++) {
				view = this.registeredOrdered[i];
				position = view.getPosition(this);

				if (pt.x >= (position.x / position.scale)
					&& pt.x <= (position.x + position.width) / position.scale
					&& pt.y >= (position.y / position.scale)
					&& pt.y <= (position.y + position.height) / position.scale)
				{
					view.publish('InputSelect', evt, pt);

					if (typeof view.onInputSelect === 'function') {
						this.registeredOrdered[i].onInputSelect(evt, pt);
					}

					return;
				}
			}

			supr(this, 'onInputSelect', arguments);
		} else {
			supr(this, 'onInputSelect', arguments);
		}
	};

	this.registerInput = function(view) {
		var idx = this.registeredOrdered.length;

		this.registeredOrdered[idx] = view;
		this.registeredHash[view.uid] = idx;
	};

	this.unregisterInput = function(view) {
		var 
			hash = this.registeredHash,
			ordered = this.registeredOrdered,
			idx = hash[view.uid],
			end;
		
		delete hash[view.uid];

		if ( idx != null ) {
			end = ordered[ordered.length - 1];
			if ( hash[end.uid] != null ) {
				hash[end.uid] = idx;
			}
			ordered[idx] = end;
			ordered.pop();
		}
	};
});
