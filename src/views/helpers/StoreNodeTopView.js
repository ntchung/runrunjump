import ui.View as View;
import ui.ImageView as ImageView;

exports = Class(View, function(supr) {

	var NODE_WIDTH = 750,
		NODE_HEIGHT = 180;

	this.init = function(opts) {
		opts.x = 0;
		opts.y = 0;
		opts.width = NODE_WIDTH;
		opts.height = NODE_HEIGHT / 4;
		supr(this, 'init', [opts]);

		this.background = new ImageView({
			parent: this,
			x: 0,
			y: -3 * NODE_HEIGHT / 4,
			width: NODE_WIDTH,
			height: NODE_HEIGHT,
			image: "resources/images/Store/nodeDark.png"
		});
	};
});
