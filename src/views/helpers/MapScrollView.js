import ui.ScrollView as ScrollView;

exports = Class(ScrollView, function(supr) {

	var MAP_FOREL_WIDTH = 196,
		MAP_FOREL_HEIGHT = 716,
		FOREL_OFFSET_X = 20,
		FRAME_PER_PIXEL = 1 / 5;

	this.init = function(opts) {
		this.parent = opts.parent;
		this.scrollSprite = null;

		supr(this, 'init', [opts]);	
		
	};

	this.setScrollSprite = function(scrollSprite) {
		this.scrollSprite = scrollSprite;	
	};

	this.onDragStart = function() {
		supr(this, 'onDragStart', arguments);

		this.lastDragTime = Date.now();
		this.scrollSprite.resume();
	};

	this.onDrag = function(dragEvt, moveEvt, delta) {
		var 
			targetFPS,
			dt = Date.now() - this.lastDragTime,
			xChange = this.getOffsetX(),
			speed;

		supr(this, 'onDrag', arguments);

		if (delta.x < 0 && xChange <= this.getFullWidth() - this.getScrollBounds().maxX ||
			delta.x > 0 && xChange >= 0 ) {
			this.scrollSprite.setFramerate(0);
			return;	
		}
	
		xChange = this.getOffsetX() - xChange;
		speed = dt === 0 ? 0 : (-xChange / dt) * 1000; // pixels / second
		
		this.lastDragTime = Date.now();
		targetFPS = Math.min(Math.max(speed * FRAME_PER_PIXEL, -45), 45);
	
		this.scrollSprite.setFramerate(targetFPS);
	};

	this.onDragStop = function() {
		supr(this, 'onDragStop', arguments);

		this.scrollSprite.pause();
	};

});
