import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;

import src.controllers.Controller as Controller;

exports = Class(ImageView, function(supr) {

	var BACK_OFFSET_X = 895,
		BACK_OFFSET_Y = 120,
		BACK_WIDTH = 200,
		BACK_HEIGHT = 166,
		TITLE_X = 460,
		TITLE_Y = 56,
		TITLE_R = -Math.PI / 44,
		TITLE_WIDTH = 400,
		TITLE_HEIGHT = 60;

	this.init = function(opts) {
		opts.image = "resources/images/Nestables/nodeNestablesTop.png";
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		var config = this.controller.config;

		this.txtTitle = new TextView({
			parent: this,
			x: TITLE_X,
			y: TITLE_Y,
			r: TITLE_R,
			anchorX: TITLE_WIDTH / 2,
			anchorY: TITLE_HEIGHT / 2,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: GC.app.l.translate("NESTABLES"),
			textAlign: 'center',
			size: config.fonts.nestablesTitle,
			color: config.colors.nestablesTitleFill,
			strokeColor: config.colors.nestablesTitleStroke,
			multiline: false,
			canHandleEvents: false
		});

		this.backButton = new TextView({
			parent: this,
			x: BACK_OFFSET_X,
			y: BACK_OFFSET_Y,
			width: BACK_WIDTH,
			height: BACK_HEIGHT,
			text: GC.app.l.translate("BACK"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false
		});
		this.backButton.onInputStart = bind(this, function() {
			this.controller.playSound("ui_click");

			var parent = this.controller.getNestablesView();
			parent.deconstructView(bind(this.controller, 'transitionToMainMenu'));
		});
	};
});
