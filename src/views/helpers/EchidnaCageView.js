import ui.ImageView as ImageView;
import ui.SpriteView as Sprite;

import src.controllers.Controller as Controller;

var ANIM_STATES = {
	DEAD: "DEAD",
	IDLE: "IDLE",
	QUILLED: "QUILLED"
};

exports = Class(Sprite, function(supr) {

/* ~ ~ ~ CLASS CONSTANTS / GLOBALS ~ ~ ~ */

	var controller = Controller.get(),
		config = controller.config,
		soundConfig = controller.soundConfig,
		model,

		// animation names
		ANIM_NAME_ATTACK = "attack",
		ANIM_NAME_DEATH = "death",
		ANIM_NAME_IDLE = "idle",

		// image names
		IMG_CAGE = "resources/images/Enemies/Echidna/cage.png",
		IMG_CAGE_BAR_1 = "resources/images/Enemies/Echidna/cageBar1.png",
		IMG_CAGE_BAR_2 = "resources/images/Enemies/Echidna/cageBar2.png",
		IMG_CAGE_BAR_3 = "resources/images/Enemies/Echidna/cageBar3.png",
		IMG_CAGE_BAR_4 = "resources/images/Enemies/Echidna/cageBar4.png",
		IMG_CAGE_BOTTOM = "resources/images/Enemies/Echidna/cageBottom.png",
		IMG_CAGE_TOP = "resources/images/Enemies/Echidna/cageTop.png",
		QUILL_FRAME = "resources/images/Enemies/Echidna/echidna_attack_0013.png",
		SOUND_NAME_QUILLS = "echidna_quills",

		// various constants
		BG_WIDTH = 1280,
		OFFSET_Y = 54,
		CAGE_OFFSET_X = 12,
		CAGE_OFFSET_Y = -16,
		CAGE_WIDTH = 175,
		CAGE_HEIGHT = 150,
		ECHIDNA_WIDTH = 150,
		ECHIDNA_HEIGHT = 150,
		FULL_HEIGHT = CAGE_HEIGHT + ECHIDNA_HEIGHT;

/* ~ ~ ~ INITIALIZATION / RESET ~ ~ ~ */

	this.init = function(opts) {
		var babyView = opts.babyView;
		opts.x = babyView.style.x + (babyView.style.width - ECHIDNA_WIDTH) / 2;
		opts.y = babyView.style.y + babyView.style.height - FULL_HEIGHT + OFFSET_Y;
		opts.width = ECHIDNA_WIDTH;
		opts.height = ECHIDNA_HEIGHT;
		opts.canHandleEvents = false;
		opts.defaultAnimation = ANIM_NAME_IDLE;
		opts.loop = false;
		opts.autoStart = false;
		supr(this, 'init', [opts]);

		this.cageView = new ImageView({
			parent: this,
			x: (ECHIDNA_WIDTH - CAGE_WIDTH) / 2 + CAGE_OFFSET_X,
			y: ECHIDNA_HEIGHT + CAGE_OFFSET_Y,
			width: CAGE_WIDTH,
			height: CAGE_HEIGHT,
			image: IMG_CAGE,
			canHandleEvents: false
		});

		this.boundStartNextAnim = bind(this, "startNextAnimation");
		this.boundHoldQuill = bind(this, "holdQuill");

		this.resetView(opts);
	};

	this.resetView = function(opts) {
		this.resetAllAnimations(opts);

		var babyView = opts.babyView;
		this.style.x = babyView.style.x + (babyView.style.width - ECHIDNA_WIDTH) / 2;
		this.style.y = babyView.style.y + babyView.style.height - FULL_HEIGHT + OFFSET_Y;
		this.style.r = 0;
		this.style.anchorX = ECHIDNA_WIDTH / 2;
		this.style.anchorY = ECHIDNA_HEIGHT / 2;
		this.style.width = ECHIDNA_WIDTH;
		this.style.height = ECHIDNA_HEIGHT;
		this.cageView.style.visible = true;

		model = controller.getGameModel();

		// animation state flags
		this.animState = ANIM_STATES.IDLE;

		this.resume();
		this.startAnimation("idle", { iterations: 1, callback: this.boundStartNextAnim });
		this.pause();
	};

/* ~ ~ ~ ANIMATION ~ ~ ~ */

	this.startNextAnimation = function() {
		if (!model.babySpawned) { return; }
		if (this.animState === ANIM_STATES.IDLE) { // default logic
			this.startAnimation(ANIM_NAME_IDLE, { iterations: 1, callback: this.boundStartNextAnim });
		}
	};

	this.pause = function() {
		this.isPaused = true;
		supr(this, 'pause', arguments);
		this.style.visible = true;
	};

	this.resume = function() {
		this.isPaused = false;
		supr(this, 'resume', arguments);
		this.style.visible = true;
	};

/* ~ ~ ~ PLAYER STATES ~ ~ ~ */

	this.setDead = function(dead) {
		this.animState = ANIM_STATES.DEAD;
		this.resume();
		this.startAnimation(ANIM_NAME_DEATH, { iterations: 1 });
		this.pause();
	};

	this.setIdle = function(idle) {
		this.animState = ANIM_STATES.IDLE;
		this.resume();
		this.startAnimation(ANIM_NAME_IDLE, { iterations: 1, callback: this.boundStartNextAnim });
	};

	this.setQuilled = function(quilled) {
		this.animState = ANIM_STATES.QUILLED;
		this.resume();
		this.startAnimation(ANIM_NAME_ATTACK, { iterations: 1, callback: this.boundHoldQuill });
	};

	this.holdQuill = function() {
		if (!model.babySpawned) { return; }
		controller.playSound(SOUND_NAME_QUILLS);
		this.setImage(QUILL_FRAME);
		this.pause();
	};

/* ~ ~ ~ TICK / UPDATING ~ ~ ~ */

	this.updateView = function(dt, cameraOffset) {
		var player = model.player;
		if (model.gameoverCountdown >= 0 || model.finished) {
			return;
		}

		var babySpawn = model.babySpawned;
		this.style.x = babySpawn.x - (model.player.x - model.player.screenX) + (babySpawn.width - ECHIDNA_WIDTH) / 2;
		this.style.y = (babySpawn.y + babySpawn.height) - FULL_HEIGHT - cameraOffset + OFFSET_Y;

		if (this.style.x < 2 / 3 * BG_WIDTH && this.animState !== ANIM_STATES.QUILLED) {
			this.setQuilled(true);
		}

		if (model.countdownActive && !this.isPaused) {
			this.pause();
		}
	};
});

exports.ANIM_STATES = ANIM_STATES;