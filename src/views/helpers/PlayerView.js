import ui.ImageView as ImageView;
import ui.SpriteView as Sprite;

import src.controllers.Controller as Controller;
import src.lib.ParticleEngine as ParticleEngine;

var ANIM_STATES = {
	DIVING: "DIVING",
	FALLING: "FALLING",
	FLOATING: "FLOATING",
	GLIDING: "GLIDING",
	JUMPING: "JUMPING",
	LANDING: "LANDING",
	ROLLING: "ROLLING",
	RUNNING: "RUNNING",
	SPIKED: "SPIKED"
};

exports = Class(Sprite, function(supr) {

/* ~ ~ ~ CLASS CONSTANTS / GLOBALS ~ ~ ~ */

	var controller = Controller.get(),
		config = controller.config,
		soundConfig = controller.soundConfig,
		rand = Math.random,
		model,

		// animation names
		ANIM_NAME_BLINK = "blink",
		ANIM_NAME_DIVE = "dive",
		ANIM_NAME_FLOAT = "float",
		ANIM_NAME_FOCUSED = "focused",
		ANIM_NAME_GLIDER = "glider",
		ANIM_NAME_GLIDER_STILL = "gliderStart",
		ANIM_NAME_JUMP = "jump",
		ANIM_NAME_LAND = "land",
		ANIM_NAME_ROLL = "roll",
		ANIM_NAME_RUN = "run",
		TRANSITION_EASE_OUT = "easeOut",

		// image names
		IMG_MAGNET_PARTICLE = "resources/images/Game/particleWind.png",
		IMG_MAGNET_AURA = "resources/images/Game/auraMagnet.png",
		IMG_GLIDER_PARTICLE = "resources/images/Game/particleWind.png",
		IMG_KIWI_GLIDER = "resources/images/Game/gliderOverlay.png",
		IMG_MEGA_GLIDER = "resources/images/Game/gliderOverlayPink.png",
		IMG_SUPER_GLIDER = "resources/images/Game/gliderOverlayBlack.png",

		// sound names and timing
		FLAPPING_TIMEOUT = soundConfig.timing.flappingDuration,
		GLIDER_WIND_TIMEOUT = soundConfig.timing.gliderWindDuration,
		GLIDER_WIND_VARIANCE = soundConfig.timing.gliderWindVariance,
		SOUND_NAME_DIVE = "dive",
		SOUND_NAME_FLAPPING = "flapping",
		SOUND_NAME_FLOAT = "struggle_flight",
		SOUND_NAME_GLIDER1 = "glider_wind1",
		SOUND_NAME_GLIDER2 = "glider_wind2",
		SOUND_NAME_JUMP = "jump",
		SOUND_NAME_LAND = "kiwi_land",
		SOUND_NAME_ROLL = "kiwi_roll",

		// mechanic constants
		BG_WIDTH = config.ui.maxWidth,
		BG_HEIGHT = config.ui.maxHeight,
		BLINK_CHANCE = 0.3,
		FOCUSED_START_SPEED = 1200,
		GLIDER_MAX_SPEED_Y = 750,
		GLIDER_WARNING_TIME = 3500,
		GLIDER_BLINK_RATE = 0.05,
		GLIDER_WIDTH = 172,
		GLIDER_HEIGHT = 101,
		GLIDER_MIN_OPACITY = 0.25,
		MAGNET_WIDTH = 160,
		MAGNET_HEIGHT = 160,

		ROLL_AMOUNT = 8 * Math.PI,
		ROLL_TIME = 500,

		// mechanic variables
		defaultOffset,
		rollOffset,

		// still anim frame variables
		jumpFrame,
		startFrame;

/* ~ ~ ~ INITIALIZATION / RESET ~ ~ ~ */

	this.init = function(opts) {
		opts.canHandleEvents = false;
		opts.defaultAnimation = ANIM_NAME_RUN;
		opts.loop = false;
		opts.autoStart = false;
		supr(this, 'init', [opts]);

		this.gliderView = new ImageView({
			parent: this,
			x: (opts.width - GLIDER_WIDTH) / 2 - 12,
			y: (opts.height - GLIDER_HEIGHT) / 2 - 12,
			anchorX: GLIDER_WIDTH / 2,
			anchorY: GLIDER_HEIGHT / 2,
			width: GLIDER_WIDTH,
			height: GLIDER_HEIGHT,
			image: IMG_KIWI_GLIDER,
			canHandleEvents: false,
			visible: false
		});

		this.magnetView = new ImageView({
			parent: this,
			x: (opts.width - MAGNET_WIDTH) / 2,
			y: (opts.height - MAGNET_HEIGHT) / 2,
			anchorX: MAGNET_WIDTH / 2,
			anchorY: MAGNET_HEIGHT / 2,
			width: MAGNET_WIDTH,
			height: MAGNET_HEIGHT,
			image: IMG_MAGNET_AURA,
			opacity: 0.5,
			canHandleEvents: false,
			visible: false
		});

		this.particleEngine = new ParticleEngine({
			parent: this,
			initCount: 30,
			x: 0,
			y: 0,
			anchorX: opts.width / 2,
			anchorY: opts.height / 2,
			width: opts.width,
			height: opts.height
		});

		this.boundStartNextAnim = bind(this, "startNextAnimation");
		this.boundFinishRoll = bind(this, "setRolling", false);
		this.boundStartFloating = bind(this, "setFloating", true);

		this.resetView(opts);
	};

	this.resetView = function(opts) {
		this.resetAllAnimations(opts);

		this.style.x = opts.x;
		this.style.y = opts.y;
		this.style.r = 0;
		this.style.width = opts.width;
		this.style.height = opts.height;

		// reset any leftover particles
		this.particleEngine.killAllParticles();

		this.gliderView.style.visible = false;
		this.gliderView.style.opacity = 1;

		this.magnetView.style.visible = false;

		model = controller.getGameModel();
		var player = model.player;

		// fit glider to avatar
		this.gliderView.style.x = player.gliderX;
		this.gliderView.style.y = player.gliderY;

		// animation state flags
		this.animState = ANIM_STATES.RUNNING;
		this.focused = false;
		this.gliderStill = false;
		this.magnetized = false;
		this.shielded = false;

		// mechanic variables
		defaultOffset = player.defaultOffset;
		rollOffset = player.rollOffset;

		// still anim frame variables
		jumpFrame = player.jumpFrame;
		startFrame = player.startFrame;

		this.startAnimation("run", { iterations: 1, callback: this.boundStartNextAnim });
		this.pause();
		this.setImage(startFrame);
	};

/* ~ ~ ~ ANIMATION ~ ~ ~ */

	this.startNextAnimation = function() {
		if (this.isPaused) {
			this.resume();
		}

		if (this.animState !== ANIM_STATES.GLIDING) { // default logic
			// default to running, this will get changed as appropriate
			this.animState = ANIM_STATES.RUNNING;

			if (model.player.jumping) {
				if (model.fingerDown) {
					this.setFloating(true);
				} else {
					this.setFalling(true);
				}
			} else if (this.focused) {
				this.startAnimation(ANIM_NAME_FOCUSED, { iterations: 1, callback: this.boundStartNextAnim });
			} else {
				var roll = Math.random();
				if (roll < BLINK_CHANCE) {
					this.startAnimation(ANIM_NAME_BLINK, { iterations: 1, callback: this.boundStartNextAnim });
				} else {
					this.startAnimation(ANIM_NAME_RUN, { iterations: 1, callback: this.boundStartNextAnim });
				}
			}
		} else { // gliding logic
			if (!this.gliderStill) {
				var iters = Math.random() * 30 + 15; // 1-3 seconds of still kiwi
				this.gliderStill = true
				this.startAnimation(ANIM_NAME_GLIDER_STILL, { iterations: iters, callback: this.boundStartNextAnim });
			} else {
				this.gliderStill = false
				this.startAnimation(ANIM_NAME_GLIDER, { iterations: 1, callback: this.boundStartNextAnim });
			}
		}
	};

	this.pause = function() {
		this.isPaused = true;
		supr(this, 'pause', arguments);
		this.style.visible = true;
	};

	this.resume = function() {
		this.isPaused = false;
		supr(this, 'resume', arguments);
		this.style.visible = true;
	};

/* ~ ~ ~ PLAYER STATES ~ ~ ~ */

	this.setDiving = function(diving) {
		this.animState = ANIM_STATES.DIVING;

		controller.pauseSound(SOUND_NAME_FLAPPING);
		controller.playSound(SOUND_NAME_DIVE);
		this.resume();
		this.startAnimation(ANIM_NAME_DIVE, { iterations: 1 });
		this.pause();
	};

	this.setFalling = function(falling) {
		if (this.animState === ANIM_STATES.ROLLING) { return; }

		this.animState = ANIM_STATES.FALLING;

		controller.pauseSound(SOUND_NAME_FLAPPING);
		this.pause();
		this.setImage(jumpFrame);
	};

	this.setFloating = function(floating) {
		if (!model.fingerDown) { return; }

		this.animState = ANIM_STATES.FLOATING;
		this.flappingTimeout = 0;

		controller.playSound(SOUND_NAME_FLOAT);
		this.resume();
		this.startAnimation(ANIM_NAME_FLOAT, { iterations: 999999 }); // no way to specify loop for one anim
	};

	this.setFocused = function(focused) {
		this.focused = focused;
	};

	this.setGliding = function(gliding) {
		if (gliding) {
			this.animState = ANIM_STATES.GLIDING;

			this.gliderView.style.visible = true;
			this.gliderView.blinkDirection = -1;

			this.gliderSound = 1;
			this.gliderWindTimeout = 0;

			if (model.superGlider) {
				this.gliderView.setImage(IMG_SUPER_GLIDER);
			} else if (model.megaGlider) {
				this.gliderView.setImage(IMG_MEGA_GLIDER);
			} else {
				this.gliderView.setImage(IMG_KIWI_GLIDER);
			}
		} else {
			// default to running, this will get changed as appropriate
			this.animState = ANIM_STATES.RUNNING;

			this.style.r = 0;
			this.gliderView.style.visible = false;
			this.gliderView.style.opacity = 1;
			controller.pauseSound(SOUND_NAME_GLIDER1);
			controller.pauseSound(SOUND_NAME_GLIDER2);
		}

		this.startNextAnimation();
	};

	this.setJumping = function(jumping) {
		if (this.animState === ANIM_STATES.ROLLING) { return; }

		this.animState = ANIM_STATES.JUMPING;

		controller.playSound(SOUND_NAME_JUMP);
		this.startAnimation(ANIM_NAME_JUMP, { iterations: 1, callback: this.boundStartFloating });
	};

	this.setLanding = function(landing) {
		this.animState = ANIM_STATES.LANDING;

		controller.pauseSound(SOUND_NAME_FLAPPING);
		controller.playSound(SOUND_NAME_LAND);
		this.resume();
		this.startAnimation(ANIM_NAME_LAND, { iterations: 1, callback: this.boundStartNextAnim });
	};

	this.setMagnetized = function(magnetized) {
		this.magnetized = magnetized;

		if (magnetized) {
			this.magnetView.style.visible = true;
		} else {
			this.magnetView.style.visible = false;
		}
	};

	this.setShielded = function(shielded) {
		this.shielded = shielded;
	}

	this.setRolling = function(rolling) {
		model.player.rolling = rolling;

		if (rolling) {
			this.animState = ANIM_STATES.ROLLING;

			controller.pauseSound(SOUND_NAME_FLAPPING);
			controller.playSound(SOUND_NAME_ROLL);
			this.resume();
			this.startAnimation(ANIM_NAME_ROLL, { iterations: 1, callback: this.boundStartNextAnim });
			this.pause();

			// pass this view in as an external particle
			var rollData = this.particleEngine.obtainObject();
			rollData.dr = ROLL_AMOUNT;
			rollData.ttl = ROLL_TIME;
			rollData.transition = TRANSITION_EASE_OUT;
			rollData.onDeath = this.boundFinishRoll;
			this.particleEngine.addExternalParticle(this, rollData);
		} else {
			this.style.r = 0;
			this.startNextAnimation();
		}
	};

	this.setSpiked = function(spiked) {
		this.animState = ANIM_STATES.SPIKED;

		controller.playSound(SOUND_NAME_FLOAT);
		this.resume();
		this.startAnimation(ANIM_NAME_FLOAT, { iterations: 999999 }); // no way to specify loop for one anim

		// pass this view in as an external particle
		var spinData = this.particleEngine.obtainObject();
		spinData.dr = ROLL_AMOUNT / 3;
		spinData.ttl = ROLL_TIME * 2;
		spinData.transition = TRANSITION_EASE_OUT;
		spinData.onDeath = this.boundFinishRoll;
		this.particleEngine.addExternalParticle(this, spinData);
	};

/* ~ ~ ~ SPECIAL EFFECTS ~ ~ ~ */

	this.sink = function() {
		var sinkData = this.particleEngine.obtainObject();
		sinkData.dy = 200;
		sinkData.ttl = 1000;
		sinkData.transition = TRANSITION_EASE_OUT;
		this.particleEngine.addExternalParticle(this, sinkData);
	};

	this.emitMagnetParticle = function() {
		if (rand() < 0.5) {
			return;
		}

		var count = 1;
		var data = this.particleEngine.obtainParticleArray(count);
		for (var i = 0; i < count; i++) {
			var width = 20;
			var height = 10;
			var ttl = 300;
			var pObj = data[i];
			var pvs = this.style;
			var rot = 6.28 * rand();

			pObj.image = IMG_MAGNET_PARTICLE;
			pObj.polar = true;
			pObj.ox = pvs.width / 2;
			pObj.oy = pvs.height / 2;
			pObj.r = rot;
			pObj.radius = 3 * model.collectionRadius / 4;
			pObj.dradius = -500 / ttl * model.collectionRadius;
			pObj.ddradius = -2000 / ttl * model.collectionRadius;
			pObj.theta = rot;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.dwidth = 4 * width;
			pObj.ddwidth = 20 * width;
			pObj.dheight = -3 * height;
			pObj.dscale = -rand() * 4;
			pObj.ttl = ttl;
			pObj.dopacity = -1000 / ttl;
		}

		this.particleEngine.emitParticles(data);
	};

/* ~ ~ ~ TICK / UPDATING ~ ~ ~ */

	this.updateView = function(dt, cameraOffset) {
		var player = model.player,
			ps = this.style;

		// update this.particleEngine so that it doesn't rotate with the player
		this.particleEngine.style.r = 2 * Math.PI - ps.r;
		this.particleEngine.runTick(dt);

		// store current anim state so babies can follow suit
		model.playerHistory[model.playerHistoryIndex].animState = this.animState;

		if (model.gameoverCountdown >= 0 || model.finished) {
			return;
		}

		// player animation updates
		if (!player.spiked) {
			if (model.queueRoll) {
				model.queueRoll = false;
				model.queueLanding = false;
				this.setRolling(true);
			} else if (model.queueLanding) {
				model.queueLanding = false;
				this.setLanding(true);
			} else if (this.animState !== ANIM_STATES.FALLING && player.jumping && !player.diving && !model.fingerDown && model.kiwiGliderTimer <= 0) {
				this.setFalling(true);
			} else if (this.animState !== ANIM_STATES.FLOATING && player.floating && player.vy >= 0 && !player.diving && this.isPaused) {
				this.setFloating(true);
			}
		}

		// handle floating sounds
		if (this.animState === ANIM_STATES.FLOATING) {
			this.flappingTimeout -= dt;
			if (this.flappingTimeout <= 0) {
				this.flappingTimeout = FLAPPING_TIMEOUT;
				controller.playSound(SOUND_NAME_FLAPPING);
			}
		}

		// handle magnets
		this.magnetized && this.emitMagnetParticle();
		if (model.berryMagnetTimer > 0) {
			!this.magnetized && this.setMagnetized(true);
		} else {
			this.magnetized && this.setMagnetized(false);
		}

		if(model.shieldTimer > 0) {
			!this.shielded && this.setShielded(true);
			//this.emitShieldParticle();
			/*
			var sv = this.shieldView,
				svs = this.shieldView.style;

			if (model.shieldTimer <= SHIELD_WARNING_TIME) {
				svs.opacity += sv.blinkDirection * SHIELD_BLINK_RATE;
				if (svs.opacity <= SHIELD_MIN_OPACITY) {
					svs.opacity = SHIELD_MIN_OPACITY;
					sv.blinkDirection = -sv.blinkDirection;
				} else if (svs.opacity >= 1) {
					svs.opacity = 1;
					sv.blinkDirection = -sv.blinkDirection;
				}
			}*/
		}
		else{
			this.shielded && this.setShielded(false);
		}

		// handle gliding
		if (this.animState === ANIM_STATES.GLIDING) {
			if (dt) {
				this.gliderWindTimeout -= dt;
				if (this.gliderWindTimeout <= 0) {
					if (this.gliderSound === 1) {
						this.gliderSound = 2;
						controller.playSound(SOUND_NAME_GLIDER2);
					} else {
						this.gliderSound = 1;
						controller.playSound(SOUND_NAME_GLIDER1);
					}
					this.gliderWindTimeout = GLIDER_WIND_TIMEOUT + Math.random() * GLIDER_WIND_VARIANCE;
				}
			}

			var gv = this.gliderView,
				gvs = this.gliderView.style;

			if (model.kiwiGliderTimer <= GLIDER_WARNING_TIME) {
				gvs.opacity += gv.blinkDirection * GLIDER_BLINK_RATE;
				if (gvs.opacity <= GLIDER_MIN_OPACITY) {
					gvs.opacity = GLIDER_MIN_OPACITY;
					gv.blinkDirection = -gv.blinkDirection;
				} else if (gvs.opacity >= 1) {
					gvs.opacity = 1;
					gv.blinkDirection = -gv.blinkDirection;
				}
			}

			// player should accelerate towards last finger y
			if (model.lastFingerY) {
				var vy = -5 * (ps.y - model.lastFingerY);
				if (vy < -GLIDER_MAX_SPEED_Y) {
					vy = -GLIDER_MAX_SPEED_Y;
				} else if (vy > GLIDER_MAX_SPEED_Y) {
					vy = GLIDER_MAX_SPEED_Y;
				}

				model.player.vy = vy;
				var targetR = Math.PI / 3 * vy / BG_HEIGHT;
				var sum = targetR + 39 * ps.r;
				ps.r = (sum) / 40;
			}
		}

		if (!this.focused && !this.isPaused && !model.vxOriginal && player.vx >= FOCUSED_START_SPEED) {
			this.setFocused(true);
		}

		// handle player bounces
		if (model.queueTurtleBounce) {
			this.style.r = 0;
			model.queueTurtleBounce = false;
			this.setFalling(true);
		}

		// update player x if necessary
		if (player.screenX != ps.x) {
			var diffX = player.screenX - ps.x;
			ps.x += diffX / 2 * dt / 1000;
		}

		// update player y
		var offset = (this.animState === ANIM_STATES.ROLLING && !player.jumping) ? rollOffset : defaultOffset;
		ps.y = offset + player.y - cameraOffset;

		if (model.countdownActive && !this.isPaused) {
			this.pause();
		}
	};
});

exports.ANIM_STATES = ANIM_STATES;