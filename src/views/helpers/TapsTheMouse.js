import ui.SpriteView as Sprite;

exports = Class(Sprite, function(supr) {

	var MOUSE_WIDTH = 110,
		MOUSE_HEIGHT = 170;

	// animation chances / distribution
	var SWING_CHANCE = 0.55,
		TOSS_CHANCE = 0.15,
		IDLE_CHANCE = 0.3;

	// animation names
	var ANIM_NAME_IDLE = "idle",
		ANIM_NAME_SLEEP = "sleeping",
		ANIM_NAME_SWING = "swing",
		ANIM_NAME_TOSS = "toss";

	this.init = function(opts) {
		opts.width = MOUSE_WIDTH;
		opts.height = MOUSE_HEIGHT;
		opts.url = "resources/images/Characters/characterTaps/taps";
		opts.defaultAnimation = ANIM_NAME_SLEEP;
		opts.loop = false;
		opts.autoStart = false;
		supr(this, 'init', [opts]);

		// state flag
		this.pointing = false;

		this.boundNextAnimation = bind(this, 'nextAnimation');
		this.startAnimation(ANIM_NAME_SLEEP, { iterations: 1, callback: this.boundNextAnimation });
	};

	this.nextAnimation = function() {
		if (this.pointing) {
			var roll = Math.random();
			if (roll < SWING_CHANCE) {
				this.startAnimation(ANIM_NAME_SWING, { iterations: 1, callback: this.boundNextAnimation });
			} else if (roll < SWING_CHANCE + TOSS_CHANCE) {
				this.startAnimation(ANIM_NAME_TOSS, { iterations: 1, callback: this.boundNextAnimation });
			} else {
				this.startAnimation(ANIM_NAME_IDLE, { iterations: 1, callback: this.boundNextAnimation });
			}
		} else {
			this.startAnimation(ANIM_NAME_SLEEP, { iterations: 1, callback: this.boundNextAnimation });
		}
	};

	this.setPointing = function(pointing) {
		this.pointing = pointing;
		this.nextAnimation();
	};
});
