import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.GameTextView as GameTextView;
import src.lib.ScoreView as ScoreView;
import src.lib.ParticleEngine as ParticleEngine;

exports = Class(ImageView, function(supr) {

	var BERRY_METER_FRAME_WIDTH = 121,
		BERRY_METER_FRAME_HEIGHT = 121,
		BERRY_METER_WIDTH = 79,
		BERRY_METER_HEIGHT = 94,
		BERRY_JUICE_HEIGHT = 74,
		BERRY_JUICE_OFFSET = 8,
		BERRY_OFFSET_X = 14,
		BERRY_OFFSET_Y = 15,
		BERRY_TEXT_OFFSET_X = 4,
		BERRY_TEXT_OFFSET_Y = 40,
		MULT_TEXT_WIDTH = BERRY_METER_WIDTH,
		MULT_TEXT_HEIGHT = 22,
		MULT_TEXT_SCALE = MULT_TEXT_HEIGHT / 93,
		MULT_SPACING = 0,
		SPARKLE_COUNT = 40,
		BERRY_STREAK_LEVEL = 50,
		IMG_BERRY_SPARKLE = "resources/images/Game/sparklePink.png";

	this.init = function(opts) {
		opts.blockEvents = true;
		opts.anchorX = 0;
		opts.anchorY = 0;
		opts.image = "resources/images/Game/berrymeter_frame.png";
		supr(this, 'init', [opts]);

		var berryX = (BERRY_METER_FRAME_WIDTH - BERRY_METER_WIDTH) / 2 - BERRY_OFFSET_X;
		var berryY = (BERRY_METER_FRAME_HEIGHT - BERRY_METER_HEIGHT) / 2 - BERRY_OFFSET_Y;

		this.berryMeterText = new ScoreView({
			parent: this,
			x: berryX + BERRY_TEXT_OFFSET_X,
			y: berryY + BERRY_TEXT_OFFSET_Y,
			anchorX: MULT_TEXT_WIDTH / 2,
			anchorY: MULT_TEXT_HEIGHT / 2,
			width: MULT_TEXT_WIDTH,
			height: MULT_TEXT_HEIGHT,
			spacing: MULT_SPACING,
			initCount: 4,
			characterData: {
				"0": { width: 90 * MULT_TEXT_SCALE, image: "resources/images/Game/white_0.png" },
				"1": { width: 35 * MULT_TEXT_SCALE, image: "resources/images/Game/white_1.png" },
				"2": { width: 65 * MULT_TEXT_SCALE, image: "resources/images/Game/white_2.png" },
				"3": { width: 65 * MULT_TEXT_SCALE, image: "resources/images/Game/white_3.png" },
				"4": { width: 67 * MULT_TEXT_SCALE, image: "resources/images/Game/white_4.png" },
				"5": { width: 62 * MULT_TEXT_SCALE, image: "resources/images/Game/white_5.png" },
				"6": { width: 67 * MULT_TEXT_SCALE, image: "resources/images/Game/white_6.png" },
				"7": { width: 67 * MULT_TEXT_SCALE, image: "resources/images/Game/white_7.png" },
				"8": { width: 62 * MULT_TEXT_SCALE, image: "resources/images/Game/white_8.png" },
				"9": { width: 67 * MULT_TEXT_SCALE, image: "resources/images/Game/white_9.png" },
				"x": { width: 67 * MULT_TEXT_SCALE, image: "resources/images/Game/white_x.png" }
			},
			textAlign: 'center',
		});

		this.pEngine = new ParticleEngine({
			parent: opts.parent,
			x: 0,
			y: 0,
			anchorX: 1,
			anchorY: 1,
			width: 2,
			height: 2,
			initCount: SPARKLE_COUNT,
			initImage: IMG_BERRY_SPARKLE
		});

		this.reset();
	};

	this.reset = function() {
		this.lastStreakLevel = 0;
		this.berryMeterText.setText("0");
		this.berryMeterText.style.x = (BERRY_METER_FRAME_WIDTH - BERRY_METER_WIDTH) / 2 - BERRY_OFFSET_X + BERRY_TEXT_OFFSET_X - 6;
	};

	this.update = function(model) {
		var streak = ~~model.berryStreak;
		this.berryMeterText.setText(streak);

		var streakLevel = ~~(streak / BERRY_STREAK_LEVEL);
		if (this.lastStreakLevel < streakLevel) {
			var data = this.pEngine.obtainParticleArray(SPARKLE_COUNT),
				random = Math.random;

			for (var i = 0; i < SPARKLE_COUNT; i++) {
				var width = random() * 10 + 20,
					height = width,
					ttl = 3000,
					pObj = data[i];

				pObj.image = IMG_BERRY_SPARKLE;
				pObj.x = BERRY_METER_WIDTH / 2;
				pObj.y = BERRY_METER_HEIGHT / 2;
				pObj.dx = random() * 1200 - 400;
				pObj.dy = random() * 1200 - 400;
				pObj.ddy = 800;
				pObj.dr = 60 * random() - 30;
				pObj.anchorX = width / 2;
				pObj.anchorY = height / 2;
				pObj.width = width;
				pObj.height = height;
				pObj.dscale = random() * 4 - 2;
				pObj.ttl = ttl;
			}

			this.pEngine.emitParticles(data);

			animate(this).now({ scale: 1.5 }, 400, animate.easeOut)
			.then({ scale: 1 }, 400, animate.easeIn);
		} else if (model.berrySize > model.lastBerrySize) {
			animate(this).now({ scale: 1.2 }, 250, animate.easeOut)
			.then({ scale: 1 }, 250, animate.easeIn);
		}

		this.lastStreakLevel = streakLevel;
	};

	this.tick = function(dt) {
		this.pEngine.runTick(dt);
	};
});
