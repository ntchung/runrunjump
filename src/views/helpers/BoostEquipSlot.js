import animate;

import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;
import src.lib.ButtonView as ButtonView;

import src.controllers.Controller as Controller;

import src.lib.NineSliceImageView as NineSliceImageView;

exports = Class(View, function(supr) {

	var COUNT_WIDTH = 60,
		COUNT_HEIGHT = 40,
		PLUS_WIDTH = 50,
		PLUS_HEIGHT = 50,
		config;

	this.init = function(opts) {

		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;

		this.designView();

		opts.item && this.setBoost(opts.item);
	};

	this.setBoost = function(itemData) {
		this.item = itemData;

		this.refresh();
	};

	this.purchaseActiveItem = function(){
		var item = this.item;


		if( this.controller.productController.purchaseUpgrade(this.controller.upgradesConfig[item.id]) ){
			this.refresh();
		}
	};

	this.refresh = function() {
		var purchased = this.controller.getData("upgradesPurchased");

		var owned = (purchased[this.item.id] && purchased[this.item.id].level) || 0;

		this.itemView.setImage(this.item.icon);
		this.count.setText(owned);

		if( this.item.id === 'angelFish' ){
			this.cost.setText(3);
		}
		else{
			this.cost.setText(this.item.levels[0].cost);
		}

		if( this.item.currency === 'gems' ){
			this.berryIcon.setImage('resources/images/Header/icon_gem.png');
		}
		else{
			this.berryIcon.setImage('resources/images/MainMenu/iconBerries.png');
		}
	}

	this.designView = function() {

		this.bg = new NineSliceImageView({
			parent: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			image: "resources/images/Modals/box_section"
		});
		this.bg.onInputSelect = bind(this, this.purchaseActiveItem);

		this.itemView = new ImageView({
			parent: this.bg,
			x: (this.bg.style.width - 110) / 2,
			y: 15,
			width: 110,
			height: 110,
			image: undefined,
			canHandleEvents: false
		});

		this.berryIcon = new ImageView({
			parent: this.bg,
			x: 40,
			y: this.itemView.style.y + this.itemView.style.height - 15,
			width: 40,
			height: 47,
			image: 'resources/images/MainMenu/iconBerries.png',
			canHandleEvents: false
		});

		this.cost = new TextView({
			parent: this.bg,
			x: this.berryIcon.style.x + this.berryIcon.style.width + 10,
			y: this.berryIcon.style.y,
			width: this.bg.style.width - (this.berryIcon.style.x + this.berryIcon.style.width),
			height: this.berryIcon.style.height,
			image: 'resources/images/MainMenu/iconBerries.png',
			text: '',
			textAlign: 'left',
			size: config.fonts.modalEquipCount,
			color: "rgb(255, 255, 255)",
			strokeColor: "rgb(0, 0, 0)",
			clip: false,
			multiline: false,
			canHandleEvents: false
		});

		this.count = new TextView({
			parent: this.bg,
			x: 15,
			y: this.berryIcon.style.y + this.berryIcon.style.height + 3,
			width: COUNT_WIDTH,
			height: COUNT_HEIGHT,
			text: '',
			textAlign: 'right',
			size: config.fonts.modalEquipCount,
			color: "rgb(255, 255, 255)",
			strokeColor: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.plus = new ButtonView({
			parent: this.bg,
			x: this.count.style.x + this.count.style.width + 25,
			y: this.count.style.y + (this.count.style.height - PLUS_HEIGHT) / 2 + 5,
			width: PLUS_WIDTH,
			height: PLUS_HEIGHT,
			image: "resources/images/Header/button_addClean_up.png",
			imagePressed: "resources/images/Header/button_addClean_down.png"
		});
	};
});
