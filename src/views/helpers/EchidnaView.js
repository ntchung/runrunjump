import ui.SpriteView as Sprite;

import src.controllers.Controller as Controller;

var ANIM_STATES = {
	DEAD: "DEAD",
	IDLE: "IDLE",
	QUILLED: "QUILLED"
};

exports = Class(Sprite, function(supr) {

/* ~ ~ ~ CLASS CONSTANTS / GLOBALS ~ ~ ~ */

	var controller = Controller.get(),
		config = controller.config,
		soundConfig = controller.soundConfig,
		model,

		// animation names
		ANIM_NAME_ATTACK = "attack",
		ANIM_NAME_DEATH = "death",
		ANIM_NAME_IDLE = "idle",

		// image names
		QUILL_FRAME = "resources/images/Enemies/Echidna/echidna_attack_0013.png",
		SOUND_NAME_QUILLS = "echidna_quills",

		// various constants
		BG_WIDTH = 1280,
		ECHIDNA_WIDTH = 150,
		ECHIDNA_HEIGHT = 150,
		OFFSET_Y = 10;

/* ~ ~ ~ INITIALIZATION / RESET ~ ~ ~ */

	this.init = function(opts) {
		opts.anchorX = ECHIDNA_WIDTH / 2;
		opts.anchorY = ECHIDNA_HEIGHT / 2;
		opts.width = ECHIDNA_WIDTH;
		opts.height = ECHIDNA_HEIGHT;
		opts.canHandleEvents = false;
		opts.defaultAnimation = ANIM_NAME_IDLE;
		opts.loop = false;
		opts.autoStart = false;
		supr(this, 'init', [opts]);

		this.boundStartNextAnim = bind(this, "startNextAnimation");
		this.boundHoldQuill = bind(this, "holdQuill");

		this.resetView(opts);
	};

	this.resetView = function(opts) {
		this.resetAllAnimations(opts);

		this.style.r = 0;
		this.style.anchorX = ECHIDNA_WIDTH / 2;
		this.style.anchorY = ECHIDNA_HEIGHT / 2;
		this.style.width = ECHIDNA_WIDTH;
		this.style.height = ECHIDNA_HEIGHT;

		model = controller.getGameModel();

		// animation state flags
		this.animState = ANIM_STATES.IDLE;

		this.resume();
		this.startAnimation("idle", { iterations: 1, callback: this.boundStartNextAnim });
		this.pause();
	};

/* ~ ~ ~ ANIMATION ~ ~ ~ */

	this.startNextAnimation = function() {
		if (!model.echidnaSpawn) { return; }
		if (this.animState === ANIM_STATES.IDLE) { // default logic
			this.startAnimation(ANIM_NAME_IDLE, { iterations: 1, callback: this.boundStartNextAnim });
		}
	};

	this.pause = function() {
		this.isPaused = true;
		supr(this, 'pause', arguments);
		this.style.visible = true;
	};

	this.resume = function() {
		this.isPaused = false;
		supr(this, 'resume', arguments);
		this.style.visible = true;
	};

/* ~ ~ ~ ECHIDNA STATES ~ ~ ~ */

	this.setDead = function(dead) {
		this.animState = ANIM_STATES.DEAD;
		this.resume();
		this.startAnimation(ANIM_NAME_DEATH, { iterations: 1 });
		this.pause();
	};

	this.setIdle = function(idle) {
		this.animState = ANIM_STATES.IDLE;
		this.resume();
		this.startAnimation(ANIM_NAME_IDLE, { iterations: 1, callback: this.boundStartNextAnim });
	};

	this.setQuilled = function(quilled) {
		this.animState = ANIM_STATES.QUILLED;
		this.resume();
		this.startAnimation(ANIM_NAME_ATTACK, { iterations: 1, callback: this.boundHoldQuill });
	};

	this.holdQuill = function() {
		if (!model.echidnaSpawn) { return; }
		controller.playSound(SOUND_NAME_QUILLS);
		this.setImage(QUILL_FRAME);
		this.pause();
	};

/* ~ ~ ~ TICK / UPDATING ~ ~ ~ */

	this.updateView = function(dt, cameraOffset) {
		var player = model.player;
		if (model.gameoverCountdown >= 0 || model.finished) {
			return;
		}

		var echidnaSpawn = model.echidnaSpawn;
		this.style.x = echidnaSpawn.x - (player.x - player.screenX) + (echidnaSpawn.width - ECHIDNA_WIDTH) / 2;
		this.style.y = OFFSET_Y + (echidnaSpawn.y + echidnaSpawn.height) - ECHIDNA_HEIGHT - cameraOffset;

		if (this.style.x < 0.9 * BG_WIDTH && this.animState !== ANIM_STATES.QUILLED) {
			this.setQuilled(true);
		}

		if (model.countdownActive && !this.isPaused) {
			this.pause();
		}
	};
});

exports.ANIM_STATES = ANIM_STATES;