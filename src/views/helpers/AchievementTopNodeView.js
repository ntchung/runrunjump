import ui.View as View;
import ui.ImageView as ImageView;

exports = Class(View, function(supr) {

	var TOP_WIDTH = 1280,
		TOP_HEIGHT = 1280,
		OVERFLOW_SIZE = 50;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.background = new ImageView({
			parent: this,
			x: (this.style.width - TOP_WIDTH) / 2,
			y: 0,
			width: TOP_WIDTH,
			height: TOP_HEIGHT,
			image: "resources/images/Achievements/treeTop.png",
			canHandleEvents: false
		});

		this.overflow = new ImageView({
			parent: this,
			x: (this.style.width - TOP_WIDTH) / 2,
			y: -OVERFLOW_SIZE,
			width: TOP_WIDTH,
			height: OVERFLOW_SIZE,
			image: "resources/images/Achievements/treeOverflowTop.png"
		});
	};
});
