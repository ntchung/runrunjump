import ui.ImageView as ImageView;

exports = Class(ImageView, function(supr) {

	this.init = function(opts) {
		opts.image = "resources/images/Nestables/nodeNestablesBottom.png";
		supr(this, 'init', [opts]);
	};
});
