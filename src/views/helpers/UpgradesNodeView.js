import device;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.controllers.Controller as Controller;

import src.lib.ButtonView as ButtonView;

exports = Class(ImageView, function(supr) {

	var BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		OFFSET_X = 60,
		ICON_WIDTH = 90,
		ICON_HEIGHT = 90,
		SPACING = 20,
		NODE_HEIGHT = 180,
		TITLE_WIDTH = 520,
		TITLE_HEIGHT = 24,
		BAR_WIDTH = 360,
		BAR_HEIGHT = 36,
		BUTTON_WIDTH = 174,
		BUTTON_HEIGHT = 74,
		BERRY_WIDTH = 60,
		BERRY_HEIGHT = 57,
		COST_WIDTH = 220,
		COST_HEIGHT = BERRY_HEIGHT,
		SEC_SPACING = 16,
		SEC_TITLE_WIDTH = 520,
		SEC_TITLE_HEIGHT = 36,
		SECTION_WIDTH = 750,
		SECTION_HEIGHT = 60;

	var TYPE_UPGRADE = "UPGRADE",
		TYPE_AVATAR = "AVATAR",
		TYPE_CONSUMABLE = "CONSUMABLE",
		BERRY_MAGNET_ID = "berryMagnet",
		MEGA_BERRY_ID = "megaBerry",
		TURTLE_FRIEND_ID = "turtleFriend",
		MEGA_GLIDER_ID = "megaGlider",
		SUPER_GLIDER_ID = "superGlider";

	var Z_MODAL = 101;

	var config;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		this.storeView = this.controller.getParentScreenView(this);

		config = this.controller.config;

		this.node1 = "resources/images/Store/nodeLight.png";
		this.node2 = "resources/images/Store/nodeDark.png";

		this.txtInfo = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - TITLE_WIDTH) / 2,
			y: 4,
			width: TITLE_WIDTH,
			height: 40,
			size: config.fonts.storeInfo,
			color: config.colors.storeInfo,
			text: "",
			horizontalAlign: "center",
			wrap: false,
			canHandleEvents: false
		});

		this.barEmpty = new ImageView({
			parent: this,
			x: OFFSET_X + (this.style.width - BAR_WIDTH) / 2,
			y: 1.5 * SPACING + TITLE_HEIGHT,
			width: BAR_WIDTH,
			height: BAR_HEIGHT,
			image: "resources/images/Upgrades/barEmpty.png",
			canHandleEvents: false
		});

		this.barClipper = new View({
			parent: this.barEmpty,
			x: 0,
			y: 0,
			width: BAR_WIDTH,
			height: BAR_HEIGHT,
			clip: true,
			canHandleEvents: false
		});

		this.barFull = new ImageView({
			parent: this.barClipper,
			x: 0,
			y: 0,
			width: BAR_WIDTH,
			height: BAR_HEIGHT,
			image: "resources/images/Upgrades/barFull.png",
			canHandleEvents: false
		});

		this.icon = new ImageView({
			parent: this,
			x: OFFSET_X,
			y: (NODE_HEIGHT - ICON_HEIGHT) / 2,
			anchorX: ICON_WIDTH / 2,
			anchorY: ICON_HEIGHT / 2,
			width: ICON_WIDTH,
			height: ICON_HEIGHT,
			scale: 1.5,
			image: "resources/images/Upgrades/iconMegaBerry.png", // placeholder
			canHandleEvents: false
		});
		this.txtIcon = new TextView({
			parent: this,
			x: OFFSET_X - ICON_WIDTH / 2,
			y: (NODE_HEIGHT - ICON_HEIGHT) / 2 + ICON_HEIGHT,
			width: ICON_WIDTH * 2,
			height: 40,
			text: "",
			horizontalAlign: 'center',
			size: 36,
			color: config.colors.storeSectionFill,
			strokeColor: config.colors.storeSectionStroke,
			wrap: false,
			autoFontSize: false,
			canHandleEvents: false
		});

		this.txtOwn = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - TITLE_WIDTH) / 2 + TITLE_WIDTH - BUTTON_WIDTH - COST_WIDTH / 2,
			y: 2 * SPACING + TITLE_HEIGHT + BAR_HEIGHT + SPACING / 4,
			width: COST_WIDTH / 2,
			height: COST_HEIGHT,
			size: config.fonts.storeOwned,
			color: config.colors.storeCost,
			text: GC.app.l.translate("Own") + ": ",
			horizontalAlign: "center",
			wrap: false,
			autoFontSize: false,
			canHandleEvents: false
		});

		this.btnUpgrade = new ButtonView({
			parent: this,
			x: OFFSET_X + (this.style.width - TITLE_WIDTH) / 2 + TITLE_WIDTH - BUTTON_WIDTH,
			y: 2 * SPACING + TITLE_HEIGHT + BAR_HEIGHT,
			width: BUTTON_WIDTH,
			height: BUTTON_HEIGHT,
			image: "resources/images/Store/btnBuy.png",
			imagePressed: "resources/images/Store/btnBuyPressed.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'purchaseUpgrade'),
			pressedOffsetY: 2
		});
		this.txtUpgrade = new TextView({
			parent: this.btnUpgrade,
			x: 25,
			y: 0,
			width: BUTTON_WIDTH - 50,
			height: BUTTON_HEIGHT,
			size: config.fonts.storeButton,
			color: config.colors.storeButton,
			text: GC.app.l.translate("UPGRADE"),
			horizontalAlign: "center",
			wrap: false,
			canHandleEvents: false
		});

		this.berryIcon = new ImageView({
			parent: this,
			x: OFFSET_X + (this.style.width - TITLE_WIDTH) / 2 + 2 * SPACING,
			y: 2 * SPACING + TITLE_HEIGHT + BAR_HEIGHT,
			width: BERRY_WIDTH,
			height: BERRY_HEIGHT,
			image: "resources/images/Upgrades/iconBerry.png"
		});
		this.txtCost = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - TITLE_WIDTH) / 2 + BERRY_WIDTH + 2 * SPACING,
			y: 2 * SPACING + TITLE_HEIGHT + BAR_HEIGHT + SPACING / 4,
			width: COST_WIDTH,
			height: COST_HEIGHT,
			size: config.fonts.storeCost,
			color: config.colors.storeCost,
			text: "",
			horizontalAlign: "left",
			wrap: false,
			autoFontSize: false,
			canHandleEvents: false
		});

		var titleWidth = this.style.width - SEC_SPACING * 2;
		this.txtTitle = new TextView({
			parent: this,
			x: (this.style.width - titleWidth) / 2,
			y: SEC_SPACING,
			width: titleWidth,
			height: SEC_TITLE_HEIGHT,
			size: config.fonts.storeSectionSmall,
			color: config.colors.storeSectionFill,
			strokeColor: config.colors.storeSectionStroke,
			text: "",
			horizontalAlign: "center",
			wrap: true,
			autoFontSize: false,
			canHandleEvents: false,
			visible: false
		});
	};

	this.purchaseUpgrade = function() {
		if( !this.controller.productController.purchaseUpgrade(this.data) ){
			this.controller.subscribeOnce('lastModalClose', this.storeView.onModalClose);
		}

		// update nodes
		this.storeView.updateUpgradeNodes();
		this.storeView.berryCounter.update();
	};

	this.updateData = function(data) {
		// grab our data
		this.data = data;
		this._uid = data.uid;
		this.index = data.index;
		this.name = data.name;
		this.type = data.type;
		this.levels = data.levels;
		this.productID = data.productID;
		this.money = false;

		this.txtInfo._opts.font = config.fonts.storeInfo;

		if (data.sectionHeader) {
			this.title = data.title;
			this.data.height = SECTION_HEIGHT;
			this.style.height = SECTION_HEIGHT;

			// show header views, hide upgrade views
			this.txtTitle.style.visible = true;
			this.txtInfo.style.visible = false;
			this.barEmpty.style.visible = false;
			this.barClipper.style.visible = false;
			this.barFull.style.visible = false;
			this.icon.style.visible = false;
			this.txtIcon.style.visible = false;
			this.txtOwn.style.visible = false;
			this.btnUpgrade.style.visible = false;
			this.txtUpgrade.style.visible = false;
			this.berryIcon.style.visible = false;
			this.txtCost.style.visible = false;

			this.txtTitle.setText(GC.app.l.translate(this.title));
			this.style.height = SECTION_HEIGHT * (data.size || 1);
		} else {
			this.data.height = NODE_HEIGHT;
			this.style.height = NODE_HEIGHT;

			// show upgrade views, hide header views
			this.txtTitle.style.visible = false;
			this.txtInfo.style.visible = true;
			this.barEmpty.style.visible = true;
			this.barClipper.style.visible = true;
			this.barFull.style.visible = true;
			this.icon.style.visible = true;
			this.txtIcon.style.visible = true;
			this.txtOwn.style.visible = true;
			this.btnUpgrade.style.visible = true;
			this.txtUpgrade.style.visible = true;
			this.berryIcon.style.visible = true;
			this.txtCost.style.visible = true;

			var currentLevel = data.currentLevel;
			// fixes a bug where currentLevel went out of bounds for some users, do not remove
			if (currentLevel > 1
				&& this.type !== TYPE_UPGRADE
				&& this.type !== TYPE_CONSUMABLE)
			{
				currentLevel = data.currentLevel = 1;
			}

			var nextLevel = currentLevel + 1;
			var totalLevels = this.levels.length;
			this.txtIcon.style.visible = false;

			if (this.type == TYPE_UPGRADE) {
				this.icon.style.x = OFFSET_X;
				this.barEmpty.style.visible = true;
				this.txtInfo.style.y = 4;
				this.txtUpgrade.setText(GC.app.l.translate("UPGRADE"));
				this.txtOwn.style.visible = false;
				this.txtIcon.style.visible = true;
				this.txtIcon.setText(GC.app.l.translate(data.subtext));

				if (nextLevel >= totalLevels) {
					this.txtInfo.setText(GC.app.l.translate("MAX LEVEL!"));
					this.txtCost.setText("--");
					this.barClipper.style.width = BAR_WIDTH;
				} else {
					var nextLevelData = this.levels[nextLevel];
					var pct = nextLevel / totalLevels;

					this.berryCost = nextLevelData.cost;
					if( nextLevelData.insert ){
						this.txtInfo.setText(GC.app.l.translate(nextLevelData.text, nextLevelData.insert));
					}
					else{
						this.txtInfo.setText(GC.app.l.translate(nextLevelData.text));
					}
					this.txtCost.setText(this.berryCost);
					this.barClipper.style.width = ~~(pct * BAR_WIDTH);
				}
			} else if (this.type == TYPE_AVATAR) {
				this.icon.style.x = OFFSET_X + SPACING;
				this.barEmpty.style.visible = false;
				this.txtInfo.style.y = (NODE_HEIGHT - 40) / 2 - SPACING;
				this.txtUpgrade.setText(GC.app.l.translate("BUY"));
				this.txtOwn.style.visible = false;

				// avatars currently don't have more than one level
				var levelData = this.levels[currentLevel];

				if (currentLevel >= totalLevels) {
					levelData = this.levels[currentLevel - 1];
					this.money = levelData.money;
					this.txtInfo.setText(GC.app.l.translate("PURCHASED!"));
					this.txtCost.setText("--");
				} else {
					this.money = levelData.money;
					this.berryCost = levelData.cost;
					if( levelData.insert ){
						this.txtInfo.setText(GC.app.l.translate(levelData.text, levelData.insert));
					}
					else{
						this.txtInfo.setText(GC.app.l.translate(levelData.text));
					}
					this.txtCost.setText(this.berryCost);
				}
			} else if (this.type == TYPE_CONSUMABLE) {
				this.icon.style.x = OFFSET_X;
				this.barEmpty.style.visible = false;
				this.txtInfo.style.y = (NODE_HEIGHT - 40) / 2 - SPACING;
				this.txtUpgrade.setText(GC.app.l.translate("BUY"));
				this.txtOwn.style.visible = true;
				this.txtOwn.setText(GC.app.l.translate("Own") + ": " + currentLevel);

				// consumables currently don't have more than one level
				var levelData = this.levels[0];

				if( this._uid === 'angelFish' ){
					this.berryCost = 3;//this.controller.checkExperiment('angelFishPrice');
				}
				else{
					this.berryCost = levelData.cost;
				}

				this.txtInfo.setText(GC.app.l.translate(levelData.text));
				this.txtCost.setText(this.berryCost);
			}

			if (this.money) {
				this.berryIcon.setImage("resources/images/Store/iconDollar.png");
			}
			else if( data.currency && data.currency === 'gems' ){
				this.berryIcon.setImage('resources/images/Header/icon_gem.png');
			}
			else {
				this.berryIcon.setImage("resources/images/Upgrades/iconBerry.png");
			}

			this.icon.style.y = (NODE_HEIGHT - data.iconHeight) / 2
			this.icon.style.anchorX = data.iconWidth / 2;
			this.icon.style.anchorY = data.iconHeight / 2;
			this.icon.style.width = data.iconWidth;
			this.icon.style.height = data.iconHeight;
			this.icon.setImage(data.icon);
		}

		// update background node color
		this.setImage(data.index % 2 ? this.node1 : this.node2);
	};
});
