import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	var config,
		upgradeConfig;

	this.init = function(opts) {

		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;
		upgradeConfig = this.controller.upgradeConfig;

		this.slotIndex = opts.slotIndex || undefined;

		this.isUnpurchasedSlot = false;

		this.designView();
	};

	this.setKiwi = function(kiwiData) {
		if( kiwiData === 'empty' ){
			this.kiwi.style.visible =false;
			this.rankBadge.style.visible = false;
			this.name.style.visible = false;
			this.lock.style.visible = true;
			this.style.opacity = 0.5;
		}
		else{
			var playerLevel = this.controller.getData('level');

			this.activeKiwi = kiwiData;
			this.style.opacity = 1.0;
			if( this.activeKiwi ){
				this.kiwi.style.visible =true;

				this.kiwi.style.x = kiwiData ? (this.style.width - kiwiData.width) / 2 - 5 : 0;
				this.kiwi.style.y = kiwiData ? (this.style.height - kiwiData.height) / 2 : 0;
				this.kiwi.style.width = kiwiData ? kiwiData.width : this.style.width;
				this.kiwi.style.height = kiwiData ? kiwiData.height : this.style.height;
				this.kiwi.setImage( kiwiData ? kiwiData.selectionIcon : undefined );

				this.rankText.setText( kiwiData.unlock );
				this.name.setText( GC.app.l.translate(kiwiData.name) );

				if( this.activeKiwi.lockedPromo ){
					var unlockedKiwis = this.controller.getData("babies"),
						isUnlocked = unlockedKiwis.indexOf(this.activeKiwi.id) !== -1;
						this.rankBadge.style.visible = false;
						if( isUnlocked ){
							this.lock.style.visible = false;
						}
						else{
							this.lock.style.visible = true;
						}
				}
				else if( playerLevel >= kiwiData.unlock ){
					this.rankBadge.style.visible = false;
					this.lock.style.visible = false;
					this.name.style.visible = true;
				}
				else if( kiwiData.cost !== undefined  ){
					this.rankBadge.style.visible = true;
					this.lock.style.visible = true;
					this.name.style.visible = false;
				}
				else{
					this.rankBadge.style.visible = false;
					this.lock.style.visible = false;
					this.name.style.visible = false;
				}
			}
			else{
				this.kiwi.style.visible =false;
				if( this.isUnpurchasedSlot ){
					this.rankBadge.setImage('resources/images/Modals/counter_gems.png');
					this.rankText.setText(this.price);
				}
				else{
					this.rankBadge.style.visible = false;
					this.lock.style.visible = false;
					this.name.style.visible = false;
				}
			}
		}
	};

	this.setPurchasedState = function(state) {
		this.isUnpurchasedSlot = !state;
	};

	this.setPrice = function(price) {
		this.price = price;
	};

	this.clearKiwi = function() {
		this.activeKiwi = undefined;
		this.kiwi.style.visible =false;
	};

	this.designView = function() {

		this.bg = new ImageView({
			parent: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			image: "resources/images/Modals/button_babyShopItem.png"
		});

		this.kiwi = new ImageView({
			parent: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			image: undefined,
			canHandleEvents: false,
			visible: false
		});

		this.lock = new ImageView({
			parent: this,
			x: this.style.width * 0.35,
			y: this.style.height * 0.13,
			width: this.style.width * 0.3,
			height: this.style.height * 0.444,
			image: 'resources/images/Modals/icon_lock.png'
		});

		this.rankBadge = new ImageView({
			parent: this,
			x: this.style.width * 0.25,
			y: this.style.height * 0.5,
			width: this.style.width * 0.5,
			height: this.style.height * 0.33,
			image: 'resources/images/Modals/counter_xp.png'
		});

		this.rankText = new TextView({
			superview: this.rankBadge,
			x: 50,
			y: 0,
			width: 45,
			height: this.rankBadge.style.height,
			text: '00',
			size: 28,
			color: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});

		this.name = new TextView({
			superview: this,
			x: 0,
			y: this.style.height * 0.56,
			width: this.style.width,
			height: this.style.height * 0.33,
			text: '',
			size: config.fonts.kiwiSlotName,
			color: "rgb(255, 255, 255)",
			strokeColor: "rgb(0, 0, 0)",
			multiline: false,
			canHandleEvents: false
		});
	};
});
