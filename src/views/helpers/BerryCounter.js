import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ParticleEngine as ParticleEngine;
import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	var OFFSET_Y = 40,
		FRAME_WIDTH = 425,
		FRAME_HEIGHT = 147,
		BERRY_WIDTH = 64,
		BERRY_HEIGHT = 61,
		BERRY_PARTICLE_WIDTH = 120,
		BERRY_PARTICLE_HEIGHT = 114,
		COUNT_WIDTH = 275,
		COUNT_HEIGHT = 60,
		COUNT_OFFSET_Y = 45,
		SLIDE_TIME = 500,
		ADD_TIME = 2000,
		PARTICLE_INC = 10,
		PARTICLE_INC_RATE = 2,
		SOUND_MED_THRESHOLD,
		SOUND_LRG_THRESHOLD;

	var SOUND_NAME_IND_BERRY = "berry_count_ind",
		SOUND_NAME_MED_BERRIES = "berry_count_med",
		SOUND_NAME_LRG_BERRIES = "berry_count_large";

	this.init = function(opts) {
		opts.blockEvents = true;
		opts.canHandleEvents = false;
		opts.x = 0;
		opts.y = opts.parent.style.height;
		opts.width = FRAME_WIDTH;
		opts.height = FRAME_HEIGHT;

		if (opts.align == 'center') {
			opts.x = (opts.parent.style.width - FRAME_WIDTH) / 2;
		}

		this.upY = opts.parent.style.height - FRAME_HEIGHT + OFFSET_Y;
		this.downY = opts.parent.style.height;

		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		var config = this.controller.config;

		SOUND_MED_THRESHOLD = this.controller.soundConfig.logic.berryCountMediumThreshold;
		SOUND_LRG_THRESHOLD = this.controller.soundConfig.logic.berryCountLargeThreshold;

		this.currentBerries = this.controller.getData("berries");

		this.particleEngine = new ParticleEngine({
			parent: this,
			x: (this.style.width - 2) / 2,
			y: (this.style.height - 2) / 2,
			anchorX: 1,
			anchorY: 1,
			width: 2,
			height: 2,
			initCount: 165,
			initImage: "resources/images/Upgrades/iconBerry.png"
		});

		this.background = new ImageView({
			parent: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			image: "resources/images/Store/berryCounter.png"
		});

		this.txtBerries = new TextView({
			parent: this,
			x: (this.style.width - COUNT_WIDTH) / 2,
			y: COUNT_OFFSET_Y,
			width: COUNT_WIDTH,
			height: COUNT_HEIGHT,
			size: config.fonts.berryCounter,
			color: config.colors.berryCounter,
			text: this.currentBerries,
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		// berries are added to this layer once they are falling, so match particle engine
		this.berryTopLayer = new View({
			parent: this,
			x: (this.style.width - 2) / 2,
			y: (this.style.height - 2) / 2,
			anchorX: 1,
			anchorY: 1,
			width: 2,
			height: 2
		});

		// berry particle onObtain
		var zIndex = 1000;
		this.boundBerryStart = bind(this, function(particle) {
			if (this.particleCount <= SOUND_MED_THRESHOLD) {
				this.controller.playSound(SOUND_NAME_IND_BERRY);
			}

			// set zIndex so that particles switch layers correctly
			particle.style.zIndex = zIndex--;
		});
		
		// particle 3D-sim
		this.boundBerryFront = bind(this, function(particle) {
			// if the particle is falling and not already on top
			particle.removeFromSuperview();
			this.berryTopLayer.addSubview(particle);
		});

		this.boundBerryBounce = bind(this, function(particle) {
			particle.pData.dy = -0.55 * particle.pData.dy;
		});

		// stop moving / rolling
		this.boundBerryStop = bind(this, function(particle) {
			var pd = particle.pData;
			pd.dx = pd.ddx = pd.dr = pd.ddr = 0;
		});

		// clean up after the first binding ^
		this.boundBerryDeathManager = bind(this, function(particle, data) {
			particle.removeFromSuperview();
			this.particleEngine.addSubview(particle);
		});
	};

	this.addBerries = function(count) {
		if (this.berryAdd) {
			return;
		}

		this.madeSound = false;
		this.berryAdd = count;
		this.finalBerryCount = this.currentBerries + count;

		// increase particles based on amount of berries earned
		var particleCount = 0;
		var inc = PARTICLE_INC;
		while (count > 0) {
			if (count > inc) {
				particleCount += PARTICLE_INC;
				count -= inc;
				inc *= PARTICLE_INC_RATE;
			} else {
				particleCount += ~~(count / (inc / PARTICLE_INC));
				count = 0;
			}
		}

		this.particleCount = particleCount;

		var data = this.particleEngine.obtainParticleArray(particleCount);
		var random = Math.random;
		var floor = Math.floor;
		for (var i = 0; i < particleCount; i++) {
			var width = BERRY_PARTICLE_WIDTH;
			var height = BERRY_PARTICLE_HEIGHT;
			var ttl = 6000;
			var pObj = data[i];

			pObj.image = "resources/images/Upgrades/iconBerry.png";
			pObj.x = random() * 40 - 20 - width / 2;
			pObj.y = -random() * 20 - 20;
			pObj.dx = random() * 640 - 320;
			pObj.dy = -random() * 300 - 600;
			pObj.dr = pObj.dx / 30;
			pObj.ddx = -pObj.dx / 4;
			pObj.ddr = pObj.ddx / 30;
			pObj.ddy = 1600;
			pObj.anchorX = floor(0.5 * width);
			pObj.anchorY = floor(0.55 * height);
			pObj.width = width;
			pObj.height = height;
			pObj.scale = 0.8;
			pObj.dscale = 0.1;
			pObj.ttl = ttl;
			pObj.delay = random() * ADD_TIME;
			pObj.onStart = this.boundBerryStart;
			pObj.onDeath = this.boundBerryDeathManager;
			pObj.triggers.push({
				property: 'y',
				value: 20 + FRAME_HEIGHT / 2 - height + ~~(random() * height / 1.5),
				count: 2,
				action: this.boundBerryBounce
			});
			pObj.triggers.push({
				property: 'y',
				value: -FRAME_HEIGHT,
				smaller: true,
				count: 1,
				action: this.boundBerryFront
			});
			pObj.triggers.push({
				property: 'dx',
				value: 0,
				smaller: pObj.dx > 0,
				count: 1,
				action: this.boundBerryStop
			});
		}

		this.particleEngine.emitParticles(data);
	};

	this.slideUp = function(cb) {
		this.madeSound = false;
		animate(this).now({ y: this.upY }, SLIDE_TIME, animate.easeOut)
		.then(bind(this, function() {
			cb && cb();
		}));
	};

	this.slideDown = function(cb) {
		animate(this).now({ y: this.downY }, SLIDE_TIME, animate.easeIn)
		.then(bind(this, function() {
			cb && cb();
		}));
	};

	this.update = function() {
		this.currentBerries = this.controller.getData("berries");
		this.txtBerries.setText(this.currentBerries);
	};

	// add berry count text up incrementally
	this.tick = function(dt) {
		this.particleEngine.runTick(dt);

		if (this.berryAdd) {
			if (!this.madeSound && this.particleCount > SOUND_MED_THRESHOLD && this.particleCount < SOUND_LRG_THRESHOLD) {
				this.madeSound = true;
				this.controller.playSound(SOUND_NAME_MED_BERRIES);
			} else if (!this.madeSound && this.particleCount >= SOUND_LRG_THRESHOLD) {
				this.madeSound = true;
				this.controller.playSound(SOUND_NAME_LRG_BERRIES);
			}

			// fail safe for infinite berry counting
			if (!this.finalBerryCount || !this.currentBerries) {
				this.update();
				this.berryAdd = 0;
				this.particleCount = 0;
			}

			this.currentBerries += this.berryAdd * dt / ADD_TIME;
			if (this.currentBerries >= this.finalBerryCount) {
				this.update();
				this.berryAdd = 0;
				this.particleCount = 0;
			} else {
				this.txtBerries.setText(~~this.currentBerries);
			}
		}
	};
});
