import ui.SpriteView as SpriteView;
import ui.View as View;
import src.lib.ParticleEngine as ParticleEngine;

import src.controllers.Controller as Controller;

var ANIM_STATES = {
	DEAD: "DEAD",
	IDLE: "IDLE"
};

exports = Class(SpriteView, function(supr) {

/* ~ ~ ~ CLASS CONSTANTS / GLOBALS ~ ~ ~ */

	var controller = Controller.get(),
		config = controller.config,
		soundConfig = controller.soundConfig,
		model,
		// animation names
		ANIM_NAME_DEATH = "death",
		ANIM_NAME_IDLE = "blink",

		// various constants
		BG_WIDTH = 1280,
		POO_WIDTH = 95,
		POO_HEIGHT = 112,
		OFFSET_Y = 17;

/* ~ ~ ~ INITIALIZATION / RESET ~ ~ ~ */

	this.init = function(opts) {
		this.gameView = opts.parentView;
		this.isDead = false;
		this.index = opts.index;

		opts.anchorX = POO_WIDTH / 2;
		opts.anchorY = POO_HEIGHT / 2;
		opts.width = POO_WIDTH;
		opts.height = POO_HEIGHT;
		opts.canHandleEvents = false;
		opts.defaultAnimation = ANIM_NAME_IDLE;
		opts.loop = false;
		opts.autoStart = false;

		supr(this, 'init', [opts]);

		//this.boundStartNextAnim = bind(this, "startNextAnimation");

		this.resetView(opts);
	};

	this.resetView = function(opts) {
		this.resetAllAnimations(opts);

		this.index = opts.index;

		this.style.r = 0;
		this.style.anchorX = POO_WIDTH / 2;
		this.style.anchorY = POO_HEIGHT / 2;
		this.style.width = POO_WIDTH;
		this.style.height = POO_HEIGHT;

		model = controller.getGameModel();
		// animation state flags
		this.animState = ANIM_STATES.IDLE;
		this.didCollide = false;
		this.isDead = false;
		this.activeAnims = 0;
	};



/* ~ ~ ~ ANIMATION ~ ~ ~ */

	this.pause = function() {
		this.isPaused = true;
		supr(this, 'pause', arguments);
		this.style.visible = true;
	};

	this.resume = function() {
		this.isPaused = false;
		supr(this, 'resume', arguments);
		this.style.visible = true;
	};


/* ~ ~ ~ Poo STATES ~ ~ ~ */

	this.setAlive = function() {
		this.animState = ANIM_STATES.IDLE;
		this.resume();
		this.startAnimation(ANIM_NAME_IDLE, {loop: true, randomFrame: true});
	}

	this.setDead = function() {
		this.animState = ANIM_STATES.DEAD;
		this.startAnimation(ANIM_NAME_DEATH, {loop: true});
		this.isDead = true;
	};

/* ~ ~ ~ TICK / UPDATING ~ ~ ~ */

	this.updateView = function(dt, cameraOffset) {
		var player = model.player;
		if (model.gameoverCountdown >= 0 || model.finished) {
			return;
		}

		var pooSpawn = model.pooSpawn;

		this.style.x = pooSpawn.x + (pooSpawn.width/pooSpawn.count * this.index) - (model.player.x - model.player.screenX);
		//logger.log(pooSpawn.x);
		this.style.y = OFFSET_Y + (pooSpawn.y + pooSpawn.height) - POO_HEIGHT - cameraOffset;

		if (model.countdownActive && !this.isPaused) {
			this.pause();
		}
	};
});

exports.ANIM_STATES = ANIM_STATES;