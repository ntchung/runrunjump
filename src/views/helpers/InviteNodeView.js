import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;
import src.views.modals.CheckAcceptModalView as CheckAcceptModalView;

exports = Class(View, function(supr) {

	var
		PLANK_EVEN = "resources/images/Leaderboard/leaderboard_contacts_a.png",
		PLANK_ODD = "resources/images/Leaderboard/leaderboard_contacts_b.png",
		WIDTH = 616,
		HEIGHT = 124,
		PADDING_RIGHT = 80,
		NAME_WIDTH = 330,
		PIC_DEFAULT = "resources/images/Leaderboard/leaderboard_pic_unknown.png",
		PIC_X = 86,
		PIC_Y = 25,
		PIC_WIDTH = 76,
		PIC_HEIGHT = 76,
		STAMP_SPACING = 10,
		STAMP_Y = 22,
		STAMP_WIDTH = 80,
		STAMP_HEIGHT = 80,

		config;
	

	this.init = function(opts) {
		supr(this, 'init', [opts]);	

		this.controller = GC.app.controller;
		config = this.controller.config;

		this.leaderboardView = this.controller.getParentScreenView(this);

		this.designView();
	};

	this.designView = function() {
		this.defaultPhoto = new ImageView({
			parent: this,
			image: PIC_DEFAULT,
			x: PIC_X,
			y: PIC_Y,
			width: PIC_WIDTH,
			height: PIC_HEIGHT,
			canHandleEvents: false
		});

		this.contactPhoto = new ImageView({
			parent: this,
			x: PIC_X,
			y: PIC_Y,
			width: PIC_WIDTH,
			height: PIC_HEIGHT,
			canHandleEvents: false
		});

		this.background = new ImageView({
			parent: this,
			x: 0,
			y: 0,
			width: WIDTH,
			height: HEIGHT
		});
		this.background.onInputStart = bind(this, function() {
			this.inputStarted = true;
		});
		this.background.onInputSelect = bind(this, function() {
			if (this.inputStarted) {
				this.controller.playSound('ui_click');
				this.inviteUser();
			}
		});

		this.checkmark = new ImageView({
			parent: this,
			image: "resources/images/Leaderboard/leaderboard_check.png",
			x: PIC_X + PIC_WIDTH + STAMP_SPACING,
			y: STAMP_Y,
			width: STAMP_WIDTH,
			height: STAMP_HEIGHT,
			visible: false
		});

		this.nameTxt = new TextView({
			parent: this,
			x: WIDTH - PADDING_RIGHT - NAME_WIDTH,
			y: 0,
			width: NAME_WIDTH,
			height: HEIGHT,
			size: 42,
			fontFamily: "Patrick",
			color: config.colors.leaderboardRank,
			textAlign: "right",
			multiline: false,
			canHandleEvents: false
		});
	};

	this.inviteUser = function() {
		if (!this.controller.getData("hideInviteModal")) {
			new CheckAcceptModalView({
				parent: this.leaderboardView,
				message: "Send invite to " + this.data.name + "? Check to always hide this message.",
				accept: bind(this, this.completeInvite)
			});	
		} else {
			this.completeInvite();
		}
	};

	this.completeInvite = function() {
		this.controller.fb.sendRequests(
			[this.data.uid],
			"Check Kiwi Run out, it's great!"
		);

		this.checkmark.style.visible = true;

		var sentInvites = this.controller.getData("sentInvites") || {};
		sentInvites[this.data.uid] = true;
		this.controller.setData("sentInvites", sentInvites);
	};

	this.updateData = function(data) {
		var sentInvites = this.controller.getData("sentInvites") || {},
			that = this;

		this.data = data;
		this.inputStarted = false;

		if (data.index % 2 === 0) {
			this.background.setImage(PLANK_EVEN);
		} else {
			this.background.setImage(PLANK_ODD);
		}

		if (this.textTimeout) {
			clearTimeout(this.textTimeout);
		}
		this.textTimeout = setTimeout(function() {
			that.nameTxt._opts.fontSize = 42;
			that.nameTxt.setText(data.name);
		}, 100);

		if (this.picTimeout) {
			clearTimeout(this.picTimeout);
		}
		this.contactPhoto.style.visible = false;
		this.picTimeout = setTimeout(function() {
			that.contactPhoto.style.visible = true;
			data.pic_small && that.contactPhoto.setImage(data.pic_small);
		}, 1000);

		if (sentInvites[data.uid]) {
			this.checkmark.style.visible = true;
		} else {
			this.checkmark.style.visible = false;
		}
	};
});
