import src.controllers.Controller as Controller;

import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;
import src.lib.LocaleManager as LocaleManager;
import src.views.modals.BasicModalView as BasicModalView;

var lastPurchaseTime = 0;

exports = Class(View, function(supr) {
	var config,
		productConfig,
		iconWidth,
		iconHeight,
		headerWidthDefault,
		headerWidthDeal;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;
		productConfig = this.controller.modalStoreConfig;

		this.buyActiveProduct = bind(this, this.buyActiveProduct);

		this.designView();
	}

	this.setProduct = function(category, item){
		this.product = productConfig.products[category].items[item];
		this.refresh();
	};

	this.buyActiveProduct = function() {
		if (Date.now() - (lastPurchaseTime || 0) >= 4000
			|| this.product.currency !== "bucks"
			|| GC.app.enableWebMode)
		{
			if (this.product) {
				this.controller.productController.purchaseModalStoreItem(this.product);
				this.controller.header.refresh();
			}

			lastPurchaseTime = Date.now();
		}
	};

	this.refresh = function(){
		if( this.product ){
			var text, visible, headerText;
			if (this.product.amount === -1) {
				headerText = GC.app.l.translate(this.product.header);
			} else {
				headerText = this.product.amount + " " + GC.app.l.translate(this.product.header)
			}

			this.header.setText(headerText);
			this.item.setImage(this.product.icon);

			text = this.product.cost;

			var currIcon = productConfig.currencies[this.product.currency].icon;
			if (currIcon === "") {
				this.cost.style.x = this.costIcon.style.x + 25;
				this.cost.style.width = this.buyButton.style.width - 50;
				this.costIcon.style.visible = false;
			} else {
				this.cost.style.x = this.costIcon.style.x + this.costIcon.style.width - 10;
				this.cost.style.width = this.buyButton.style.width - this.costIcon.style.width;
				this.costIcon.style.visible = true;
				this.costIcon.setImage(currIcon);
			}

			this.cost.setText(GC.app.l.translate(this.product.cost));


			if( this.product.deal ){
				this.dealText.setText(GC.app.l.translate(this.product.deal));
				this.header.style.width = headerWidthDeal;
				this.dealDecor.style.visible = true;
			}
			else{
				this.header.style.width = headerWidthDefault;
				this.dealDecor.style.visible = false;
			}
		}
	};

	this.designView = function(){
		this.productFrame = new ImageView({
			superview: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			image: 'resources/images/Header/frame_dropshadow.png'
		});

		this.innerFrame = new ImageView({
			superview: this.productFrame,
			x: 3,
			y: 3,
			width: this.style.width * 0.83,
			height: this.style.height * 0.83,
			image: 'resources/images/Header/frame_resource.png'
		});

		headerWidthDefault = this.innerFrame.style.width - 10;

		this.header = new TextView({
			superview: this.innerFrame,
			x: 5,
			y: 0,
			width: headerWidthDefault,
			height: this.style.height * 0.18,
			text: GC.app.l.translate((this.product && this.product.header) || 'ITEM'),
			textAlign: 'center',
			lineHeight: 1,
			size: config.fonts.modalStoreHeader,
			color: config.colors.modalDealStroke,
			multiline: false,
			canHandleEvents: false
		});

		iconWidth = this.innerFrame.style.width * 0.82;
		iconHeight = this.innerFrame.style.height * 0.73;

		this.item = new ImageView({
			superview: this.innerFrame,
			x: (this.innerFrame.style.width - iconWidth) * 0.5,
			y: this.header.style.height - 10,
			width: iconWidth,
			height: iconHeight
		});

		this.buyButton = new ButtonView({
			superview: this.productFrame,
			x: this.productFrame.style.width - 147,
			y: this.productFrame.style.height - 91,
			width: 137,
			height: 79,
			image: 'resources/images/Modals/btnGreen.png',
			imagePressed: 'resources/images/Modals/btnGreenPressed.png',
			pressedOffsetY: 2,
			onClick: this.buyActiveProduct
		});

		this.costIcon = new ImageView({
			superview: this.buyButton,
			x: 5,
			y: (this.buyButton.style.height - 80) * 0.5,
			width: 72,
			height: 72,
			canHandleEvents: false
		});

		this.cost = new TextView({
			superview: this.buyButton,
			x: this.costIcon.style.x + this.costIcon.style.width - 10,
			y: 0,
			width: this.buyButton.style.width - this.costIcon.style.width,
			height: this.buyButton.style.height,
			text: GC.app.l.translate((this.product && this.product.header) || 'ITEM'),
			textAlign: 'center',
			size: config.fonts.modalStoreCost,
			color: config.colors.modalStoreCost,
			multiline: false,
			canHandleEvents: false
		});

		this.dealDecor = new ImageView({
			superview: this.productFrame,
			x: this.innerFrame.style.width - 55,
			y: -10,
			width: 75,
			height: 75,
			image: 'resources/images/Header/badge_upsell.png',
			anchorX: 75 * 0.5,
			anchorY: 75 * 0.5
		});

		headerWidthDeal = headerWidthDefault - this.dealDecor.style.width + 20;

		var dealTextOpts = {
			superview: this.dealDecor,
			x: 10,
			y: 0,
			width: this.dealDecor.style.width - 20,
			height: this.dealDecor.style.height - 14,
			text: 'FREE',
			horizontalAlign: 'center',
			verticalAlign: 'middle',
			lineHeight: 0.9,
			size: config.fonts.modalDealBadge,
			color: config.colors.modalDealStroke,
			wrap: true,
			canHandleEvents: false
		};

		// hack work around for now; TextViews don't properly revert to TTF for multiline text
		if (LocaleManager.getLocale() !== 'en') {
			dealTextOpts = {
				superview: this.dealDecor,
				x: 10,
				y: 4,
				width: this.dealDecor.style.width - 20,
				height: this.dealDecor.style.height - 14,
				text: 'FREE',
				horizontalAlign: 'center',
				verticalAlign: 'middle',
				lineHeight: 0.9,
				fontFamily: "Patrick",
				size: 20,
				color: config.colors.modalDealStroke,
				wrap: true,
				canHandleEvents: false
			}
		}

		this.dealText = new TextView(dealTextOpts);
	};
});
