import ui.View as View;
import src.views.helpers.ProductView as ProductView;

exports = Class(View, function(supr) {

	var productWidth,
		productHeight;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.productViews = [];

		productWidth = this.style.width /3;
		productHeight = this.style.height/2;

		this.designView();
	};

	this.setCategory = function(cat){
		this.category = cat;

		for( var i in this.productViews ){
			this.productViews[i].setProduct(cat, i);
		}
	};

	this.designView = function(){
		for(var i = 0; i < 6; i++){
			var product = new ProductView({
				superview: this,
				x: (i%3)*productWidth,
				y: ~~(i/3) * productHeight,
				width: productWidth,
				height: productHeight
			});

			this.productViews[i] = product;
		}
	};


});