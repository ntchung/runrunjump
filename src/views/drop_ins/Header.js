import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import animate;

import src.lib.ParticleEngine as ParticleEngine;
import src.views.helpers.ResourceContainer as ResourceContainer;
import src.lib.NineSliceImageView as NineSliceImageView;

exports = Class(View, function(supr) {
	var HIDE_Y,
		SHOW_Y,
		xpBarHeight,
		xpCapWidth,
		barFillMaxWidth,
		animTime,
		config,
		HEART_RESPAWN_TIME,
		FULL_HEARTS,
		TIMER_VISIBLE_Y,
		TIMER_HIDDEN_Y,
		HEART_COUNTER_SHOWTIME = 10000;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = opts.controller;
		config = this.controller.config;

		HEART_RESPAWN_TIME = config.heartRespawnTime;

		//EXPERIMENT LOGIC
		FULL_HEARTS = config.maxHearts; //this.controller.checkExperiment('maxHearts');

		SHOW_Y = 0;
		HIDE_Y = -this.style.height;

		animTime = 350;
		this.active = false;
		this.showingTimer = false;
		this.dontShowTimer = false;
		this.currentCounterTime = 0;
		this.heartIcon = undefined;

		this.makeInvisible = bind(this, this.makeInvisible);
		this.spewHearts = bind(this, this.spewHearts);
		this.levelUpSparkles = bind(this, this.levelUpSparkles);

		this.designView();
	};

	this.showBerryStore = function(){
		this.controller.closeActiveModal(true,  bind(this.controller, this.controller.showBerryStore));
	};

	this.showGemStore = function(){
		this.controller.closeActiveModal(true, bind(this.controller, this.controller.showGemStore));
	};

	this.showHeartStore =function(){
		this.controller.closeActiveModal(true,  bind(this.controller, this.controller.showHeartStore));
	};

	this.updateHeartData = function(dt) {
		var now = Date.now();
		var hearts = this.controller.getData("hearts");
		var gainedHearts = false;
		var nextHeartAt = this.controller.getData("nextHeartAt") || now + HEART_RESPAWN_TIME;

		// add up hearts earned since last update
		while (hearts < FULL_HEARTS && nextHeartAt < now) {
			hearts += 1;
			nextHeartAt += HEART_RESPAWN_TIME;

			this.currentCounterTime = 0;
			this.showHeartGainAnimation();
		}

		// set respawn timer
		if (hearts >= FULL_HEARTS) {
			nextHeartAt = Number.MAX_VALUE;

			if( this.showingTimer ){
				this.hideHeartTimer();
			}
		}
		else if (nextHeartAt - now > HEART_RESPAWN_TIME) {
			nextHeartAt = now + HEART_RESPAWN_TIME;
		}
		else{
			var timeDiff = nextHeartAt - now,
				mins = ~~(timeDiff/60000),
				secs = ~~((timeDiff%60000)/1000),
				secs = secs < 10 ? '0' + secs : secs;

			if( this.showingTimer){
				this.counterText.setText(mins + ":" + secs);

				if( dt ){
					this.currentCounterTime += dt;

					if( this.currentCounterTime >= HEART_COUNTER_SHOWTIME && hearts !== 0 && !(mins === 0 && secs < 18)){
						this.hideHeartTimer();
					}
				}
			}
			else if( mins === 0 && secs < 15 && !this.showingTimer ){
				this.showHeartTimer();
			}
		}

		this.controller.setData("hearts", hearts, false, false);
		this.controller.setData("nextHeartAt", nextHeartAt, false, false);
	};

	this.tick = function(dt) {
		this.active && this.refreshOnTick(dt);
	};

	this.refresh = function(){
		var lvl = this.controller.getData('level');
		this.levelText.setText(lvl);

		var xp = this.controller.getData('XP'),
			cur = this.controller.getLevelXP(lvl),
			max = this.controller.getLevelXP(lvl+1),
			percent = (xp-cur)/(max-cur);

		this.setXPBar(percent);
	};

	this.refreshOnTick = function(dt){
		this.berryBar.setResourceAmount(this.controller.getData("berries"));
		this.diamondBar.setResourceAmount(this.controller.getData("gems"));

		// update heart counter
		if (this.controller.getData('unlimitedHearts')) {
			this.heartBar.setResourceAmount("∞");
		} else {
			// update heart data first
			this.updateHeartData(dt);
			this.heartBar.setResourceAmount(this.controller.getData("hearts"));
		}

		this.heartSplosion.runTick(dt);
		this.xpCapParts.runTick(dt);
		this.xpSplosion.runTick(dt);
	};

	this.toggleTimer = function(){
		if( this.showingTimer ){
			this.hideHeartTimer();
		}
		else{
			this.showHeartTimer();
		}
	}

	this.showHeartTimer = function(){
		var hearts = this.controller.getData("hearts");
		if( hearts >= FULL_HEARTS || this.dontShowTimer){
			return;
		}

		this.counterWrapper.style.visible = true;
		this.showingTimer = true;
		this.currentCounterTime = 0;

		animate(this.counterWrapper)
		.then({y: TIMER_VISIBLE_Y, opacity: 1}, 500, animate.easeOut);
	};

	this.hideHeartTimer = function(){
		this.showingTimer = false;

		animate(this.counterWrapper)
		.then({y: TIMER_HIDDEN_Y, opacity: 0}, 500, animate.easeOut);
	};

	this.showHeartGainAnimation = function(){
		var that = this;

		animate(this.heartIcon)
		.then({scale: 1.5}, 300, animate.easeOut)
		.then(this.spewHearts)
		.then({scale: 1}, 300, animate.easeOut);
	};

	this.spewHearts = function(){
		var data = this.heartSplosion.obtainParticleArray(25);

		for (var i = 0; i < 25; i++) {
			var pObj = data[i];
			var width = Math.random() * 15 + 30;
			var height = width;
			var ttl = 1500;

			pObj.image = "resources/images/Header/icon_heart.png";
			pObj.x = - width/2;
			pObj.y = - height/2;
			pObj.r = 2 * Math.PI * Math.random();
			pObj.dx = Math.random() * 500 - 250;
			pObj.ddx = - pObj.dx / 1.5;
			pObj.dy = Math.random() * 500 - 250;
			pObj.ddy = -pObj.dy / 1.5;
			pObj.dr = Math.random() * Math.PI;
			pObj.ddr = -Math.random() * Math.PI;
			pObj.anchorX = width / 2 + (Math.random() * width / 4 - width / 8);
			pObj.anchorY = height / 2 + (Math.random() * height / 4 - height / 8);
			pObj.width = width;
			pObj.height = height;
			pObj.ttl = ttl;
			pObj.delay = Math.random() * 500;
			pObj.opacity = 1;
			pObj.dopacity = -1;
		}

		this.heartSplosion.emitParticles(data);
	};

	this.triggerLevelCapParts = function(duration) {
		var data = this.xpCapParts.obtainParticleArray(25);

		for (var i = 0; i < 25; i++) {
			var pObj = data[i];
			var width = Math.random() * 15 + 20;
			var height = width;
			var ttl = duration;

			pObj.image = "resources/images/Game/itemSparkle.png";
			pObj.x = - width/2;
			pObj.y = - height/2;
			pObj.r = 2 * Math.PI * Math.random();
			pObj.dx = Math.random() * 500 - 250;
			pObj.ddx = - pObj.dx / 1.5;
			pObj.dy = Math.random() * 500 - 250;
			pObj.ddy = -pObj.dy / 1.5;
			pObj.dr = Math.random() * Math.PI;
			pObj.ddr = -Math.random() * Math.PI;
			pObj.anchorX = width / 2 + (Math.random() * width / 4 - width / 8);
			pObj.anchorY = height / 2 + (Math.random() * height / 4 - height / 8);
			pObj.width = width;
			pObj.height = height;
			pObj.ttl = ttl * 0.2;
			pObj.delay = Math.random() * ttl;
			pObj.opacity = 1;
			pObj.dopacity = -1;
		}

		this.xpCapParts.emitParticles(data);
	};

	this.levelUpSparkles = function(){
		var data = this.xpSplosion.obtainParticleArray(25);

		for (var i = 0; i < 25; i++) {
			var pObj = data[i];
			var width = Math.random() * 15 + 20;
			var height = width;
			var ttl = 1500;

			pObj.image = "resources/images/Game/itemSparkle.png";
			pObj.x = - width/2;
			pObj.y = - height/2;
			pObj.r = 2 * Math.PI * Math.random();
			pObj.dx = Math.random() * 500 - 250;
			pObj.ddx = - pObj.dx / 1.5;
			pObj.dy = Math.random() * 500 - 250;
			pObj.ddy = -pObj.dy / 1.5;
			pObj.dr = Math.random() * Math.PI;
			pObj.ddr = -Math.random() * Math.PI;
			pObj.anchorX = width / 2 + (Math.random() * width / 4 - width / 8);
			pObj.anchorY = height / 2 + (Math.random() * height / 4 - height / 8);
			pObj.width = width;
			pObj.height = height;
			pObj.ttl = ttl;
			pObj.delay = Math.random() * 500;
			pObj.opacity = 1;
			pObj.dopacity = -1;
		}

		this.xpSplosion.emitParticles(data);
	};

	this.animateLevelUp = function(oldXP, newXP, oldLevel, newLevel){
		var oldLevelBase = this.controller.getLevelXP(oldLevel),
			oldLevelNext = this.controller.getLevelXP(oldLevel + 1),
			oldMax = oldLevelNext - oldLevelBase,
			oldPercent = (oldXP - oldLevelBase)/oldMax,
			newLevelBase = this.controller.getLevelXP(newLevel),
			newLevelNext = this.controller.getLevelXP(newLevel+1),
			newMax = newLevelNext - newLevelBase,
			newPercent = (newXP - newLevelBase)/newMax,
			newX = ~~(barFillMaxWidth * newPercent) + this.barFullStretch.style.x,
			that = this,
			unique;

		this.levelText.setText(oldLevel);

		if( newLevel > oldLevel ){
			var barAnim = animate(this.barFullStretch),
				capAnim = animate(this.barFullCap),
				levelTxtAnim = animate(this.levelText),

				maxW = barFillMaxWidth,
				that = this;

				this.setXPBar((oldXP-oldLevelBase)/(oldLevelNext-oldLevelBase));

				barAnim
				.then({width: ~~(barFillMaxWidth)}, 1000, animate.linear)
				.then(function(){
					that.levelUpSparkles();
					that.levelText.setText(newLevel);
				})
				.then({width: 0}, 1, animate.linear)
				.then({width: newPercent * maxW}, 1000, animate.linear);

				capAnim
				.then(function(){
					that.triggerLevelCapParts(1000);
				})
				.then({x: ~~(barFillMaxWidth + this.barFullStretch.style.x)}, 1000, animate.linear)
				.then(function(){
					that.triggerLevelCapParts(1000);
				})
				.then({x: 0}, 1, animate.linear)
				.then({x: newPercent * maxW + this.barFullStretch.style.x }, 1000, animate.linear);

		}
		else{
			this.setXPBar(oldPercent);

			animate(this.barFullStretch, 'levelUpAnim').then({width: ~~(barFillMaxWidth * newPercent)}, 1500, animate.linear);
			animate(this.barFullCap, 'levelUpAnim').then({x: newX}, 1500, animate.linear);
			this.triggerLevelCapParts(1500);
		}
	}

	this.setXPBar = function(percent) {
		this.barFullStretch.style.width = ~~(barFillMaxWidth * percent);
		this.barFullCap.style.x = this.barFullStretch.style.x + this.barFullStretch.style.width;
	};

	this.showHeader = function(doAnimate, cb) {
		var that = this;
		this.active = true;
		this.style.visible = true;

		if( doAnimate ){
			this.style.y = HIDE_Y;
			if( cb ){
				animate(this).then({y: SHOW_Y}, animTime, animate.easeOut).then(cb);
			}
			else{
				animate(this).then({y: SHOW_Y}, animTime, animate.easeOut)
				.then(bind(this, this.showHeartTimer));
			}
		}
		else{
			this.style.y = SHOW_Y;
			this.showHeartTimer();
			cb && cb();
		}
	};

	this.hideHeader = function(doAnimate, cb) {
		this.active = false;
		if( doAnimate ){
			if( cb ){
				animate(this).then({y: HIDE_Y}, animTime, animate.easeIn)
				.then(this.makeInvisible)
				.then(cb);
			}
			else{
				animate(this).then({y: HIDE_Y}, animTime, animate.easeIn)
				.then(this.makeInvisible);
			}
		}
		else{
			this.style.y = HIDE_Y;
			this.makeInvisible();
			cb && cb();
		}
	};

	this.makeInvisible = function(){
		this.style.visible = false;
		this.hideHeartTimer();
	};

	this.designView = function() {
		this.bg = new View({
			superview: this,
			width: this.style.width,
			height: this.style.height,
			x: 0,
			y: 0,
			canHandleEvents: false
		});

		this.barLeft = new View({
			superview: this,
			x: 0,
			y: 0,
			width: this.style.width*0.66,
			height: this.style.height,
			canHandleEvents: false
		});

		this.berryBar = new ResourceContainer({
			superview: this.barLeft,
			width: this.barLeft.style.width * 0.4,
			height: this.style.height,
			x: 0,
			y: 0,
			controller: this.controller,
			onClick: bind(this, this.showBerryStore)
		});

		this.diamondBar = new ResourceContainer({
			superview: this.barLeft,
			width: this.barLeft.style.width * 0.3,
			height: this.style.height,
			x: this.berryBar.style.x + this.berryBar.style.width,
			y: 0,
			icon: 'resources/images/Header/icon_gem.png',
			controller: this.controller,
			onClick: bind(this, this.showGemStore)
		});

		this.heartBar = new ResourceContainer({
			superview: this.barLeft,
			width: this.barLeft.style.width * 0.3,
			height: this.style.height,
			x: this.diamondBar.style.x + this.diamondBar.style.width,
			y: 0,
			icon: 'resources/images/Header/icon_heart.png',
			controller: this.controller,
			onClick: bind(this, this.showHeartStore)
		});

		this.heartIcon = this.heartBar.icon_image;
		this.heartIcon.style.anchorX = this.heartIcon.style.width/2;
		this.heartIcon.style.anchorY = this.heartIcon.style.height/2;

		this.heartSplosion = new ParticleEngine({
			parent: this.heartBar,
			x: this.heartIcon.style.width /2,
			y: this.heartIcon.style.height /2,
			width: 1,
			height: 1,
			zIndex: 4
		});

		TIMER_VISIBLE_Y = this.heartBar.style.height * 0.6;
		TIMER_HIDDEN_Y = this.heartBar.style.height * 0.1;

		this.counterWrapper = new NineSliceImageView({
			superview: this.heartBar,
			x: this.heartBar.style.width * 0.2,
			y: TIMER_HIDDEN_Y,
			width: this.heartBar.style.width * 0.6,
			height: this.style.height * 0.7,
			opacity: 0,
			image: "resources/images/Modals/box_section",
			canHandleEvents: false
		});

		this.counterText = new TextView({
			superview: this.counterWrapper,
			x: 0,
			y: this.counterWrapper.style.height * 0.2,
			width: this.counterWrapper.style.width * 0.85,
			height: this.counterWrapper.style.height * 0.7,
			text: '5:00',
            horizontalAlign: 'right',
            verticalAlign: 'middle',
            size: config.fonts.timerFont,
            color: 'black',
            multiline: false,
            canHandleEvents: false
		});

		this.barRight = new View({
			superview: this,
			x: this.barLeft.style.x + this.barLeft.style.width,
			y: 0,
			width: this.style.width*0.34,
			height: this.style.height,
			canHandleEvents: false
		});

		var starMiddle = this.barRight.style.height * 0.5;

		this.xpSplosion = new ParticleEngine({
			parent: this.barRight,
			x: starMiddle,
			y: starMiddle,
			width: 1,
			height: 1
		});

		this.xpStar = new ImageView({
			superview: this.barRight,
			x: 0,
			y: 0,
			width: this.barRight.style.height,
			height: this.barRight.style.height,
			anchorX: starMiddle,
			anchorY: starMiddle,
			image: 'resources/images/Header/icon_star.png',
			zIndex: 5,
			canHandleEvents: false
		});

		this.levelText = new TextView({
			superview: this.xpStar,
            x: this.xpStar.style.width * 0.25,
            y: 0,
            width: this.xpStar.style.width * 0.5,
            height: this.xpStar.style.height,
            text: '0',
            textAlign: 'center',
            verticalAlign: 'middle',
            size: config.fonts.resourceContainer,
            color: 'black',
            multiline: false,
            canHandleEvents: false
		});

		xpBarHeight = this.barRight.style.height * 0.6;

		this.xpBarWrapper = new View({
			superview: this.barRight,
			x: this.barRight.style.height * 0.6,
			y: this.barRight.style.height * 0.275,
			width: this.barRight.style.width - (this.barRight.style.height * 0.5) - this.barRight.style.width * 0.075,
			height: xpBarHeight,
			canHandleEvents: false
		});

		xpCapWidth = xpBarHeight * 0.667;

		this.barEmptyStretch = new ImageView({
			superview: this.xpBarWrapper,
			x: 0,
			y: 0,
			width: this.xpBarWrapper.style.width - xpCapWidth,
			height: this.xpBarWrapper.style.height,
			image: 'resources/images/Header/bar_xpTrough_mid.png',
			canHandleEvents: false
		});

		this.barEmptyCap = new ImageView({
			superview: this.xpBarWrapper,
			x: this.barEmptyStretch.style.x + this.barEmptyStretch.style.width,
			y: this.barEmptyStretch.style.y,
			width: xpCapWidth,
			height: this.xpBarWrapper.style.height,
			image: 'resources/images/Header/bar_xpTrough_right.png',
			canHandleEvents: false
		});

		barFillMaxWidth = this.barEmptyStretch.style.width * 0.97;

		this.barFullStretch = new ImageView({
			superview: this.xpBarWrapper,
			x: 0,
			y: this.barEmptyStretch.style.y,
			width: barFillMaxWidth * 1,
			height: this.xpBarWrapper.style.height,
			image: 'resources/images/Header/bar_xpFill_mid.png',
			canHandleEvents: false
		});

		this.barFullCap = new ImageView({
			superview: this.xpBarWrapper,
			x: this.barFullStretch.style.x + this.barFullStretch.style.width,
			y: this.barFullStretch.style.y,
			width: xpBarHeight * 0.334,
			height: this.xpBarWrapper.style.height,
			image: 'resources/images/Header/bar_xpFill_right.png',
			canHandleEvents: false
		});

		this.xpCapParts = new ParticleEngine({
			parent: this.barFullCap,
			x: this.barFullCap.style.width /2,
			y: this.barFullCap.style.height /2,
			width: 1,
			height: 1
		});
	};

});
