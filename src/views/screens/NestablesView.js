import ui.View as View;
import ui.ImageView as ImageView;

import src.lib.ListView as ListView;

import src.controllers.Controller as Controller;

import src.views.helpers.NestablesTopNodeView as NestablesTopNodeView;
import src.views.helpers.NestablesBottomNodeView as NestablesBottomNodeView;
import src.views.helpers.NestablesNodeView as NestablesNodeView;

import src.lib.LocaleManager as W.LocaleManager;

exports = Class(View, function(supr) {

	var BG_WIDTH,
		BG_HEIGHT,
		TOP_HEIGHT = 491,
		NODE_HEIGHT = 195,
		BOTTOM_HEIGHT = 230,
		OVERFLOW_SIZE = 50;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		this.nestablesConfig = this.controller.nestablesConfig;

		var config = this.controller.config;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;

		this.designView();
	};

	this.designView = function() {
		this.nestablesList = new ListView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			nodeCtor: NestablesNodeView,
			nodeHeight: NODE_HEIGHT,
			updateNode: function(node, data) {
				node.updateData(data);
			}
		});

		var topNode = this.nestablesList.addFixedTopNode({
			ctor: NestablesTopNodeView,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: TOP_HEIGHT
		});
		topNode.overflow = new ImageView({
			parent: topNode,
			x: 0,
			y: -OVERFLOW_SIZE,
			width: BG_WIDTH,
			height: OVERFLOW_SIZE,
			image: "resources/images/Nestables/nodeNestablesTopOverflow.png"
		});
		this.backButtonFunction = bind(topNode.backButton, topNode.backButton.onInputStart);

		var bottomNode = this.nestablesList.addFixedBottomNode({
			ctor: NestablesBottomNodeView,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: BOTTOM_HEIGHT
		});
		bottomNode.overflow = new ImageView({
			parent: bottomNode,
			x: 0,
			y: bottomNode.style.height,
			width: BG_WIDTH,
			height: OVERFLOW_SIZE,
			image: "resources/images/Nestables/nodeNestablesBottomOverflow.png"
		});
	};

	this.resetView = function() {
		if (this.controller.playingSong != "run_menu_music") {
			this.controller.pauseSong();
			this.controller.playSong("run_menu_music");
		}

		this.controller.setHardwareBackButton(this.backButtonFunction);

		this.nodeData = [];
		this.updateNestableNodes();

		var gamesPlayed = this.controller.getData("gamesPlayed") || 0;
		this.controller.trackEvent("Nestables", {
			gamesPlayed: gamesPlayed + "",
			nestableCount: this.nestableCount + ""
		});
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		this.controller.transitionLockDown(false);
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			callback();
		}
	};

	var fnSort = function(setA, setB) {
		if (setA.order < setB.order) {
			return -1;
		} else if (setA.order > setB.order) {
			return 1;
		} else {
			return 0;
		}
	};

	this.updateNestableNodes = function() {
		var sets = this.nestablesConfig.sets;
		var collected = this.controller.getData("nestablesCollected") || {};
		this.nestableCount = 0;

		// populate node data
		var index = 0;
		for (var i in sets) {
			var set = sets[i];
			var setCollection = collected[set.id];

			if( set.id === '25000' && W.LocaleManager.getLocale() !== 'ko-kr' ){
				continue;
			}

			// set data
			var node = {
				uid: set.id,
				order: set.order,
				index: index++,
				name: set.name,
				reward: set.reward,
				pieces: [],
				height: NODE_HEIGHT
			};

			for (var j in set.pieces) {
				var piece = set.pieces[j];
				var owned = setCollection && setCollection[piece.id];

				// copy config data for each piece
				var pieceData = {
					id: piece.id,
					type: piece.type,
					chance: piece.chance,
					distance: piece.distance,
					image: piece.image,
					shadow: piece.shadow,
					owned: owned
				}

				if (owned) {
					this.nestableCount++;
				}

				node.pieces.push(pieceData);
			}

			this.nodeData.push(node);
		}

		this.nodeData.sort(fnSort);
		this.nestablesList.updateData(this.nodeData, true);
	};
});
