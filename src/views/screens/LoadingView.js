import ui.View as View;
import ...lib.TextView as TextView;
import ui.ImageView as ImageView;

import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	var BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		TITLE_WIDTH = 460,
		TITLE_HEIGHT = 60,
		LOADING_HEIGHT = 80,
		MESSAGE_WIDTH = 520,
		MESSAGE_HEIGHT = 200,
		OFFSET_X = 25,
		OFFSET_Y = 212,

		config;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;

		this.isMainView = true;

		this.designView();
	};

	this.designView = function() {
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: GC.app.l.translate("resources/images/Locales/en") + "/Title/loadingBlank.png",
			canHandleEvents: false
		});

		this.loading = new TextView({
			parent: this,
			x: (this.style.width / 2) - TITLE_WIDTH / 2,
			y: this.style.height - 1.5 * LOADING_HEIGHT,
			width: TITLE_WIDTH,
			height: LOADING_HEIGHT,
			size: config.fonts.loading,
			color: config.colors.gameoverTitleFill,
			strokeColor: config.colors.gameoverTitleStroke,
			text: GC.app.l.translate("Loading"),
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.title = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - TITLE_WIDTH) / 2,
			y: OFFSET_Y,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: "",
			textAlign: 'center',
			size: config.fonts.trainerTipsTitle,
			color: config.colors.trainerTipsTitle,
			multiline: false,
			canHandleEvents: false,
			visible: false
		});

		this.message = new TextView({
			parent: this,
			x: OFFSET_X + (this.style.width - MESSAGE_WIDTH) / 2,
			y: OFFSET_Y + TITLE_HEIGHT,
			width: MESSAGE_WIDTH,
			height: MESSAGE_HEIGHT,
			text: "",
			textAlign: 'center',
			size: config.fonts.trainerTips,
			color: config.colors.trainerTips,
			multiline: true,
			canHandleEvents: false,
			visible: false
		});
	};

	this.resetView = function() {
		var opts = this.controller.getLoadingOpts();
		if (opts.showTip) {
			this.title.style.visible = true;
			this.title._opts.font = config.fonts.trainerTipsTitle;
			this.message.style.visible = true;
			this.loading.style.visible = true;
			this.title.setText(GC.app.l.translate(opts.title));
			this.message.setText(GC.app.l.translate(opts.message));
			this.background.setImage("resources/images/MainMenu/loadingTips.png");
		} else {
			this.title.style.visible = false;
			this.message.style.visible = false;
			this.loading.style.visible = true;
			this.background.setImage(GC.app.l.translate("resources/images/Locales/en") + "/Title/loadingBlank.png");
		}
	};

	// loading screen should not be unblocking input or changing transition state
	this.constructView = function() {};
	this.deconstructView = function(callback) { callback && callback(); };
});
