import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ListView as ListView;
import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;

import src.views.helpers.LeaderboardNodeView as LeaderboardNodeView;
import src.views.helpers.InviteNodeView as InviteNodeView;

exports = Class(View, function(supr) {

	// CLASS CONSTANTS
	var BG_WIDTH,
		BG_HEIGHT,
		SUN_WIDTH = 282,
		SUN_HEIGHT = 282,
		POST_WIDTH = 812,
		POST_HEIGHT = 756,
		SIGN_OFFSET_Y = -50,
		BACK_WIDTH = 187,
		BACK_HEIGHT = 116,
		LABEL_HEIGHT = 60,
		BACK_LABEL_Y = 50,
		BACK_LABEL_R = -Math.PI / 18,
		INVITE_WIDTH = 270,
		INVITE_HEIGHT = 155,
		INVITE_LABEL_Y = 35,
		INVITE_LABEL_R = Math.PI / 16,

		TITLE_X_OFFSET = 110,
		TITLE_Y = 95,
		TITLE_R = -Math.PI / 128,
		TITLE_WIDTH = 450,
		TITLE_HEIGHT = 99,

		SUN_ROTATION_RATE = Math.PI / 16,
		SUN_SCALE_RATE = 0.1,
		MIN_SUN_SCALE = 1,
		MAX_SUN_SCALE = 1.5,
		INNER_ROTATION_RATE = Math.PI / 32,
		INNER_SCALE_RATE = 0.025,
		MIN_INNER_SCALE = 1,
		MAX_INNER_SCALE = 1.05,
		OUTER_ROTATION_RATE = Math.PI / 32,
		OUTER_SCALE_RATE = 0.05,
		MIN_OUTER_SCALE = 1,
		MAX_OUTER_SCALE = 1.1,
		CHALK_DUST_COUNT = 6,

		ROPE_LEFT_X = 45,
		ROPE_RIGHT_X = 550,
		ROPE_WIDTH = 20,
		ROPE_HEIGHT = 597,

		NODE_HEIGHT = 124,
		NODE_WIDTH = 616,
		CLIPPING_WIDTH = NODE_WIDTH,
		CLIPPING_X_OFFSET = -76,
		CLIPPING_Y = 150,
		CLIPPING_HEIGHT = 570,
		CLOSED_UP_OFFSET = 48,
		LIST_HEIGHT = CLIPPING_HEIGHT - CLOSED_UP_OFFSET,
		LIST_WIDTH = NODE_WIDTH,
		MODE_LEADERBOARD = 0,
		MODE_INVITES = 1,

		LOADING_NODE = {
			uid: "LOADING",
			loading: true
		},
		FB_NODE = {
			uid: "FACEBOOK",
			facebook: true
		},
		ERROR_NODE = {
			uid: "ERROR",
			loading: true,
			message: GC.app.l.translate("Connectivity Trouble") + "..."
		},
		LEADERBOARD_TIMEOUT = 10000,
		LOADING_TIMEOUT = 15000,
		SLIDE_TIME = 750,

		config,
		controller;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		controller = GC.app.controller;
		config = controller.config;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;

		this.invitesOn = true;
		this.isMainView = true;
		this.mode = MODE_LEADERBOARD;

		this.designView();
	};

	// build the component subviews of the screen
	this.designView = function() {

		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: (this.style.height - BG_HEIGHT) / 2,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/Leaderboard/background.png",
			canHandleEvents: false
		});

		this.sunRadial = new View({
			parent: this,
			x: this.style.width - SUN_WIDTH,
			y: 0,
			anchorX: SUN_WIDTH / 2,
			anchorY: SUN_HEIGHT / 2,
			width: SUN_WIDTH,
			height: SUN_HEIGHT,
			opacity: 0.5,
			canHandleEvents: false
		});

		this.sunRadialBig = new View({
			parent: this,
			x: this.style.width - SUN_WIDTH - (0.25 * SUN_WIDTH) / 2,
			y: 0,
			r: Math.PI / 2,
			anchorX: 1.25 * SUN_WIDTH / 2,
			anchorY: 1.25 * SUN_HEIGHT / 2,
			width: SUN_WIDTH * 1.25,
			height: SUN_HEIGHT * 1.25,
			opacity: 0.5,
			canHandleEvents: false
		});

		this.sun = new ImageView({
			parent: this,
			x: this.style.width - SUN_WIDTH,
			y: 0,
			width: SUN_WIDTH,
			height: SUN_HEIGHT,
			image: "resources/images/Leaderboard/sun.png",
			canHandleEvents: false
		});

		this.innerRays = new ImageView({
			parent: this,
			x: this.style.width - SUN_WIDTH,
			y: 0,
			anchorX: SUN_WIDTH / 2,
			anchorY: SUN_HEIGHT / 2,
			width: SUN_WIDTH,
			height: SUN_HEIGHT,
			image: "resources/images/Leaderboard/raysInner.png",
			canHandleEvents: false
		});

		this.outerRays = new ImageView({
			parent: this,
			x: this.style.width - SUN_WIDTH,
			y: 0,
			anchorX: SUN_WIDTH / 2,
			anchorY: SUN_HEIGHT / 2,
			width: SUN_WIDTH,
			height: SUN_HEIGHT,
			image: "resources/images/Leaderboard/raysOuter.png",
			canHandleEvents: false
		});

		this.clippingLayer = new View({
			parent: this,
			x: (this.style.width - CLIPPING_WIDTH) / 2 + CLIPPING_X_OFFSET,
			y: CLIPPING_Y,
			width: CLIPPING_WIDTH,
			height: CLIPPING_HEIGHT,
			clip: true,
			canHandleEvents: true
		});

		this.slidingLayer = new View({
			parent: this.clippingLayer,
			x: 0,
			y: CLOSED_UP_OFFSET,
			width: CLIPPING_WIDTH,
			height: CLIPPING_HEIGHT,
			canHandleEvents: false
		});

		this.ropeClip = new View({
			parent: this.slidingLayer,
			x: 0,
			y: -CLOSED_UP_OFFSET,
			width: CLIPPING_WIDTH,
			height: CLIPPING_HEIGHT,
			clip: true,
			blockEvents: true
		});

		this.ropeLeft = new ImageView({
			parent: this.ropeClip,
			image: "resources/images/Leaderboard/rope.png",
			x: ROPE_LEFT_X,
			y: 0,
			width: ROPE_WIDTH,
			height: ROPE_HEIGHT
		});

		this.ropeRight = new ImageView({
			parent: this.ropeClip,
			image: "resources/images/Leaderboard/rope.png",
			x: ROPE_RIGHT_X,
			y: 0,
			width: ROPE_WIDTH,
			height: ROPE_HEIGHT
		});

		this.currentList = this.leaderboardList = new ListView({
			parent: this.slidingLayer,
			x: 0,
			y: 0,
			width: LIST_WIDTH,
			height: LIST_HEIGHT,
			nodeCtor: LeaderboardNodeView,
			nodeHeight: NODE_HEIGHT,
			updateNode: function(node, data) {
				node.updateData(data);
			},
			visible: this.mode === MODE_LEADERBOARD ? true : false
		});

		this.inviteList = new ListView({
			parent: this.slidingLayer,
			x: 0,
			y: 0,
			width: LIST_WIDTH,
			height: LIST_HEIGHT,
			nodeCtor: InviteNodeView,
			nodeHeight: NODE_HEIGHT,
			updateNode: function(node, data) {
				node.updateData(data);
			},
			visible: this.mode === MODE_INVITES ? true : false
		});

		this.post = new ImageView({
			parent: this,
			x: (this.style.width - POST_WIDTH) / 2,
			y: 0,
			width: POST_WIDTH,
			height: POST_HEIGHT,
			image: "resources/images/Leaderboard/post.png",
			canHandleEvents: false
		});

		this.txtTitle = new TextView({
			parent: this,
			x: (this.style.width - POST_WIDTH) / 2 + TITLE_X_OFFSET,
			y: TITLE_Y,
			r: TITLE_R,
			anchorX: TITLE_WIDTH / 2,
			anchorY: TITLE_HEIGHT / 2,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: GC.app.l.translate("LEADERBOARD"),
			textAlign: 'center',
			size: config.fonts.nestablesTitle,
			color: config.colors.nestablesTitleFill,
			strokeColor: config.colors.nestablesTitleStroke,
			multiline: false,
			canHandleEvents: false
		});

		this.chalkDust = new ParticleEngine({
			parent: this.txtTitle,
			x: 0,
			y: 0,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
		});

		this.btnBack = new ButtonView({
			parent: this.post,
			x: POST_WIDTH - BACK_WIDTH,
			y: POST_HEIGHT - BACK_HEIGHT + SIGN_OFFSET_Y,
			width: BACK_WIDTH,
			height: BACK_HEIGHT,
			image: "resources/images/Leaderboard/btnBack.png",
			soundOnStart: bind(controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'deconstructView', bind(controller, 'transitionToMainMenu'))
		});
		this.txtBack = new TextView({
			parent: this.btnBack,
			x: 0,
			y: BACK_LABEL_Y,
			r: BACK_LABEL_R,
			width: BACK_WIDTH,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("MENU"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.btnInvite = new ButtonView({
			parent: this.post,
			x: POST_WIDTH - 0.75 * INVITE_WIDTH,
			y: POST_HEIGHT - BACK_HEIGHT - INVITE_HEIGHT + SIGN_OFFSET_Y,
			width: INVITE_WIDTH,
			height: INVITE_HEIGHT,
			anchorX: INVITE_WIDTH / 2,
			anchorY: INVITE_HEIGHT / 2,
			image: "resources/images/Leaderboard/btnInvite.png",
			soundOnStart: bind(controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'cycleModes'),
			visible: this.invitesOn ? true : false
		});
		this.txtInvite = new TextView({
			parent: this.btnInvite,
			x: 45,
			y: INVITE_LABEL_Y,
			r: INVITE_LABEL_R,
			width: INVITE_WIDTH - 82,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("INVITES"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.lbData = {};

		this.leaderboardList.addNode(FB_NODE);
		this.updateRopes();

		// FACEBOOK SOCIAL
		var friends = controller.getData("friends");
		if (friends) {
			this.loadData(friends, false);
		}

		var myFacebookData = controller.getData("myFacebookData");
		if (myFacebookData) {
			this.loadData(myFacebookData, false);
		}

		controller.fb.subscribe('Auth', this, 'onFacebookAuth');
		controller.fb.subscribe('User', this, 'gotFacebookUser');
		controller.fb.subscribe('Friends', this, 'loadInviteData');
		controller.fb.onScores(bind(this, 'gotFacebookFriends'));

		//gcsocial.on('online',  bind(this, this.onOnlineStateChanged));
		//gcsocial.on('offline', bind(this, this.onOnlineStateChanged));

		controller.fb.getFriends();
	};

	// ensures state and data get reset each time the view becomes active
	this.resetView = function() {
		if (controller.playingSong != "run_menu_music") {
			controller.pauseSong();
			controller.playSong("run_menu_music");
		}

		this.dscaleSun = SUN_SCALE_RATE;
		this.dscaleInner = INNER_SCALE_RATE;
		this.dscaleOuter = OUTER_SCALE_RATE;

		controller.setHardwareBackButton(this.btnBack, false);

		// reset to leaderboard screen
		this.currentList = this.leaderboardList;
		this.leaderboardList.style.visible = true;
		this.inviteList.style.visible = false;
		this.txtInvite.updateOpts({
			size: config.fonts.buttonSign
		});
		this.txtInvite.setText(GC.app.l.translate("Invites"));
		this.btnInvite.style.y = POST_HEIGHT - BACK_HEIGHT - INVITE_HEIGHT + SIGN_OFFSET_Y;
		this.btnInvite.style.r = 0;

		this.txtTitle.setText(GC.app.l.translate("LEADERBOARD"));

		this.mode = MODE_LEADERBOARD;

		controller.fb._scores = undefined;
		controller.fb.onScores(bind(this, 'gotFacebookFriends'));
		controller.fb.getFriends();
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		controller.blockInput();

		controller.transitionLockDown(false);
		controller.unblockInput();
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!controller.transitioning) {
			controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			controller.blockInput();
			controller.unblockInput();
			callback();
		}
	};

	this.loadData = function(data, save) {
		//save && controller.setData('friends', data);

		// update this.lbData hash
		if (data.length === undefined) {
			data.uid = data.id;
			this.lbData[data.id] = data;
		} else {
			for (var d in data) {
				var item = data[d],
					uid = item.id;

				item.uid = uid;
				this.lbData[uid] = item;
			}
		}

		// populate the list with a hash to prevent removals by ListView updateData
		var listData = [];
		for (var l in this.lbData) {
			listData.push(this.lbData[l]);
		}
		this.leaderboardList.updateData(listData, false);
		this.leaderboardList.sortList(function(a, b) {
			if (a.score < b.score) {
				return 1;
			} else {
				return -1;
			}
		});
	};

	this.loadInviteData = function(data, save) {
		//save && controller.setData('invites', data);

		var invitees = [];
		for (var d in data) {
			var item = data[d];
			var inv = {};
			for (var i in item) {
				inv[i] = item[i];
			}

			inv.uid = item.id;
			invitees.push(inv);
		}

		this.inviteList.updateData(invitees, false);
		this.inviteList.sortList(function(a, b) {
			if (a.name > b.name) {
				return 1;
			} else {
				return -1;
			}
		});
	};

	this.onFacebookAuth = function(status) {
		if (status.success) {
			logger.log("~~~ FB SUCCESSFUL LOGIN!");
		} else {
			logger.log("~~~ FB FAILED LOGIN!");
		}
	};

	this.gotFacebookUser = function(data) {
		if (data) {
			logger.log("~~~ FB USER DATA!");
			this.loadData(data, false);
		} else {
			logger.log("~~~ FB USER DATA ERROR!");
		}
	};

	this.gotFacebookFriends = function(data) {
		if (data && data.length) {
			logger.log("~~~ FB GOT FRIENDS!");
			this.loadData(data, true);
		} else {
			logger.log("~~~ FB FRIENDS ERROR!");
		}
	};

	this.resetScores = function() {
		logger.log("~~~ RESET SCORES!");
		for (var l in this.lbData) {
			var item = this.lbData[l];
			item.score = 0;
			if (l === controller.myFacebookID) {
				controller.setData("myFacebookData", item);
			}
		}
		this.loadData([], false);
	};

	this.updateMyScore = function(score) {
		logger.log("~~~ UPDATE MY SCORE!");
		if (controller.myFacebookID) {
			var myData = this.lbData[controller.myFacebookID];
			if (myData) {
				myData.score = score;
				controller.setData("myFacebookData", myData);
				this.loadData(myData, false);
			}
		}
	};

	this.onOnlineStateChanged = function() {
		logger.log("~~~ ONLINE STATE CHANGE!");
	};

	this.getScores = function() {
		logger.log("~~~ FB GET SCORES!");
		controller.fb.onScores(bind(this, 'gotFacebookFriends'));
		controller.fb.getScores();
	};

	this.setScoresCallback = function() {
		logger.log("~~~ FB SET SCORES CALLBACK!");
		controller.fb.onScores(bind(this, 'gotFacebookFriends'));
	};

	this.updateRopes = function() {
		this.ropeClip.style.height = Math.min(
			this.currentList.totalHeight + CLOSED_UP_OFFSET - (NODE_HEIGHT / 2),
			LIST_HEIGHT + CLOSED_UP_OFFSET - (NODE_HEIGHT / 2));
	};

	this.slideListUp = function(cb) {
		var that = this,
			offset = this.currentList.getOffset(),
			target = Math.min(0, -this.currentList.totalHeight + LIST_HEIGHT);
			diff = target - offset,
			pct = Math.abs(diff / this.currentList.totalHeight);

		this.currentList.scrollTo(target, SLIDE_TIME * pct, animate.easeIn, function() {
			var anim = animate(that.slidingLayer).now({
				y: -CLIPPING_HEIGHT
			}, SLIDE_TIME, animate.easeOut);

			if ( cb != null ) {
				anim.then(cb);
			}
		});
	};

	this.slideListDown = function(cb) {
		var that = this,
			offset = this.currentList.getOffset(),
			diff = 0 - offset;
			pct = Math.abs(diff / this.currentList.totalHeight);

		var anim = animate(this.slidingLayer).now({
			y: CLOSED_UP_OFFSET
		}, SLIDE_TIME, animate.easeIn).then(function() {
			var callback = typeof cb === 'function' ? cb : function() {};
			that.currentList.scrollTo(0, pct * SLIDE_TIME, animate.easeOut, callback);
		});
	};

	this.cycleModes = function() {
		var
			that = this,
			currentList,
			nextList;

		if (!controller.transitioning) {
			controller.transitionLockDown(true);
		} else {
			return;
		}

		controller.blockInput();

		if ( this.mode === MODE_LEADERBOARD ) {
			this.mode = MODE_INVITES;

			currentList = this.leaderboardList;
			nextList = this.inviteList;

			animate(this.btnInvite).now({ dr: 6 * Math.PI }, SLIDE_TIME, animate.easeOut);
			this.txtInvite.updateOpts({
				size: config.fonts.buttonSign
			});
			this.txtInvite.setText(GC.app.l.translate("Leaderboard"));
			this.txtTitle.setText(GC.app.l.translate("INVITES"));

		} else {
			this.mode = MODE_LEADERBOARD;

			currentList = this.inviteList;
			nextList = this.leaderboardList;

			animate(this.btnInvite).now({ dr: 6 * Math.PI }, SLIDE_TIME, animate.easeOut);
			this.txtInvite.updateOpts({
				size: config.fonts.buttonSign
			});
			this.txtInvite.setText(GC.app.l.translate("Invites"));
			this.txtTitle.setText(GC.app.l.translate("LEADERBOARD"));
		}

		this.emitChalkDustParticles();

		// transition between screens
		var offset = currentList.getOffset();
		var target = -currentList.totalHeight + NODE_HEIGHT / 4;
		var diff = target - offset;
		var pct = Math.abs(diff / target);

		this.slideListUp(function() {
			currentList.style.visible = false;
			nextList.style.visible = true;
			that.currentList = nextList;
			that.updateRopes();

			that.slideListDown(function() {
				controller.unblockInput();
				controller.transitionLockDown(false);
			});
		});
	};

	this.emitChalkDustParticles = function() {
		var data = this.chalkDust.obtainParticleArray(CHALK_DUST_COUNT);

		for (var i = 0; i < CHALK_DUST_COUNT; i++) {
			var pObj = data[i];
			var width = Math.random() * 15 + 30;
			var height = width;
			var ttl = SLIDE_TIME * 1.5;

			pObj.image = "resources/images/Leaderboard/particleDust.png";
			pObj.x = TITLE_WIDTH / 6 + Math.random() * 2 * TITLE_WIDTH / 3 - width / 2;
			pObj.y = TITLE_HEIGHT / 6 + Math.random() * 2 * TITLE_HEIGHT / 3 - height / 2;
			pObj.r = 2 * Math.PI * Math.random();
			pObj.dx = Math.random() * 200 - 100;
			pObj.ddx = -pObj.dx / 2;
			pObj.dy = Math.random() * 200 - 100;
			pObj.ddy = -pObj.dy / 2;
			pObj.dr = Math.random() * Math.PI;
			pObj.ddr = -Math.random() * Math.PI;
			pObj.anchorX = width / 2 + (Math.random() * width / 4 - width / 8);
			pObj.anchorY = height / 2 + (Math.random() * height / 4 - height / 8);
			pObj.dscale = 7 + Math.random() * 2;
			pObj.ddscale = -8 - Math.random() * 2;
			pObj.width = width;
			pObj.height = height;
			pObj.ttl = ttl;
			pObj.delay = Math.random() * SLIDE_TIME / 3;
			pObj.opacity = 0.75;
			pObj.dopacity = -750 / ttl;
		}

		this.chalkDust.emitParticles(data);
	};

	this.tick = function(dt) {
		if (!this.style.visible) { return; }

		this.chalkDust.runTick(dt);

		var pct = dt / 1000;

		var sunStyle = this.sunRadial.style;
		var sunStyleBig = this.sunRadialBig.style;
		sunStyle.r += pct * SUN_ROTATION_RATE;
		sunStyleBig.r = sunStyle.r + Math.PI / 2;
		sunStyleBig.scale = sunStyle.scale += pct * this.dscaleSun;
		if (sunStyle.scale > MAX_SUN_SCALE || sunStyle.scale < MIN_SUN_SCALE) {
			this.dscaleSun = -this.dscaleSun;
		}

		var innerStyle = this.innerRays.style;
		innerStyle.r += pct * INNER_ROTATION_RATE;
		innerStyle.scale += pct * this.dscaleInner;
		if (innerStyle.scale > MAX_INNER_SCALE || innerStyle.scale < MIN_INNER_SCALE) {
			this.dscaleInner = -this.dscaleInner;
		}

		var outerStyle = this.outerRays.style;
		outerStyle.r += pct * OUTER_ROTATION_RATE;
		outerStyle.scale += pct * this.dscaleOuter;
		if (outerStyle.scale > MAX_OUTER_SCALE || outerStyle.scale < MIN_OUTER_SCALE) {
			this.dscaleOuter = -this.dscaleOuter;
		}
	};
});
