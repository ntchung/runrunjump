import device;
import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;
import ui.SpriteView as SpriteView;

import src.views.modals.BinaryModalView as BinaryModalView;
import src.views.modals.MapPreviewModal as MapPreviewModal;
import src.views.helpers.MapScrollView as MapScrollView;
import src.lib.ButtonView as ButtonView;
import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	var
		BG_WIDTH,
		BG_HEIGHT,
		BTN_BACK_WIDTH = 304,
		BTN_BACK_HEIGHT = 230,
		BTN_BACK_SPACING_X = -5,
		BTN_BACK_SPACING_Y = 15,
		BTN_PLAY_SPACING_X = 3,
		BTN_PLAY_SPACING_Y = 15,
		BTN_PLAY_WIDTH = 348,
		BTN_PLAY_HEIGHT = 230,
		UI_PADDING = 50,
		FOREL_OFFSET_X = 20,
		MAP_SCROLL_WIDTH,
		MAP_SCROLL_HEIGHT = 640,
		MAP_FOREL_WIDTH = 196,
		MAP_FOREL_HEIGHT = 716,
		MAP_NODE_WIDTH = 250,
		MAP_NODE_HEIGHT = 640,
		MAP_NODE_IMGS = [
			"resources/images/Map/paper2.png",
			"resources/images/Map/paper3.png",
			"resources/images/Map/paper4.png",
			"resources/images/Map/paper5.png",
		],

		WATER_WIDTH = 288,
		WATER_HEIGHT = 205,
		LOCK_WIDTH = 114,
		LOCK_HEIGHT = 168,
		KIWI_WIDTH = 76,
		KIWI_HEIGHT = 73,
		KIWI_SPEED = 300, // pixels/second
		WOBBLE_SPEED = 3 * Math.PI / 4, // radians/second
		WOBBLE_DELAY = 0,
		LOCK_DUR_MIN = 2000,
		LOCK_DUR_MAX = 5000,
		mapTotalWidth = 0,
		scrollMax = 0,
		mapConfig,
		config;

	/**
	 * @method init
	 */
	this.init = function(opts) {

		this.controller = Controller.get();
		this.levelViews = {};
		this.lockShineTimeout = null;
		this.kiwiPlacement = "default";
		config = this.controller.config;
		mapConfig = this.controller.mapConfig

		supr(this, 'init', [opts]);

		MAP_SCROLL_WIDTH = this.style.width - UI_PADDING;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;

		this.designView();
	};

	/**
	 * @method designView
	 */
	this.designView = function() {
		var that = this;

		this.background	= new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/Map/background.png"
		});

		this.mapClip = new View({
			parent: this,
			x: 0,
			y: (this.style.height - MAP_SCROLL_HEIGHT) / 2,
			width: MAP_SCROLL_WIDTH,
			height: MAP_SCROLL_HEIGHT,
			clip: true
		});

		this.scrollView = new MapScrollView({
			parent: this.mapClip,
			x: 0,
			y: 0,
			width: MAP_SCROLL_WIDTH,
			height: MAP_SCROLL_HEIGHT,
			fullWidth: MAP_SCROLL_WIDTH,
			fullHeight: MAP_SCROLL_HEIGHT,
			scrollX: true,
			scrollY: false,
			clip: false,
			bounce: true,
			scrollBounds: {
				minX: 0,
				maxX: 0,
				minY: 0,
				maxY: 0
			},
			dragRadius: 15
		});

		this.forel = new SpriteView({
			parent: this,
			x: this.style.width - MAP_FOREL_WIDTH + FOREL_OFFSET_X,
			y: (this.style.height - MAP_FOREL_HEIGHT) / 2,
			width: MAP_FOREL_WIDTH,
			height: MAP_FOREL_HEIGHT,
			url: "resources/images/Map/forelSpin",
			autoStart: true,
			frameRate: -15
		});

		this.forel.pause();

		this.scrollView.setScrollSprite(this.forel);

		this.btnBack = new ButtonView({
			parent: this,
			x: 0,
			y: this.style.height - BTN_BACK_HEIGHT,
			width: BTN_BACK_WIDTH,
			height: BTN_BACK_HEIGHT,
			image: "resources/images/Map/buttonBack.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'deconstructView', bind(this.controller, 'transitionToMainMenu'))
		});

		this.btnBackTxt = new TextView({
			parent: this.btnBack,
			x: BTN_BACK_SPACING_X,
			y: BTN_BACK_SPACING_Y,
			width: BTN_BACK_WIDTH,
			height: BTN_BACK_HEIGHT,
			anchorX: BTN_BACK_WIDTH / 2,
			anchorY: BTN_BACK_HEIGHT / 2,
			r: -Math.PI / 28,
			scale: 1.2,
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			text: GC.app.l.translate("MENU"),
			multiline: false,
			canHandleEvents: false
		});

		this.btnPlay = new ButtonView({
			parent: this,
			x: this.style.width - BTN_PLAY_WIDTH,
			y: this.style.height - BTN_PLAY_HEIGHT,
			width: BTN_PLAY_WIDTH,
			height: BTN_PLAY_HEIGHT,
			image: "resources/images/Map/buttonPlay.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this.controller, 'showEquipModal')
		});

		this.btnPlayTxt = new TextView({
			parent: this.btnPlay,
			x: BTN_PLAY_SPACING_X,
			y: BTN_PLAY_SPACING_Y,
			width: BTN_PLAY_WIDTH,
			height: BTN_PLAY_HEIGHT,
			anchorX: BTN_PLAY_WIDTH / 2,
			anchorY: BTN_PLAY_HEIGHT / 2,
			r: Math.PI / 28,
			scale: 1.2,
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			text: GC.app.l.translate("PLAY!"),
			multiline: false,
			canHandleEvents: false
		});

		this.mapPreviewModal = new MapPreviewModal({
			parent: this,
			accept: bind(this, this.purchaseLevel),
			noAutoOpen: true
		});

		this.loadMapNodes();
	};

	/**
	 * Creates UI contained without scrollView map.
	 * @method loadMapNodes
	 */
	this.loadMapNodes = function() {
		var
			config = this.controller.mapConfig,
			mapKeys = Object.keys(config.levels),
			farthestX = 0,
			nodeData = [],
			totalWidth = 0,
			mapWidth,
			level,
			i;

		// calculate the farthest map icon position
		// and make map that wide, or width of screen.
		i = 0;
		while ((level = config.levels[mapKeys[i++]]) != null) {
			farthestX = Math.max(farthestX, level.map.x + level.map.iconWidth);
		}

		mapWidth = Math.max(MAP_SCROLL_WIDTH, farthestX);

		// first node cap
		new ImageView({
			parent: this.scrollView,
			x: 0,
			y: 0,
			width: MAP_NODE_WIDTH,
			height: MAP_NODE_HEIGHT,
			image: "resources/images/Map/paper1.png"
		});

		totalWidth += MAP_NODE_WIDTH;

		// insert rest of nodes
		i = 0;
		while(totalWidth < mapWidth) {
			new ImageView({
				parent: this.scrollView,
				x: totalWidth - 1,
				y: 0,
				width: MAP_NODE_WIDTH + 1,
				height: MAP_NODE_HEIGHT,
				image: MAP_NODE_IMGS[i]
			});

			i = (i + 1) % 4;
			totalWidth += MAP_NODE_WIDTH;
		}

		mapTotalWidth = totalWidth;
		scrollMax = totalWidth - MAP_SCROLL_WIDTH;

		this.scrollView.setScrollBounds({
			minX: 0,
			maxX: totalWidth,
			minY: 0,
			maxY: 0
		});

		// place water icons
		// better to do by config for aesthetic purposes.
		var waterData;

		i = 0;
		while((waterData = config.water[i++]) != null) {
			new ImageView({
				parent: this.scrollView,
				x: waterData.x,
				y: waterData.y,
				width: WATER_WIDTH,
				height: WATER_HEIGHT,
				image: "resources/images/Map/waves01.png"
			});
		}

		// place level icons
		var
			mapData,
			levelViews;

		i = 0;
		while ((level = config.levels[mapKeys[i++]]) != null) {
			mapData = level.map;

			levelViews = {
				"icon": new ButtonView({
					parent: this.scrollView,
					x: mapData.x,
					y: mapData.y,
					width: mapData.iconWidth,
					height: mapData.iconHeight,
					image: mapData.icon,
					soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
					onClick: bind(this, this.selectLevel, level.id)
				}),
				"lock": new ImageView({
					parent: this.scrollView,
					x: mapData.x + (mapData.iconWidth - LOCK_WIDTH) / 2,
					y: mapData.y + (mapData.iconHeight - LOCK_HEIGHT) / 2,
					width: LOCK_WIDTH,
					height: LOCK_HEIGHT,
					image: "resources/images/Map/lock.png",
					canHandleEvents: false
				})
			};

			levelViews.lockShine = new SpriteView({
				parent: levelViews.lock,
				x: 0,
				y: 0,
				width: LOCK_WIDTH,
				height: LOCK_HEIGHT,
				url: "resources/images/Map/lockShine",
				canHandleEvents: false,
				autoStart: false,
				loop: false,
				visible: false
			});

			this.levelViews[level.id] = levelViews;
		}

		// create and place kiwi
		this.selectorKiwi = new ImageView({
			parent: this.scrollView,
			x: 0,
			y: 0,
			width: KIWI_WIDTH,
			height: KIWI_HEIGHT,
			anchorX: KIWI_WIDTH / 2,
			anchorY: KIWI_HEIGHT / 2,
			image: "resources/images/Map/iconKiwi.png",
			canHandleEvents: false
		});
	};

	/**
	 * @method resetView
	 */
	this.resetView = function(dontScroll) {
		var	levelKeys = Object.keys(mapConfig.levels),
			levelStorage = this.controller.getData("levels"),
			selectedLevel = this.controller.getData("selectedLevel"),
			level,
			levelViewData,
			i;

		i = 0;
		while((level = mapConfig.levels[levelKeys[i++]]) != null) {
			levelViewData = this.levelViews[level.id];
			levelViewData.locked = levelStorage.indexOf(level.id) === -1;
			levelViewData.lock.style.visible = levelViewData.locked;

			if (level.id === selectedLevel) {
				this.selectorKiwi.style.x = level.map.x + (level.map.iconWidth - KIWI_WIDTH) / 2;
				this.selectorKiwi.style.y = level.map.y + (level.map.iconHeight - KIWI_HEIGHT) * .65;
				this.kiwiPlacement = level.id;

				if( !dontScroll && this.controller.getData('valentinePromoShown') ){
					var off = this.getScrollOffset(level.id);

					this.scrollMap(off);
				}
			}
		}

		this.startLockShine();
	};

	this.scrollMap = function(x){
		if (x < -scrollMax) {
			x = -scrollMax;
		} else if (x > 0) {
			x = 0;
		}

		animate(this.scrollView._contentView)
		.then({x: x}, 1000, animate.easeOut);
	};

	this.getScrollOffset = function(id){
		var level = mapConfig.levels[id];
		var centerX = ( (level.map.x + level.map.iconWidth * 0.5) - MAP_SCROLL_WIDTH * 0.5 );

		if( centerX < 0 ){
			centerX = 0;
		}
		if( centerX > scrollMax ){
			centerX = scrollMax;
		}

		centerX -= ( UI_PADDING );
		return -centerX;
	};


	/**
	 * @method selectLevel
	 * @param value selects a level based on the level config id.
	 */
	this.selectLevel = function(id) {

		if (this.levelViews[id].locked) {
			this.mapPreviewModal.setData(this.controller.mapConfig.levels[id]);
			this.mapPreviewModal.openModal();
		} else if (this.kiwiPlacement !== id) {
			this.controller.setData("selectedLevel", id);

			this.startWobble();
			this.walk(id, id);
			this.scrollMap(this.getScrollOffset(id));
		}
	};

	/**
	 * Walks kiwi to destination and between all
	 * intermediary destinations.
	 * @method walk
	 * @param dest final dest id
	 */
	this.walk = function(dest, target) {
		var
			that = this,
			levelConfig = this.controller.mapConfig.levels,
			levelData = levelConfig[target],
			closest,
			required,
			target,
			i;

		this.controller.transitionLockDown(true);
		this.controller.blockInput();

		// find target
		// check for an edge between destinations
		/*if (levelData.require.indexOf(this.kiwiPlacement) !== -1 ||
			levelConfig[this.kiwiPlacement].require.indexOf(target) !== -1) {
			logger.log('walk to target: ' + target);*/

			// can walk directly
		this.animateWalk(target, function() {
			if (dest === target) {
				that.stopWobble();
				that.controller.transitionLockDown(false);
				that.controller.unblockInput();
			} else {
				that.walk(dest, dest);
			}
		});
		/*} else {
			// find closest node in the direction of the
			// target node
			if (levelData.map.x > levelConfig[this.kiwiPlacement].map.x) {
				i = 0;
				while((required = levelData.require[i++]) != null) {
					if (closest == null || levelConfig[required].map.x < levelConfig[closest].map.x) {
						closest = required;
					}
				}
			} else {
				i = 0;
				while((required = levelConfig[this.kiwiPlacement].require[i++]) != null) {
					if(closest == null || levelConfig[required].map.x > levelConfig[closest].map.x) {
						closest = required;
					}
				}
			}

			this.walk(dest, closest);
		}*/
	};

	/**
	 * Abstracts walk of kiwi from one destination to
	 * another.
	 * @method
	 */
	this.animateWalk = function(target, cb) {
		var
			that = this,
			levelConfig = this.controller.mapConfig.levels,
			originData = levelConfig[this.kiwiPlacement],
			levelData = levelConfig[target],
			visibleStartX = -this.scrollView.getOffset().x,
			visibleEndX = visibleStartX + MAP_SCROLL_WIDTH,
			destX = levelData.map.x + (levelData.map.iconWidth - KIWI_WIDTH) / 2,
			destY = levelData.map.y + (levelData.map.iconHeight - KIWI_HEIGHT) * .65,
			startX = this.selectorKiwi.style.x,
			startY = this.selectorKiwi.style.y,
			distX = destX - startX,
			distY = destY - startY,
			dist = Math.sqrt(distX * distX + distY * distY),
			dirX = distX / dist,
			dirY = distY / dist,
			t = dist / KIWI_SPEED * 1000;

		this.kiwiPlacement = target;

		// if both walking points are not on the visible map
		// then just continue without animating
		if ((levelData.map.x + levelData.map.iconWidth < visibleStartX ||
			levelData.map.x > visibleEndX) &&
			(originData.map.x + originData.map.iconWidth < visibleStartX ||
			originData.map.x > visibleEndX)) {

			this.selectorKiwi.style.x = destX;
			this.selectorKiwi.style.y = destY;

			cb();
			return;
		}

		// animate part way
		if (originData.map.x + originData.map.iconWidth < visibleStartX ||
			originData.map.x > visibleEndX) {
			var oldStartX = startX,

			startX = oldStartX > visibleEndX ? visibleEndX : visibleStartX - KIWI_WIDTH;
			startY = dirY * ((startX - oldStartX) / dirX) + startY;

			this.selectorKiwi.style.x = startX;
			this.selectorKiwi.style.y = startY;

			distX = destX - startX;
			distY = destY - startY;
			dist = Math.sqrt(distX * distX + distY * distY);
			t = dist / KIWI_SPEED * 1000;
		}

		this.selectorKiwi.style.flipX = startX > destX ? true : false;

		// walking animation
		animate(this.selectorKiwi).then({
			x: destX,
			y: destY
		}, t, animate.linear).wait().then(function() {
			cb();
		});
	};

	/**
	 * Wobbles between edge, starts going towards
	 * closest side.
	 * @method startWobble
	 */
	this.startWobble = function() {
		var
			that = this,
			currR = this.selectorKiwi.style.r,
			edge = Math.PI / 8,
			diff = edge - currR,
			dir = diff > edge ? 1 : -1,
			t = (dir === 1 ? diff : edge + currR) / WOBBLE_SPEED * 1000;

		this.wobbling = true;

		animate(this.selectorKiwi, 'rotate').then({
			r: edge * dir
		}, t, animate.linear).wait(WOBBLE_DELAY).then(function() {
			if (that.wobbling) {
				that.startWobble();
			}
		});
	};

	/**
	 * @method stopWobble
	 */
	this.stopWobble = function() {
		var
			that = this,
			diff = Math.abs(this.selectorKiwi.style.r),
			t = diff / WOBBLE_SPEED * 1000;

		this.wobbling = false;

		animate(this.selectorKiwi, 'rotate').now({
			r: 0
		}, t, animate.linear);
	};

	this.transitionToStore = function() {
		this.controller.storeMode = "BANK";
		this.controller.activeView.deconstructView(bind(this.controller, 'transitionToStore'));
	};

	/**
	 * @method purchaseLevel
	 * @id level id
	 */
	this.purchaseLevel = function(data) {

		if( this.controller.productController.purchaseLevel( data ) ){
			this.finishPurchase(data.id);
		}
		else{
			return this.mapPreviewModal.closeModal(bind(this, function() {
				new BinaryModalView({
					parent: this,
					message: "You need more gems! Buy some more?",
					onAccept: bind(this.controller, 'showGemStore'),
					onCancel: bind(this.mapPreviewModal, 'openModal')
				});
			}));
		}

		this.mapPreviewModal.closeModal();
	};

	/**
	 * @method startLockShine
	 */
	this.startLockShine = function() {
		if (this.lockShineTimeout == null && this.controller.activeView	=== this) {
			setTimeout(bind(this, this.doShine), Math.random() * (LOCK_DUR_MAX - LOCK_DUR_MIN) + LOCK_DUR_MIN);
		}
	};

	/**
	 * @method doShine
	 */
	this.doShine = function() {
		var
			that = this,
			keys = Object.keys(this.levelViews).filter(function(key) { return that.levelViews[key].locked; }),
			randomView = this.levelViews[keys[~~(Math.random() * keys.length)]];

		if (randomView != null) {
			randomView.lockShine.startAnimation("shine", {
				iterations: 1,
				callback: function() {
					that.lockShineTimeout = null;
					that.startLockShine();
				}
			});
		}
	};

	/**
	 * On a successful purchase callback called
	 * to update UI.
	 * @method finishPurchase
	 */
	this.finishPurchase = function(id) {
		this.resetView(true);
		var off = this.getScrollOffset(id);
		this.scrollMap(off);
		this.selectLevel(id);
	};

	/**
	 * @method constructView
	 */
	this.constructView = function() {
		this.controller.showHeader(true);

		if (!this.controller.getData('valentinePromoShown')) {
			this.controller.setData('valentinePromoShown', true);
			this.scrollMap(-this.levelViews.valentine.icon.style.x + 50);
		} else {
			this.scrollMap(-this.selectorKiwi.style.x + this.style.width / 2);
		}
	};

	/**
	 * @method deconstructView
	 */
	this.deconstructView = function(cb) {
		this.controller.hideHeader(true);
		cb();
	};
});
