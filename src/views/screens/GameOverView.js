import animate;
import ui.View as View;
import ...lib.TextView as TextView;
import ui.ImageView as ImageView;

import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;

import src.controllers.Controller as Controller;

import src.views.helpers.BerryCounter as BerryCounter;
import src.views.helpers.TapsTheMouse as TapsTheMouse;

exports = Class(View, function(supr) {

	var BG_WIDTH,
		BG_HEIGHT,
		LOW_SCORE_THRESHOLD,
		COL_LEFT,
		COL_RIGHT,
		LABEL_WIDTH = 250,
		LABEL_HEIGHT = 55,
		OFFSET_X = 70,
		OFFSET_Y = 60,
		TITLE_WIDTH = 500,
		TITLE_HEIGHT = 75,
		POST_WIDTH = 91,
		POST_HEIGHT = 445,
		POST_SPACING = 40,
		MENU_BUTTON_WIDTH = 234,
		MENU_BUTTON_HEIGHT = 107,
		MENU_LABEL_R = -Math.PI / 96,
		UPGRADES_BUTTON_WIDTH = 234,
		UPGRADES_BUTTON_HEIGHT = 107,
		GLOW_OFFSET = -2,
		GLOWPACITY_CHANGE = 0.02,
		RETRY_BUTTON_WIDTH = 234,
		RETRY_BUTTON_HEIGHT = 107,
		RETRY_LABEL_R = Math.PI / 80,
		NESTABLES_WIDTH = 480,
		NESTABLES_HEIGHT = 150,
		RADIAL_WIDTH = 240,
		RADIAL_HEIGHT = 240,
		RADIAL_SPIN_RATE = Math.PI / 2,
		ANIM_TIME = 400,
		UNLOCK_TIME = 600,
		UNLOCK_WAIT = 2000,
		MAX_NESTABLES = 5,
		UNLOCKABLE_SIZE = 200,
		REWARD_TYPE_BERRIES = "Berries",
		REWARD_TYPE_AVATAR = "Avatar",
		REWARD_TYPE_BABY = "Baby",
		REWARD_TYPE_RESOURCES = "Resources",
		FADE_RATE = 1000 / (UNLOCK_TIME / 4),
		SIGN_LABEL_X = 32,
		SIGN_LABEL_OFFSET = -9,
		UPGRADES_LABEL_OFFSET = 5,
		SCREEN_WIDTH = 681,
		SCREEN_HEIGHT = 629,
		MOUSE_WIDTH = 110,
		MOUSE_HEIGHT = 170,
		MOUSE_OFFSET = 96,
		LEVEL_STAR_WIDTH = 300,
		LEVEL_STAR_HEIGHT = 300,
		AD_FREQUENCY;

	// Z-INDICES
	var Z_NESTABLES = 30,
		Z_MOUSE = 35,
		Z_UNLOCKS = 40,
		oldXP,
		newXP,
		oldLevel,
		newLevel;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();

		var config = this.controller.config;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;
		COL_LEFT = BG_WIDTH / 2 - LABEL_WIDTH;
		COL_RIGHT = BG_WIDTH / 2 + LABEL_WIDTH;
		LOW_SCORE_THRESHOLD = config.game.model.lowScoreThreshold;

		AD_FREQUENCY = 5; //this.controller.checkExperiment("gemPriceAdFrequency")['adFrequency'];

		this.nestableViews = [];

		this.isMainView = true;
		this.showedLevelUp = false;

		this.designView();
	};

	this.designView = function() {
		var config = this.controller.config;

		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/GameOver/background.png"
		});

		this.buildScoreLayer();
		this.buildPrizeLayer();

		var defaultSignX = this.scoreScreen.style.x + this.scoreScreen.style.width;
		var tightSignX = BG_WIDTH + this.background.style.x - MENU_BUTTON_WIDTH;
		var signX = Math.min(defaultSignX, tightSignX);
		var diffSignX = tightSignX - defaultSignX;
		this.signPostParent = new View({
			parent: this.background,
			x: signX,
			y: BG_HEIGHT,
			width: MENU_BUTTON_WIDTH,
			height: POST_HEIGHT,
			canHandleEvents: false
		});

		var transitionToUpgrades = bind(this, function() {
			var gamesPlayed = this.controller.getData("gamesPlayed") || 0;
			var strGamesPlayed = gamesPlayed + ""; // convert to string
			this.controller.trackEvent("UpgradeStore", {
				gamesPlayed: strGamesPlayed,
				from: "GameOver"
			});

			this.deconstructView(bind(this.controller, 'transitionToUpgrades'));
		});

		this.sprTaps = new TapsTheMouse({
			parent: this.scoreScreen,
			x: this.scoreScreen.style.width - MOUSE_WIDTH + (signX == tightSignX ? diffSignX : 0),
			y: this.scoreScreen.style.height - MOUSE_HEIGHT - MOUSE_OFFSET,
			zIndex: Z_MOUSE
		});
		this.sprTaps.onInputStart = transitionToUpgrades;

		this.signPost = new ImageView({
			parent: this.signPostParent,
			x: (MENU_BUTTON_WIDTH - POST_WIDTH) / 2,
			y: 0,
			width: POST_WIDTH,
			height: POST_HEIGHT,
			image: "resources/images/GameOver/signPost.png",
			canHandleEvents: false
		});

		this.btnRetry = new ButtonView({
			parent: this.signPostParent,
			x: (POST_SPACING + MENU_BUTTON_WIDTH - RETRY_BUTTON_WIDTH) / 2,
			y: POST_SPACING,
			width: RETRY_BUTTON_WIDTH,
			height: RETRY_BUTTON_HEIGHT,
			image: "resources/images/GameOver/btnRetry.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.controller.showEquipModal();
			})
		});
		this.txtRetry = new TextView({
			parent: this.btnRetry,
			x: SIGN_LABEL_X + SIGN_LABEL_OFFSET + 16,
			y: POST_SPACING / 6,
			r: RETRY_LABEL_R,
			anchorX: (RETRY_BUTTON_WIDTH - 2 * SIGN_LABEL_X) / 2,
			anchorY: RETRY_BUTTON_HEIGHT / 2,
			width: RETRY_BUTTON_WIDTH - 2 * SIGN_LABEL_X - 32,
			height: RETRY_BUTTON_HEIGHT,
			text: GC.app.l.translate("PLAY!"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.btnUpgrades = new ButtonView({
			parent: this.signPostParent,
			x: (POST_SPACING / 2 + MENU_BUTTON_WIDTH - UPGRADES_BUTTON_WIDTH) / 2,
			y: RETRY_BUTTON_HEIGHT + 5 * POST_SPACING / 4,
			width: UPGRADES_BUTTON_WIDTH,
			height: UPGRADES_BUTTON_HEIGHT,
			image: "resources/images/GameOver/btnUpgrades.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: transitionToUpgrades
		});
		this.txtUpgrades = new TextView({
			parent: this.btnUpgrades,
			x: SIGN_LABEL_X + SIGN_LABEL_OFFSET + UPGRADES_LABEL_OFFSET,
			y: UPGRADES_LABEL_OFFSET,
			width: UPGRADES_BUTTON_WIDTH - 2 * SIGN_LABEL_X - 2 * UPGRADES_LABEL_OFFSET,
			height: UPGRADES_BUTTON_HEIGHT,
			text: GC.app.l.translate("UPGRADES"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});
		this.txtUpgradesGlow = new TextView({
			parent: this.btnUpgrades,
			x: SIGN_LABEL_X + SIGN_LABEL_OFFSET + UPGRADES_LABEL_OFFSET + GLOW_OFFSET,
			y: UPGRADES_LABEL_OFFSET + GLOW_OFFSET,
			width: UPGRADES_BUTTON_WIDTH - 2 * SIGN_LABEL_X - 2 * UPGRADES_LABEL_OFFSET,
			height: UPGRADES_BUTTON_HEIGHT,
			opacity: 0,
			text: GC.app.l.translate("UPGRADES"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSignGlow,
			multiline: false,
			canHandleEvents: false
		});

		this.btnMenu = new ButtonView({
			parent: this.signPostParent,
			x: 0,
			y: RETRY_BUTTON_HEIGHT + UPGRADES_BUTTON_HEIGHT + 3 * POST_SPACING / 2,
			width: MENU_BUTTON_WIDTH,
			height: MENU_BUTTON_HEIGHT,
			image: "resources/images/GameOver/btnMenu.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.deconstructView(bind(this.controller, 'transitionToMainMenu'));
			})
		});
		this.txtMenu = new TextView({
			parent: this.btnMenu,
			x: SIGN_LABEL_X + 30,
			y: -SIGN_LABEL_OFFSET,
			r: MENU_LABEL_R,
			anchorX: (MENU_BUTTON_WIDTH - 2 * SIGN_LABEL_X) / 2,
			anchorY: MENU_BUTTON_HEIGHT / 2,
			width: MENU_BUTTON_WIDTH - 2 * SIGN_LABEL_X - 60,
			height: MENU_BUTTON_HEIGHT,
			text: GC.app.l.translate("MENU"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.berryCounter = new BerryCounter({
			parent: this.scoreLayer,
			align: 'center'
		});
	};

	this.buildScoreLayer = function() {
		var config = this.controller.config;
		var model = this.controller.getGameModel();

		this.scoreLayer = new View({
			parent: this.background,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			visible: false
		});

		this.scoreScreen = new ImageView({
			parent: this.scoreLayer,
			x: (BG_WIDTH - SCREEN_WIDTH) / 2,
			y: 56,
			width: SCREEN_WIDTH,
			height: SCREEN_HEIGHT,
			image: "resources/images/GameOver/screen.png",
			canHandleEvents: false
		});

		this.txtTitle = new TextView({
			parent: this.scoreScreen,
			x: (SCREEN_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.gameoverTitle,
			color: config.colors.gameoverTitleFill,
			strokeColor: config.colors.gameoverTitleStroke,
			text: "",
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});

		this.txtScoreLabel = new TextView({
			parent: this.scoreScreen,
			x: OFFSET_X,
			y: OFFSET_Y + TITLE_HEIGHT,
			width: LABEL_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: GC.app.l.translate("SCORE:"),
			textAlign: 'left',
			multiline: false,
			canHandleEvents: false
		});
		this.txtScore = new TextView({
			parent: this.scoreScreen,
			x: this.scoreScreen.style.width - OFFSET_X - LABEL_WIDTH,
			y: OFFSET_Y + TITLE_HEIGHT,
			width: LABEL_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: "",
			textAlign: 'right',
			multiline: false,
			canHandleEvents: false
		});

		this.txtBerriesLabel = new TextView({
			parent: this.scoreScreen,
			x: OFFSET_X,
			y: OFFSET_Y + TITLE_HEIGHT + LABEL_HEIGHT,
			width: LABEL_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: GC.app.l.translate("BERRIES:"),
			textAlign: 'left',
			multiline: false,
			canHandleEvents: false
		});
		this.txtBerries = new TextView({
			parent: this.scoreScreen,
			x: this.scoreScreen.style.width - OFFSET_X - LABEL_WIDTH,
			y: OFFSET_Y + TITLE_HEIGHT + LABEL_HEIGHT,
			width: LABEL_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: "",
			textAlign: 'right',
			multiline: false,
			canHandleEvents: false
		});

		this.txtMetersLabel = new TextView({
			parent: this.scoreScreen,
			x: OFFSET_X,
			y: OFFSET_Y + TITLE_HEIGHT + 2 * LABEL_HEIGHT,
			width: LABEL_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: GC.app.l.translate("DISTANCE:"),
			textAlign: 'left',
			multiline: false,
			canHandleEvents: false
		});
		this.txtMeters = new TextView({
			parent: this.scoreScreen,
			x: this.scoreScreen.style.width - OFFSET_X - LABEL_WIDTH,
			y: OFFSET_Y + TITLE_HEIGHT + 2 * LABEL_HEIGHT,
			width: LABEL_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: "",
			textAlign: 'right',
			multiline: false,
			canHandleEvents: false
		});

		this.txtNestablesLabel = new TextView({
			parent: this.scoreScreen,
			x: (SCREEN_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y + TITLE_HEIGHT + 3.3 * LABEL_HEIGHT,
			width: TITLE_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverHeader,
			color: config.colors.gameoverHeaderFill,
			strokeColor: config.colors.gameoverHeaderStroke,
			text: GC.app.l.translate("NESTABLES"),
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});
		this.txtNestables = new TextView({
			parent: this.scoreScreen,
			x: (SCREEN_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y + TITLE_HEIGHT + 4.5 * LABEL_HEIGHT + NESTABLES_HEIGHT - 65,
			width: TITLE_WIDTH,
			height: LABEL_HEIGHT,
			size: config.fonts.gameoverInfo,
			color: config.colors.gameoverInfo,
			text: "",
			textAlign: 'center',
			multiline: false,
			canHandleEvents: false
		});
	};

	this.buildPrizeLayer = function() {
		var config = this.controller.config;

		this.prizeLayer = new View({
			parent: this.background,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			visible: false
		});

		this.txtPrizeTitle = new TextView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.gameoverPrize,
			color: config.colors.gameoverPrizeFill,
			strokeColor: config.colors.gameoverPrizeStroke,
			text: GC.app.l.translate("NESTABLE SET COMPLETE!"),
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.txtLevelTitle = new TextView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.gameoverPrize,
			color: config.colors.gameoverPrizeFill,
			strokeColor: config.colors.gameoverPrizeStroke,
			text: GC.app.l.translate("LEVEL UP!"),
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.txtPrizeBanner = new TextView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y + TITLE_HEIGHT,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.gameoverPrize,
			color: config.colors.gameoverPrizeFill,
			strokeColor: config.colors.gameoverPrizeStroke,
			text: GC.app.l.translate("YOU UNLOCKED"),
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.txtLevelBanner = new TextView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - TITLE_WIDTH) / 2,
			y: OFFSET_Y + TITLE_HEIGHT,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.gameoverPrize,
			color: config.colors.gameoverPrizeFill,
			strokeColor: config.colors.gameoverPrizeStroke,
			text: GC.app.l.translate("CONGRATULATIONS!"),
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.unlockParticles = new ParticleEngine({
			parent: this.prizeLayer,
			x: BG_WIDTH / 2,
			y: BG_HEIGHT / 2,
			width: 1,
			height: 1
		});

		this.rewardRadial = new ImageView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - 2 * UNLOCKABLE_SIZE) / 2,
			y: (BG_HEIGHT - 2 * UNLOCKABLE_SIZE) / 2,
			anchorX: UNLOCKABLE_SIZE,
			anchorY: UNLOCKABLE_SIZE,
			width: 2 * UNLOCKABLE_SIZE,
			height: 2 * UNLOCKABLE_SIZE,
			image: "resources/images/Game/itemRadial.png"
		});

		this.rewardView = new ImageView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - UNLOCKABLE_SIZE) / 2,
			y: (BG_HEIGHT - UNLOCKABLE_SIZE) / 2,
			width: UNLOCKABLE_SIZE,
			height: UNLOCKABLE_SIZE,
			image: "resources/images/Game/itemBerryBlue.png",
			visible: false
		});

		this.levelUpStar = new ImageView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - LEVEL_STAR_WIDTH) / 2,
			y: (BG_HEIGHT - LEVEL_STAR_HEIGHT) / 2,
			width: LEVEL_STAR_WIDTH,
			height: LEVEL_STAR_HEIGHT,
			image: "resources/images/GameOver/levelUpStar.png",
			canHandleEvents: false,
			visible: false
		});
		this.levelUpText = new TextView({
			parent: this.levelUpStar,
			x: 0,
			y: 20,
			width: LEVEL_STAR_WIDTH,
			height: LEVEL_STAR_HEIGHT,
			text: this.controller.getData("level"),
			textAlign: 'center',
			size: config.fonts.levelUpStar,
			color: config.colors.levelUpStar,
			strokeColor: config.colors.levelUpStarStroke,
			multiline: false,
			canHandleEvents: false
		});

		this.txtPrizeName = new TextView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - TITLE_WIDTH) / 2,
			y: this.rewardView.style.y + this.rewardView.style.width,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			size: config.fonts.gameoverPrize,
			color: config.colors.gameoverPrizeFill,
			strokeColor: config.colors.gameoverPrizeStroke,
			text: "",
			textAlign: "center",
			multiline: false,
			canHandleEvents: false
		});

		this.fullscreenFade = new ImageView({
			parent: this.prizeLayer,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			backgroundColor: "rgb(255, 255, 255)",
			visible: false,
			opacity: 0
		});

		this.fullscreenRadial = new ImageView({
			parent: this.prizeLayer,
			x: (BG_WIDTH - UNLOCKABLE_SIZE) / 2,
			y: (BG_HEIGHT - UNLOCKABLE_SIZE) / 2,
			anchorX: UNLOCKABLE_SIZE / 2,
			anchorY: UNLOCKABLE_SIZE / 2,
			width: UNLOCKABLE_SIZE,
			height: UNLOCKABLE_SIZE,
			image: "resources/images/Game/itemRadial.png"
		});

		// particle 3D Simulation binding
		this.boundParticle3DSimManager = bind(this, function(particle) {
			var view;
			//if (this.runningLevelUp) {
				//view = this.levelUpStar;
			//} else {
				view = this.rewardView;
			//}
			var style = view.style;

			// if the particle is falling and not already on top of the prize
			particle.removeFromSuperview();
			particle.style.x += style.height / 2;
			particle.style.y += style.width / 2;
			view.addSubview(particle);
		});

		// clean up after the first binding ^
		this.boundParticleDeathManager = bind(this, function(particle) {
			particle.removeFromSuperview();
			this.unlockParticles.addSubview(particle);
		});
	};

	this.resetView = function() {
		this.controller.setHardwareBackButton(false);

		// reset flags
		this.runningUnlockAnimation = false;
		this.runningLevelUp = false;

		this.txtLevelTitle.style.visible = false;
		this.txtLevelBanner.style.visible = false;
		this.txtPrizeTitle.style.visible = false;
		this.txtPrizeBanner.style.visible = false;
		this.txtPrizeName.style.visible = false;
		this.rewardView.style.visible = false;
		this.levelUpStar.style.visible = false;

		// setup stats values and text
		var model = this.controller.getGameModel();
		var score = ~~model.score;
		var newBerries = ~~model.coinsCollected;
		var distance = model.getDisplayDistance();
		var time = model.elapsed;
		var gamesPlayed = this.controller.getData("gamesPlayed") || 0;
		var strGamesPlayed = gamesPlayed + ""; // convert to string

		var XP = this.controller.getData("XP");
		oldXP = XP;

		XP += distance;

		var level = this.controller.getData("level");
		oldLevel = level;

		var nextLevel = this.controller.getLevelXP(level + 1);
		this.levelUpCount = 0;

		this.controller.header.refresh();

		if (XP > nextLevel) {
			level++;

			var currentLevel = this.controller.getLevelXP(level);
			nextLevel = this.controller.getLevelXP(level + 1);

			if( XP - currentLevel > (nextLevel - currentLevel) * 0.5){
				XP = currentLevel + (nextLevel - currentLevel) * 0.5;
			}

			this.runningLevelUp = true;
			this.showedLevelUp = false;
			this.levelUpCount++;

			this.controller.trackEvent('LevelUp', {'gamesPlayed': strGamesPlayed});

			if (level === 2) {
				var babyData = this.controller.babyConfig,
					baby = babyData["babyTeal/babyTeal"],
					boughtBabies = this.controller.getData("babies");

				boughtBabies[boughtBabies.length] = baby.id;
				this.controller.setData("babies", boughtBabies);
				this.controller.setData("selectedBabies", [baby.id]);
			}
		}
		newXP = XP;
		newLevel = level;

		this.controller.setData("level", level);
		this.controller.setData("XP", XP);

		if (this.levelUpCount) {
			this.levelUpRewards = this.controller.rewardLevelUp(level - this.levelUpCount, level);
		} else {
			this.levelUpRewards = false;
		}

		this.newBerries = newBerries;
		this.prizeBerries = 0;

		// update berries collected for achievements
		var berriesCollected = this.controller.getData("berriesCollected");
		this.controller.berriesCollected = berriesCollected = berriesCollected + newBerries;
		this.controller.setData("berriesCollected", berriesCollected);



		this.controller.trackEvent("GameOverNew", {
			gamesPlayed: strGamesPlayed,
			score: (~~(score / 1000) * 1000) + "",
			berries: (~~(newBerries / 100) * 100) + "",
			distance: (~~(distance / 100) * 100) + "",
			time: (~~(time / 60000)) + ""
		});

		if (gamesPlayed == 1) {
			this.controller.trackEvent("Run1", {
				score: (~~(score / 1000) * 1000) + "",
				berries: (~~(newBerries / 100) * 100) + "",
				distance: (~~(distance / 100) * 100) + "",
				time: (~~(time / 60000)) + ""
			});
		} else if (gamesPlayed == 5) {
			this.controller.trackEvent("Run5", {
				score: (~~(score / 1000) * 1000) + "",
				berries: (~~(newBerries / 100) * 100) + "",
				distance: (~~(distance / 100) * 100) + "",
				time: (~~(time / 60000)) + ""
			});
		} else if (gamesPlayed == 10) {
			this.controller.trackEvent("Run10", {
				score: (~~(score / 1000) * 1000) + "",
				berries: (~~(newBerries / 100) * 100) + "",
				distance: (~~(distance / 100) * 100) + "",
				time: (~~(time / 60000)) + ""
			});
		}

		// update before we save new berries, so we can animate them
		this.berryCounter.update();

		var currentBerries = this.controller.getData("berries");
		this.controller.setData("berries", currentBerries + newBerries);

		this.txtScore.setText(score);
		this.txtBerries.setText(newBerries);
		this.txtMeters.setText(distance + "m");
		this.txtNestables.setText(GC.app.l.translate(model.nestableSet.name));

		var congratsText = "GREAT JOB!";
		if (score >= this.controller.getData("topScores")[0]){
			if (GC.app.enableSocial) {
				this.controller.fb.postScore(score);
			}
			congratsText = "NEW HISCORE!";
		} else if (score < LOW_SCORE_THRESHOLD) {
			congratsText = "TRY AGAIN!"
		}
		this.txtTitle.setText(GC.app.l.translate(congratsText));

		// reset animated parts
		this.signPostParent.style.y = BG_HEIGHT;

		this.resetNestables();

		if (this.runningUnlockAnimation || this.runningLevelUp) {
			this.prizeLayer.style.visible = true;
			this.scoreLayer.style.visible = false;
		} else {
			this.prizeLayer.style.visible = false;
			this.scoreLayer.style.visible = true;
		}

		this.controller.pauseSong();
		this.controller.playSong("run_win_music");
	};

	this.resetNestables = function() {
		this.runningUnlockAnimation = false;
		this.nestableViews = [];

		if (this.nestableLayer) {
			this.nestableLayer.removeFromSuperview();
		}

		this.nestableLayer = new View({
			parent: this.scoreScreen,
			x: (SCREEN_WIDTH - NESTABLES_WIDTH) / 2,
			y: OFFSET_Y + TITLE_HEIGHT + 4.5 * LABEL_HEIGHT - 38,
			width: NESTABLES_WIDTH,
			height: NESTABLES_HEIGHT,
			zIndex: Z_NESTABLES
		});
		this.nestableLayer.onInputStart = bind(this, function() {
			this.controller.playSound("ui_click");
			this.deconstructView(bind(this.controller, 'transitionToNestables'));
		});

		var model = this.controller.getGameModel();
		var nestableSet = model.nestableSet;
		var newNestables = model.collectedNestables;

		if (nestableSet) {
			var allCollected = this.controller.getData("nestablesCollected") || {};
			var setCollection = allCollected[nestableSet.id] || {};

			this.reward = nestableSet.reward;

			var nestableCount = 0;
			for (var i in nestableSet.pieces) {
				nestableCount++;
			};

			// determine rewards with these
			var brandNewCount = 0;
			var collectedCount = 0;

			var index = 0;
			var pieceWidth = NESTABLES_WIDTH / nestableCount;
			var pieceHeight = pieceWidth;
			var x = 0;
			var y = (NESTABLES_HEIGHT - pieceHeight) / 2;
			for (var i in nestableSet.pieces) {
				var nestable = nestableSet.pieces[i];

				// has the item been collected?
				if (setCollection && setCollection[i]) {
					var owned = true;
					collectedCount++;
				} else {
					var owned = false;
				}

				// was the item collected last round?
				if (newNestables && newNestables[i]) {
					var brandNew = true;
					brandNewCount++;
				} else {
					var brandNew = false;
				}

				if (owned) {
					var imgNestable = nestable.image;
					var opNestable = 1;
				} else {
					var imgNestable = nestable.shadow;
					var opNestable = 0.5;
				}

				var nestableView = new ImageView({
					parent: this.nestableLayer,
					x: x,
					y: y,
					width: pieceWidth,
					height: pieceHeight,
					image: imgNestable,
					opacity: opNestable,
					canHandleEvents: false
				});
				this.nestableViews[index] = nestableView;

				x += pieceWidth;
				index++;
			}

			// reward the player if they just completed their set
			if (collectedCount >= index && brandNewCount && !setCollection.unlocked) {
				setCollection.unlocked = true;
				allCollected[nestableSet.id] = setCollection;
				this.controller.setData("nestablesCollected", allCollected);

				this.runningUnlockAnimation = true;
			}
		}
	};

	this.animateReward = function() {
		this.sprayUnlockParticles = true;

		this.showPrizeLayer();

		this.txtLevelTitle.style.visible = false;
		this.txtLevelBanner.style.visible = false;
		this.txtPrizeTitle.style.visible = true;
		this.txtPrizeBanner.style.visible = true;
		this.txtPrizeName.style.visible = true;

		this.rewardView.style.visible = false;
		this.levelUpStar.style.visible = false;
		this.fullscreenRadial.style.visible = true;
		this.fullscreenRadial.style.opacity = 0;
		this.fullscreenRadial.style.scale = 0.01;

		if (this.runningUnlockAnimation) {
			this.txtPrizeTitle.setText(GC.app.l.translate("NESTABLE SET COMPLETE!"));
			this.txtPrizeBanner.setText(GC.app.l.translate("YOU UNLOCKED"));
			this.txtPrizeName.setText(GC.app.l.translate(this.reward.name, this.reward.value));
		} else {
			var nr = this.nextReward;
			if (!nr && this.levelUpRewards) {
				nr = this.nextReward = {
					type: REWARD_TYPE_RESOURCES,
					gems: this.levelUpRewards.gems,
					berries: this.levelUpRewards.berries,
					width: 188,
					height: 188,
					image: "resources/images/GameOver/rewardResources.png"
				};
			} else if (!nr) {
				// TODO: FIXME avoid crashes for now
				logger.log("~~~~ Reward logic error: bailing gracefully");
				this.showScoreLayer();
				return;
			}

			var title = "";
			var banner = "";
			var name = "";
			var type = nr.type;

			if (type === REWARD_TYPE_BABY) {
				title = GC.app.l.translate("NEW BABY AVAILABLE!");
				banner = GC.app.l.translate(nr.name) + "!";
			} else if (type === REWARD_TYPE_RESOURCES) {
				title = GC.app.l.translate("LEVEL UP BONUS!");
				banner = GC.app.l.translate("Gems") + ": " + nr.gems;
				name = GC.app.l.translate("Berries") + ": " + nr.berries;
				this.prizeBerries += nr.berries;
				var currBerries = this.controller.getData("berries");
				this.controller.setData("berries", nr.berries + currBerries);
				var currGems = this.controller.getData("gems");
				this.controller.setData("gems", nr.gems + currGems);
				if(!this.showedLevelUp){
					this.controller.header.animateLevelUp(oldXP, newXP, oldLevel, newLevel);
					this.showedLevelUp = true;
				}
			}

			this.txtPrizeTitle.setText(title);
			this.txtPrizeBanner.setText(banner);
			this.txtPrizeName.setText(name);
		}

		// grow radial over screen
		animate(this.fullscreenRadial)
		.now({ dr: Math.PI, opacity: 1, scale: 1 }, UNLOCK_TIME, animate.easeOut)
		.then({ dr: Math.PI, scale: 8 }, UNLOCK_TIME, animate.easeIn)
		.then(bind(this, function() {
			this.fullscreenFade.style.visible = true;
			this.fullscreenFadeUp = true;
		}))
		.then({ dr: Math.PI, scale: 16 }, UNLOCK_TIME, animate.easeOut)
		.then(bind(this, function() {
			this.fullscreenFadeUp = false;
			this.fullscreenFadeDown = true;
			this.showReward();
		}))
		.then({ opacity: 0 }, UNLOCK_TIME)
		.then(bind(this, function() {
			this.fullscreenFadeDown = false;
			this.fullscreenFade.style.visible = false;
			this.fullscreenRadial.style.visible = false;
		}))
		.wait(UNLOCK_WAIT)
		.then(bind(this, function() {

			var newReward = false;
			if( this.runningUnlockAnimation ){
				this.runningUnlockAnimation = false;

				if( this.levelUpRewards ){
					newReward = true;
				}
			}
			else{
				if( this.levelUpRewards ){
					if (this.levelUpRewards.prizes.length) {
						var prize = this.levelUpRewards.prizes.pop();
						this.nextReward = {
							name: prize.name,
							type: prize.type,
							value: prize.value,
							width: prize.width,
							height: prize.height,
							image: prize.image
						};

						newReward = true;
					}
				}
			}

			if( newReward ){
				this.animateReward();
			}
			else{
				this.nextReward = undefined;
				this.reward = undefined;
				this.levelUpRewards = undefined;
				this.transitionToScoreLayer();
			}
		}));
	};

	this.showReward = function() {
			var reward = this.nextReward || this.reward;
			var name = reward.name;
			var type = reward.type;
			var value = reward.value;
			var gems = reward.gems;
			var berries = reward.berries;
			var width = reward.width;
			var height = reward.height;
			var image = reward.image;

			// reposition
			this.rewardView.style.x = (BG_WIDTH - width) / 2;
			this.rewardView.style.y = (BG_HEIGHT - height) / 2;
			this.rewardView.style.width = width;
			this.rewardView.style.height = height;
			this.rewardView.style.visible = true;
			this.rewardView.setImage(image);

			// save reward
			if (type == REWARD_TYPE_BERRIES) {
				this.prizeBerries += value;
				var currBerries = this.controller.getData("berries");
				currBerries += value;
				this.controller.setData("berries", currBerries);
			} else if (type == REWARD_TYPE_AVATAR) {
				this.controller.addAvatar(value);
			}
		//}
	};

	this.constructView = function() {
		this.controller.header.dontShowTimer = true;
		this.controller.showHeader(true, null, true);
		this.controller.blockInput();
		if (!this.runningUnlockAnimation && !this.runningLevelUp) {
			this.showScoreLayer();
		} else {
			this.animateReward();
		}
	};

	this.showScoreLayer = function() {
		var gamesPlayed = this.controller.getData("gamesPlayed") || 0,
			level = this.controller.getData("level");
		if (this.controller.showAds && GC.app.enableAds && gamesPlayed % AD_FREQUENCY === 0) {
			GLOBAL.NATIVE && NATIVE.ads.showInterstitial();
			jsio('import src.views.modals.RemoveAdsModalView as RemoveAdsModalView');
			new RemoveAdsModalView({parent: this});
		} else if (level >= 5) {
			// trackSessionM logic allows these through only once
			this.controller.trackSessionM("level_5");
		} else if (level >= 3) {
			this.controller.trackSessionM("level_3");
		}

		this.txtUpgradesGlow.style.opacity = 0;
		this.glowpacityChange = GLOWPACITY_CHANGE;
		if (this.controller.canPurchaseUpgrade()) {
			this.upgradeButtonGlowing = true;
			this.sprTaps.setPointing(true);
		} else {
			this.upgradeButtonGlowing = false;
			this.sprTaps.setPointing(false);
		}

		this.prizeLayer.style.visible = false;
		this.scoreLayer.style.visible = true;

		this.berryCounter.addBerries(this.newBerries + this.prizeBerries);

		this.berryCounter.slideUp();
		animate(this.signPostParent).now({ dy: -POST_HEIGHT + POST_SPACING }, ANIM_TIME, animate.easeOut)
		.then(bind(this, function() {
			this.controller.transitionLockDown(false);
			if( !this.showedLevelUp ){
				this.controller.header.animateLevelUp(oldXP, newXP, oldLevel, newLevel);
			}
			else{
				this.showedLevelUp = false;
			}
			this.controller.unblockInput();

			//Only does something if a purchase returned from the play store
			this.controller.showPurchaseSuccessNotification();
		}));
	};

	this.showPrizeLayer = function() {
		this.prizeLayer.style.opacity = 1;
		this.prizeLayer.style.visible = true;
		this.scoreLayer.style.visible = false;
	};

	this.transitionToScoreLayer = function() {
		this.scoreLayer.style.opacity = 0;

		animate(this.prizeLayer).now({ opacity: 0 }, ANIM_TIME, animate.linear)
		.then(bind(this, function() {
			this.sprayUnlockParticles = false; // stop particle emission here
			this.prizeLayer.style.visible = false;
			this.scoreLayer.style.visible = true;

			animate(this.scoreLayer).now({ opacity: 1 }, ANIM_TIME, animate.linear)
			.then(bind(this, function() {
				this.showScoreLayer();
			}));
		}));
	};

	this.deconstructView = function(callback) {
		this.controller.header.dontShowTimer = false;
		this.controller.hideHeader(true);

		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			this.controller.blockInput();
			this.berryCounter.slideDown();

			animate(this.signPostParent).now({ dy: POST_HEIGHT - POST_SPACING }, ANIM_TIME, animate.easeIn)
			.then(bind(this, function() {
				this.controller.unblockInput();
				callback();
			}));
		}
	};

	this.stopAnimations = function() {
		this.sprTaps.stopAnimation();
		this.sprTaps.style.visible = true;
	};

	this.tick = function(dt) {
		if (!this.style.visible) { return; }

		if( dt > 100 ){
			dt = 100;
		}

		this.unlockParticles.runTick(dt);

		if (this.upgradeButtonGlowing) {
			var s = this.txtUpgradesGlow.style;
			s.opacity += this.glowpacityChange;
			if (s.opacity >= 1) {
				s.opacity = 1;
				this.glowpacityChange = -this.glowpacityChange;
			} else if (s.opacity < 0) {
				s.opacity = 0;
				this.glowpacityChange = -this.glowpacityChange;
			}
		}

		// handle special effects
		if (this.sprayUnlockParticles && Math.random() < 0.6) {
			var pct = dt / 1000;
			var data = this.unlockParticles.obtainParticleArray(1);
			var pObj = data[0];
			var ttl = 3000;
			var width = Math.random() * 30 + 10;
			var height = width;

			if (!this.rewardView.style.visible
				&& !this.levelUpStar.style.visible)
			{
				ttl = 750;
			}

			if (this.rewardRadial) {
				this.rewardRadial.style.r += RADIAL_SPIN_RATE * pct;
			}

			// handle fullscreen fade effect
			if (this.fullscreenFadeUp) {
				this.fullscreenFade.style.opacity = Math.min(this.fullscreenFade.style.opacity + pct * FADE_RATE, 1);
			} else if (this.fullscreenFadeDown) {
				this.fullscreenFade.style.opacity = Math.max(this.fullscreenFade.style.opacity - pct * FADE_RATE, 0);
			} else {
				this.fullscreenFade.style.opacity = 0;
			}

			// throw a particle out!
			pObj.width = width;
			pObj.height = height;
			pObj.dx = Math.random() * 600 - 300;
			pObj.dy = -Math.random() * 400 - 500;
			pObj.ddy = 1600;
			pObj.dscale = 2 * Math.random() - 1;
			pObj.dr = 2 * Math.PI * Math.random() - Math.PI;
			pObj.ttl = ttl;
			pObj.image = "resources/images/Game/itemSparkle.png";
			pObj.onDeath = this.boundParticleDeathManager;
			pObj.triggers.push({
				property: 'dy',
				value: 0,
				count: 1,
				action: this.boundParticle3DSimManager
			});

			this.unlockParticles.emitParticles(data);
		}
	};

});
