import animate;
import ui.View as View;
import ...lib.TextView as TextView;
import ui.ImageView as ImageView;

import src.lib.ListView as ListView;
import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;
import src.lib.InputExpander as InputExpander;

import src.controllers.Controller as Controller;

import src.views.helpers.UpgradesNodeView as UpgradesNodeView;
import src.views.helpers.StoreNodeTopView as StoreNodeTopView;
import src.views.helpers.StoreNodeBottomView as StoreNodeBottomView;
import src.views.helpers.TapsTheMouse as TapsTheMouse;
import src.views.helpers.WhistlesTheTrainer as WhistlesTheTrainer;

import src.views.helpers.BerryCounter as BerryCounter;

exports = Class(View, function(supr) {

	var BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		STORE_BASE_WIDTH = 959,
		STORE_BASE_HEIGHT = 181,
		STICK_WIDTH = 70,
		STICK_HEIGHT = 545,
		STICK_OFFSET_Y = 26,
		STORE_TOP_WIDTH = 851,
		STORE_TOP_HEIGHT = 101,
		NODE_WIDTH = 750,
		NODE_HEIGHT = 180,
		LIST_WIDTH = NODE_WIDTH,
		LIST_HEIGHT = STICK_HEIGHT,
		CLIPPING_Y = BG_HEIGHT - STORE_BASE_HEIGHT - STICK_HEIGHT + STICK_OFFSET_Y,
		CLIPPING_WIDTH = NODE_WIDTH,
		CLIPPING_HEIGHT = STICK_HEIGHT,
		SLIDE_TIME = 600,
		NODE_BOTTOM_WIDTH = NODE_WIDTH,
		NODE_BOTTOM_HEIGHT = 480,
		SECTION_WIDTH = NODE_WIDTH,
		SECTION_HEIGHT = NODE_HEIGHT / 3,
		BACK_WIDTH = 166,
		BACK_HEIGHT = 226,
		BACK_LABEL_Y = 56,
		BACK_LABEL_R = -Math.PI / 18,
		SWAP_WIDTH = 186,
		SWAP_HEIGHT = 205,
		SWAP_LABEL_X = 26,
		SWAP_LABEL_Y = 20,
		SWAP_LABEL_R = Math.PI / 17,
		LABEL_HEIGHT = 50,
		SIGN_OFFSET = 16,
		TITLE_WIDTH = 319,
		TITLE_HEIGHT = 100,
		SPACING = 30,
		CLOSED_UP_OFFSET = -1.6 * NODE_BOTTOM_HEIGHT,
		MOUSE_WIDTH = 110,
		MOUSE_HEIGHT = 170,
		TRAINER_HEIGHT = 200,
		EXTRA_INPUT = 42,
		GRAVITY,

		config,
		upgradesConfig,
		upgradesOrder,
		storeConfig;

	var TYPE_UPGRADE = "UPGRADE",
		TYPE_AVATAR = "AVATAR",
		TYPE_CONSUMABLE = "CONSUMABLE";

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;
		upgradesConfig = this.controller.upgradesConfig;
		upgradesOrder = this.controller.upgradesOrder;
		storeConfig = this.controller.storeConfig;
		GRAVITY = config.game.model.gravity;

		this.goToBerryStore = bind(this, this.goToBerryStore);
		this.goToGemStore = bind(this, this.goToGemStore);
		this.onModalClose = bind(this, this.onModalClose);

		this.isMainView = true;

		this.designView();
	};

	this.designView = function() {
		var config = this.controller.config;

		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/Store/background.png",
			canHandleEvents: false
		});

		// this layer clips the scrolling elements
		this.clippingLayer = new View({
			parent: this,
			x: (this.style.width - CLIPPING_WIDTH) / 2,
			y: CLIPPING_Y,
			width: CLIPPING_WIDTH,
			height: CLIPPING_HEIGHT,
			clip: true,
			canHandleEvents: false
		});

		// this layer slides the list view and store canvas up and down for transitions
		this.slidingLayer = new View({
			parent: this.clippingLayer,
			x: 0,
			y: CLOSED_UP_OFFSET,
			width: CLIPPING_WIDTH,
			height: CLIPPING_HEIGHT + NODE_BOTTOM_HEIGHT,
			canHandleEvents: false
		});

		this.upgradesList = new ListView({
			parent: this.slidingLayer,
			x: 0,
			y: this.slidingLayer.style.height - NODE_BOTTOM_HEIGHT - LIST_HEIGHT,
			width: LIST_WIDTH,
			height: LIST_HEIGHT,
			nodeCtor: UpgradesNodeView,
			nodeHeight: NODE_HEIGHT,
			updateNode: function(node, data) {
				node.updateData(data);
			}
		});
		this.upgradesList.addFixedTopNode({
			ctor: StoreNodeTopView,
			height: NODE_HEIGHT / 4
		});
		this.upgradesList.addFixedBottomNode({
			ctor: StoreNodeBottomView,
			height: 2 * NODE_HEIGHT / 3
		});

		this.nodeBottom = new ImageView({
			parent: this.slidingLayer,
			x: 0,
			y: this.slidingLayer.style.height - NODE_BOTTOM_HEIGHT,
			width: NODE_BOTTOM_WIDTH,
			height: NODE_BOTTOM_HEIGHT,
			image: "resources/images/Store/nodeBottom.png",
			canHandleEvents: false
		});

		this.stickLeft = new ImageView({
			parent: this,
			x: (this.style.width - CLIPPING_WIDTH - STICK_WIDTH / 2) / 2,
			y: this.style.height - STORE_BASE_HEIGHT - STICK_HEIGHT + STICK_OFFSET_Y,
			width: STICK_WIDTH,
			height: STICK_HEIGHT,
			image: "resources/images/Store/stickLeft.png",
			canHandleEvents: false
		});
		this.stickRight = new ImageView({
			parent: this,
			x: (this.style.width + CLIPPING_WIDTH - STICK_WIDTH) / 2,
			y: this.style.height - STORE_BASE_HEIGHT - STICK_HEIGHT + STICK_OFFSET_Y,
			width: STICK_WIDTH,
			height: STICK_HEIGHT,
			image: "resources/images/Store/stickRight.png",
			canHandleEvents: false
		});

		this.sprTrainer = new WhistlesTheTrainer({
			parent: this,
			x: (this.style.width - STORE_BASE_WIDTH) / 2,
			y: this.style.height - STORE_BASE_HEIGHT - 9 * TRAINER_HEIGHT / 10
		});

		this.storeBase = new ImageView({
			parent: this,
			x: (this.style.width - STORE_BASE_WIDTH) / 2,
			y: this.style.height - STORE_BASE_HEIGHT,
			width: STORE_BASE_WIDTH,
			height: STORE_BASE_HEIGHT,
			image: "resources/images/Store/storeBase.png",
			canHandleEvents: false
		});
		this.storeTop = new ImageView({
			parent: this,
			x: (this.style.width - STORE_TOP_WIDTH) / 2,
			y: this.style.height - STORE_BASE_HEIGHT - STICK_HEIGHT + STICK_OFFSET_Y,
			width: STORE_TOP_WIDTH,
			height: STORE_TOP_HEIGHT,
			image: "resources/images/Store/storeTop.png",
			canHandleEvents: false
		});

		var mouseX = this.style.width - SIGN_OFFSET - SWAP_WIDTH - MOUSE_WIDTH;
		var mouseMinX = this.storeBase.style.x + this.storeBase.style.width - SWAP_WIDTH - MOUSE_WIDTH;
		if (mouseX < mouseMinX) {
			mouseX = mouseMinX;
		}
		this.sprTaps = new TapsTheMouse({
			parent: this,
			x: mouseX,
			y: this.style.height - STORE_BASE_HEIGHT - 3 * MOUSE_HEIGHT / 4 - SPACING / 3,
			canHandleEvents: false
		});

		this.titleSign = new ImageView({
			parent: this.storeBase,
			x: (this.storeBase.style.width - TITLE_WIDTH) / 2,
			y: -TITLE_HEIGHT / 4,
			anchorX: TITLE_WIDTH / 2,
			anchorY: TITLE_HEIGHT / 2,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			image: "resources/images/Store/storeSign.png",
			canHandleEvents: false
		});

		this.txtTitleSign = new TextView({
			parent: this.titleSign,
			x: SPACING,
			y: 0,
			width: TITLE_WIDTH - 2 * SPACING,
			height: TITLE_HEIGHT,
			text: GC.app.l.translate("UPGRADES"),
			textAlign: 'center',
			size: config.fonts.storeSign,
			color: config.colors.storeSign,
			multiline: false,
			canHandleEvents: false
		});

		this.berryCounter = new BerryCounter({
			parent: this,
			align: 'center'
		});

		this.topInputExpander = new InputExpander({
			parent: this,
			x: (this.style.width - CLIPPING_WIDTH) / 2,
			y: 0,
			width: CLIPPING_WIDTH,
			height: CLIPPING_Y + 2 * EXTRA_INPUT,
			realView: this.upgradesList.scrollView
		});

		this.btnBack = new ButtonView({
			parent: this,
			x: SIGN_OFFSET,
			y: this.style.height,
			width: BACK_WIDTH,
			height: BACK_HEIGHT,
			image: "resources/images/Store/btnBack.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'deconstructView', bind(this.controller, 'transitionToMainMenu'))
		});
		this.txtBack = new TextView({
			parent: this.btnBack,
			x: 0,
			y: BACK_LABEL_Y,
			r: BACK_LABEL_R,
			width: BACK_WIDTH,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("MENU"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});
		this.backButtonFunction = bind(this.btnBack, this.btnBack.onClick);

		var swapX = this.style.width - SIGN_OFFSET - SWAP_WIDTH;
		var swapMinX = this.storeBase.style.x + this.storeBase.style.width - SWAP_WIDTH;
		if (swapX < swapMinX) {
			swapX = swapMinX;
		}
		this.btnSwap = new ButtonView({
			parent: this,
			x: swapX,
			y: this.style.height,
			width: SWAP_WIDTH,
			height: SWAP_HEIGHT,
			image: "resources/images/Store/btnSwap.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: this.goToBerryStore
		});

		this.txtSwap = new TextView({
			parent: this.btnSwap,
			x: SWAP_LABEL_X,
			y: SWAP_LABEL_Y,
			r: SWAP_LABEL_R,
			width: SWAP_WIDTH - 2 * SWAP_LABEL_X,
			height: LABEL_HEIGHT + 20,
			text: GC.app.l.translate("GET\nBERRIES"),
			textAlign: 'left',
			lineHeight: 0.8,
			xAnchor: SWAP_LABEL_X * 0.5,
			yAnchor: SWAP_LABEL_Y * 0.5,
			size: config.fonts.swapSign,
			color: config.colors.buttonSign,
			wrap: true,
			canHandleEvents: false
		});

		this.resetScrollOffset = true;
	};

	this.resetView = function() {
		if (this.controller.playingSong != "store_music") {
			this.controller.pauseSong();
			this.controller.playSong("store_music");
		}

		this.slidingLayer.style.y = CLOSED_UP_OFFSET;
		this.upgradesList.setOffset(-this.upgradesList.totalHeight);
		this.controller.setHardwareBackButton(this.backButtonFunction);
		this.berryCounter.update();
		this.updateUpgradeNodes();
		this.topInputExpander.setRealView(this.upgradesList.scrollView);
		this.sprTrainer.nextAnimation();
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		this.controller.blockInput();

		this.berryCounter.slideUp();
		animate(this.btnBack).now({ y: this.style.height - 2 * BACK_HEIGHT / 3 }, SLIDE_TIME, animate.easeOut);
		animate(this.btnSwap).now({ y: this.style.height - SWAP_HEIGHT }, SLIDE_TIME, animate.easeOut);
		animate(this.slidingLayer).now({ y: CLIPPING_HEIGHT - NODE_BOTTOM_HEIGHT }, SLIDE_TIME, animate.easeIn)
		.then(bind(this, function() {
			this.upgradesList.scrollTo(0, 3 * SLIDE_TIME / 2, animate.easeOut, bind(this, function() {
				this.controller.transitionLockDown(false);
				this.controller.unblockInput();
			}));
		}));
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			this.controller.blockInput();
			animate(this.btnBack).now({ y: this.style.height }, SLIDE_TIME / 2, animate.easeIn);
			animate(this.btnSwap).now({ y: this.style.height }, SLIDE_TIME / 2, animate.easeIn);
			this.berryCounter.slideDown(bind(this, function() {
				this.controller.unblockInput();
				callback();
			}));
		}
	};

	this.stopAnimations = function() {
		this.sprTaps.stopAnimation();
		this.sprTaps.style.visible = true;
		this.sprTrainer.stopAnimation();
		this.sprTrainer.style.visible = true;
	};

	this.goToBerryStore = function(){
		this.controller.showBerryStore();
		this.controller.showHeader(true);
		this.controller.subscribeOnce('lastModalClose', this.onModalClose);
	};

	this.goToGemStore = function(){
		this.controller.showGemStore();
		this.controller.showHeader(true);
		this.controller.subscribeOnce('lastModalClose', this.onModalClose);
	};

	this.onModalClose = function(){
		if (this.style.visible) {
			this.berryCounter.update();
			this.controller.hideHeader(true);
		}
	};

	this.updateUpgradeNodes = function() {
		var upgrades = upgradesConfig,
			purchased = this.controller.getData("upgradesPurchased");

		this.upgradeNodeData = [];

		// populate node data
		var index = 0,
			lastType = null;

		var arrUpgrades = [];

		for (var i in upgradesOrder) {
			arrUpgrades.push(upgrades[upgradesOrder[i]]);
		}

		for (i in arrUpgrades) {
			var upgrade = arrUpgrades[i];
			var purchase = purchased[upgrade.id];

			if (lastType != upgrade.type) {
				var sectionTitle = "";
				switch(upgrade.type) {
					case TYPE_UPGRADE:
						lastType = TYPE_UPGRADE;
						sectionTitle = GC.app.l.translate("PICK-UPS");
						break;
					case TYPE_CONSUMABLE:
						lastType = TYPE_CONSUMABLE;
						sectionTitle = GC.app.l.translate("BOOSTS");
						break;
					case TYPE_AVATAR:
						lastType = TYPE_AVATAR;
						sectionTitle = GC.app.l.translate("RUNNERS");
						break;
				}

				var section = {
					uid: lastType,
					index: index++,
					sectionHeader: true,
					title: sectionTitle,
					height: SECTION_HEIGHT
				}

				this.upgradeNodeData.push(section);
			}

			// set data
			var node = {
				uid: upgrade.id,
				index: index++,
				name: upgrade.name,
				iconWidth: upgrade.iconWidth,
				iconHeight: upgrade.iconHeight,
				icon: upgrade.icon,
				type: upgrade.type,
				spawnID: upgrade.spawnID,
				levels: upgrade.levels,
				max: upgrade.max,
				currentLevel: purchase ? purchase.level : 0,
				height: NODE_HEIGHT,
				productID: upgrade.productID,
				currency: upgrade.currency,
				subtext: upgrade.subtext
			};

			this.upgradeNodeData.push(node);
		}

		this.upgradesList.updateData(this.upgradeNodeData, true);

		this.sprTaps.setPointing(true);

		if (this.resetScrollOffset) {
			this.resetScrollOffset = false;
			this.upgradesList.setOffset(-this.upgradesList.totalHeight);
		}
	};
});
