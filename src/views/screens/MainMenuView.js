import animate;
import device;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;
import ui.SpriteView as Sprite;

import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;
import src.lib.LocaleManager as LocaleManager;

import src.controllers.Controller as Controller;

import src.views.modals.BasicModalView as BasicModalView;
import src.views.modals.BinaryModalView as BinaryModalView;
import src.views.modals.RatingModalView as RatingModalView;

exports = Class(View, function(supr) {

	// CLASS CONSTANTS
	var BG_WIDTH,
		BG_HEIGHT,
		SPACING = 15,
		BUTTON_SPACING = 225,
		TITLE_WIDTH = 541,
		TITLE_HEIGHT = 181,
		PLAY_Y = 225,
		PLAY_WIDTH = 270,
		PLAY_HEIGHT = 270,
		ACHIEVEMENTS_WIDTH = 261,
		ACHIEVEMENTS_HEIGHT = 141,
		ACHIEVEMENTS_LABEL_X = 60,
		ACHIEVEMENTS_LABEL_WIDTH = ACHIEVEMENTS_WIDTH - ACHIEVEMENTS_LABEL_X,
		AVATAR_LABEL_HEIGHT = 80,
		UPGRADES_WIDTH = 196,
		UPGRADES_HEIGHT = 169,
		NESTABLES_WIDTH = 194,
		NESTABLES_HEIGHT = 126,
		STATS_Y = 282,
		STATS_WIDTH = 171,
		STATS_HEIGHT = 196,
		STATS_LABEL_X = 26,
		STATS_LABEL_Y = 33,
		STATS_LABEL_R = -Math.PI / 18,
		STATS_LABEL_WIDTH = STATS_WIDTH - 40,
		LABEL_HEIGHT = 40,
		PLAY_LABEL_X = 30,
		PLAY_LABEL_Y = 25,
		PLAY_LABEL_R = -Math.PI / 24,
		PLAY_LABEL_WIDTH = 210,
		PLAY_LABEL_HEIGHT = 100,
		AVATAR_FEET_Y = 500,
		AVATAR_WIDTH = 160,
		AVATAR_HEIGHT = 160,
		AVATAR_OFFSET,
		AVATAR_LABEL_WIDTH = 250,
		AVATAR_LABEL_FLOAT = 0.25,
		AVATAR_LABEL_FLOAT_BOUNDS = 5,
		ROCK_WIDTH = 125,
		ROCK_HEIGHT = 56,
		ROCK_OFFSET,
		GRAVITY,
		SOUND_WIDTH = 87,
		SOUND_HEIGHT = 87,
		LEVELS_Y = 325,
		LEVELS_WIDTH = 201,
		LEVELS_HEIGHT = 171,
		LEVELS_PIC_Y = 25,
		LEVELS_PIC_WIDTH = 164,
		LEVELS_PIC_HEIGHT = 99,
		SCREEN_TOP_Y = 75,
		PROMO_TEXT = "Hey there Love Birds! Pick up our new Valentine's Day level for that special someone in your life!",
		promoCb,
		config;

	var RAINBOW_SPARKLES = [
		"resources/images/Game/sparklePink.png",
		"resources/images/Game/sparkleRed.png",
		"resources/images/Game/sparkleOrange.png",
		"resources/images/Game/itemSparkle.png",
		"resources/images/Game/sparkleGreen.png",
		"resources/images/Game/sparkleBlue.png",
		"resources/images/Game/sparklePurple.png"
	];

	// initialization
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;
		GRAVITY = config.game.model.gravity;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;
		AVATAR_OFFSET = 7 * this.style.width / 32;
		ROCK_OFFSET = AVATAR_OFFSET + 12;

		this.isMainView = true;

		promoCb = bind(this, function(){
			this.controller.transitionToMap();
		});

		this.designView();
	};

	// build the component subviews of the screen
	this.designView = function() {
		var opts = this._opts;

		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/MainMenu/background.png",
			canHandleEvents: false
		});

		this.avatars = this.controller.getData("avatars") || ["avatarKiwiTeal/kiwiTeal"];
		this.selectedAvatar = this.controller.getData("selectedAvatar") || "avatarKiwiTeal/kiwiTeal";
		this.avatarConfig = this.controller.avatarConfig[this.selectedAvatar];

		this.sprAvatar = new Sprite({
			parent: this,
			x: this.style.width / 2 - AVATAR_OFFSET,
			y: AVATAR_FEET_Y - AVATAR_HEIGHT,
			anchorX: AVATAR_WIDTH / 2,
			anchorY: AVATAR_HEIGHT / 2,
			width: AVATAR_WIDTH,
			height: AVATAR_HEIGHT,
			url: "resources/images/Avatars/" + this.selectedAvatar,
			defaultAnimation: "workout",
			loop: false,
			autoStart: false
		});
		this.txtAvatar = new TextView({
			parent: this.sprAvatar,
			x: (this.avatarConfig.workoutWidth - AVATAR_LABEL_WIDTH) / 2,
			y: -AVATAR_LABEL_HEIGHT - SPACING / 2,
			width: AVATAR_LABEL_WIDTH,
			height: AVATAR_LABEL_HEIGHT,
			text: GC.app.l.translate(this.avatarConfig.name),
			textAlign: 'center',
			size: config.fonts.avatar,
			color: this.avatarConfig.labelFill,
			strokeColor: this.avatarConfig.labelStroke,
			multiline: false,
			canHandleEvents: false
		});
		this.txtAvatarStartY = this.txtAvatar.style.y;
		this.txtAvatarOffset = 0;
		this.txtAvatarDirection = -AVATAR_LABEL_FLOAT;

		this.sprAvatar.style.x = this.style.width / 2 - AVATAR_OFFSET + AVATAR_WIDTH / 2 - this.avatarConfig.workoutWidth / 2;
		this.sprAvatar.style.y = AVATAR_FEET_Y - this.avatarConfig.workoutHeight;
		this.sprAvatar.style.width = this.avatarConfig.workoutWidth;
		this.sprAvatar.style.height = this.avatarConfig.workoutHeight;

		this.btnRockLeft = new ButtonView({
			parent: this,
			x: this.style.width / 2 - ROCK_OFFSET + AVATAR_WIDTH / 2 - ROCK_WIDTH - 40,
			y: AVATAR_FEET_Y - ROCK_HEIGHT / 2,
			width: ROCK_WIDTH,
			height: ROCK_HEIGHT,
			image: "resources/images/MainMenu/btnRockLeft.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.cycleAvatarsLeft();
			})
		});
		this.btnRockRight = new ButtonView({
			parent: this,
			x: this.style.width / 2 - ROCK_OFFSET + AVATAR_WIDTH / 2 + 40,
			y: AVATAR_FEET_Y - ROCK_HEIGHT / 2,
			width: ROCK_WIDTH,
			height: ROCK_HEIGHT,
			image: "resources/images/MainMenu/btnRockRight.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.cycleAvatarsRight();
			})
		});

		this.levels = this.controller.getData("levels");
		this.selectedLevel = this.controller.getData("selectedLevel");
		this.levelConfig = this.controller.levelConfig[this.selectedLevel];

		this.btnLevels = new ButtonView({
			parent: this,
			x: this.style.width - SPACING - LEVELS_WIDTH,
			y: LEVELS_Y,
			width: LEVELS_WIDTH,
			height: LEVELS_HEIGHT,
			image: "resources/images/MainMenu/btnLevels.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function(){
				this.deconstructView(bind(this.controller, 'transitionToMap'));
			}),
			clip: false
		});
		this.txtLevels = new TextView({
			parent: this.btnLevels,
			x: 0,
			y: LEVELS_PIC_Y - 16,
			r: -STATS_LABEL_R / 2,
			width: LEVELS_WIDTH,
			height: 40,
			text: GC.app.l.translate("Map"),
			textAlign: 'center',
			size: config.fonts.button,
			color: config.colors.button,
			multiline: false,
			canHandleEvents: false
		});
		this.levelPic = new ImageView({
			parent: this.btnLevels,
			x: (LEVELS_WIDTH - LEVELS_PIC_WIDTH) / 2,
			y: LEVELS_PIC_Y + 20,
			width: LEVELS_PIC_WIDTH,
			height: LEVELS_PIC_HEIGHT,
			image: this.levelConfig.picture,
			canHandleEvents: false
		});

		var locale = LocaleManager.getLocale();
		this.btnPlay = new ButtonView({
			parent: this,
			x: this.style.width / 2 + SPACING,
			y: PLAY_Y,
			width: PLAY_WIDTH,
			height: PLAY_HEIGHT,
			image: "resources/images/MainMenu/btnPlay.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				if (this.controller.getData("firstPlay")) {
					this.deconstructView(bind(this.controller, 'transitionToGame'));
				} else {
					this.controller.showEquipModal();
				}
			})
		});
		this.txtPlay = new TextView({
			parent: this.btnPlay,
			x: PLAY_LABEL_X,
			y: locale === "en" ? PLAY_LABEL_Y + 12 : PLAY_LABEL_Y,
			r: PLAY_LABEL_R,
			anchorX: PLAY_LABEL_WIDTH / 2,
			anchorY: PLAY_LABEL_HEIGHT / 2,
			width: PLAY_LABEL_WIDTH,
			height: PLAY_LABEL_HEIGHT,
			text: GC.app.l.translate("PLAY!"),
			horizontalAlign: 'center',
      size: 80,
			color: config.colors.buttonPlayFill,
			strokeColor: config.colors.buttonPlayStroke,
			lineWidth: 6,
			wrap: false,
			canHandleEvents: false
		});
    window.tv = this.txtPlay;

		this.btnAchievements = new ButtonView({
			parent: this,
			x: (this.style.width - ACHIEVEMENTS_WIDTH) / 2,
			y: this.style.height - ACHIEVEMENTS_HEIGHT - SPACING,
			width: ACHIEVEMENTS_WIDTH,
			height: ACHIEVEMENTS_HEIGHT,
			image: "resources/images/MainMenu/btnAchievements.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.deconstructView(bind(this.controller, 'transitionToAchievements'));
			})
		});
		this.txtAchievements = new TextView({
			parent: this.btnAchievements,
			x: ACHIEVEMENTS_LABEL_X,
			y: ACHIEVEMENTS_HEIGHT / 2 - 6,
			width: ACHIEVEMENTS_LABEL_WIDTH,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("Achievements"),
			textAlign: 'center',
			size: config.fonts.button,
			color: config.colors.button,
			multiline: false,
			canHandleEvents: false
		});

		this.btnUpgrades = new ButtonView({
			parent: this,
			x: this.style.width / 2 + BUTTON_SPACING,
			y: this.style.height - UPGRADES_HEIGHT - SPACING,
			width: UPGRADES_WIDTH,
			height: UPGRADES_HEIGHT,
			image: "resources/images/MainMenu/btnUpgrades.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				var gamesPlayed = this.controller.getData("gamesPlayed") || 0;
				var strGamesPlayed = gamesPlayed + ""; // convert to string
				this.controller.trackEvent("UpgradeStore", {
					gamesPlayed: strGamesPlayed,
					from: "MainMenu"
				});

				this.deconstructView(bind(this.controller, 'transitionToUpgrades'));
			})
		});
		this.txtUpgrades = new TextView({
			parent: this.btnUpgrades,
			x: 30,
			y: this.btnUpgrades.style.height - LABEL_HEIGHT - SPACING / 2 - 5,
			width: UPGRADES_WIDTH - 60,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("Upgrades"),
			textAlign: 'center',
			size: config.fonts.button,
			color: config.colors.button,
			multiline: false,
			canHandleEvents: false
		});

		this.btnNestables = new ButtonView({
			parent: this,
			x: this.style.width / 2 - BUTTON_SPACING - NESTABLES_WIDTH,
			y: this.style.height - NESTABLES_HEIGHT - SPACING,
			width: NESTABLES_WIDTH,
			height: NESTABLES_HEIGHT,
			image: "resources/images/MainMenu/btnNestables.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, function() {
				this.deconstructView(bind(this.controller, 'transitionToNestables'));
			})
		});
		this.txtNestables = new TextView({
			parent: this.btnNestables,
			x: 0,
			y: this.btnNestables.style.height - LABEL_HEIGHT - SPACING - 4,
			width: NESTABLES_WIDTH,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("Nestables"),
			textAlign: 'center',
			size: config.fonts.button,
			color: config.colors.button,
			multiline: false,
			canHandleEvents: false
		});

		if (GC.app.enableSocial) {
			this.btnStats = new ButtonView({
				parent: this,
				x: SPACING,
				y: STATS_Y,
				width: STATS_WIDTH,
				height: STATS_HEIGHT,
				image: "resources/images/MainMenu/btnStats.png",
				soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
				onClick: bind(this, function() {
					this.deconstructView(bind(this.controller, 'transitionToLeaderboard'));
				})
			});
			this.txtStats = new TextView({
				parent: this.btnStats,
				x: STATS_LABEL_X,
				y: STATS_LABEL_Y,
				r: STATS_LABEL_R,
				width: STATS_LABEL_WIDTH,
				height: LABEL_HEIGHT,
				text: GC.app.l.translate("Friends"),
				textAlign: 'center',
				size: config.fonts.button,
				color: config.colors.button,
				multiline: false,
				canHandleEvents: false
			});
		}

		var sound = this.controller.getData("sfxEnabled");
		this.soundToggle = new ImageView({
			parent: this,
			x: SPACING,
			y: SCREEN_TOP_Y,
			width: SOUND_WIDTH,
			height: SOUND_HEIGHT,
			image: "resources/images/MainMenu/" + (sound ? "iconSoundOn.png" : "iconSoundOff.png")
		});
		this.soundToggle.onInputStart = bind(this, function() {
			var sound = !this.controller.getData("sfxEnabled");
			this.soundToggle.setImage("resources/images/MainMenu/" + (sound ? "iconSoundOn.png" : "iconSoundOff.png"));
			this.controller.setData("sfxEnabled", sound);
			this.controller.setData("musicEnabled", sound);

			if (sound) {
				this.controller.playSong("run_menu_music");
			} else {
				this.controller.pauseSong();
			}
		});


		var TAPJOY_BUTTON_WIDTH = 135;
		var TAPJOY_BUTTON_HEIGHT = 80;
		var TAPJOY_BUTTON_SPACING = 20;

		this.sparkles = new ParticleEngine({
			parent: this,
			x: 0,
			y: 0,
			width: 1,
			height: 1
		});

		if( config.debug ){
			this.debugButton = new ImageView({
				superview: this,
				x: 0,
				y: 500,
				width: 137,
				height: 79,
				image: 'resources/images/Modals/btnGreen.png'
			});

			this.debugText = new TextView({
				superview: this.debugButton,
				x: 0,
				y: 0,
				width: 137,
				height: 79,
				text: 'DEBUG',
				textAlign: 'center',
				size: config.fonts.button,
				color: config.colors.button,
				multiline: false,
				canHandleEvents: false
			});

			this.debugButton.on('InputStart', bind(this.controller, this.controller.transitionToCheatView));
		}

		this.boundStartNextWorkout = bind(this, 'startNextWorkout');
	};

	this.cycleAvatarsLeft = function() {
		var index = this.avatars.indexOf(this.selectedAvatar) - 1;
		if (index < 0) {
			index = this.avatars.length - 1;
		}

		this.selectedAvatar = this.avatars[index];
		this.controller.setData("selectedAvatar", this.selectedAvatar);
		this.avatarConfig = this.controller.avatarConfig[this.selectedAvatar];

		this.sprAvatar.resetAllAnimations({
			url: "resources/images/Avatars/" + this.selectedAvatar,
			defaultAnimation: "workout",
			loop: false,
			autoStart: false
		});
		this.sprAvatar.startAnimation("workout", { iterations: 1, callback: this.boundStartNextWorkout });

		this.sprAvatar.style.x = this.style.width / 2 - AVATAR_OFFSET + AVATAR_WIDTH / 2 - this.avatarConfig.workoutWidth / 2;
		this.sprAvatar.style.y = AVATAR_FEET_Y - this.avatarConfig.workoutHeight;
		this.sprAvatar.style.width = this.avatarConfig.workoutWidth;
		this.sprAvatar.style.height = this.avatarConfig.workoutHeight;

		this.txtAvatar.setText(GC.app.l.translate(this.avatarConfig.name));
		this.txtAvatar._opts.color = this.avatarConfig.labelFill;
		this.txtAvatar._opts.strokeColor = this.avatarConfig.labelStroke;
		this.txtAvatar.style.x = (this.avatarConfig.workoutWidth - AVATAR_LABEL_WIDTH) / 2;
	};

	this.cycleAvatarsRight = function() {
		var index = this.avatars.indexOf(this.selectedAvatar) + 1;
		if (index >= this.avatars.length) {
			index = 0;
		}

		this.selectedAvatar = this.avatars[index];
		this.controller.setData("selectedAvatar", this.selectedAvatar);
		this.avatarConfig = this.controller.avatarConfig[this.selectedAvatar];

		this.sprAvatar.resetAllAnimations({
			url: "resources/images/Avatars/" + this.selectedAvatar,
			defaultAnimation: "workout",
			loop: false,
			autoStart: false
		});
		this.sprAvatar.startAnimation("workout", { iterations: 1, callback: this.boundStartNextWorkout });

		this.sprAvatar.style.x = this.style.width / 2 - AVATAR_OFFSET + AVATAR_WIDTH / 2 - this.avatarConfig.workoutWidth / 2;
		this.sprAvatar.style.y = AVATAR_FEET_Y - this.avatarConfig.workoutHeight;
		this.sprAvatar.style.width = this.avatarConfig.workoutWidth;
		this.sprAvatar.style.height = this.avatarConfig.workoutHeight;

		this.txtAvatar.setText(GC.app.l.translate(this.avatarConfig.name));
		this.txtAvatar._opts.color = this.avatarConfig.labelFill;
		this.txtAvatar._opts.strokeColor = this.avatarConfig.labelStroke;
		this.txtAvatar.style.x = (this.avatarConfig.workoutWidth - AVATAR_LABEL_WIDTH) / 2;
	};

	// ensures state and data get reset each time the view becomes active
	this.resetView = function() {
		if (this.controller.playingSong != "run_menu_music") {
			this.controller.pauseSong();
			this.controller.playSong("run_menu_music");
		}

		this.controller.trackEvent("MainMenu", {});

		// hide buttons on first play
		if (this.controller.getData("firstPlay")) {
			if (this.btnStats) {
				this.btnStats.style.visible = false;
			}
			this.btnAchievements.style.visible = false;
			this.btnUpgrades.style.visible = false;
			this.btnNestables.style.visible = false;
			this.btnLevels.style.visible = false;
		} else {
			if (this.btnStats) {
				this.btnStats.style.visible = true;
			}
			this.btnAchievements.style.visible = true;
			this.btnUpgrades.style.visible = true;
			this.btnNestables.style.visible = true;
			this.btnLevels.style.visible = true;
		}

		this.controller.setHardwareBackButton(bind(this, 'showExitGameModal'));

		this.avatars = this.controller.getData("avatars") || ["avatarKiwiTeal/kiwiTeal"];
		this.selectedAvatar = this.controller.getData("selectedAvatar") || "avatarKiwiTeal/kiwiTeal";
		this.avatarConfig = this.controller.avatarConfig[this.selectedAvatar];

		if (this.avatars.length > 1) {
			this.btnRockLeft.style.visible = true;
			this.btnRockRight.style.visible = true;
		} else {
			this.btnRockLeft.style.visible = false;
			this.btnRockRight.style.visible = false;
		}

		this.levels = this.controller.getData("levels");
		this.selectedLevel = this.controller.getData("selectedLevel");
		this.levelConfig = this.controller.levelConfig[this.selectedLevel];
		this.levelPic.setImage(this.levelConfig.picture);

		var gamesPlayed = this.controller.getData("gamesPlayed") || 0;
		var appRated = this.controller.getData("appRated");
		var req = this.controller.getData("RatingReq") || 20;
		if (gamesPlayed && gamesPlayed >= req && !appRated) {
			this.showRatingModal();
			this.controller.setData("RatingReq", 2 * req);
		}

		this.sprAvatar.startAnimation("workout", { iterations: 1, callback: this.boundStartNextWorkout });
	};

	this.startNextWorkout = function() {
		if (!this.workoutStill && this.avatarConfig.workoutBreak) {
			var iters = Math.random() * 30 + 15; // 1-3 seconds of still kiwi
			this.workoutStill = true
			this.sprAvatar.startAnimation("workoutStart", { iterations: iters, callback: this.boundStartNextWorkout });
		} else {
			this.workoutStill = false
			this.sprAvatar.startAnimation("workout", { iterations: 1, callback: this.boundStartNextWorkout });
		}
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		this.controller.accountForOldUsers();
		this.controller.transitionLockDown(false);
		this.controller.showHeader(true);

		this.controller.showPurchaseSuccessNotification();

		var firstPlay = this.controller.getData('firstPlay');

		var showedPromo = this.controller.getData('valentinePromoShown');
		var gamesPlayed = this.controller.getData('gamesPlayed');
		if (!showedPromo && !firstPlay && gamesPlayed >= 6) {
			this.showPromotionalModal(PROMO_TEXT, promoCb);
			this.controller.promoEffectController.newMap('valentine');
		}

		var showedBabies = this.controller.getData('babyPromoShown');
		if( !showedBabies && !firstPlay){
			this.controller.promoEffectController.newBabyKiwis([
				"babyHeart/babyHeart",
				"babyRose/babyRose",
				"babyHyo/babyHyo",
				"babyEun/babyEun"
			]);
		}
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			this.controller.hideHeader(true, callback);
		}
		else{
			this.controller.hideHeader(true);
		}
	};

	this.stopAnimations = function() {
		this.sprAvatar.stopAnimation();
		this.sprAvatar.style.visible = true;
	};

	this.showRatingModal = function() {
		new RatingModalView({ parent: this });
	};

	this.showExitGameModal = function() {
		new BinaryModalView({
			parent: this,
			message: "Are you sure you want to exit?",
			onAccept: bind(this, function() {
				NATIVE.sendActivityToBack && NATIVE.sendActivityToBack();
			})
		});
	};

	this.emitAchieveParticles = function() {
		var count = 24;
		var data = this.sparkles.obtainParticleArray(count);
		for (var i = 0; i < count; i++) {
			var width = Math.random() * 30 + 30;
			var height = width;
			var ttl = 4000;
			var pObj = data[i];
			var tvs = this.controller.achievementToast.style;

			pObj.image = RAINBOW_SPARKLES[~~(Math.random() * RAINBOW_SPARKLES.length)];

			pObj.x = tvs.x + Math.random() * tvs.width - width / 2;
			pObj.y = tvs.y + Math.random() * tvs.height - height / 2;
			pObj.dx = Math.random() * 500 - 250;
			pObj.dy = -Math.random() * 200 - 1000;
			pObj.ddx = -pObj.dx / 6;
			pObj.ddy = GRAVITY;
			pObj.r = Math.random() * 2 * Math.PI;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.ttl = ttl;
		}

		this.sparkles.emitParticles(data);
	};

	this.showPromotionalModal = function(text, cb){
		this.controller.hideHeader(true,
			bind(this, function() {
				new BasicModalView({
					parent: this,
					onAccept: cb,
					message: GC.app.l.translate(text),
					size: config.fonts.modalMessageSmall
				});
			})
		);
	};

	this.tick = function(dt) {
		if (!this.style.visible) { return; }

		this.sparkles.runTick(dt);

		this.txtAvatarOffset += this.txtAvatarDirection;
		if (Math.abs(this.txtAvatarOffset) >= AVATAR_LABEL_FLOAT_BOUNDS) {
			this.txtAvatarDirection = -this.txtAvatarDirection;
		}

		this.txtAvatar.style.y = this.txtAvatarStartY + this.txtAvatarOffset;
	};
});
