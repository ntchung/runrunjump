import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;

import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	// CLASS CONSTANTS
	var BG_WIDTH,
		BG_HEIGHT,
		HORIZON_CLIPPING = 315,
		STAR_COUNT = 60,
		STAR_SIZE_MIN = 5,
		STAR_SIZE_RANGE = 8,
		STAR_TTL = 999999999,
		STAR_SPREAD = 1000,
		STAR_SPIN_RATE = Math.PI,
		STAR_CHANGE_CHANCE = 0.02,
		STAR_SPAWN_CHANCE = 0.04,
		KIWI_PLANET_RELATIVE_ROTATION_RATE = Math.PI / 1000,
		BACK_WIDTH = 166,
		BACK_HEIGHT = 226,
		LABEL_HEIGHT = 50,
		BACK_LABEL_Y = 56,
		BACK_LABEL_R = -Math.PI / 18,
		FRIENDS_WIDTH = 236,
		FRIENDS_HEIGHT = 205,
		FRIENDS_LABEL_X = 30,
		FRIENDS_LABEL_Y = 30,
		FRIENDS_LABEL_R = Math.PI / 18,
		SIGN_OFFSET = 16,
		SLIDE_TIME = 600,
		POST_WIDTH = 579,
		POST_HEIGHT = 700,
		SCORE_WIDTH = POST_WIDTH / 3,
		SCORE_HEIGHT = LABEL_HEIGHT,
		METERS_WIDTH = POST_WIDTH / 5,
		METERS_HEIGHT = LABEL_HEIGHT,
		TITLE_X = 156,
		TITLE_Y = 46,
		TITLE_R = -Math.PI / 80,
		TITLE_WIDTH = POST_WIDTH / 2,
		TITLE_HEIGHT = LABEL_HEIGHT,
		FIRST_SCORE_X = 116,
		FIRST_SCORE_Y = 168,
		FIRST_SCORE_R = Math.PI / 72,
		FIRST_METERS_X = 402,
		FIRST_METERS_Y = 169,
		FIRST_METERS_R = Math.PI / 54,
		SECOND_SCORE_X = 113,
		SECOND_SCORE_Y = 273,
		SECOND_SCORE_R = -Math.PI / 54,
		SECOND_METERS_X = 387,
		SECOND_METERS_Y = 261,
		SECOND_METERS_R = -Math.PI / 48,
		THIRD_SCORE_X = 119,
		THIRD_SCORE_Y = 380,
		THIRD_SCORE_R = Math.PI / 90,
		THIRD_METERS_X = 400,
		THIRD_METERS_Y = 382,
		THIRD_METERS_R = -Math.PI / 126,
		FOURTH_SCORE_X = 107,
		FOURTH_SCORE_Y = 495,
		FOURTH_SCORE_R = Math.PI / 66,
		FOURTH_METERS_X = 400,
		FOURTH_METERS_Y = 498,
		FOURTH_METERS_R = Math.PI / 54,
		FIFTH_SCORE_X = 99,
		FIFTH_SCORE_Y = 582,
		FIFTH_SCORE_R = Math.PI / 144,
		FIFTH_METERS_X = 406,
		FIFTH_METERS_Y = 584,
		FIFTH_METERS_R = -Math.PI / 90;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		var config = this.controller.config;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;

		this.isMainView = true;

		this.designView();
	};

	// build the component subviews of the screen
	this.designView = function() {
		var opts = this._opts;
		var config = this.controller.config;

		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: (this.style.height - BG_HEIGHT) / 2,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/Stats/background.png",
			canHandleEvents: false
		});

		// TODO: star formations
		this.horizonClipping = new View({
			parent: this.background,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: HORIZON_CLIPPING,
			clip: true,
			canHandleEvents: false,
			blockEvents: true
		});
		this.starryNight = new ParticleEngine({
			parent: this.horizonClipping,
			x: 2,
			y: 2,
			anchorX: 0,
			anchorY: BG_HEIGHT,
			width: 1,
			height: 1
		});
		this.starryAtmosphere = new ParticleEngine({
			parent: this.horizonClipping,
			x: 2,
			y: 2,
			width: 1,
			height: 1
		});

		// keeps stars alive forever, and randomly changes their opacity
		this.boundStarTick = bind(this, function(particle) {
			var data = particle.pData;
			if (Math.random() < STAR_CHANGE_CHANCE) {
				particle.style.opacity = Math.random() * data.opacityRange + data.opacityMin;
			}
		});

		// init stars
		var starData = this.starryNight.obtainParticleArray(STAR_COUNT);
		for (var i = 0; i < STAR_COUNT; i++) {
			var width = Math.random() * STAR_SIZE_RANGE + STAR_SIZE_MIN;
			var height = width;

			var pObj = starData[i];
			pObj.ttl = STAR_TTL;
			pObj.x = Math.random() * BG_WIDTH;
			pObj.y = Math.random() * HORIZON_CLIPPING;
			pObj.r = Math.random() * 2 * Math.PI;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.dr = Math.random() * STAR_SPIN_RATE;
			pObj.opacityMin = Math.random() * 0.6 + 0.2;
			pObj.opacityRange = Math.random() * 0.6;
			pObj.image = Math.random() < 0.8 ? "resources/images/Stats/star2.png" : "resources/images/Stats/star.png";
			pObj.onTick = this.boundStarTick;
		}
		this.starryNight.emitParticles(starData);

		this.hiscorePost = new ImageView({
			parent: this,
			x: (this.style.width - POST_WIDTH) / 2,
			y: this.style.height - POST_HEIGHT,
			width: POST_WIDTH,
			height: POST_HEIGHT,
			image: "resources/images/Stats/hiscorePost.png",
			canHandleEvents: false
		});

		this.txtTitle = new TextView({
			parent: this.hiscorePost,
			x: TITLE_X,
			y: TITLE_Y,
			r: TITLE_R,
			anchorX: TITLE_WIDTH / 2,
			anchorY: TITLE_HEIGHT / 2,
			width: TITLE_WIDTH,
			height: TITLE_HEIGHT,
			text: GC.app.l.translate("BEST RUNS!"),
			textAlign: 'center',
			size: config.fonts.statsTitle,
			color: config.colors.statsTitleFill,
			strokeColor: config.colors.statsTitleStroke,
			multiline: false,
			canHandleEvents: false
		});

		this.arrScoreText = [];
		this.arrMetersText = [];

		// define individual text views for each score
		// the art is organic, so shall the code be!
		this.txtFirstScore = new TextView({
			parent: this.hiscorePost,
			x: FIRST_SCORE_X,
			y: FIRST_SCORE_Y,
			r: FIRST_SCORE_R,
			anchorX: SCORE_WIDTH / 2,
			anchorY: SCORE_HEIGHT / 2,
			width: SCORE_WIDTH,
			height: SCORE_HEIGHT,
			text: "",
			textAlign: 'left',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.txtFirstMeters = new TextView({
			parent: this.hiscorePost,
			x: FIRST_METERS_X,
			y: FIRST_METERS_Y,
			r: FIRST_METERS_R,
			anchorX: METERS_WIDTH / 2,
			anchorY: METERS_HEIGHT / 2,
			width: METERS_WIDTH,
			height: METERS_HEIGHT,
			text: "",
			textAlign: 'right',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.arrScoreText.push(this.txtFirstScore);
		this.arrMetersText.push(this.txtFirstMeters);

		this.txtSecondScore = new TextView({
			parent: this.hiscorePost,
			x: SECOND_SCORE_X,
			y: SECOND_SCORE_Y,
			r: SECOND_SCORE_R,
			anchorX: SCORE_WIDTH / 2,
			anchorY: SCORE_HEIGHT / 2,
			width: SCORE_WIDTH,
			height: SCORE_HEIGHT,
			text: "",
			textAlign: 'left',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.txtSecondMeters = new TextView({
			parent: this.hiscorePost,
			x: SECOND_METERS_X,
			y: SECOND_METERS_Y,
			r: SECOND_METERS_R,
			anchorX: METERS_WIDTH / 2,
			anchorY: METERS_HEIGHT / 2,
			width: METERS_WIDTH,
			height: METERS_HEIGHT,
			text: "",
			textAlign: 'right',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.arrScoreText.push(this.txtSecondScore);
		this.arrMetersText.push(this.txtSecondMeters);

		this.txtThirdScore = new TextView({
			parent: this.hiscorePost,
			x: THIRD_SCORE_X,
			y: THIRD_SCORE_Y,
			r: THIRD_SCORE_R,
			anchorX: SCORE_WIDTH / 2,
			anchorY: SCORE_HEIGHT / 2,
			width: SCORE_WIDTH,
			height: SCORE_HEIGHT,
			text: "",
			textAlign: 'left',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.txtThirdMeters = new TextView({
			parent: this.hiscorePost,
			x: THIRD_METERS_X,
			y: THIRD_METERS_Y,
			r: THIRD_METERS_R,
			anchorX: METERS_WIDTH / 2,
			anchorY: METERS_HEIGHT / 2,
			width: METERS_WIDTH,
			height: METERS_HEIGHT,
			text: "",
			textAlign: 'right',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.arrScoreText.push(this.txtThirdScore);
		this.arrMetersText.push(this.txtThirdMeters);

		this.txtFourthScore = new TextView({
			parent: this.hiscorePost,
			x: FOURTH_SCORE_X,
			y: FOURTH_SCORE_Y,
			r: FOURTH_SCORE_R,
			anchorX: SCORE_WIDTH / 2,
			anchorY: SCORE_HEIGHT / 2,
			width: SCORE_WIDTH,
			height: SCORE_HEIGHT,
			text: "",
			textAlign: 'left',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.txtFourthMeters = new TextView({
			parent: this.hiscorePost,
			x: FOURTH_METERS_X,
			y: FOURTH_METERS_Y,
			r: FOURTH_METERS_R,
			anchorX: METERS_WIDTH / 2,
			anchorY: METERS_HEIGHT / 2,
			width: METERS_WIDTH,
			height: METERS_HEIGHT,
			text: "",
			textAlign: 'right',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.arrScoreText.push(this.txtFourthScore);
		this.arrMetersText.push(this.txtFourthMeters);

		this.txtFifthScore = new TextView({
			parent: this.hiscorePost,
			x: FIFTH_SCORE_X,
			y: FIFTH_SCORE_Y,
			r: FIFTH_SCORE_R,
			anchorX: SCORE_WIDTH / 2,
			anchorY: SCORE_HEIGHT / 2,
			width: SCORE_WIDTH,
			height: SCORE_HEIGHT,
			text: "",
			textAlign: 'left',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.txtFifthMeters = new TextView({
			parent: this.hiscorePost,
			x: FIFTH_METERS_X,
			y: FIFTH_METERS_Y,
			r: FIFTH_METERS_R,
			anchorX: METERS_WIDTH / 2,
			anchorY: METERS_HEIGHT / 2,
			width: METERS_WIDTH,
			height: METERS_HEIGHT,
			text: "",
			textAlign: 'right',
			size: config.fonts.statsScores,
			color: config.colors.statsScoresFill,
			multiline: false,
			canHandleEvents: false
		});
		this.arrScoreText.push(this.txtFifthScore);
		this.arrMetersText.push(this.txtFifthMeters);

		this.btnBack = new ButtonView({
			parent: this,
			x: SIGN_OFFSET,
			y: this.style.height,
			width: BACK_WIDTH,
			height: BACK_HEIGHT,
			image: "resources/images/Store/btnBack.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'deconstructView', bind(this.controller, 'transitionToMainMenu'))
		});
		this.txtBack = new TextView({
			parent: this.btnBack,
			x: 0,
			y: BACK_LABEL_Y,
			r: BACK_LABEL_R,
			width: BACK_WIDTH,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("MENU"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.btnFriends = new ButtonView({
			parent: this,
			x: this.style.width - SIGN_OFFSET - FRIENDS_WIDTH,
			y: this.style.height,
			width: FRIENDS_WIDTH,
			height: FRIENDS_HEIGHT,
			image: "resources/images/Store/btnSwap.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'deconstructView', bind(this.controller, 'transitionToAchievements'))
		});
		this.txtFriends = new TextView({
			parent: this.btnFriends,
			x: FRIENDS_LABEL_X,
			y: FRIENDS_LABEL_Y,
			r: FRIENDS_LABEL_R,
			width: FRIENDS_WIDTH - 2 * FRIENDS_LABEL_X,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("ACHIEVE"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});
	};

	// ensures state and data get reset each time the view becomes active
	this.resetView = function() {
		if (this.controller.playingSong != "achieves_music") {
			this.controller.pauseSong();
			this.controller.playSong("achieves_music");
		}

		var scores = this.controller.getData('topScores') || [];
		var meters = this.controller.getData('topMeters') || [];
		var topCount = this.controller.config.game.scores.topCount;
		for (var i = 0; i < topCount; i++) {
			if (i < scores.length) {
				this.arrScoreText[i].setText(scores[i]);
			} else {
				this.arrScoreText[i].setText("");
			}

			if (i < meters.length && meters[i]) { // don't show 0m for users with older versions
				this.arrMetersText[i].setText(meters[i] + "m");
			} else {
				this.arrMetersText[i].setText("");
			}
		}

		this.controller.setHardwareBackButton(this.btnBack.onClick);
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		this.controller.blockInput();
		animate(this.btnFriends).now({ y: this.style.height - FRIENDS_HEIGHT }, SLIDE_TIME, animate.easeOut);
		animate(this.btnBack).now({ y: this.style.height - 2 * BACK_HEIGHT / 3 }, SLIDE_TIME, animate.easeOut)
		.then(bind(this, function() {
			this.controller.transitionLockDown(false);
			this.controller.unblockInput();
		}));
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			this.controller.blockInput();
			animate(this.btnFriends).now({ y: this.style.height }, SLIDE_TIME, animate.easeIn);
			animate(this.btnBack).now({ y: this.style.height }, SLIDE_TIME, animate.easeIn)
			.then(bind(this, function() {
				this.controller.unblockInput();
				callback();
			}));
		}
	};

	this.tick = function(dt) {
		if (!this.style.visible) { return; }

		this.starryNight.runTick(dt);
		this.starryAtmosphere.runTick(dt);

		// note that the starryAtmosphere ParticleEngine does not rotate
		this.starryNight.style.r += KIWI_PLANET_RELATIVE_ROTATION_RATE * dt / 1000;

		// spawn a random short-lived star or shooting star
		if (Math.random() < STAR_SPAWN_CHANCE) {
			var starData = this.starryAtmosphere.obtainParticleArray(1);
			for (var i = 0; i < 1; i++) {
				var width = Math.random() * STAR_SIZE_RANGE + STAR_SIZE_MIN;
				var height = width;
				var shootingStar = ~~(Math.random() + 0.5);

				var pObj = starData[i];
				pObj.ttl = shootingStar ? 3000 : Math.random() * 179000 + 1000;
				pObj.x = Math.random() * BG_WIDTH;
				pObj.y = Math.random() * HORIZON_CLIPPING;
				pObj.r = Math.random() * 2 * Math.PI;
				pObj.anchorX = width / 2;
				pObj.anchorY = height / 2;
				pObj.width = width;
				pObj.height = height;
				pObj.dx = shootingStar ? Math.random() * 450 - 300 : KIWI_PLANET_RELATIVE_ROTATION_RATE;
				pObj.dy = shootingStar ? Math.random() * 150 + 100 : KIWI_PLANET_RELATIVE_ROTATION_RATE;
				pObj.dr = Math.random() * STAR_SPIN_RATE;
				pObj.ddx = shootingStar ? -pObj.dx / 4 : 0;
				pObj.ddy = shootingStar ? -pObj.dy / 4 : 0;
				pObj.opacity = Math.random() * 1;
				pObj.dopacity = Math.random() * 2 - 1;
				pObj.ddopacity = Math.random() * 4 - 2;
				pObj.image = Math.random() < 0.6 ? "resources/images/Stats/star2.png" : "resources/images/Stats/star.png";
				if (shootingStar) {
					pObj.image = "resources/images/Stats/star3.png";
				}
			}
			this.starryAtmosphere.emitParticles(starData);
		}
	};
});
