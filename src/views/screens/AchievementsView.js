import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;

import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;
import src.lib.ListView as ListView;

import src.controllers.Controller as Controller;

import src.views.helpers.AchievementNodeView as AchievementNodeView;
import src.views.helpers.AchievementBottomNodeView as AchievementBottomNodeView;
import src.views.helpers.AchievementTopNodeView as AchievementTopNodeView;

exports = Class(View, function(supr) {

	var BG_WIDTH,
		BG_HEIGHT,
		NODE_WIDTH = 729,
		NODE_HEIGHT = 149,
		TOP_HEIGHT = 1280,
		BOTTOM_HEIGHT = 346,
		BACK_WIDTH = 166,
		BACK_HEIGHT = 226,
		LABEL_HEIGHT = 50,
		BACK_LABEL_Y = 56,
		BACK_LABEL_R = -Math.PI / 18,
		STAT_WIDTH = 186,
		STAT_HEIGHT = 205,
		STAT_LABEL_R = -BACK_LABEL_R,
		STAT_LABEL_X = 10,
		STAT_LABEL_Y = -50,
		SIGN_OFFSET = -132,
		SLIDE_TIME = 600,
		TORCH_LEFT_WIDTH = 84,
		TORCH_LEFT_HEIGHT = 248,
		TORCH_RIGHT_WIDTH = 66,
		TORCH_RIGHT_HEIGHT = 251,
		FLAME_WIDTH = 51,
		FLAME_HEIGHT = 76,
		LEFT_FLAME_R = -Math.PI / 18,
		RIGHT_FLAME_R = Math.PI / 36,
		STAR_COUNT = 40,
		STAR_SIZE_MIN = 5,
		STAR_SIZE_RANGE = 8,

		LEFT_FLAMES = [
			"resources/images/Achievements/fire1Left.png",
			"resources/images/Achievements/fire2Left.png",
			"resources/images/Achievements/fire3Left.png",
			"resources/images/Achievements/fire4Left.png"
		],
		RIGHT_FLAMES = [
			"resources/images/Achievements/fire1Right.png",
			"resources/images/Achievements/fire2Right.png",
			"resources/images/Achievements/fire3Right.png",
			"resources/images/Achievements/fire4Right.png"
		],
		SPARK_IMAGE = "resources/images/Achievements/spark.png",
		SMOKE_IMAGE = "resources/images/Achievements/smoke.png",

		TYPE_COUNTER = "COUNTER",

		config,
		achievementsConfig;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		achievementsConfig = this.controller.achievementsConfig;
		config = this.controller.config;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;

		this.designView();
	};

	this.designView = function() {
		var opts = this._opts;
		this.viewBuilt = true;

		// this sky gradient will scroll to match the achievement tree
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: -1.1 * BG_HEIGHT,
			width: BG_WIDTH,
			height: 2.1 * BG_HEIGHT,
			image: "resources/images/Achievements/background.png",
			canHandleEvents: false,
			blockEvents: true
		});

		// init stars
		for (var i = 0; i < STAR_COUNT; i++) {
			var width = Math.random() * STAR_SIZE_RANGE + STAR_SIZE_MIN;
			var height = width;

			new ImageView({
				parent: this.background,
				x: Math.random() < 0.5
					? Math.random() * (BG_WIDTH - NODE_WIDTH) / 2
					: BG_WIDTH - Math.random() * (BG_WIDTH - NODE_WIDTH) / 2,
				y: Math.random() * 2.1 * BG_HEIGHT,
				width: width,
				height: height,
				r: Math.random() * 2 * Math.PI,
				opacity: Math.random() * 0.8 + 0.2,
				image: Math.random() < 0.8 ? "resources/images/Stats/star2.png" : "resources/images/Stats/star.png"
			});
		}

		this.achievementList = new ListView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			nodeCtor: AchievementNodeView,
			nodeHeight: NODE_HEIGHT,
			updateNode: function(node, data) {
				node.updateData(data);
			}
		});
		this.topNode = this.achievementList.addFixedTopNode({
			ctor: AchievementTopNodeView,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: TOP_HEIGHT
		});
		this.bottomNode = this.achievementList.addFixedBottomNode({
			ctor: AchievementBottomNodeView,
			x: 0,
			y: 0,
			width: BG_WIDTH,
			height: BOTTOM_HEIGHT,
			zIndex: 80
		});

		this.torchLeft = new ImageView({
			parent: this.bottomNode,
			x: (BG_WIDTH - NODE_WIDTH) / 2 + TORCH_LEFT_WIDTH / 2,
			y: 7 * TORCH_LEFT_HEIGHT / 16,
			width: TORCH_LEFT_WIDTH,
			height: TORCH_LEFT_HEIGHT,
			image: "resources/images/Achievements/torchLeft.png",
			canHandleEvents: false
		});
		this.torchFlameLeft = new ParticleEngine({
			parent: this.torchLeft,
			x: 0,
			y: 0,
			r: LEFT_FLAME_R,
			anchorX: FLAME_WIDTH / 2,
			anchorY: FLAME_HEIGHT / 2,
			width: FLAME_WIDTH,
			height: FLAME_HEIGHT
		});

		this.torchRight = new ImageView({
			parent: this.bottomNode,
			x: (BG_WIDTH - NODE_WIDTH) / 2 + NODE_WIDTH - TORCH_RIGHT_WIDTH,
			y: TORCH_RIGHT_HEIGHT / 3,
			width: TORCH_RIGHT_WIDTH,
			height: TORCH_RIGHT_HEIGHT,
			image: "resources/images/Achievements/torchRight.png",
			canHandleEvents: false
		});
		this.torchFlameRight = new ParticleEngine({
			parent: this.torchRight,
			x: TORCH_RIGHT_WIDTH - FLAME_WIDTH,
			y: 0,
			r: RIGHT_FLAME_R,
			anchorX: FLAME_WIDTH / 2,
			anchorY: FLAME_HEIGHT / 2,
			width: FLAME_WIDTH,
			height: FLAME_HEIGHT
		});

		this.btnBack = new ButtonView({
			parent: this.bottomNode,
			x: (BG_WIDTH - NODE_WIDTH) / 2 + SIGN_OFFSET,
			y: this.bottomNode.style.height,
			width: BACK_WIDTH,
			height: BACK_HEIGHT,
			image: "resources/images/Store/btnBack.png",
			soundOnStart: bind(this.controller, 'playSound', 'ui_click'),
			onClick: bind(this, 'deconstructView', bind(this.controller, 'transitionToMainMenu'))
		});
		this.txtBack = new TextView({
			parent: this.btnBack,
			x: 0,
			y: BACK_LABEL_Y,
			r: BACK_LABEL_R,
			width: BACK_WIDTH,
			height: LABEL_HEIGHT,
			text: GC.app.l.translate("MENU"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});

		this.btnStats = new ButtonView({
			parent: this.bottomNode,
			x: (BG_WIDTH - NODE_WIDTH) / 2 + NODE_WIDTH - STAT_WIDTH * (1 / 8),
			y: this.bottomNode.style.height,
			width: STAT_WIDTH,
			height: STAT_HEIGHT,
			image: "resources/images/Store/btnSwap.png",
			soundOnStart: bind(this.controller, "playSound", "ui_click"),
			onClick: bind(this, "deconstructView", bind(this.controller, "transitionToStats"))
		});

		this.txtStats = new TextView({
			parent: this.btnStats,
			x: STAT_LABEL_X,
			y: STAT_LABEL_Y,
			r: STAT_LABEL_R,
			width: STAT_WIDTH,
			height: STAT_HEIGHT,
			text: GC.app.l.translate("STATS"),
			textAlign: 'center',
			size: config.fonts.buttonSign,
			color: config.colors.buttonSign,
			multiline: false,
			canHandleEvents: false
		});
	};

	this.resetView = function() {
		if (this.controller.playingSong != "achieves_music") {
			this.controller.pauseSong();
			this.controller.playSong("achieves_music");
		}

		this.controller.setHardwareBackButton(this.btnBack.onClick);

		this.nodeData = [];
		this.updateAchievementNodes();

		this.achievementList.setOffset(this.treeBounds.minY);

		this.controller.trackEvent("Achievements", {});
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		this.controller.blockInput();
		animate(this.btnBack).now({ y: this.bottomNode.style.height - 7 * BACK_HEIGHT / 8 }, SLIDE_TIME, animate.easeOut)
		.then(bind(this, function() {
			this.controller.transitionLockDown(false);
			this.controller.unblockInput();
		}));

		animate(this.btnStats).now({ 
			y: this.bottomNode.style.height - 7 * STAT_HEIGHT / 8
		}, SLIDE_TIME, animate.easeOut);
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			this.controller.blockInput();
			animate(this.btnBack).now({ y: this.style.height }, SLIDE_TIME, animate.easeIn)
			.then(bind(this, function() {
				this.controller.unblockInput();
				callback();
			}));

			animate(this.btnStats).now({ y: this.style.height }, SLIDE_TIME, animate.easeIn);
		}
	};

	var fnSort = function(a, b) {
		if (a.order < b.order) {
			return 1;
		} else if (a.order > b.order) {
			return -1;
		} else {
			return 0;
		}
	};

	this.updateAchievementNodes = function() {
		var achievements = this.controller.getData("achievements");

		// populate node data
		var index = 0;
		for (var i in achievementsConfig) {
			var achievo = achievementsConfig[i];
			var earnedLevel = achievements[i].level;

			if (achievo.type == TYPE_COUNTER) {
				var levels = achievo.levels;
				for (var i = 0; i < earnedLevel; i++) {
					var lvlData = levels[i];
					var node = {
						uid: achievo.id + i,
						order: lvlData.order,
						index: index++,
						name: lvlData.name,
						info: lvlData.info,
						icon: lvlData.icon,
						width: NODE_WIDTH,
						height: NODE_HEIGHT,
						barText: false,
						earned: true
					};

					this.nodeData.push(node);
				};

				if (i < levels.length) {
					var lvlData = levels[i];
					if (achievo.id == "berryCount") {
						var current = this.controller.getData("berriesCollected") || 0;
						var total = lvlData.value;
					} else if (achievo.id == "scoreCount") {
						var current = ((this.controller.getData("topScores") || [0])[0]) || 0;
						var total = lvlData.value;
					} else {
						var current = this.controller.getData("bestRun") || 0;
						var total = lvlData.value;
					}

					if (current >= total) {
						var barText = false;
					} else {
						var barText = current + " / " + total;
					}

					var node = {
						uid: achievo.id + i,
						order: lvlData.order,
						index: index++,
						name: lvlData.name,
						info: lvlData.info,
						icon: "resources/images/Achievements/iconMystery.png",
						width: NODE_WIDTH,
						height: NODE_HEIGHT,
						barText: barText,
						earned: false
					};

					this.nodeData.push(node);
				}
			} else {
				var node = {
					uid: achievo.id,
					order: achievo.order,
					index: index++,
					name: achievo.name,
					info: achievo.info,
					icon: earnedLevel ? achievo.icon : "resources/images/Achievements/iconMystery.png",
					width: NODE_WIDTH,
					height: NODE_HEIGHT,
					barText: false,
					earned: earnedLevel
				};

				this.nodeData.push(node);
			}
		}

		this.achievementList.updateData(this.nodeData);
		this.achievementList.sortList(fnSort);

		this.treeBounds = this.achievementList.getBounds();
	};

	this.emitFlameParticle = function(engine, flameImageArray, sign, rotation) {
		var roll = Math.random();
		if (roll < 0.7) { // FLAMES
			var data = engine.obtainParticleArray(1);
			var pObj = data[0];
			var ttl = Math.random() * 1200 + 400;
			var width = Math.random() * FLAME_WIDTH / 3 + 2 * FLAME_WIDTH / 3;
			var height = FLAME_HEIGHT * width / FLAME_WIDTH;

			pObj.image = flameImageArray[~~(Math.random() * flameImageArray.length)];
			pObj.x = Math.random() * FLAME_WIDTH / 3 - FLAME_WIDTH / 6 + 3 + Math.random() * 3;
			pObj.y = FLAME_HEIGHT - 1.2 * height;
			pObj.dr = rotation * 1000 / ttl;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.dheight = pObj.ddheight = Math.random() * 150;
			pObj.dy = pObj.ddy = -pObj.dheight;
			pObj.dscale = 0.1;
			pObj.opacity = 0;
			pObj.dopacity = 4000 / ttl;
			pObj.ddopacity = -4 * pObj.dopacity;
			pObj.ttl = ttl;

			engine.emitParticles(data);
		} else if (roll < 0.8) { // SPARKS
			var data = engine.obtainParticleArray(1);
			var pObj = data[0];
			var ttl = Math.random() * 6000 + 2000;
			var width = Math.random() * 10 + 2;
			var height = width;

			pObj.image = SPARK_IMAGE;
			pObj.x = FLAME_WIDTH / 2 - width / 2 + Math.random() * 16 + 8;
			pObj.y = Math.random() * FLAME_HEIGHT - height;
			pObj.dx = Math.random() * 50 - 25;
			pObj.dy = -Math.random() * 150 - 75;
			pObj.dr = Math.random() * 2 * Math.PI;
			pObj.ddx = Math.random() * 100 - 50;
			pObj.ddy = -Math.random() * 75;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.dax = Math.random() * 60 - 30;
			pObj.day = Math.random() * 60 - 30;
			pObj.ddax = -pObj.dax / 2;
			pObj.dday = -pObj.day / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.dopacity = -1000 / ttl;
			pObj.ttl = ttl;

			engine.emitParticles(data);
		} else if (roll < 0.85) { // SMOKE
			var data = engine.obtainParticleArray(1);
			var pObj = data[0];
			var ttl = Math.random() * 20000 + 10000;
			var width = Math.random() * 32 + 32;
			var height = width;

			pObj.image = SMOKE_IMAGE;
			pObj.x = FLAME_WIDTH / 2 - width / 2 + Math.random() * 16 + 8;
			pObj.y = -height;
			pObj.dx = Math.random() * 10 - 5;
			pObj.dy = -Math.random() * 140 - 70;
			pObj.dr = Math.random() * Math.PI / 4;
			pObj.ddx = Math.random() * 36 - 18;
			pObj.ddy = -Math.random() * 44 + 6;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.dscale = Math.random() * 0.5 + 0.25,
			pObj.opacity = 0.6,
			pObj.dopacity = -1000 / ttl;
			pObj.ddopacity = -Math.random() * 1000 / ttl;
			pObj.ttl = ttl;

			engine.emitParticles(data);
		}
	};

	this.tick = function(dt) {
		if (!this.style.visible || !this.viewBuilt) { return; }

		this.torchFlameLeft.runTick(dt);
		this.torchFlameRight.runTick(dt);

		var offset = this.achievementList.getOffset();
		var minY = this.treeBounds.minY;
		this.background.style.y = -1.1 * BG_HEIGHT + BG_HEIGHT * (minY - offset) / minY;

		// reduce particles on quick phones to avoid over clutter
		var roll = Math.random();
		if (dt <= 18 && roll <= 0.5) {
			return;
		} else if (dt <= 32 && roll <= 0.25) {
			return;
		}

		if (Math.random() < 0.5) {
			this.emitFlameParticle(this.torchFlameLeft, LEFT_FLAMES, -1, -LEFT_FLAME_R);
		} else {
			this.emitFlameParticle(this.torchFlameLeft, RIGHT_FLAMES, 1, -LEFT_FLAME_R);
		}

		if (Math.random() < 0.5) {
			this.emitFlameParticle(this.torchFlameRight, LEFT_FLAMES, -1, -RIGHT_FLAME_R);
		} else {
			this.emitFlameParticle(this.torchFlameRight, RIGHT_FLAMES, 1, -RIGHT_FLAME_R);
		}
	};
});
