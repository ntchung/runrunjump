import animate;
import ui.View as View;
import ...lib.TextView as TextView;
import ui.ImageView as ImageView;

import src.controllers.Controller as Controller;

exports = Class(View, function(supr) {

	var BG_WIDTH,
		BG_HEIGHT,
		TIME_UNTIL_FADE;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		BG_WIDTH = this.controller.config.ui.maxWidth;
		BG_HEIGHT = this.controller.config.ui.maxHeight;
		TIME_UNTIL_FADE = this.controller.config.ui.titleFadeTime;

		this.isMainView = true;

		this.designView();
	};

	this.designView = function() {
		this.title = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: GC.app.l.translate("resources/images/Locales/en") + "/Title/loadingBlank.png"
		});
	};

	this.resetView = function() {
		this.controller.playSong("run_menu_music");

		this.controller.setHardwareBackButton(false);

		animate(this).wait(TIME_UNTIL_FADE).then(bind(this, function() {
			this.deconstructView(bind(this.controller, 'transitionToMainMenu'));
		}));
	};

	this.constructView = function() {
		this.controller.transitionLockDown(false);
	};

	this.deconstructView = function(callback) {
		if (!this.controller.transitioning) {
			this.controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			callback();
		}
	};
});
