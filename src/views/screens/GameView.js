import animate;
import device;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.SpriteView as Sprite;
import ...lib.TextView as TextView;

import src.views.modals.PauseModalView as PauseModalView;
import src.views.modals.BasicModalView as BasicModalView;
import src.views.helpers.PlayerView as PlayerView;
import src.views.helpers.BabyView as BabyView;
import src.views.helpers.BeeView as BeeView;
import src.views.helpers.EchidnaView as EchidnaView;
import src.views.helpers.PooView as PooView;
import src.views.helpers.BerryMeter as BerryMeter;
import src.views.helpers.PowerupDisplayView as PowerupDisplayView;
import src.views.helpers.AngelSaveModal as AngelSaveModal;

import src.controllers.Controller as Controller;

import src.models.GameModel as GameModel;

import src.lib.ScoreView as ScoreView;
import src.lib.ButtonView as ButtonView;
import src.lib.ViewPool as ViewPool;
import src.lib.ParticleEngine as ParticleEngine;
import src.lib.Utils as Utils;

exports = Class(View, function(supr) {

	var controller = Controller.get(),
		config = controller.config,

		// CLASS CONSTANTS
		BG_WIDTH = config.ui.maxWidth,
		BG_HEIGHT = config.ui.maxHeight,
		BG_HEIGHT_BASE = 562,
		TRANS_TIME = config.ui.transitionTime,
		CAMERA_SPEED = config.game.camera.speed,
		CAMERA_MARGINS = config.game.camera.margins,
		CAMERA_OFFSET_MIN = config.game.camera.minY,
		CAMERA_OFFSET_MAX = config.game.camera.maxY,
		COIN_THRESHOLD = config.game.coins.threshold,
		COLLECTION_TIME = config.game.coins.collectionTime,
		COLLECTION_SCALING = config.game.coins.collectionScaling,
		COLLECTION_SCALING_CHANGE = config.game.coins.collectionScalingChange,
		PLATFORM_BUFFER_SIZE = config.game.model.platformBufferSize,
		DIVING_DRAG_RADIUS = config.game.player.divingDragRadius,
		PIXELS_TO_METERS = config.game.model.pixelsToMeters,
		UI_SPACING = config.ui.gameScreenSpacing,
		GRAVITY = config.game.model.gravity,
		ANGEL_VELOCITY = config.game.player.powers.angelFish.velocity,
		ANGEL_DX = 100,
		ANGEL_ROTATION = Math.PI / 7,
		ANGEL_TTL = 1875,
		ANGEL_WIDTH = 244,
		ANGEL_HEIGHT = 188,
		SHIELD_WIDTH = 108,
		SHIELD_HEIGHT = 150,
		SHIELD_INSET = 15,
		SHIELD_MIN_OPACITY = 0.25,
		SHIELD_WARNING_TIME = 3500,
		SHIELD_BLINK_RATE = 0.01,
		SHIELD_HOVER_MIN = -20,
		SHIELD_HOVER_MAX = 20,
		SHIELD_HOVER_RATE = 0.5,
		TURTLE_Y = 664,
		TURTLE_WIDTH = 173,
		TURTLE_HEIGHT = 133,
		TURTLE_EMERGE_TIME = 500,
		TURTLE_FLOAT_MIN = -12,
		TURTLE_FLOAT_MAX = 12,
		SPLASH_OFFSET = 25,
		RADIAL_WIDTH = 192,
		RADIAL_HEIGHT = 192,
		RADIAL_SPIN_RATE = Math.PI,
		BERRY_METER_FRAME_WIDTH = 121,
		BERRY_METER_FRAME_HEIGHT = 121,
		SCORE_WIDTH = 320,
		SCORE_HEIGHT = 42,
		SCORE_SCALE = SCORE_HEIGHT / 95,
		SCORE_SPACING = 0,
		FRIEND_COUNTER_WIDTH = 320,
		FRIEND_COUNTER_HEIGHT = 42,
		FRIEND_ICON_WIDTH = 60,
		FRIEND_ICON_HEIGHT = 60,
		MARKER_TEXT_WIDTH = 220,
		MARKER_TEXT_HEIGHT = 56,
		MARKER_TEXT_SCALE = MARKER_TEXT_HEIGHT / 95,
		MARKER_TEXT_SPACING = 0,
		MARKER_SPACING = config.game.markers.spacing,
		MARKER_OFFSET_X = config.game.markers.offsetX,
		MARKER_OFFSET_Y = config.game.markers.offsetY,
		PAUSE_BUTTON_WIDTH = 83,
		PAUSE_BUTTON_HEIGHT = 83,
		SPECIAL_SPARKLE_COUNT = 32,
		HALF_PARTICLES_SPEED = 900,
		QUARTER_PARTICLES_SPEED = 1500,
		NUMERAL_WIDTH = 130,
		NUMERAL_HEIGHT = 164,
		READY_WIDTH = 603,
		READY_HEIGHT = 160,
		GO_WIDTH = 477,
		GO_HEIGHT = 203,
		COUNTDOWN_START_TIME = 250,
		COUNTDOWN_PAUSE_TIME = 150,
		COUNTDOWN_SPARKLE_WIDTH = RADIAL_WIDTH * 3,
		COUNTDOWN_SPARKLE_HEIGHT = RADIAL_HEIGHT * 3,
		COUNTDOWN_START = 1,
		COUNTDOWN_PAUSE = 2,
		ALERT_ARROW_WIDTH = 80,
		ALERT_ARROW_HEIGHT = 80,
		ALERT_ICON_WIDTH = 160,
		ALERT_ICON_HEIGHT = 160,
		ENEMY_ICON_WIDTH = 120,
		ENEMY_ICON_HEIGHT = 120,
		ALERT_FRAME_WIDTH = 125,
		ALERT_FRAME_HEIGHT = 125,
		CALLOUT_TIME = 600,
		CALLOUT_WIDTH = 600,
		CALLOUT_HEIGHT = 77,
		TUT_CALLOUT_TIME = 1000,
		TUT_CALLOUT_Y = 540,
		TUT_CALLOUT_WIDTH = 600,
		TUT_CALLOUT_HEIGHT = 77,
		TUT_CALLOUT_SCALE = 1.5,
		TUT_CALLOUT_BOUNCE_SCALE = 2,
		HAND_WIDTH = 220,
		HAND_HEIGHT = 122;
		BESTRUN_WIDTH = 401,
		BESTRUN_HEIGHT = 76,
		FRIENDRUN_WIDTH = 500,
		FRIENDRUN_HEIGHT = 100,
		FRIENDRUN_INFO_WIDTH = 250,
		FRIENDRUN_INFO_HEIGHT = 50,
		ITEM_TYPE_BASIC = 1,
		ITEM_TYPE_BLUE = 2,
		ITEM_TYPE_MAGNET = 3,
		ITEM_TYPE_TURTLE_BAIT = 4,
		ITEM_TYPE_NESTABLE = 5,
		ITEM_TYPE_GLIDER = 6,
		ITEM_TYPE_SHIELD = 7,
		FIREFLY_WIDTH = 28,
		FIREFLY_HEIGHT = 28,
		FIREFLY_HOVER_MIN = -60,
		FIREFLY_HOVER_MAX = 60,
		FIREFLY_HOVER_RATE = 1,
		FIREFLY_SPEED = 125,
		FIREFLY_SPAWN_CHANCE = 0.12,
		FIREFLY_TOGGLE_CHANCE = 0.01,
		SNOW_FLAKE_SPAWN_CHANCE = 0.5,
		WATER_LEVEL = 675,
		BERRY_SCALE_INC = 0.015,

		// Z-INDICES, those in config are not used but documented here for reference
		Z_BACKGROUND = 10,
		Z_CLOUDS = 11, // in config
		Z_FARGROUND_TREES = 12, // in config
		Z_FARGROUND = 13, // in config
		Z_MIDGROUND_TREES = 14, // in config
		Z_MIDGROUND = 15, // in config
		Z_WATER_SLOW = 27, // in config
		Z_PLATFORMS = 28,
		Z_MARKERS = 29,
		Z_JOINTS = 29,
		Z_PLAYER = 30,
		Z_ENEMIES = 30.5,
		Z_EFFECTS = 31,
		Z_WATER_FAST = 31, // in config
		Z_COINS = 32,
		Z_SPARKLES = 33,
		Z_UI = 40,
		Z_UI_SPARKLES = 41,
		Z_PAUSE = 45,

		DISTANCE_UNIT = "m",
		ANIM_NAME_FLY = "fly",
		SOUND_NAME_SPLASH = "splash",
		SOUND_NAME_CLOUD_SPLASH = "cloud_splash",
		SOUND_NAME_ANGEL_FISH = "angel_fish",
		SOUND_NAME_POWERUP = "special_get",
		SOUND_NAME_BERRY_GRAB = "fruit_get",
		SOUND_NAME_CAGE_BREAK = "cage_break",
		IMG_ALERT_ARROW_YELLOW = "resources/images/Game/arrowAlert.png",
		IMG_ALERT_ARROW_ORANGE = "resources/images/Game/markerEnemy.png",
		IMG_ALERT_FRAME = "resources/images/Game/starBurstEnemy.png",
		IMG_SHIELD_AURA = "resources/images/Game/auraShield.png",
		URL_ECHIDNA = "resources/images/Enemies/Echidna/echidna",
		URL_BEE = "resources/images/Enemies/Bee/bee",
		URL_POO = "resources/images/Enemies/Poop/poop",
		IMG_RADIAL = "resources/images/Game/itemRadial.png",
		IMG_SPLASH = "resources/images/Game/particleSplash.png",
		IMG_SPLASH_PIRATE = "resources/images/Levels/levelPirate/splash.png",
		IMG_SPLASH_CLOUDS = [
			{
				img: "resources/images/Levels/levelValentine/splashBig.png",
				width: 194,
				height: 123
			},
			{
				img: "resources/images/Levels/levelValentine/splashMedium.png",
				width: 123,
				height: 101
			},
			{
				img: "resources/images/Levels/levelValentine/splashSmall.png",
				width: 67,
				height: 59
			}
		],
		IMG_SPARKLE = "resources/images/Game/itemSparkle.png",
		IMG_SPARKLE_WHITE = "resources/images/Game/sparkleWhite.png",
		IMG_SPARKLE_PINK = "resources/images/Game/sparklePink.png",
		IMG_BERRY = "resources/images/Game/itemBerry.png",
		IMG_FIREFLY_DARK = "resources/images/Game/fireflyDark.png",
		IMG_FIREFLY_LIT = "resources/images/Game/fireflyLit.png",
		IMG_SNOW_FLAKE = "resources/images/Avatars/avatarKiwiSanta/particleFeatherSanta.png",
		IMG_SHIELD_PARTICLE = "resources/images/Game/particleWisp.png",
		KEY_NESTABLES_COLLECTED = "nestablesCollected",
		CALLOUT_NESTABLE = "NESTABLE!",
		CALLOUT_NEW_NESTABLE = "NEW NESTABLE!",
		CALLOUT_HISCORE = "NEW HISCORE!",
		CALLOUT_BESTRUN = "BEST RUN!",
		CALLOUT_SHIELD = "KIWI SHIELD!",
		TRANSITION_EASE_OUT = "easeOut",
		TRANSITION_EASE_IN = "easeIn",
		ACHIEVE_ID_PINK = "pinkNestables",
		ACHIEVE_ID_GREEN = "greenNestables",
		ACHIEVE_ID_BUGS = "bugs",
		ACHIEVE_ID_BOTTLECAPS = "bottlecaps",
		ACHIEVE_ID_EGGS = "eggs",
		ACHIEVE_ID_JAPAN = "japanese",
		ACHIEVE_ID_PINK_KIWI = "pinkKiwi",
		ACHIEVE_ID_GREEN_KIWI = "greenKiwi",
		ACHIEVE_ID_ACE_KIWI = "aceKiwi",
		NESTABLE_ID_PINK = "5000",
		NESTABLE_ID_GREEN = "6000",
		NESTABLE_ID_BUGS = ["7000", "11000"],
		NESTABLE_ID_BOTTLECAPS = ["4000", "15000"],
		NESTABLE_ID_EGGS = ["2000", "16000"],
		NESTABLE_ID_JAPAN = ["13000", "14000"],
		AMB_TYPE_FIREFLY = "firefly",
		AMB_TYPE_SNOW = "snow",
		PHYS_TYPE_BOUNCY = "BOUNCY",
		PHYS_TYPE_LIGHT = "LIGHT",
		PHYS_TYPE_DUSTY = "DUSTY",
		LEVEL_DEFAULT = "default",
		LEVEL_MOONFEST = "moonfest",
		LEVEL_CHRISTMAS = "christmas",
		LEVEL_PIRATE = "pirate",
		LEVEL_VALENTINE = "valentine",
		RAINBOW_SPARKLES = [
			"resources/images/Game/sparklePink.png",
			"resources/images/Game/sparkleRed.png",
			"resources/images/Game/sparkleOrange.png",
			"resources/images/Game/itemSparkle.png",
			"resources/images/Game/sparkleGreen.png",
			"resources/images/Game/sparkleBlue.png",
			"resources/images/Game/sparklePurple.png"
		],

		TUTORIAL_HAND_IMAGE = "resources/images/Tutorial/tutorialHand.png",
		TUTORIAL_TAP_IMAGE = "resources/images/Tutorial/tutorialHandTap.png",
		TUTORIAL_HOLD_IMAGE = "resources/images/Tutorial/tutorialHandHold.png",
		CALLOUT_TIPS = "KIWI QUICK TIPS!",
		CALLOUT_JUMP = "TAP TO JUMP!",
		CALLOUT_FLOAT = "HOLD TO FLOAT!",
		CALLOUT_DIVE = "SWIPE TO DIVE!",
		CALLOUT_GREAT = "GREAT!";
		CALLOUT_EXCELLENT = "EXCELLENT!";

		// avatar variables
	var diveParticleImage,
		diveParticleType,
		diveParticleWidth,
		diveParticleHeight,
		diveParticleCount,
		diveDustImage,
		diveDustType,
		diveDustCount,
		playerX,
		playerWidth,
		playerHeight,
		angelX,
		onScreenX;


	var shieldParticleSpawns = [
			[ -15, 65, -120000],
			[ -15, -65, -120000],
			[ -45, 55, -120000],
			[ -45, -55, -120000],
			[ -60, 30, -120000],
			[ -60, -30, -120000],
			[ -75, 15, -140000],
			[ -75, -15, -140000],
			[ -90, 0, -160000]
		],

		shieldParticleTarget = [-400, 0];

	// initialization
	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.gameStarted = false;
		this.updateFriends = false;
		this.cameraOffset = CAMERA_OFFSET_MAX / 2;
		this.radialR = 0;
		this.shieldActive = false;

		this.firstPlay = controller.getData("firstPlay");
		this.usedSnowEngines = [];

		this.isMainView = true;
		this.designView();
	};

	// build the component subviews of the screen
	this.designView = function() {
		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: 0,
			width: BG_WIDTH,
			height: BG_HEIGHT_BASE,
			image: "resources/images/Levels/levelDefault/backgroundSky.png",
			zIndex: Z_BACKGROUND,
			canHandleEvents: false
		});

		// gameplay particles that move with the camera
		this.cameraParticles = new ParticleEngine({
			parent: this,
			x: 0,
			y: 0,
			anchorX: 1,
			anchorY: 1,
			width: 2,
			height: 2,
			zIndex: Z_EFFECTS,
			initCount: 200,
			initImage: IMG_SPARKLE
		});

		// all gameplay elements are behind these, but not the UI
		this.closeParticles = new ParticleEngine({
			parent: this,
			x: 0,
			y: 0,
			anchorX: 1,
			anchorY: 1,
			width: 2,
			height: 2,
			zIndex: Z_SPARKLES,
			initCount: 80,
			initImage: IMG_SPARKLE
		});

		// used for fireflies etc.
		this.ambientPool = new ViewPool({
			ctor: ImageView,
			initCount: 50,
			initOpts: {
				parent: this.stationaryParticles,
				x: 0,
				y: 0,
				width: 1,
				height: 1,
				image: IMG_FIREFLY_DARK
			}
		});

		// UI particles that are not modified by the camera
		this.stationaryParticles = new ParticleEngine({
			parent: this,
			initCount: 200,
			x: 0,
			y: 0,
			anchorX: 1,
			anchorY: 1,
			width: 2,
			height: 2,
			zIndex: Z_UI_SPARKLES
		});

		/*
		this.shieldView = new ImageView({
			parent: this,
			x: 0,
			y: 0,
			zIndex: Z_PLAYER,
			width: SHIELD_WIDTH,
			height: SHIELD_HEIGHT,
			image: IMG_SHIELD_AURA,
			opacity: 1,
			canHandleEvents: false,
			visible: false
		});
		*/

		this.shieldView = new Sprite({
			parent: this,
			x: 0,
			y: 0,
			zIndex: Z_PLAYER,
			width: SHIELD_WIDTH,
			height: SHIELD_HEIGHT,
			image: IMG_SHIELD_AURA,
			opacity: 1,
			canHandleEvents: false,
			visible: false,
			loop: true,
			autoStart: false,
			url: 'resources/images/Game/shield',
			defaultAnimation: 'wind'
		});

		this.shieldView.blinkDirection = -1;
		this.shieldView.hoverDirection = -1;
		this.shieldView.hover = 0;

		this.shieldParticles = new ParticleEngine({
			parent: this,
			initCount: 30,
			x: 0,
			y: 0,
			zIndex: 29.5,
			width: 1,
			height: 1,
			opacity: 1,
			canHandleEvents: false,
			visible: false
		})

		// UI parent layer
		this.UILayer = new View({
			parent: this,
			x: 0,
			y: 0,
			width: this.style.width,
			height: this.style.height,
			zIndex: Z_UI,
			canHandleEvents: false
		});

		this.pauseModal = new PauseModalView({
			parent: this,
			x: 0,
			y: 0,
			height: this.style.height,
			width: this.style.width,
			visible: false,
			noAutoOpen: true,
			noSlide: true,
			zIndex: Z_PAUSE
		});

		this.berryMeter = new BerryMeter({
			parent: this.UILayer,
			x: 0,
			y: 0,
			width: BERRY_METER_FRAME_WIDTH,
			height: BERRY_METER_FRAME_HEIGHT,
			blockEvents: true,
			canHandleEvents: false,
			config: config
		});

		this.scoreCounter = new ScoreView({
			parent: this.UILayer,
			x: BERRY_METER_FRAME_WIDTH + UI_SPACING,
			y: UI_SPACING,
			width: SCORE_WIDTH,
			height: SCORE_HEIGHT,
			spacing: SCORE_SPACING,
			initCount: 12,
			characterData: {
				"0": { width: 95 * SCORE_SCALE, image: "resources/images/Game/teal_0.png" },
				"1": { width: 42 * SCORE_SCALE, image: "resources/images/Game/teal_1.png" },
				"2": { width: 70 * SCORE_SCALE, image: "resources/images/Game/teal_2.png" },
				"3": { width: 71 * SCORE_SCALE, image: "resources/images/Game/teal_3.png" },
				"4": { width: 74 * SCORE_SCALE, image: "resources/images/Game/teal_4.png" },
				"5": { width: 69 * SCORE_SCALE, image: "resources/images/Game/teal_5.png" },
				"6": { width: 74 * SCORE_SCALE, image: "resources/images/Game/teal_6.png" },
				"7": { width: 75 * SCORE_SCALE, image: "resources/images/Game/teal_7.png" },
				"8": { width: 69 * SCORE_SCALE, image: "resources/images/Game/teal_8.png" },
				"9": { width: 74 * SCORE_SCALE, image: "resources/images/Game/teal_9.png" }
			},
			textAlign: 'left'
		});

		var
			friendUIWidth = FRIEND_ICON_WIDTH + ALERT_ARROW_WIDTH + FRIEND_COUNTER_WIDTH + UI_SPACING * 2,
			friendUIHeight = ALERT_ARROW_HEIGHT;

		this.friendUIWrapper = new View ({
			parent: this.UILayer,
			x: this.style.width - (friendUIWidth + UI_SPACING),
			y: this.style.height - (friendUIHeight + UI_SPACING),
			width: friendUIWidth,
			height: friendUIHeight,
			visible: false
		})

		this.friendCounterIcon = new ImageView({
			parent: this.friendUIWrapper,
			x: this.friendUIWrapper.style.width - (ALERT_ARROW_WIDTH + FRIEND_ICON_WIDTH + UI_SPACING),
			y: (ALERT_ARROW_HEIGHT - FRIEND_ICON_HEIGHT) / 2,
			width: FRIEND_ICON_WIDTH,
			height: FRIEND_ICON_HEIGHT,
			image: "resources/images/Game/iconFriend.png"
		});

		this.friendCounterArrow = new ImageView({
			parent: this.friendUIWrapper,
			x: this.friendUIWrapper.style.width - ALERT_ARROW_WIDTH,
			y: 0,
			width: ALERT_ARROW_WIDTH,
			height: ALERT_ARROW_HEIGHT,
			anchorX: ALERT_ARROW_WIDTH / 2,
			anchorY: ALERT_ARROW_HEIGHT / 2,
			r: -Math.PI / 2,
			image: "resources/images/Game/markerFriend.png",
			canHandleEvents: false
		});

		this.friendCounter = new ScoreView({
			parent: this.friendUIWrapper,
			x: 0,
			y: (ALERT_ARROW_HEIGHT - FRIEND_COUNTER_HEIGHT) / 2,
			width: FRIEND_COUNTER_WIDTH,
			height: FRIEND_COUNTER_HEIGHT,
			spacing: SCORE_SPACING,
			initCount: 6,
			characterData: {
				"0": { width: 95 * SCORE_SCALE, image: "resources/images/Game/friend_0.png" },
				"1": { width: 42 * SCORE_SCALE, image: "resources/images/Game/friend_1.png" },
				"2": { width: 70 * SCORE_SCALE, image: "resources/images/Game/friend_2.png" },
				"3": { width: 71 * SCORE_SCALE, image: "resources/images/Game/friend_3.png" },
				"4": { width: 74 * SCORE_SCALE, image: "resources/images/Game/friend_4.png" },
				"5": { width: 69 * SCORE_SCALE, image: "resources/images/Game/friend_5.png" },
				"6": { width: 74 * SCORE_SCALE, image: "resources/images/Game/friend_6.png" },
				"7": { width: 75 * SCORE_SCALE, image: "resources/images/Game/friend_7.png" },
				"8": { width: 69 * SCORE_SCALE, image: "resources/images/Game/friend_8.png" },
				"9": { width: 74 * SCORE_SCALE, image: "resources/images/Game/friend_9.png" }
			},
			textAlign: "right"
		});

		this.powerupDisplay = new PowerupDisplayView({
			parent: this.UILayer,
			x: 0,
			y: BERRY_METER_FRAME_HEIGHT
		});

		this.pauseButton = new ButtonView({
			parent: this.UILayer,
			x: this.style.width - (PAUSE_BUTTON_WIDTH + UI_SPACING),
			y: UI_SPACING,
			width: PAUSE_BUTTON_WIDTH,
			height: PAUSE_BUTTON_HEIGHT,
			image: "resources/images/Game/button_pause.png",
			soundOnStart: bind(this, function() {
				controller.playSound("ui_click");
			}),
			onClick: bind(this, function(evt) {
				evt && evt.cancel();
				!this.gamePaused ? this.pause() : this.unpause();
			}),
			canHandleEvents: false
		});

		this.countdownSparkle = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - COUNTDOWN_SPARKLE_WIDTH) / 2,
			y: BG_HEIGHT,
			anchorX: COUNTDOWN_SPARKLE_WIDTH / 2,
			anchorY: COUNTDOWN_SPARKLE_HEIGHT / 2,
			width: COUNTDOWN_SPARKLE_WIDTH,
			height: COUNTDOWN_SPARKLE_HEIGHT,
			image: IMG_RADIAL,
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});

		this.countdownOne = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - NUMERAL_WIDTH) / 2,
			y: BG_HEIGHT,
			width: NUMERAL_WIDTH,
			height: NUMERAL_HEIGHT,
			image: "resources/images/Game/orange1.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});
		this.countdownOne.white = new ImageView({
			parent: this.countdownOne,
			x: 0,
			y: 0,
			width: NUMERAL_WIDTH,
			height: NUMERAL_HEIGHT,
			image: "resources/images/Game/white1.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});

		this.countdownTwo = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - NUMERAL_WIDTH) / 2,
			y: BG_HEIGHT,
			width: NUMERAL_WIDTH,
			height: NUMERAL_HEIGHT,
			image: "resources/images/Game/orange2.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});
		this.countdownTwo.white = new ImageView({
			parent: this.countdownTwo,
			x: 0,
			y: 0,
			width: NUMERAL_WIDTH,
			height: NUMERAL_HEIGHT,
			image: "resources/images/Game/white2.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});

		this.countdownThree = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - NUMERAL_WIDTH) / 2,
			y: BG_HEIGHT,
			width: NUMERAL_WIDTH,
			height: NUMERAL_HEIGHT,
			image: "resources/images/Game/orange3.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});
		this.countdownThree.white = new ImageView({
			parent: this.countdownThree,
			x: 0,
			y: 0,
			width: NUMERAL_WIDTH,
			height: NUMERAL_HEIGHT,
			image: "resources/images/Game/white3.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});

		this.countdownReady = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - READY_WIDTH) / 2,
			y: BG_HEIGHT,
			width: READY_WIDTH,
			height: READY_HEIGHT,
			image: GC.app.l.translate("resources/images/Locales/en") + "/Game/Intro/orangeReady.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});
		this.countdownReady.white = new ImageView({
			parent: this.countdownReady,
			x: 0,
			y: 0,
			width: READY_WIDTH,
			height: READY_HEIGHT,
			image: GC.app.l.translate("resources/images/Locales/en") + "/Game/Intro/whiteReady.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});

		this.countdownGo = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - GO_WIDTH) / 2,
			y: BG_HEIGHT,
			width: GO_WIDTH,
			height: GO_HEIGHT,
			image: GC.app.l.translate("resources/images/Locales/en") + "/Game/Intro/orangeGo.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});
		this.countdownGo.white = new ImageView({
			parent: this.countdownGo,
			x: 0,
			y: 0,
			width: GO_WIDTH,
			height: GO_HEIGHT,
			image: GC.app.l.translate("resources/images/Locales/en") + "/Game/Intro/whiteGo.png",
			opacity: 0,
			canHandleEvents: false,
			visible: false
		});

		this.alertFrame = new ImageView({
			parent:this.UILayer,
			x: this.style.width - ALERT_ARROW_WIDTH - ALERT_FRAME_WIDTH,
			y: 0,
			width: ALERT_FRAME_WIDTH,
			height: ALERT_FRAME_HEIGHT,
			image: IMG_ALERT_FRAME,
			opacity: 1,
			visible: false,
			canHandleEvents: false
		});

		this.alertIcon = new ImageView({
			parent: this.UILayer,
			x: this.style.width - ALERT_ARROW_WIDTH - ALERT_ICON_WIDTH,
			y: 0,
			width: ALERT_ICON_WIDTH,
			height: ALERT_ICON_HEIGHT,
			image: IMG_RADIAL, // placeholder
			opacity: 0.75,
			visible: false,
			canHandleEvents: false
		});
		this.alertIcon.iconSet = false;

		this.alertArrow = new ImageView({
			parent: this.UILayer,
			x: this.style.width - ALERT_ARROW_WIDTH,
			y: 0,
			width: ALERT_ARROW_WIDTH,
			height: ALERT_ARROW_HEIGHT,
			image: IMG_ALERT_ARROW_YELLOW,
			visible: false,
			canHandleEvents: false
		});

		this.calloutSparkle = new ImageView({
			parent: this.stationaryParticles,
			x: 0,
			y: 0,
			r: this.radialR,
			anchorX: RADIAL_WIDTH / 2,
			anchorY: RADIAL_HEIGHT / 2,
			width: RADIAL_WIDTH,
			height: RADIAL_HEIGHT,
			image: IMG_RADIAL,
			visible: false,
			canHandleEvents: false
		});

		this.calloutText = new ImageView({
			parent: this.stationaryParticles,
			x: (this.style.width - CALLOUT_WIDTH) / 2,
			y: -CALLOUT_HEIGHT,
			anchorX: CALLOUT_WIDTH / 2,
			anchorY: CALLOUT_HEIGHT / 2,
			width: CALLOUT_WIDTH,
			height: CALLOUT_HEIGHT,
			scale: 0.5,
			visible: false
		});

		if (this.firstPlay) {
			this.tutCalloutSparkle = new ImageView({
				parent: this.UILayer,
				x: 0,
				y: 0,
				r: this.radialR,
				anchorX: RADIAL_WIDTH / 2,
				anchorY: RADIAL_HEIGHT / 2,
				width: RADIAL_WIDTH,
				height: RADIAL_HEIGHT,
				image: IMG_RADIAL,
				visible: false,
				canHandleEvents: false
			});

			this.tutCalloutText = new ImageView({
				parent: this.UILayer,
				x: BG_WIDTH,
				y: TUT_CALLOUT_Y,
				anchorX: CALLOUT_WIDTH / 2,
				anchorY: CALLOUT_HEIGHT / 2,
				width: CALLOUT_WIDTH,
				height: CALLOUT_HEIGHT,
				scale: TUT_CALLOUT_SCALE,
				visible: false
			});

			this.tutHand = new ImageView({
				parent: this.UILayer,
				x: 0,
				y: 0,
				anchorX: HAND_WIDTH / 2,
				anchorY: HAND_HEIGHT / 2,
				width: HAND_WIDTH,
				height: HAND_HEIGHT,
				image: "resources/images/Tutorial/tutorialHand.png",
				opacity: 0,
				canHandleEvents: false,
				visible: false
			});

			this.tutEndModal = new BasicModalView({
				parent: this,
				message: "Great work! Now, you're ready to run! Good luck!",
				noAutoOpen: true,
				visible: false
			});
		}

		this.angelModal = new AngelSaveModal({
			parent: this.UILayer,
			gameView: this
		});
	};

	this.pause = function() {
		// tutorial scripting
		if (this.firstPlay) {
			this.gamePaused = true;
			this.playerView.pause();
			return;
		}

		if (this.model.angelModalActive || this.model.gameoverCountdown || this.model.finished) {
			return;
		}

		this.gamePaused = true;
		controller.deactivateGameInput();

		this.pauseModal.openModal();
		controller.pauseSong();

		// pause player view
		this.playerView.pause();
		// pause echidna that may have spawned
		if (this.model.echidnaSpawn && this.model.echidnaSpawn.view) {
			this.model.echidnaSpawn.view.pause();
		}
		// pause bee that may have spawned
		if (this.model.beeSpawn && this.model.beeSpawn.view) {
			this.model.beeSpawn.view.pause();
		}
		// pause poo that may have spawned
		if (this.model.pooSpawn && this.model.pooSpawn.view) {
			for(var i = 0 ; i< this.model.pooSpawn.count; i++){
				this.model.pooSpawn.view[i].pause();
			}
		}

		this.shieldActive && this.shieldView.pause();


		// pause any trailing babies
		for (var i = 0; i < this.babyViews.length; i++) {
			this.babyViews[i].pause();
		}
	};

	this.unpause = function() {
		if (this.firstPlay) {
			this.gamePaused = false;
			this.playerView.resume();
			return;
		}

		this.runPauseCountdown();
	};

	this.runStartingCountdown = function() {
		var model = this.model;

		controller.enableBackButton(false);
		controller.blockInput();
		this.countdownType = COUNTDOWN_START;
		model.countdownActive = true;

		// tutorial scripting
		if (this.firstPlay) {
			this.playerView.pause();
			this.playerView.setImage(model.player.startFrame);
			this.playerView.style.visible = true;

			setTimeout(bind(this, function() {
				this.gameStarted = true;
				model.countdownActive = false;

				// unpause player view
				this.playerView.resume();
				// unpause echidna that may have spawned
				if (model.echidnaSpawn && model.echidnaSpawn.view) {
					model.echidnaSpawn.view.resume();
				}
				// unpause bee that may have spawned
				if (model.beeSpawn && model.beeSpawn.view) {
					model.beeSpawn.view.resume();
				}
				// unpause poo that may have spawned
				if (model.pooSpawn && model.pooSpawn.view) {
					for(var i = 0 ; i< this.model.pooSpawn.count; i++){
						this.model.pooSpawn.view[i].resume();
					}
				}

				this.animateCallout("KIWI QUICK TIPS!");

				// don't re-enable input or back button during tutorial
				controller.transitionLockDown(false);
			}), TRANS_TIME);
		} else {
			setTimeout(bind(this, function() {
				this.animateCountdownView(this.countdownReady, bind(this, function() {
					this.animateCountdownView(this.countdownGo, bind(this, function() {
						this.gameStarted = true;
						model.countdownActive = false;

						// unpause player view
						this.playerView.resume();
						// unpause echidna that may have spawned
						if (model.echidnaSpawn && model.echidnaSpawn.view) {
							model.echidnaSpawn.view.resume();
						}
						// unpause bee that may have spawned
						if (model.beeSpawn && model.beeSpawn.view) {
							model.beeSpawn.view.resume();
						}
						// unpause poo that may have spawned
						if (model.pooSpawn && model.pooSpawn.view) {
							for(var i = 0 ; i< this.model.pooSpawn.count; i++){
								this.model.pooSpawn.view[i].resume();
							}
						}
						// unpause any trailing babies
						for (var i = 0; i < this.babyViews.length; i++) {
							this.babyViews[i].resume();
						}

						if (model.player.selectedAvatar == "avatarKiwiPink/kiwiPink") {
							controller.achieveCheck(ACHIEVE_ID_PINK_KIWI);
						} else if (model.player.selectedAvatar == "avatarKiwiGreen/kiwigreen") {
							controller.achieveCheck(ACHIEVE_ID_GREEN_KIWI);
						} else if (model.player.selectedAvatar == "avatarKiwiAce/kiwiAce") {
							controller.achieveCheck(ACHIEVE_ID_ACE_KIWI);
						}

						controller.transitionLockDown(false);
						controller.unblockInput();
						controller.enableBackButton(true);
					}));
				}));
			}), TRANS_TIME);
		}
	};

	this.runPauseCountdown = function() {
		var model = this.model;

		controller.enableBackButton(false);
		controller.blockInput();
		this.countdownType = COUNTDOWN_PAUSE;
		model.countdownActive = true;

		this.animateCountdownView(this.countdownThree, bind(this, function() {
			this.animateCountdownView(this.countdownTwo, bind(this, function() {
				this.animateCountdownView(this.countdownOne, bind(this, function() {
					this.animateCountdownView(this.countdownGo, bind(this, function() {
						model.countdownActive = false;
						this.gamePaused = false;

						// unpause player view
						this.playerView.resume();
						// unpause a baby that may have spawned
						if (model.echidnaSpawn && model.echidnaSpawn.view) {
							model.echidnaSpawn.view.resume();
						}
						// unpause bee that may have spawned
						if (model.beeSpawn && model.beeSpawn.view) {
							model.beeSpawn.view.resume();
						}
						// unpause poo that may have spawned
						if (this.model.pooSpawn && this.model.pooSpawn.view) {
							for(var i = 0 ; i< this.model.pooSpawn.count; i++){
								this.model.pooSpawn.view[i].resume();
							}
						}
						// unpause any trailing babies
						for (var i = 0; i < this.babyViews.length; i++) {
							this.babyViews[i].resume();
						}

						this.shieldActive && this.shieldView.resume();

						controller.resumeSong();
						controller.activateGameInput(this);
						controller.unblockInput();
						controller.enableBackButton(true);
					}));
				}));
			}));
		}));
	};

	this.animateCountdownView = function(view, cb) {
		var vs = view.style;
		var ts = this.style;
		vs.x = (ts.width - vs.width) / 2;
		vs.y = BG_HEIGHT;
		vs.opacity = 1;
		vs.visible = true;

		var ws = view.white.style;
		ws.opacity = 1;
		ws.visible = true;

		var sparkle = this.countdownSparkle;
		var ss = sparkle.style;
		ss.x = (ts.width - ss.width) / 2;
		ss.y = (ts.height - ss.height) / 2;
		ss.opacity = 0;
		ss.visible = true;

		if (this.countdownType == COUNTDOWN_START) {
			var time = COUNTDOWN_START_TIME;
			var dr = Math.PI / 3;
		} else if (this.countdownType == COUNTDOWN_PAUSE) {
			var time = COUNTDOWN_PAUSE_TIME;
			var dr = Math.PI / 3 * COUNTDOWN_PAUSE_TIME / COUNTDOWN_START_TIME;
		}

		// bind fns to animate view
		var boundCountdownFinish = bind(this, function() {
			vs.visible = false;
			cb && cb();
		});

		var boundCountdownSparkleHide = bind(this, function() {
			ss.visible = false;

			// animate orange fade to white while leaving the center
			var orangeFadeData = this.stationaryParticles.obtainObject();
			orangeFadeData.delay = 2 * time;
			orangeFadeData.target = true;
			orangeFadeData.topacity = 1;
			orangeFadeData.ttl = time / 3;
			this.stationaryParticles.addExternalParticle(view.white, orangeFadeData);

			var moveData = this.stationaryParticles.obtainObject();
			moveData.target = true;
			moveData.ty = -vs.height;
			moveData.ttl = time;
			moveData.transition = TRANSITION_EASE_IN;
			moveData.onDeath = boundCountdownFinish;
			this.stationaryParticles.addExternalParticle(view, moveData);
		});

		var boundCountdownSparkleFade = bind(this, function() {
			var spinFadeData = this.stationaryParticles.obtainObject();
			spinFadeData.target = true;
			spinFadeData.tr = ss.r + dr;
			spinFadeData.topacity = 0;
			spinFadeData.ttl = time;
			spinFadeData.onDeath = boundCountdownSparkleHide;
			this.stationaryParticles.addExternalParticle(sparkle, spinFadeData);
		});

		var boundCountdownSparkleSpin = bind(this, function() {
			var spinData = this.stationaryParticles.obtainObject();
			spinData.target = true;
			spinData.tr = ss.r + dr;
			spinData.topacity = 1;
			spinData.ttl = time;
			spinData.onDeath = boundCountdownSparkleFade;
			this.stationaryParticles.addExternalParticle(sparkle, spinData);
		});

		// animate white fade to orange while centering
		var whiteFadeData = this.stationaryParticles.obtainObject();
		whiteFadeData.target = true;
		whiteFadeData.topacity = 0;
		whiteFadeData.ttl = time / 3;
		whiteFadeData.delay = 2 * time / 3;
		this.stationaryParticles.addExternalParticle(view.white, whiteFadeData);

		// animate view to center
		var centerData = this.stationaryParticles.obtainObject();
		centerData.target = true;
		centerData.ty = (ts.height - vs.height) / 2;
		centerData.ttl = time;
		centerData.transition = TRANSITION_EASE_OUT;
		centerData.onDeath = boundCountdownSparkleSpin;
		this.stationaryParticles.addExternalParticle(view, centerData);
	};

	// ensures state and data get reset each time the view becomes active
	this.resetView = function() {
		// if an echidna had spawned and been assigned a view, we should release it
		if (this.model && this.model.echidnaSpawn && this.model.echidnaSpawn.view) {
			this.echidnaPool.releaseView(this.model.echidnaSpawn.view);
			this.model.echidnaSpawn = false;
		}

		// if a bee had spawned and been assigned a view, we should release it
		if (this.model && this.model.beeSpawn && this.model.beeSpawn.view) {
			this.beePool.releaseView(this.model.beeSpawn.view);
			this.model.beeSpawn = false;
		}

		// if a bee had spawned and been assigned a view, we should release it
		if (this.model && this.model.pooSpawn && this.model.pooSpawn.view) {
			for(var i = 0 ; i< this.model.pooSpawn.count; i++){
				this.pooPool.releaseView(this.model.pooSpawn.view[i]);
			}
			this.model.pooSpawn = undefined;
		}

		this.deconstructed = false;
		this.onInputMove = false;
		this.gameStarted = false;
		this.gamePaused = false;
		this.onInputMove = false;
		this.updateFriends = false;

		controller.activateGameInput(this);

		controller.setHardwareBackButton(this.pauseButton.onClick);

		this.powerupDisplay.deactivateAll();

		this.model = new GameModel();

		if (!this.babyPool) {
			this.babyPool = new ViewPool({
				ctor: BabyView,
				initCount: 3,
				initOpts: {
					parent: this,
					babyID: "babyTeal/babyTeal",
					url: "resources/images/Babies/babyTeal/babyTeal"
				}
			});

			this.echidnaPool = new ViewPool({
				ctor: EchidnaView,
				initCount: 2,
				initOpts: {
					parent: this,
					url: "resources/images/Enemies/Echidna/echidna"
				}
			});

			this.beePool = new ViewPool({
				ctor: BeeView,
				initCount: 2,
				initOpts: {
					parent: this,
					url: "resources/images/Enemies/Bee/bee"
				}
			});

			this.pooPool = new ViewPool({
				ctor: PooView,
				initCount: 6,
				initOpts: {
					parent: this,
					url: "resources/images/Enemies/Poop/poop"
				}
			});

			this.boundEchidnaObtain = bind(this, function(view, opts) {
				view.resetView(opts);
				view.startNextAnimation();
			});
			this.boundEchidnaRelease = bind(this, function(view) {
				this.echidnaPool.releaseView(view);
			});

			this.boundBeeObtain = bind(this, function(view, opts) {
				view.resetView(opts);
				view.setFlying(true);
			});
			this.boundBeeRelease = bind(this, function(view) {
				this.beePool.releaseView(view);
			});

			this.boundPooObtain = bind(this, function(view, opts) {
				view.resetView(opts);
				view.setAlive();
			});
			this.boundPooRelease = bind(this, function(view) {
				this.pooPool.releaseView(view);
			});

			this.boundSplash = bind(this, function(view) {
				this.animateSplash(view);
			});

			this.boundBounce = bind(this, function(particle) {
				particle.pData.dy = -0.55 * particle.pData.dy;
			});

			this.boundAngelSound = bind(this, function() {
				controller.playSound(SOUND_NAME_ANGEL_FISH);
			});
		}

		this.resetPirateViews();
		this.resetSpriteViews();
		this.resetPlayerView();
		this.resetBabyViews();
		this.resetParallaxViews(); // must happen before ambient
		this.resetAmbientViews(); // must happen after parallax
		this.resetPlatformViews();
		this.resetDistanceMarkers();
		this.resetAlerts();
		this.resetCoinViews();
		this.resetCounterViews();
		this.resetSpecialViews();
		this.resetShieldView();

		// clean up any left over particles floating about
		this.cameraParticles.killAllParticles();
		this.closeParticles.killAllParticles();
		this.stationaryParticles.killAllParticles();

		this.radialR = 0;
		this.cameraOffset = CAMERA_OFFSET_MAX / 2;

		controller.playSong(this.model.levelConfig.music);

		this.gameInitialized = true;

		this.angelModal.style.visible = false;
		this.model.subscribe('angelSaveModal', this.angelModal, 'showModal');
	};

	/*this.asyncReset = function(cb) {
		var that = this,
			socialController = controller.socialController,
			finish = function() {
				var friendsActive;

				// generate friends list
				that.model.setFriendRuns();
				that.model.postAsyncCheckMarkerSpawn();

				friendsActive = that.model.isFriendsActive();

				if (friendsActive) {
					that.friendUIWrapper.style.visible = true;
					that.updateFriends = true;
				}

				logger.log("===Finish async reset!!!");
				cb(false, friendsActive);
			};
		finish();
		if ( socialController.isScoresReady() ) {
			socialController.getFriendsScores();
			finish();
		} else {
			socialController.getFriendsScores(finish, 8000);
		}
	};*/

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {
		this.runStartingCountdown();
	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (!controller.transitioning) {
			controller.transitionLockDown(true);
		} else {
			return;
		}

		if (callback) {
			this.deconstructed = true;
			controller.deactivateGameInput();
			callback();
		}
	};
/*
	this.getTextPreloadArray = function() {
		var
			config = controller.config,
			markerConfig = config.game.markers,
			achievementsConfig = controller.achievementsConfig,
			coinConfig = config.game.coins,
			achievementData = controller.getData('achievements'),
			result,
			achievementsList,
			keys,
			key,
			configData,
			data,
			i,
			len,
			j,
			jLen;

		// initialize base
		result = [
			{
				size: config.fonts.achievementToast,
				color: config.colors.achievementToast,
				multiline: false,
				width: FRIENDRUN_WIDTH,
				height: FRIENDRUN_HEIGHT,
				text: this.model.friendRuns.map(function(score) {
					return controller.socialController.getUsername(score.getUser());
				})
			}, {
				size: config.fonts.achievementInfo,
				color: config.colors.achievementToast,
				multiline: false,
				width: FRIENDRUN_INFO_WIDTH,
				height: FRIENDRUN_INFO_HEIGHT,
				text: [GC.app.l.translate("You Passed")]
			}
		];

		return result;
	};
*/
	this.resetPlayerView = function() {
		var model = this.model,
			player = model.player;

		playerX = player.x;
		playerWidth = player.w;
		playerHeight = player.h;
		angelX = playerX - ANGEL_WIDTH / 2;

		if (this.playerView) { // reset playerView and subviews
			this.playerView.stopAnimation();
			this.playerView.resetView({
				x: playerX,
				y: player.y - this.cameraOffset,
				anchorX: playerWidth / 2,
				anchorY: playerHeight / 2,
				width: playerWidth,
				height: playerHeight,
				url: "resources/images/Avatars/" + player.selectedAvatar
			});
		} else { // initialize playerView and subviews only once
			this.playerView = new PlayerView({
				parent: this,
				x: player.x,
				y: player.y - this.cameraOffset,
				anchorX: playerWidth / 2,
				anchorY: playerHeight / 2,
				width: playerWidth,
				height: playerHeight,
				url: "resources/images/Avatars/" + player.selectedAvatar,
				zIndex: Z_PLAYER
			});
		}

		diveParticleImage = player.diveParticleImage;
		diveParticleType = player.diveParticleType;
		diveParticleWidth = player.diveParticleWidth;
		diveParticleHeight = player.diveParticleHeight;
		diveParticleCount = player.diveParticleCount;
		diveDustImage = player.diveDustImage;
		diveDustType = player.diveDustType;
		diveDustCount = player.diveDustCount;

		this.boundInputMove = bind(this, "gliderDrag");
	};

	this.resetShieldView = function() {
		this.shielded = false;
		this.shieldView.style.visible = false;
		this.shieldParticles.style.visible = false;
	}

	this.resetBabyViews = function() {
		var model = this.model,
			babies = model.babies;

		if (this.babyViews) {
			for (var i = 0; i < this.babyViews.length; i++) {
				this.babyPool.releaseView(this.babyViews[i]);
			}
		}

		this.babyViews = [];

		for (var b in babies) {
			var baby = babies[b];
			var babyData = this.babyPool.obtainObject();
			babyData.parentView = this;
			babyData.babyID = baby.id;
			babyData.x = baby.x;
			babyData.y = baby.y;
			babyData.zIndex = Z_PLAYER;
			babyData.anchorX = baby.width / 2;
			babyData.anchorY = baby.height / 2;
			babyData.width = baby.width;
			babyData.height = baby.height;
			babyData.url = "resources/images/Babies/" + baby.id;
			babyData.visible = true;
			var view = this.babyPool.obtainView(babyData, bind(this, function(v, opts) {
				v.resetView(opts);
				v.setArrayIndex(b);
			}));

			view.goneForGood = false;
			this.babyViews.push(view);
		}
	};

	this.gliderDrag = function(evt) {
		this.model.lastFingerY = evt.srcPt.y * BG_HEIGHT / device.height;
		evt.cancel();
	};

	this.resetParallaxViews = function() {
		var random = Math.random;

		this.parallaxConfig = this.model.levelConfig.parallax;
		this.background.setImage(this.model.levelConfig.background);

		// clean out any leftover parallax views
		for (var i in this.parallaxLayers) {
			var oldLayer = this.parallaxLayers[i];
			if (oldLayer) {
				// save particle engines, they would be expensive to GC
				var oldEngine = oldLayer.engine;
				if (oldEngine) {
					oldEngine.removeFromSuperview();
					this.usedSnowEngines.push(oldEngine);
				}

				var oldView = oldLayer.layerView;
				if (oldView) {
					oldView.removeFromSuperview();
				}
			}
		}

		this.parallaxLayers = [];

		for (var i = 0; i < this.parallaxConfig.length; i++) {
			var layer = this.parallaxConfig[i];
			var model = this.model;
			var layerData = {
				name: layer.name,
				offsetX: layer.offsetX,
				offsetY: layer.offsetY,
				relativeSpeed: layer.relativeSpeed,
				loop: layer.loop,
				pauseOnGameover: layer.pauseOnGameover,
				camera: layer.relativeCamera,
				zIndex: layer.zIndex,
				choices: layer.choices,
				ambientType: layer.ambientType,
				views: []
			};

			var minWidth = BG_WIDTH;
			for (var j = 0; j < layer.choices.length; j++) {
				var choice = layer.choices[j];
				if (choice.w < minWidth) {
					minWidth = choice.w;
				}
			}

			var viewBufferSize = Math.ceil(BG_WIDTH / minWidth) + 1;

			// use this view as a parent to group parallax views into layers
			layerData.layerView = new View({
				parent: this,
				x: 0,
				y: 0,
				width: 1,
				height: 1,
				zIndex: layer.zIndex,
				blockEvents: true, // accept no input to minimize view traversal
				canHandleEvents: false
			});

			var x = 0;
			for (var k = 0; k < viewBufferSize; k++) {
				var choice = layer.choices[~~(layer.choices.length * random())];
				var offsetX = ~~(layer.offsetX.base + layer.offsetX.range * random());

				if (layer.offsetY.bottomUp) {
					var offsetY = ~~(layer.offsetY.base + layer.offsetY.range * random()) - choice.h;
				} else {
					var offsetY = ~~(layer.offsetY.base + layer.offsetY.range * random());
				}

				var newView = new ImageView({
					parent: layerData.layerView,
					x: x + offsetX,
					y: offsetY,
					width: choice.w,
					height: choice.h,
					image: choice.image,
					canHandleEvents: false
				});

				newView.initialY = offsetY; // save for camera manipulation
				newView.choice = choice;

				x += choice.w + offsetX;
				layerData.views.push(newView);
			}

			layerData.nextX = x;

			this.parallaxLayers.push(layerData);
		}
	};

	this.resetPlatformViews = function() {
		// clear out old platforms if necessary
		this.platformLayerView && this.platformLayerView.removeFromSuperview();

		// use this view as a parent to group platform views into a layer
		this.platformLayerView = new View({
			parent: this,
			x: 0,
			y: 0,
			width: 1,
			height: 1,
			zIndex: Z_PLATFORMS,
			blockEvents: true, // accept no input to minimize view traversal
			canHandleEvents: false
		});

		this.platformViews = [];
		for (var i = 0; i < PLATFORM_BUFFER_SIZE; i++) {
			var platform = this.model.platforms[i];
			var joint = platform.joint;
			var view = new ImageView({
				parent: this.platformLayerView,
				x: platform.x,
				y: platform.y,
				width: platform.w,
				height: platform.h,
				image: platform.image,
				canHandleEvents: false
			});
			view.platformID = platform.id;

			if (this.model.selectedLevel === LEVEL_PIRATE) {
				this.decorateShip(platform, view);
			} else {
				// add joint
				if ( joint !== undefined ) {
					view.jointView = new ImageView({
						parent: this.platformLayerView,
						x: view.style.x - joint.w / 2,
						y: view.style.y,
						width: joint.w,
						height: joint.h,
						image: joint.image,
						zIndex: Z_JOINTS,
						canHandleEvents: false,
						visible: true
					});
				} else {
					view.jointView = new ImageView({
						parent: this.platformLayerView,
						x: 0,
						y: 0,
						width: 100,
						height: 100,
						zIndex: Z_JOINTS,
						canHandleEvents: false,
						visible: false
					});
				}
			}

			this.platformViews.push(view);
		}
	};

	this.resetSpriteViews = function() {
		if (!this.spritePools) {
			this.spritePools = {};
		}
		for (var k in this.model.levelConfig.sprites) {
			if (!this.spritePools[k]) {
				var d = this.model.levelConfig.sprites[k];
				this.spritePools[k] = new ViewPool({
					ctor: Sprite,
					initCount: 20,
					initOpts: {
						canHandleEvents: false,
						loop: true,
						autoStart: false,
						x: d.x,
						y: d.y,
						width: d.w,
						height: d.h,
						url: d.url,
						defaultAnimation: d.defaultAnimation
					}
				});
			} else {
				var pool = this.spritePools[k];
				for (var i = 0; i < pool.views.length; i++) {
					pool.views[i].stopAnimation();
				}
				pool.releaseAllViews();
			}
		}
	};

	this.resetPirateViews = function() {
		if (this.flockView) { this.flockView.style.visible = false; }
		if (this.model.selectedLevel !== LEVEL_PIRATE) { return; }
		if (!this.shipStuffPool) {
			this.shipStuffPool = new ViewPool({
				ctor: ImageView,
				initCount: 20,
				initOpts: {
					canHandleEvents: false
				}
			});
			this.flockView = new View({
				zIndex: Z_CLOUDS,
				parent: this,
				x: this.style.width,
				y: Math.random() * 160 + 40,
				width: 250,
				height: 250,
				blockEvents: true,
				canHandleEvents: false
			});
			this.flockView.startX = this.style.width;
			this.flockView.xRange = this.flockView.style.width - this.model.levelConfig.sprites.bird.w / 2;
			this.flockView.yRange = this.flockView.style.height - this.model.levelConfig.sprites.bird.h / 2;
		}
		else {
			this.shipStuffPool.releaseAllViews();
		}
	};

	this.resetCoinViews = function() {
		// use this view as a parent to group item views into a layer
		if (!this.coinLayerView) {
			this.coinLayerView = new View({
				parent: this,
				x: 0,
				y: 0,
				width: 1,
				height: 1,
				zIndex: Z_COINS,
				blockEvents: true, // accept no input to minimize view traversal
				canHandleEvents: false
			});

			// pre-initialized, 0-GC ViewPools
			this.itemPool = new ViewPool({
				ctor: ImageView,
				initCount: 100,
				initOpts: {
					parent: this.coinLayerView,
					x: 0,
					y: 0,
					width: 1,
					height: 1,
					image: IMG_BERRY
				}
			});

			this.effectPool = new ViewPool({
				ctor: ImageView,
				initCount: 20,
				initOpts: {
					parent: this.coinLayerView,
					x: 0,
					y: 0,
					width: 1,
					height: 1,
					image: IMG_RADIAL
				}
			});
		} else {
			this.itemPool.releaseAllViews();
			this.effectPool.releaseAllViews();
		}

		this.coinViews = [];
		this.radialViews = [];
	};

	this.resetCounterViews = function() {
		this.berryMeter.reset();
		this.scoreCounter.setText("0");
		this.friendCounter.setText("0");
		this.friendUIWrapper.style.visible = false;
	};

	this.resetDistanceMarkers = function() {
		var config = controller.config;
		var markerConfig = config.game.markers;
		var bestRun = controller.getData("bestRun") || 0;

		if (!this.markerLayerView) {
			this.markerLayerView = new View({
				parent: this,
				x: 0,
				y: 0,
				width: 1,
				height: 1,
				zIndex: Z_MARKERS,
				blockEvents: true, // accept no input to minimize view traversal
				canHandleEvents: false
			});

			this.marker = new ImageView({
				parent: this.markerLayerView,
				x: 0,
				y: 0,
				width: markerConfig.width,
				height: markerConfig.height,
				image: "resources/images/Game/markerDistance.png",
				visible: false
			});

			this.markerText = new ScoreView({
				parent: this.markerLayerView,
				x: 0,
				y: 0,
				width: MARKER_TEXT_WIDTH,
				height: MARKER_TEXT_HEIGHT,
				spacing: MARKER_TEXT_SPACING,
				initCount: 6,
				characterData: {
					"0": { width: 95 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_0.png" },
					"1": { width: 42 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_1.png" },
					"2": { width: 70 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_2.png" },
					"3": { width: 71 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_3.png" },
					"4": { width: 74 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_4.png" },
					"5": { width: 69 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_5.png" },
					"6": { width: 74 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_6.png" },
					"7": { width: 75 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_7.png" },
					"8": { width: 69 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_8.png" },
					"9": { width: 74 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_9.png" },
					"m": { width: 96 * MARKER_TEXT_SCALE, image: "resources/images/Game/blue_m.png" }
				},
				textAlign: 'center',
				visible: false
			});

			this.bestRunMarker = new ImageView({
				parent: this.markerLayerView,
				x: -markerConfig.width,
				y: 0,
				width: markerConfig.width,
				height: markerConfig.height,
				image: "resources/images/Game/markerHiScore.png",
				visible: false
			});

			this.bestRunMarkerText = new ImageView({
				parent: this.markerLayerView,
				image: GC.app.l.translate('resources/images/Locales/en') + '/Game/marker_bestrun.png',
				x: -BESTRUN_WIDTH,
				y: 0,
				width: BESTRUN_WIDTH,
				height: BESTRUN_HEIGHT,
				visible: false
			});

			this.friendMarker = new ImageView({
				parent: this.markerLayerView,
				x: -markerConfig.width,
				y: 0,
				width: markerConfig.width,
				height: markerConfig.height,
				image: "resources/images/Game/markerFriend.png",
				visible: false
			});

			this.friendMarkerIcon = new ImageView({
				parent: this.friendMarker,
				x: 0,
				y: -markerConfig.height * 1.25,
				width: markerConfig.width,
				height: markerConfig.height,
				image: "resources/images/Game/iconFriend.png"
			});

		} else {
			this.marker.style.visible = false;
			this.markerText.style.visible = false;
			this.bestRunMarker.style.visible = false;
			this.bestRunMarkerText.style.visible = false;
			this.friendMarker.style.visible = false;
		}

		this.marker.absoluteX = 0;
		this.marker.absoluteY = 0;
		this.bestRunMarker.absoluteX = PIXELS_TO_METERS * bestRun;
		this.bestRunMarker.absoluteY = 0;
		this.friendMarker.absoluteX = 0;
		this.friendMarker.absoluteY = 0;
	};

	this.resetSpecialViews = function() {
		if (!this.turtleView) {
			this.turtleView = new ImageView({
				parent: this.cameraParticles,
				x: this.playerView.style.x,
				y: TURTLE_Y,
				anchorX: TURTLE_WIDTH / 2,
				anchorY: TURTLE_HEIGHT / 2,
				width: TURTLE_WIDTH,
				height: TURTLE_HEIGHT,
				image: "resources/images/Game/turtleFriend.png",
				zIndex: Z_PLAYER,
				canHandleEvents: false,
				visible: false
			});

			this.boundTurtleShow = bind(this, function() {
				this.turtleView.animating = false;
			});

			this.boundTurtleHide = bind(this, function() {
				this.turtleEntrance = false;
				this.turtleView.animating = false;
				this.turtleView.style.visible = false;
			});
		} else {
			this.turtleView.style.visible = false;
		}

		if (!this.angelFish) {
			this.angelFish = new Sprite({
				parent: this.cameraParticles,
				x: angelX,
				y: BG_HEIGHT,
				anchorX: ANGEL_WIDTH / 2,
				anchorY: ANGEL_HEIGHT / 2,
				width: ANGEL_WIDTH,
				height: ANGEL_HEIGHT,
				url: "resources/images/Game/angelfish",
				zIndex: Z_PLAYER - 1,
				canHandleEvents: false,
				defaultAnimation: "fly",
				loop: true,
				autoStart: false,
				visible: false
			});

			this.boundAngelHide = bind(this, function() {
				this.animateSplash(this.angelFish);
				this.angelFishAnimating = false;
				this.angelFish.style.visible = false;
			});
		} else {
			this.angelFish.style.visible = false;
		}

		if (this.model.angelFish) {
			this.powerupDisplay.activateFish();
		}

		this.angelFishAnimating = false;

		this.turtleView.animating = false;
		this.turtleEntrance = false;
		this.turtleDirection = 0.5;
		this.turtleFloat = 0;
	};

	this.resetAmbientViews = function() {
		// if the game was interrupted, make sure we clean up leftovers
		if (this.ambientViews) {
			for (var i in this.ambientViews) {
				var view = this.ambientViews[i];
				view.removeFromSuperview();
				this.ambientPool.releaseView(view);
			}
		}

		// snow flakes recycled in resetParallaxViews

		this.ambientViews = [];
		this.fireflySpawns = [];
		this.snowflakeSpawns = [];

		// which layers have ambient effects?
		for (var i in this.parallaxLayers) {
			var layer = this.parallaxLayers[i];
			if (layer.ambientType === AMB_TYPE_FIREFLY) {
				this.fireflySpawns.push({
					parent: layer.layerView,
					scale: layer.relativeSpeed
				});
			} else if (layer.ambientType === AMB_TYPE_SNOW) {
				// establish snow flake particle engines
				var engine;
				if (this.usedSnowEngines && this.usedSnowEngines.length) {
					engine = this.usedSnowEngines.pop();
					layer.layerView.addSubview(engine);
				} else {
					engine = new ParticleEngine({
						parent: layer.layerView,
						x: 0,
						y: 0,
						anchorX: 1,
						anchorY: 1,
						width: 2,
						height: 2,
						initCount: 100,
						initImage: IMG_SNOW_FLAKE
					});
				}

				this.snowflakeSpawns.push({
					parent: layer.layerView,
					scale: layer.relativeSpeed,
					engine: engine
				});
			}
		}

		// foreground spawn
		if (this.fireflySpawns.length) {
			this.fireflySpawns.push({
				parent: this.stationaryParticles,
				scale: 1
			});
		} else if (this.snowflakeSpawns.length) {
			this.snowflakeSpawns.push({
				parent: this.stationaryParticles,
				scale: 1,
				engine: this.stationaryParticles
			});
		}
	};

	this.resetAlerts = function() {
		this.incomingNestable = false;
		this.alertArrow.style.visible = false;
		this.alertIcon.style.visible = false;
	};

	this.decorationObj = function(view) {
		var obj = this.shipStuffPool.obtainObject();
		obj.parentView = view;
		obj.anchorX = 0;
		obj.anchorY = 0;
		obj.r = 0;
		return obj;
	};

	this.decorateShip = function(platform, view, gameon) {
		var model = this.model,
			levelConfig = model.levelConfig,
			rail = platform.railing,
			generator = platform.generator,
			random = Math.random,
			min = Math.min,
			max = Math.max,
			vs = view.style;

		// strip out old decorations
		if (view.railing) {
			view.railing.removeFromSuperview();
			this.shipStuffPool.releaseView(view.railing);
			view.railing = undefined;
		}
		if (view.cannon) {
			view.cannon.removeFromSuperview();
			this.shipStuffPool.releaseView(view.cannon);
			view.cannon = undefined;
		}
		if (view.hullCritter) {
			view.hullCritter.removeFromSuperview();
			this.shipStuffPool.releaseView(view.hullCritter);
			view.hullCritter = undefined;
		}
		if (view.mast) {
			if (view.mast.flag) {
				view.mast.flag.stopAnimation();
				view.mast.flag.removeFromSuperview();
				this.spritePools.flag.releaseView(view.mast.flag);
				view.mast.flag = undefined;
			}
			view.mast.removeFromSuperview();
			this.shipStuffPool.releaseView(view.mast);
			view.mast = undefined;
		}

		// railing
		if (rail) {
			var obj = this.decorationObj(view);
			obj.x = generator ? 0 : 35;
			obj.y = -rail.h;
			obj.width = rail.w;
			obj.height = rail.h;
			obj.image = rail.image;

			view.railing = this.shipStuffPool.obtainView(obj);
			view.railing.setImage(rail.image);
		}

		// cannons
		if (!platform.dingy && !(rail && !generator)) {
			var cannons = levelConfig.extras.cannon,
				cannon = cannons[~~(random() * cannons.length)],
				obj = this.decorationObj(view);

			obj.x = vs.width - cannon.w;
			obj.y = (vs.height - cannon.h) / 2;
			obj.width = cannon.w;
			obj.height = cannon.h;
			obj.image = cannon.image;

			view.cannon = this.shipStuffPool.obtainView(obj);
			view.cannon.setImage(cannon.image);
		}

		// hull critters
		if (random() < 0.5) {
			var critters = levelConfig.extras.hullCritter,
				hullCritter = critters[~~(random() * critters.length)],
				obj = this.decorationObj(view),
				maxDim = max(hullCritter.w, hullCritter.h),
				halfMaxDim = maxDim / 2;

			if (platform.dingy) {
				obj.x = random() * 180;
				obj.y = 20 + random() * 45;
			} else {
				obj.x = halfMaxDim + random() * (256 - maxDim);
				obj.y = 100 + random() * (70 - halfMaxDim);
				if (generator) { // platformBack
					obj.x = max(obj.x, 150);
				} else if (rail) { // platformFront
					obj.x = min(obj.x, 50);
				}
			}

			obj.anchorX = hullCritter.w / 2;
			obj.anchorY = hullCritter.h / 2;
			obj.r = random() * 4;
			obj.width = hullCritter.w;
			obj.height = hullCritter.h;
			obj.image = hullCritter.image;

			view.hullCritter = this.shipStuffPool.obtainView(obj);
			view.hullCritter.setImage(hullCritter.image);
		}

		// mast, flag
		if (platform.mast) {
			var mast = levelConfig.extras.mast,
				obj = this.decorationObj(view);

			obj.x = -131;
			obj.y = -mast.h;
			obj.width = mast.w;
			obj.height = mast.h;
			obj.image = mast.image;

			view.mast = this.shipStuffPool.obtainView(obj);
			view.mast.setImage(mast.image);

			if (random() < 0.5) {
				var d = levelConfig.sprites.flag;
				obj = this.spritePools.flag.obtainObject();
				obj.parentView = view.mast;
				obj.canHandleEvents = false;
				obj.loop = true;
				obj.autoStart = false;
				obj.x = d.x;
				obj.y = d.y;
				obj.width = d.w;
				obj.height = d.h;
				obj.url = d.url;
				obj.defaultAnimation = d.defaultAnimation;
				view.mast.flag = this.spritePools.flag.obtainView(obj);
				view.mast.flag.startAnimation("wave", { randomFrame: true });
			}
		}

		// birds
		if (gameon && generator && !this.flocking && random() < 0.3) {
			this.flocking = true;

			var flockView = this.flockView,
				fvs = flockView.style,
				ts = this.style,
				numBirds = Math.ceil(random() * 3 + 2),
				birds = [],
				baseSize = random() * 0.2 + 0.4;

			fvs.visible = true;

			for (var i = 0; i < numBirds; i++) {
				var d = model.levelConfig.sprites.flag,
					obj = this.spritePools.bird.obtainObject();

				obj.parentView = flockView;
				obj.canHandleEvents = false;
				obj.loop = true;
				obj.autoStart = false;
				obj.x = random() * flockView.xRange;
				obj.y = random() * flockView.yRange;
				obj.width = d.w;
				obj.height = d.h;
				obj.scale = baseSize + random() * 0.2;
				obj.url = d.url;
				obj.defaultAnimation = d.defaultAnimation;

				var bird = this.spritePools.bird.obtainView(obj);
				bird.startAnimation("flap", { randomFrame: true });
				birds.push(bird);
			}

			fvs.x = flockView.startX;
			fvs.y = random() * 160 + 40;
			fvs.scale = 1;

			animate(flockView).now({
				x: random() * ts.width / 4 + ts.width / 2,
				y: random() * ts.height / 2 + ts.height / 4,
				scale: 0
			}, 40000, animate.linear)
			.then(bind(this, function() {
				this.flocking = false;
				for (var i = 0; i < numBirds; i++) {
					birds[i].stopAnimation();
				}
				flockView.removeAllSubviews();
				this.spritePools.bird.releaseAllViews();
			}));
		}
	};

	this.flyAwayBaby = function(babyView) {
		if (!babyView || !babyView.style) {
			return;
		}

		var random = Math.random;

		babyView.style.r = 0;
		babyView.setFlyAway(true);
		angelX = this.playerView.style.x - ANGEL_WIDTH / 2;

		animate(babyView).now({
			dx: BG_WIDTH / 2 + random() * BG_WIDTH / 3 - BG_WIDTH / 6,
			dy: -BG_HEIGHT + random() * BG_WIDTH / 4 - BG_WIDTH / 8
		}, 5000, animate.easeOut);
	};

	this.flyAwayBabies = function() {
		for (var i = 0; i < this.babyViews.length; i++) {
			this.flyAwayBaby(this.babyViews[i]);
		}
	};

	this.bringBackBabies = function() {
		var babyModels = this.model.babies;
		for (var i = 0; i < this.babyViews.length; i++) {
			var baby = this.babyViews[i];
			if (baby.goneForGood) {
				continue;
			}

			baby.setFlyAway(false);

			// cancel fly away anim
			animate(baby).clear();

			// update baby models to match their new animation position
			var babyModel = babyModels[i];
			babyModel.y = baby.style.y + this.cameraOffset - baby.babyData.offsetY;
		}
	};

	this.animateSplash = function(view) {
		if (!view) { return; }

		var model = this.model,
			player = model.player,
			vs = view.style,
			random = Math.random;

		if (model.selectedLevel === LEVEL_VALENTINE) {
			controller.playSound(SOUND_NAME_CLOUD_SPLASH);
		} else {
			controller.playSound(SOUND_NAME_SPLASH);
		}

		var multiplier;
		if (view === this.playerView) {
			multiplier = 1.5;
			this.playerView.sink();
		} else if (view === this.angelFish) {
			multiplier = 0.1;
		} else if (model.echidnaSpawn && model.echidnaSpawn.view === view) {
			multiplier = 0.4;
		} else if (model.beeSpawn && model.beeSpawn.view === view) {
			multiplier = 0.4;
		} else {
			multiplier = 0.1;
		}

		var yOffset = WATER_LEVEL + SPLASH_OFFSET,
			splashCount = 10,
			splashImg = model.selectedLevel === LEVEL_PIRATE
				? IMG_SPLASH_PIRATE
				: IMG_SPLASH,
			data = this.cameraParticles.obtainParticleArray(3 * splashCount);

		var width, height;
		for (var i = 0; i < splashCount; i++) {
			if (model.selectedLevel === LEVEL_VALENTINE) {
				splashImg = IMG_SPLASH_CLOUDS[~~(random() * IMG_SPLASH_CLOUDS.length)];
				scale = random() * 0.2 + 0.1;
				width = splashImg.width * scale;
				height = splashImg.height * scale;
				splashImg = splashImg.img;
				splashCount = 4;
			} else {
				width = random() * 20 + 20;
				height = random() * 15 + 15;
			}

			var ttl = 1500,
				pObj = data[i];

			pObj.image = splashImg;

			pObj.x = vs.x + vs.width / 2;

			pObj.y = yOffset + vs.height / 2;
			pObj.dx = multiplier * -player.vx;
			pObj.ddx = multiplier * player.vx / 2;
			pObj.dy = -200 * random() - 400;
			pObj.ddy = 1600;
			pObj.anchorX = width;
			pObj.anchorY = height;
			pObj.width = width;
			pObj.height = height;
			pObj.dscale = random() * 5 + 2.5;
			pObj.ttl = ttl;
			pObj.delay = random() * ttl / 7;
		}

		for (var i = splashCount; i < 2 * splashCount; i++) {
			if (model.selectedLevel === LEVEL_VALENTINE) {
				splashImg = IMG_SPLASH_CLOUDS[~~(random() * IMG_SPLASH_CLOUDS.length)];
				scale = random() * 0.2 + 0.1;
				width = splashImg.width * scale;
				height = splashImg.height * scale;
				splashImg = splashImg.img;
				splashCount = 4;
			} else {
				width = random() * 20 + 20;
				height = random() * 15 + 15;
			}

			var ttl = 1500,
				pObj = data[i];

			pObj.image = splashImg;

			pObj.x = vs.x + vs.width / 2 - width;

			pObj.y = yOffset + vs.height / 2;
			pObj.dx = multiplier * 3 * player.vx;
			pObj.ddx = multiplier * -3 * player.vx / 2;
			pObj.dy = -200 * random() - 400;
			pObj.ddy = 1600;
			pObj.anchorX = 0;
			pObj.anchorY = height;
			pObj.width = width;
			pObj.height = height;
			pObj.dscale = random() * 5 + 3;
			pObj.ttl = ttl;
			pObj.delay = random() * ttl / 7;
		}

		for (var i = 2 * splashCount; i < 3 * splashCount; i++) {
			if (model.selectedLevel === LEVEL_VALENTINE) {
				splashImg = IMG_SPLASH_CLOUDS[~~(random() * IMG_SPLASH_CLOUDS.length)];
				scale = random() * 0.2 + 0.1;
				width = splashImg.width * scale;
				height = splashImg.height * scale;
				splashImg = splashImg.img;
				splashCount = 4;
			} else {
				width = random() * 12 + 12;
				height = random() * 8 + 8;
			}

			var ttl = 1500,
				pObj = data[i];

			pObj.image = splashImg;
			pObj.flipX = true;

			pObj.x = vs.x + vs.width / 2 - width / 2;

			pObj.y = yOffset + vs.height / 2;
			pObj.dx = 2 * multiplier * player.vx * random() - multiplier * player.vx;
			pObj.dy = -400 * random() - 400;
			pObj.ddy = 1600;
			pObj.anchorX = width / 2;
			pObj.anchorY = random() * height;
			pObj.dr = random() * 4;
			pObj.width = width;
			pObj.height = height;
			pObj.dscale = random() * 6;
			pObj.ttl = ttl;
			pObj.delay = random() * ttl / 7;
		}

		this.cameraParticles.emitParticles(data);
	};

	this.animateCallout = function(text) {
		// redirect to tutorial callout
		if (this.firstPlay) {
			this.tutAnimateCallout(text);
			return;
		}

		this.calloutAnimating = true;

		var cot = this.calloutText,
			cots = cot.style,
			cos = this.calloutSparkle,
			coss = cos.style,
			config = controller.config,
			coinConfig = config.game.coins,
			path;

		switch (text) {
			case coinConfig.choices.kiwiGlider.callout:
				path = "call_out_glider.png";
				break;
			case CALLOUT_NESTABLE:
				path = "call_out_nestable.png";
				break;
			case coinConfig.choices.berryMagnet.callout:
				path = "call_out_new_magnet.png";
				break;
			case CALLOUT_NEW_NESTABLE:
				path = "call_out_new_nestable.png";
				break;
			case coinConfig.choices.turtleFriend.callout:
				path = "call_out_turtle.png";
				break;
			case CALLOUT_HISCORE:
				path = "call_out_highscore.png";
				break;
			case CALLOUT_BESTRUN:
				path = "call_out_bestrun.png";
				break;
			case CALLOUT_SHIELD:
				path = "call_out_shield.png";
				break;

		}

		cot.setImage(GC.app.l.translate('resources/images/Locales/en') + '/Game/' + path);
		cots.visible = true;

		// create some bound animation functions to reuse
		if (!this.boundCallout) {
			this.boundCalloutHide = bind(this, function() {
				cots.opacity = 1;
				cots.y = -100;
				cots.scale = 0.5;
				cots.visible = false;
				coss.visible = false;
				this.calloutAnimating = false;
			});

			this.boundCalloutFade = bind(this, function() {
				// fade callout text
				var textFadeData = this.stationaryParticles.obtainObject();
				textFadeData.target = true;
				textFadeData.topacity = 0;
				textFadeData.ttl = CALLOUT_TIME / 2;
				textFadeData.onDeath = this.boundCalloutHide;
				this.stationaryParticles.addExternalParticle(cot, textFadeData);

				// fade callout sparkle
				var sparkFadeData = this.stationaryParticles.obtainObject();
				sparkFadeData.target = true;
				sparkFadeData.topacity = 0;
				sparkFadeData.ttl = CALLOUT_TIME / 2;
				this.stationaryParticles.addExternalParticle(cos, sparkFadeData);
			});

			this.boundCallout = bind(this, function() {
				coss.x = cots.x - RADIAL_WIDTH / 2 + 15;
				coss.y = cots.y - RADIAL_HEIGHT / 2 + 40;
				coss.visible = true;
				coss.opacity = 1;

				// animate sparkle behind text
				var sparkMoveData = this.stationaryParticles.obtainObject();
				sparkMoveData.target = true;
				sparkMoveData.tx = coss.x + cots.width - RADIAL_WIDTH / 2 + 150;
				sparkMoveData.tr = coss.r + Math.PI;
				sparkMoveData.ttl = CALLOUT_TIME;
				sparkMoveData.transition = TRANSITION_EASE_OUT;
				sparkMoveData.onDeath = this.boundCalloutFade;
				this.stationaryParticles.addExternalParticle(cos, sparkMoveData);
			});
		}

		// start the animation
		var textMoveData = this.stationaryParticles.obtainObject();
		textMoveData.target = true;
		textMoveData.ty = 100;
		textMoveData.tscale = 1.5;
		textMoveData.ttl = CALLOUT_TIME;
		textMoveData.transition = TRANSITION_EASE_OUT;
		textMoveData.onDeath = this.boundCallout;
		this.stationaryParticles.addExternalParticle(cot, textMoveData);
	};

	this.tutAnimateCallout = function(text) {
		this.calloutAnimating = true;

		var cot = this.tutCalloutText,
			cots = cot.style,
			cos = this.tutCalloutSparkle,
			coss = cos.style,
			path;

		switch (text) {
			case CALLOUT_TIPS:
				path = "tut_quicktips.png";
				break;
			case CALLOUT_JUMP:
				path = "tut_jump.png";
				break;
			case CALLOUT_FLOAT:
				path = "tut_float.png";
				break;
			case CALLOUT_DIVE:
				path = "tut_swipe.png";
				break;
			case CALLOUT_GREAT:
				path = "tut_great.png";
				break;
			case CALLOUT_EXCELLENT:
				path = "tut_excellent.png";
		}

		cot.setImage(GC.app.l.translate('resources/images/Locales/en') + '/Tutorial/' + path);
		cots.visible = true;

		// create some bound animation functions to reuse
		if (!this.tutBoundCallout) {
			this.tutBoundCalloutHide = bind(this, function() {
				cots.x = BG_WIDTH;
				cots.visible = false;
				coss.visible = false;
				this.calloutAnimating = false;
			});

			this.tutBoundCalloutExit = bind(this, function() {
				this.tutBoundCalloutBounce("EXCELLENT!");

				// callout text slides out
				var textExitData = this.stationaryParticles.obtainObject();
				textExitData.target = true;
				textExitData.tx = -TUT_CALLOUT_SCALE * cots.width;
				textExitData.delay = TUT_CALLOUT_TIME / 2;
				textExitData.ttl = TUT_CALLOUT_TIME;
				textExitData.transition = TRANSITION_EASE_IN;
				textExitData.onDeath = this.tutBoundCalloutHide;
				this.stationaryParticles.addExternalParticle(cot, textExitData);
			});

			this.tutBoundCalloutBounceBack = bind(this, function() {
				var textBackData = this.stationaryParticles.obtainObject();
				textBackData.target = true;
				textBackData.tscale = TUT_CALLOUT_SCALE;
				textBackData.ttl = TUT_CALLOUT_TIME / 4;
				textBackData.transition = TRANSITION_EASE_IN;
				this.stationaryParticles.addExternalParticle(cot, textBackData);
			});

			this.tutBoundCalloutBounce = bind(this, function(text) {
				var path;

				switch (text) {
					case CALLOUT_TIPS:
						path = "tut_quicktips.png";
						break;
					case CALLOUT_JUMP:
						path = "tut_jump.png";
						break;
					case CALLOUT_FLOAT:
						path = "tut_float.png";
						break;
					case CALLOUT_DIVE:
						path = "tut_swipe.png";
						break;
					case CALLOUT_GREAT:
						path = "tut_great.png";
						break;
					case CALLOUT_EXCELLENT:
						path = "tut_excellent.png";
				}

				cot.setImage(GC.app.l.translate('resources/images/Locales/en') + '/Tutorial/' + path);

				var textBounceData = this.stationaryParticles.obtainObject();
				textBounceData.target = true;
				textBounceData.tscale = TUT_CALLOUT_BOUNCE_SCALE;
				textBounceData.ttl = TUT_CALLOUT_TIME / 4;
				textBounceData.transition = TRANSITION_EASE_OUT;
				textBounceData.onDeath = this.tutBoundCalloutBounceBack;
				this.stationaryParticles.addExternalParticle(cot, textBounceData);
			});

			this.tutBoundCalloutFade = bind(this, function() {
				// fade callout sparkle
				var sparkFadeData = this.stationaryParticles.obtainObject();
				sparkFadeData.target = true;
				sparkFadeData.topacity = 0;
				sparkFadeData.ttl = TUT_CALLOUT_TIME / 2;
				this.stationaryParticles.addExternalParticle(cos, sparkFadeData);
			});

			this.tutBoundCallout = bind(this, function() {
				coss.x = cots.x + 15;
				coss.y = cots.y - RADIAL_HEIGHT / 4 + 15;
				coss.visible = true;
				coss.opacity = 1;

				// animate sparkle behind text
				var sparkMoveData = this.stationaryParticles.obtainObject();
				sparkMoveData.target = true;
				sparkMoveData.tx = coss.x + cots.width - RADIAL_WIDTH + 150;
				sparkMoveData.tr = coss.r + Math.PI;
				sparkMoveData.ttl = TUT_CALLOUT_TIME;
				sparkMoveData.transition = TRANSITION_EASE_OUT;
				sparkMoveData.onDeath = this.tutBoundCalloutFade;
				this.stationaryParticles.addExternalParticle(cos, sparkMoveData);
			});
		}

		// start the animation
		var textMoveData = this.stationaryParticles.obtainObject();
		textMoveData.target = true;
		textMoveData.tx = (this.style.width - CALLOUT_WIDTH) / 2;
		textMoveData.ttl = TUT_CALLOUT_TIME;
		textMoveData.transition = TRANSITION_EASE_OUT;
		textMoveData.onDeath = this.tutBoundCallout;
		this.stationaryParticles.addExternalParticle(cot, textMoveData);
	};

	this.emitAchieveParticles = function() {
		var count = 20,
			data = this.stationaryParticles.obtainParticleArray(count),
			tvs = controller.achievementToast.style,
			ttl = 2500,
			random = Math.random,
			numOptions = RAINBOW_SPARKLES.length;

		for (var i = 0; i < count; i++) {
			var width = random() * 20 + 30;
			var height = width;
			var pObj = data[i];

			pObj.image = RAINBOW_SPARKLES[~~(random() * numOptions)];
			pObj.x = tvs.x + random() * tvs.width - width / 2;
			pObj.y = tvs.y + random() * tvs.height - height / 2;
			pObj.r = random() * 6;
			pObj.dx = (pObj.x + width / 2 - (tvs.x + tvs.width / 2)) * (random() * 1.2 + 0.2);
			pObj.dy = -random() * 250 - 600;
			pObj.dr = random() * 12;
			pObj.ddx = -pObj.dx / 5;
			pObj.ddy = GRAVITY / 2;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.ttl = ttl;
		}

		this.stationaryParticles.emitParticles(data);
	};

	this.emitTutorialSparkles = function() {
		var sparkleCount = 40,
			data = this.stationaryParticles.obtainParticleArray(sparkleCount),
			cots = this.tutCalloutText.style,
			random = Math.random;

		for (var i = 0; i < sparkleCount; i++) {
			var width = random() * 10 + 20,
				height = width,
				ttl = 3000,
				pObj = data[i];

			pObj.image = IMG_SPARKLE;
			pObj.x = 100 + cots.x + random() * (cots.width - 200) - width / 2;
			pObj.y = 20 + cots.y + random() * (cots.height - 40) - height / 2;
			pObj.dx = random() * 1000 - 500;
			pObj.dy = random() * 1400 - 700;
			pObj.ddy = 800;
			pObj.dr = 36 * random() - 18;
			pObj.anchorX = width / 2;
			pObj.anchorY = height / 2;
			pObj.width = width;
			pObj.height = height;
			pObj.dscale = random() * 4 - 2;
			pObj.ttl = ttl;
		}

		this.stationaryParticles.emitParticles(data);
	};

	this.onInputStart = function() {
		var model = this.model;

		// tutorial scripting
		if (this.firstPlay) {
			if (model.teachJump || model.teachFloat) {
				this.unpause();
			}
		}

		if ((model && !this.gamePaused && !model.finished && !model.player.spiked)
			|| (this.firstPlay && model && model.pauseDive && model.teachDive))
		{
			model.setFingerDown(true);

			if (model.player.jumping && model.kiwiGliderTimer <= 0) {
				this.startDrag({ radius: 0 }); // this.onDrag handles radius
				this.playerView.setFloating(true);
			} else if (model.kiwiGliderTimer <= 0) {
				this.playerView.setJumping(true);
			}
		}
	};

	this.onInputSelect = function() {
		var model = this.model;
		!this.gamePaused && model && model.setFingerDown(false);
	};

	this.onDrag = function(dragEvt, moveEvt, delta) {
		var model = this.model;

		// tutorial scripting
		if (this.firstPlay) {
			if (!model.teachDive || model.teachJump || model.teachFloat) {
				return;
			}
		} else if (this.gamePaused || model.kiwiGliderTimer > 0) {
			return;
		}

		if (!model.player.diving && model.player.jumping && !model.player.spiked && model.turtleDiveCooldown <= 0) {
			var mpt = moveEvt.srcPt, dpt = dragEvt.srcPt;
			var dx = mpt.x - dpt.x;
			var dy = mpt.y - dpt.y;
			var radius = dx * dx + dy * dy;
			var reqRadius = device.width / 10;

			// only allow valid downward swipes to trigger the dive
			if (radius >= reqRadius * reqRadius && dy > reqRadius / 4 && !model.player.didDive) {
				model.player.diving = true;
				model.player.didDive = true;

				// tutorial scripting
				if (this.firstPlay) {
					model.teachDive = false;
					model.queueDiveFinish = true;
					this.unpause();
				}

				this.playerView.setDiving(true);
			}
		}
	};

	this.onDragStop = function() {
		var model = this.model;
		model.player.didDive = false;
		!this.gamePaused && model && model.setFingerDown(false);
	};

	this.animateFriendCallout = function(name) {
		controller.animateAchievement({
			type: "pass",
			name: name
		});
	};

	this.updateTutorialViews = function(dt) {
		var model = this.model,
			hand = this.tutHand,
			hs = hand.style,
			ts = this.style,
			endModal = this.tutEndModal;

		if (model.queuePause) {
			controller.unblockInput();
			model.queuePause = false;
			this.pause();

			if (model.teachJump) {
				this.tutBoundCalloutBounce("TAP TO JUMP!");
			} else if (model.teachFloat) {
				this.tutBoundCalloutBounce("HOLD TO FLOAT!");
			} else if (model.teachDive) {
				this.tutBoundCalloutBounce("SWIPE TO DIVE!");
			} else {
				hs.visible = false;
				endModal.style.visible = true;
				endModal.openModal();
				endModal.onAccept = bind(this, function() {
					controller.gameInput.unregisterInput(endModal.btnAccept);
					this.unpause();
					this.firstPlay = false;
				});
				controller.gameInput.registerInput(endModal.btnAccept);
			}
		}

		if (model.failFloat) {
			model.failFloat = false;
			this.pause();
		}

		if (model.queueJumpFinish) {
			model.queueJumpFinish = false;
			this.tutBoundCalloutBounce("GREAT!");
			this.emitTutorialSparkles();
		} else if (model.queueDiveFinish) {
			model.queueDiveFinish = false;
			this.tutBoundCalloutExit();
			this.emitTutorialSparkles();
		}

		if (this.gamePaused && !this.handAnimating) {
			hs.visible = true;

			if (model.teachJump) {
				this.handAnimating = true;
				hs.x = (ts.width - HAND_WIDTH) / 2;
				hs.y = 2 * ts.height / 3 - HAND_HEIGHT;
				hs.r = Math.PI / 8;
				hand.setImage(TUTORIAL_HAND_IMAGE);

				animate(hand)
				.now({ opacity: 1, scale: 1.5 }, 300, animate.easeOut)
				.then({ scale: 1 }, 300, animate.easeIn)
				.then(bind(this, function() {
					hand.setImage(TUTORIAL_TAP_IMAGE);
				}))
				.wait(600)
				.then(bind(this, function() {
					this.handAnimating = false;
				}));
			} else if (model.teachFloat) {
				this.handAnimating = true;
				hs.x = (ts.width - HAND_WIDTH) / 2;
				hs.y = 2 * ts.height / 3 - HAND_HEIGHT;
				hs.r = Math.PI / 8;
				hand.setImage(TUTORIAL_HAND_IMAGE);

				animate(hand)
				.now({ opacity: 1, scale: 1.5 }, 300, animate.easeOut)
				.then({ scale: 1 }, 300, animate.easeIn)
				.then(bind(this, function() {
					hand.setImage(TUTORIAL_HOLD_IMAGE);
				}))
				.wait(1200)
				.then(bind(this, function() {
					this.handAnimating = false;
				}));
			} else if (model.teachDive) {
				this.handAnimating = true;
				hs.x = (ts.width - HAND_WIDTH) / 2 - 40;
				hs.y = ts.height / 2 - HAND_HEIGHT - 150;
				hs.r = -Math.PI / 8;
				hs.scale = 1.3;
				hs.opacity = 0;
				hand.setImage(TUTORIAL_HAND_IMAGE);

				animate(hand)
				.now({ opacity: 1 }, 150, animate.linear)
				.then({ scale: 1, dx: 40, dy: 150 }, 500, animate.easeIn)
				.then({ scale: 1.3, dx: 40, dy: 150 }, 500, animate.easeOut)
				.then({ opacity: 0 }, 150, animate.linear)
				.wait(500)
				.then(bind(this, function() {
					this.handAnimating = false;
				}));
			}
		} else if (!this.gamePaused) {
			hs.visible = false;
			this.handAnimating = false;
		}
	};



	var everyOther = 0;
	this.tick = function(dt) {
		if (!this.style.visible) { return; }

		var model = this.model,
			player = model.player,
			dtOrig = dt,
			random = Math.random,
			min = Math.min,
			max = Math.max;

		if (this.gameInitialized && !this.gamePaused && !model.finished) {
			// modify dt for smooth user experience
			dt = this.gameStarted ? controller.tickManager(dt) : 0;
			dtPct = dt / 1000;

			var playerView = this.playerView,
				pvs = playerView.style;

			// UPDATE CAMERA OFFSET
			// camera margins around center of the screen determine whether to move camera or not
			var camOff = this.cameraOffset;
			if (!model.gameoverCountdown) {
				var target = (player.y + playerHeight - camOff - BG_HEIGHT / 2);
				if (target > CAMERA_MARGINS || target < -CAMERA_MARGINS) {
					camOff += CAMERA_SPEED * dt / 1000 * target;
					if (camOff < CAMERA_OFFSET_MIN) {
						camOff = CAMERA_OFFSET_MIN;
					} else if (camOff > CAMERA_OFFSET_MAX) {
						camOff = CAMERA_OFFSET_MAX;
					}
					this.cameraOffset = camOff;
				}
			}



			// step the model forward
			model.step(dt, everyOther);

			if (model.queueBabyRemoval) {
				model.queueBabyRemoval = false;
				if (this.babyViews.length) {
					var baby = this.babyViews.shift();
					if (baby) {
						baby.goneForGood = true;
						this.flyAwayBaby(baby);
					}

					for (var b in this.babyViews) {
						this.babyViews[b].arrayIndex--;
					}
				}
			}

			if (everyOther === 0) {
				everyOther = 1;

				// UPDATE COUNTER VIEWS
				if (model.updateScore) {
					model.updateScore = false;
					this.scoreCounter.setText(~~model.score);
				}

				if (model.queueUpdateMultiplier) {
					model.queueUpdateMultiplier = false;
					this.berryMeter.update(model);
				}

				// release views queued by the model
				while (model.queueItemPoolReleases.length) {
					this.itemPool.releaseView(model.queueItemPoolReleases.pop());
				}
				while (model.queueEffectPoolReleases.length) {
					this.effectPool.releaseView(model.queueEffectPoolReleases.pop());
				}
			} else {
				everyOther = 0;

				// UPDATE FRIENDS
				if (this.updateFriends) {
					var friendRuns = model.friendRuns,
						nextFriend = friendRuns[0],
						distToNext;

					if (nextFriend !== null) {
						distToNext = ~~max(0, nextFriend.getData().distance - (player.x / PIXELS_TO_METERS));
						this.friendCounter.setText(distToNext);
					} else {
						this.friendUIWrapper.style.visible = false;
						this.updateFriends = false;
					}
				}

				// animate any callouts sent by model
				if (!this.calloutAnimating && model.queueCallouts.length) {
					this.animateCallout(model.queueCallouts.shift());
				}
				if (!this.friendCalloutAnimating && model.queueFriendCallouts.length) {
					this.animateFriendCallout(model.queueFriendCallouts.shift());
				}
			}



			// UPDATE PARALLAX VIEWS
			var laxLayers = this.parallaxLayers;
			for (var i = 0; i < laxLayers.length; i++) {
				var layer = laxLayers[i];

				var speedKiller = 1;
				if ((model.gameoverCountdown || model.angelModalActive) && layer.pauseOnGameover) {
					speedKiller = 0;
				}

				var speed = speedKiller * layer.relativeSpeed * player.vx * dtPct;
				layer.nextX -= speed;

				for (var j = 0; j < layer.views.length; j++) {
					var laxView = layer.views[j],
						laxvs = laxView.style;

					laxvs.x -= speed;
					laxvs.y = laxView.initialY - layer.camera * camOff;

					// recycle passed parallax views
					if (layer.loop && laxvs.x <= -laxView.choice.w) {
						var newChoice = Utils.choose(layer.choices);

						var offsetX = ~~(layer.offsetX.base + layer.offsetX.range * random());
						laxvs.x = layer.nextX + offsetX;
						layer.nextX += newChoice.w + offsetX;

						if (layer.offsetY.bottomUp) {
							laxView.initialY = laxvs.y = ~~(layer.offsetY.base + layer.offsetY.range * random()) - newChoice.h;
						} else {
							laxView.initialY = laxvs.y = ~~(layer.offsetY.base + layer.offsetY.range * random());
						}

						if (newChoice.flipX && random() < 0.5) {
							laxvs.flipX = true;
						} else {
							laxvs.flipX = false;
						}

						laxvs.width = newChoice.w;
						laxvs.height = newChoice.h;
						laxView.setImage(newChoice.image);
						laxView.choice = newChoice;
					}
				}
			}



			// UPDATE PLATFORM VIEWS
			for (var i = 0; i < PLATFORM_BUFFER_SIZE; i++) {
				var platform = model.platforms[i],
					platView = this.platformViews[i],
					platvs = platView.style,
					joint = platform.joint;

				// check for new platform
				if (i === 0 && platView.platformID !== platform.id) {
					// recycle old platform view
					var oldView = this.platformViews.shift();
					var newPlatform = model.platforms[PLATFORM_BUFFER_SIZE - 1];
					oldView.platformID = newPlatform.id;
					oldView.setImage(newPlatform.image);
					oldView.style.width = newPlatform.w;
					oldView.style.height = newPlatform.h;

					if (model.selectedLevel === LEVEL_PIRATE) {
						this.decorateShip(newPlatform, oldView, true);
					} else if (oldView.jointView) {
						var jointView = oldView.jointView,
							jointvs = jointView.style;

						joint = newPlatform.joint;
						if (joint) {
							jointvs.visible = true;
							jointView.setImage(joint.image);
							jointvs.width = joint.w;
							jointvs.height = joint.h;
						} else {
							jointvs.visible = false;
						}
					}

					this.platformViews.push(oldView);

					// now swap to the correct view
					platView = this.platformViews[i];
					platvs = platView.style;
				}

				platvs.x = platform.x - (player.x - pvs.x);
				platvs.y = platform.y - camOff;

				var jv = platView.jointView;
				if (joint && jv) {
					jv.style.x = platvs.x - joint.w / 2;
					jv.style.y = platvs.y;
				}
			}



			// UPDATE PLAYER VIEW
			if (model.queuePlayerSplash) {
				model.queuePlayerSplash = false;
				this.animateSplash(playerView);
				this.flyAwayBabies();
			} else if (model.queueRoll) {
				// emit dive particles!
				var data = this.closeParticles.obtainParticleArray(diveParticleCount + diveDustCount),
					width = diveParticleWidth,
					height = diveParticleHeight,
					ttl = 2000;

				for (var i = 0; i < diveParticleCount; i++) {
					var pObj = data[i];
					pObj.image = diveParticleImage;
					pObj.width = width;
					pObj.height = height;
					pObj.ttl = ttl;

					switch (diveParticleType) {
						case PHYS_TYPE_BOUNCY:
							pObj.x = pvs.x + pvs.width * random() + pvs.width / 2;
							pObj.y = pvs.y + 0.75 * pvs.height - random() * pvs.height / 3 + camOff;
							pObj.r = random() * 6;
							pObj.dx = -player.vx * 1.1;
							pObj.ddx = random() * player.vx / 2;
							pObj.dy = -random() * 400 - 200;
							pObj.ddy = GRAVITY;
							pObj.dr = random() * 18 + 4;
							pObj.ddr = -pObj.dr / 2;
							pObj.anchorX = width / 2;
							pObj.anchorY = height / 2;
							var modelX = player.x - pvs.x + pObj.x + width / 2;
							if (modelX < model.currentPlatform.endX) {
								pObj.triggers.push({
									property: 'y',
									value: model.currentPlatform.y - 0.9 * height,
									count: 2,
									action: this.boundBounce
								});
							}
							break;

						case PHYS_TYPE_LIGHT:
						default:
							pObj.x = pvs.x + pvs.width;
							pObj.y = pvs.y + pvs.height / 2 + camOff;
							pObj.r = 1.2 + random() * 0.8;
							pObj.dx = -player.vx;
							pObj.ddx = random() * player.vx / 2;
							pObj.dy = 30;
							pObj.ddy = 60;
							pObj.dr = random() * 10 + 3;
							pObj.ddr = -pObj.dr / 2;
							pObj.anchorX = random() * 2 * pvs.width / 3 - pvs.width / 3;
							pObj.anchorY = random() * 2 * pvs.height / 3 - pvs.height / 3;
							break;
					}
				}

				width = random() * 40 + 20;
				height = width;
				ttl = 1000;
				for (var i = diveParticleCount; i < diveParticleCount + diveDustCount; i++) {
					pObj = data[i];
					pObj.image = diveDustImage;
					pObj.width = width;
					pObj.height = height;
					pObj.ttl = ttl;

					switch (diveDustType) {
						case PHYS_TYPE_DUSTY:
						default:
							pObj.x = pvs.x + random() * pvs.width;
							pObj.y = pvs.y + 2 * pvs.height / 3 + camOff;
							pObj.r = 6 * random();
							pObj.dx = -player.vx;
							pObj.ddx = random() * player.vx;
							pObj.dy = -random() * 300;
							pObj.ddy = -random() * 800;
							pObj.dr = random() * 12;
							pObj.ddr = random() * -25;
							pObj.anchorX = width / 2;
							pObj.anchorY = height / 2;
							pObj.dscale = 10;
							pObj.ddscale = -14;
							pObj.dopacity = -3000 / ttl;
							break;
					}
				}

				this.closeParticles.emitParticles(data);
			} else if (player.spiked && playerView.animState !== PlayerView.ANIM_STATES.SPIKED) {
				playerView.setSpiked(true);
			}

			playerView.updateView(dt, camOff);



			// UPDATE ENEMY VIEWS
			var echidnaSpawn = model.echidnaSpawn,
				beeSpawn = model.beeSpawn,
				pooSpawn = model.pooSpawn,
				enemyDistance = 0,
				rightX = this.style.width;

			if (echidnaSpawn) {
				var echView = echidnaSpawn.view;
				if (!echView) {
					var opts = this.echidnaPool.obtainObject();
					opts.parentView = this;
					opts.x = echidnaSpawn.x;
					opts.y = echidnaSpawn.y;
					opts.zIndex = Z_ENEMIES;
					opts.url = URL_ECHIDNA;
					opts.visible = true;
					echView = echidnaSpawn.view = this.echidnaPool.obtainView(opts, this.boundEchidnaObtain);
				}

				if (echidnaSpawn.killed) {
					var fallData = this.cameraParticles.obtainObject();
					fallData.dy = random() * -300 - 300;
					fallData.ddy = GRAVITY;
					fallData.dx = -player.vx / 2 + random() * 400;
					fallData.dr = 3;
					fallData.ddr = -0.6;
					fallData.ttl = 2500;
					fallData.onDeath = this.boundEchidnaRelease;
					fallData.triggers.push({
						property: 'y',
						value: WATER_LEVEL + camOff,
						count: 1,
						action: this.boundSplash
					});
					this.cameraParticles.addExternalParticle(echView, fallData);

					echView.setDead(true);
					model.echidnaSpawn = undefined;
				} else if (echidnaSpawn.passed) {
					this.echidnaPool.releaseView(echView);
					model.echidnaSpawn = undefined;
				} else {
					echView.updateView(dt, camOff);
					enemyDistance = echView.style.x;
					this.incomingEnemy = echView;
					this.incomingEnemy.type = 'echidna';
				}
			}

			if (beeSpawn) {
				var beeView = beeSpawn.view;
				if (!beeView) {
					var opts = this.beePool.obtainObject();
					opts.parentView = this;
					opts.x = beeSpawn.x;
					opts.y = beeSpawn.y;
					opts.zIndex = Z_ENEMIES;
					opts.url = URL_BEE;
					opts.visible = true;
					beeView = beeSpawn.view = this.beePool.obtainView(opts, this.boundBeeObtain);
				}

				if (beeSpawn.killed) {
					var fallData = this.cameraParticles.obtainObject();
					fallData.dy = random() * -300 - 300;
					fallData.ddy = GRAVITY;
					fallData.dx = -player.vx / 2 + random() * 400;
					fallData.dr = 3;
					fallData.ddr = -0.6;
					fallData.ttl = 2500;
					fallData.onDeath = this.boundBeeRelease;
					fallData.triggers.push({
						property: 'y',
						value: WATER_LEVEL + camOff,
						count: 1,
						action: this.boundSplash
					});
					this.cameraParticles.addExternalParticle(beeView, fallData);

					beeView.setDead(true);
					model.beeSpawn = undefined;
				} else if (beeSpawn.passed) {
					controller.pauseSound("bee_buzz");
					this.beePool.releaseView(beeView);
					model.beeSpawn = undefined;
				} else {
					beeView.updateView(dt, camOff);

					var beeDistance = beeView.style.x;

					if( enemyDistance > 0 ){
						if( beeDistance < enemyDistance ){
							enemyDistance = beeDistance;
							this.incomingEnemy = beeView;
							this.incomingEnemy.type = 'bee';
						}
					}
					else{
						enemyDistance = beeDistance;
						this.incomingEnemy = beeView;
						this.incomingEnemy.type = 'bee';
					}
				}
			}

			if (pooSpawn) {
				var pooViews = pooSpawn.view,
					numDead = 0;

				if (!pooViews) {
					pooSpawn.view = pooViews = [];
				}

				for(var p = 0; p < pooSpawn.count; p++){
					var view = pooViews[p],
						pooStates = pooSpawn.pooStates;

					if( pooStates[p] === model.POO_STATE_NEW ){
						var opts = this.pooPool.obtainObject();
						opts.gameView = this;
						opts.parentView = this;
						opts.x = pooSpawn.x + pooSpawn.width * p;
						opts.y = pooSpawn.y;
						opts.width = pooSpawn.width / pooSpawn.count;
						opts.height = pooSpawn.height;
						opts.zIndex = Z_ENEMIES;
						opts.url = URL_POO;
						opts.visible = true;
						opts.index = p;
						view = this.pooPool.obtainView(opts, this.boundPooObtain);
						pooViews[p] = view;
						pooStates[p] = model.POO_STATE_ACTIVE;
					}

					//If the player just ran into it
					if ( pooStates[p] === model.POO_STATE_DEAD) {
						var fallData = this.cameraParticles.obtainObject();
							fallData.dy = random() * -300 - 300;
							fallData.ddy = GRAVITY;
							fallData.dx = -player.vx / 2 + random() * 400;
							fallData.dr = 3;
							fallData.ddr = -0.6;
							fallData.ttl = 2500;
							fallData.onDeath = this.boundPooRelease;
							fallData.triggers.push({
								property: 'y',
								value: WATER_LEVEL + camOff,
								count: 1,
								action: this.boundSplash
							});
							this.cameraParticles.addExternalParticle(view, fallData);
							view.setDead();

							pooStates[p] = model.POO_STATE_EMPTY;
					}
					//If the poo is off the left side of the screen
					else if (pooStates[p] === model.POO_STATE_PASSED) {
						view.setDead(true);
						this.pooPool.releaseView(view);
						pooStates[p] = model.POO_STATE_EMPTY;
					//If neither of the last states are true and it's not empty, then it's alive
					} else if( pooStates[p] !== model.POO_STATE_EMPTY ) {
						view.updateView(dt, camOff);
					}
					//If it's empty
					else{
						numDead += 1;
					}
				}

				if( numDead === pooSpawn.count ){
					model.pooSpawn = undefined;
				}
				else{
					var pooDistance = pooViews[0].style.x;
					if (enemyDistance > 0) {
						if (pooDistance < enemyDistance) {
							enemyDistance = pooDistance;
							this.incomingEnemy = pooViews[0];
							this.incomingEnemy.type = 'poo';
						}
					} else {
						enemyDistance = pooDistance;
						this.incomingEnemy = pooViews[0];
						this.incomingEnemy.type = 'poo';
					}
				}

			}

			if( (!beeSpawn && !echidnaSpawn && !pooSpawn) || enemyDistance > (rightX * 2.5) || enemyDistance < rightX ){
				this.incomingEnemy = undefined;
			}

			// UPDATE BABY VIEWS
			for (var i = 0; i < this.babyViews.length; i++) {
				this.babyViews[i].updateView(dt, camOff);
			}

			// UPDATE BERRY / ITEM VIWS
			var coinModels = model.activeCoins,
				firstNestableFound = false;

			// spin any radials around special items
			this.radialR += RADIAL_SPIN_RATE * dtPct;
			this.radialR %= 2 * Math.PI;

			// update berry sizes
			model.lastBerrySize = model.berrySize || 0;
			model.berrySize = min(~~(model.berryStreak / 10), 20);

			if (model.berrySize !== model.lastBerrySize) {
				// animate berry size
				var size = model.berrySize || 0,
					coins = model.activeCoins,
					scaleSize = BERRY_SCALE_INC,
					scaleTime = 400,
					sparkles = false,
					size1, size2;

				// pop or grow berries
				if (size === 0) {
					size1 = model.lastBerrySize + 0.5 / scaleSize;
					size2 = -0.3 / scaleSize;
					sparkles = true;
					controller.playSound("combo_lost");
				} else {
					size1 = size + 0.3 / scaleSize;
					size2 = size - 0.12 / scaleSize;
				}

				for (var i = 0; i < coins.length; i++) {
					var coin = coins[i];
					if (coin.type !== ITEM_TYPE_BASIC) {
						continue;
					}

					animate(coin.view)
					.now({ scale: 1 + size1 * scaleSize }, scaleTime / 2, animate.easeOut)
					.then({ scale: 1 + size2 * scaleSize }, scaleTime, animate.easeInOut)
					.then({ scale: 1 + size * scaleSize }, scaleTime, animate.easeInOut);
				}
			}

			for (var i = 0; i < coinModels.length; i++) {
				var coin = coinModels[i];

				// add coin view if needed
				var coinView = coin.view;
				if (!coinView) {
					var coinScale = coin.type === ITEM_TYPE_BASIC
						? 1 + model.berrySize * BERRY_SCALE_INC
						: 1;

					var opts = this.itemPool.obtainObject();
					opts.parentView = this.coinLayerView;
					opts.x = coin.x - (player.x - pvs.x);
					opts.y = coin.y + coin.hover - camOff;
					opts.anchorX = coin.width / 2;
					opts.anchorY = coin.height / 2;
					opts.width = coin.width;
					opts.height = coin.height;
					opts.opacity = coin.alreadyCollected ? 0.5 : 1;
					opts.zIndex = Z_COINS;
					opts.image = coin.image;
					opts.canHandleEvents = false;
					coinView = coin.view = this.itemPool.obtainView(opts);
					coinView.setImage(coin.image);
					coinView.style.scale = coinScale;
				}

				var cvs = coinView.style;
				cvs.x = coin.x - (player.x - pvs.x);
				cvs.y = coin.y + coin.hover - camOff;

				if (coin.type !== ITEM_TYPE_BASIC) {
					if (!firstNestableFound
						&& coin.type === ITEM_TYPE_NESTABLE
						&& cvs.x > this.style.width)
					{
						firstNestableFound = true;
						if (this.incomingNestable !== coinView) {
							this.alertIcon.iconSet = false;
							this.incomingNestable = coinView;
							this.incomingNestable.nestableImage = coin.shadow;
						}
					}

					// add coin radial if needed
					var coinRadial = coin.radial;
					if (!coinRadial) {
						var opts = this.effectPool.obtainObject();
						opts.parentView = this.coinLayerView;
						opts.x = cvs.x + cvs.width / 2 - RADIAL_WIDTH / 2;
						opts.y = cvs.y + cvs.height / 2 - RADIAL_HEIGHT / 2;
						opts.r = this.radialR;
						opts.anchorX = RADIAL_WIDTH / 2;
						opts.anchorY = RADIAL_HEIGHT / 2;
						opts.width = RADIAL_WIDTH;
						opts.height = RADIAL_HEIGHT;
						opts.zIndex = Z_EFFECTS;
						opts.image = IMG_RADIAL;
						opts.canHandleEvents = false;
						coinRadial = coin.radial = this.effectPool.obtainView(opts);
					}

					var crs = coinRadial.style;
					crs.x = cvs.x + cvs.width / 2 - RADIAL_WIDTH / 2;
					crs.y = cvs.y + cvs.height / 2 - RADIAL_HEIGHT / 2;
					crs.r = this.radialR;
				}
			}

			if (!firstNestableFound) {
				this.incomingNestable = undefined;
			}

			// trigger animations and sparkles
			var queueCollections = model.queueCollections;
			for (var i = 0; i < queueCollections.length; i++) {
				var collection = queueCollections[i],
					babyGrab = collection.babyGrab;

				if (collection.collectionTime > 0) {
					collection.collectionTime -= dt;

					if (!collection.startX) {
						if (babyGrab === false) {
							collection.startX = collection.x - (player.x - pvs.x);
				 			collection.startY = collection.y;

							if (collection.radial) {
								collection.radial.style.visible = false;
							}
						} else {
							var baby = model.babies[babyGrab],
								babyView = this.babyViews[babyGrab];

							collection.startX = collection.x - (baby.x - babyView.style.x);
				 			collection.startY = collection.y;

							if (collection.radial) {
								collection.radial.style.visible = false;
							}
						}
					}

					var pct = (COLLECTION_TIME - collection.collectionTime) / COLLECTION_TIME;
					var elapsedPct = dt / COLLECTION_TIME;

					var grabStyle = pvs;
					if (babyGrab !== false) {
						var babyGrabView = this.babyViews[babyGrab];
						if (babyGrabView) {
							grabStyle = babyGrabView.style;
						}
					}
					var coinStyle = collection.view.style;
					var dx = grabStyle.x + grabStyle.width / 2 - coinStyle.width / 2 - coinStyle.x;
					var dy = grabStyle.y + grabStyle.height / 2 - coinStyle.height / 2 - coinStyle.y;
					var ds = COLLECTION_SCALING - COLLECTION_SCALING_CHANGE * pct;

					coinStyle.x += dx * pct;
					coinStyle.y += dy * pct;
					coinStyle.opacity = max(0, 1 - 2.4 * pct);
					coinStyle.scale = max(0.001, coinStyle.scale + ds * elapsedPct);
				} else {
					model.freeCoins.push(queueCollections.splice(i, 1)[0]);
					i--;

					this.itemPool.releaseView(collection.view);
					if (collection.radial) {
						this.effectPool.releaseView(collection.radial);
					}
				}

				if (!collection.sparkles) {
					collection.sparkles = true;

					var xColl = collection.startX,
						yColl = collection.startY;

					var sparkleCount = 0;
					if (collection.type !== ITEM_TYPE_BASIC) {
						controller.playSound(SOUND_NAME_POWERUP);
						sparkleCount = SPECIAL_SPARKLE_COUNT;
					} else {
						controller.playSound(SOUND_NAME_BERRY_GRAB);
						sparkleCount = 0;
					}

					// reduce particle effects because they are off screen so quickly
					if (player.vx >= QUARTER_PARTICLES_SPEED) {
						sparkleCount /= 4;
					} else if (player.vx >= HALF_PARTICLES_SPEED) {
						sparkleCount /= 2;
					}

					if (sparkleCount) {
						var data = this.closeParticles.obtainParticleArray(sparkleCount);

						if (collection.callout) {
							model.queueCallouts.push(collection.callout);
						}

						if (collection.type === ITEM_TYPE_MAGNET) {
							model.activateBerryMagnet();
						} else if (collection.type === ITEM_TYPE_TURTLE_BAIT) {
							model.activateTurtleBait();
						} else if (collection.type === ITEM_TYPE_GLIDER) {
							model.activateKiwiGlider();
						} else if (collection.type === ITEM_TYPE_SHIELD) {
							model.activateShield();
						} else if (collection.type === ITEM_TYPE_NESTABLE) {
							if (!collection.alreadyCollected) {
								model.queueCallouts.push(CALLOUT_NEW_NESTABLE);
							} else {
								model.queueCallouts.push(CALLOUT_NESTABLE);
							}

							// save collection to model
							var collectedNestables = model.collectedNestables || {};
							collectedNestables[collection.nestableID] = true;
							model.collectedNestables = collectedNestables;

							// save nestable collection!
							var nestableData = controller.getData(KEY_NESTABLES_COLLECTED) || {};
							var setData = nestableData[model.nestableSet.id] || {};
							setData[collection.nestableID] = true;
							nestableData[model.nestableSet.id] = setData;
							controller.setData(KEY_NESTABLES_COLLECTED, nestableData, true, true);

							// achievement checking
							switch (model.nestableSet.id) {
								case NESTABLE_ID_PINK:
									controller.achieveCheck(ACHIEVE_ID_PINK, NESTABLE_ID_PINK);
									break;

								case NESTABLE_ID_GREEN:
									controller.achieveCheck(ACHIEVE_ID_GREEN, NESTABLE_ID_GREEN);
									break;

								case NESTABLE_ID_BUGS[0]:
								case NESTABLE_ID_BUGS[1]:
									controller.achieveCheck(ACHIEVE_ID_BUGS, NESTABLE_ID_BUGS);
									break;

								case NESTABLE_ID_BOTTLECAPS[0]:
								case NESTABLE_ID_BOTTLECAPS[1]:
									controller.achieveCheck(ACHIEVE_ID_BOTTLECAPS, NESTABLE_ID_BOTTLECAPS);
									break;

								case NESTABLE_ID_EGGS[0]:
								case NESTABLE_ID_EGGS[1]:
									controller.achieveCheck(ACHIEVE_ID_EGGS, NESTABLE_ID_EGGS);
									break;

								case NESTABLE_ID_JAPAN[0]:
								case NESTABLE_ID_JAPAN[1]:
									controller.achieveCheck(ACHIEVE_ID_JAPAN, NESTABLE_ID_JAPAN);
									break;
							}
						}

						// particle explosion for special items
						for (var j = 0; j < sparkleCount; j++) {
							var width = 26,
								height = width,
								ttl = 3000,
								pObj = data[j];

							pObj.image = IMG_SPARKLE;
							pObj.x = xColl + collection.width / 2 - width / 2 + random() * collection.width - collection.width / 2;
							pObj.y = yColl + collection.height / 2 - height / 2 + random() * collection.height - collection.height / 2;
							pObj.dx = -player.vx + random() * 1600 - 800;
							pObj.dy = random() * 1600 - 800;
							pObj.ddy = GRAVITY / 2;
							pObj.dr = 36 * random() - 18;
							pObj.anchorX = width / 2;
							pObj.anchorY = height / 2;
							pObj.width = width;
							pObj.height = height;
							pObj.dscale = random() * 4 - 2;
							pObj.ttl = ttl;
						}

						this.closeParticles.emitParticles(data);
					}
				}
			}

			// UPDATE ALERTS
			if( this.incomingEnemy ){
				var type = this.incomingEnemy.type;

				if(!this.alertIcon.iconSet || type != this.alertIcon.iconType ){
					if( type === 'echidna' ){
						this.alertIcon.setImage("resources/images/Game/iconEchidna.png");
						this.alertIcon.iconType = type;
					}
					else if( type === 'bee' ){
						this.alertIcon.setImage("resources/images/Game/iconBee.png");
						this.alertIcon.iconType = type;
					}
					else if( type === 'poo' ){
						this.alertIcon.setImage("resources/images/Game/iconPoop.png");
						this.alertIcon.iconType = type;
					}

					this.alertIcon.iconSet = true;
					this.alertArrow.style.visible = true;

					this.alertIcon.style.visible = true;
					this.alertIcon.style.opacity = 1;
					this.alertIcon.style.width = ENEMY_ICON_WIDTH;
					this.alertIcon.style.height = ENEMY_ICON_HEIGHT;
					this.alertIcon.style.x = this.style.width - ALERT_ARROW_WIDTH - ENEMY_ICON_WIDTH;

					this.alertFrame.style.visible = true;
					this.alertArrow.setImage(IMG_ALERT_ARROW_ORANGE);
				}

				this.alertArrow.style.y = this.incomingEnemy.style.y;
				this.alertIcon.style.y = this.incomingEnemy.style.y - (ENEMY_ICON_HEIGHT - ALERT_ARROW_HEIGHT) / 2;
				this.alertFrame.style.y = this.incomingEnemy.style.y - (ALERT_FRAME_HEIGHT - ALERT_ARROW_HEIGHT) / 2;
			}
			else if (this.incomingNestable) {
				if (!this.alertIcon.iconSet || !this.alertIcon.iconType !== 'nestable') {
					this.alertIcon.iconSet = true;
					this.alertArrow.style.visible = true;

					this.alertIcon.style.visible = true;
					this.alertIcon.style.opacity = 0.75;
					this.alertIcon.style.width = ALERT_ICON_WIDTH;
					this.alertIcon.style.height = ALERT_ICON_HEIGHT;
					this.alertIcon.style.x = this.style.width - ALERT_ARROW_WIDTH - ALERT_ICON_WIDTH;

					this.alertFrame.style.visible = false;
					this.alertIcon.iconType = 'nestable';
					this.alertIcon.setImage(this.incomingNestable.nestableImage);
					this.alertArrow.setImage(IMG_ALERT_ARROW_YELLOW);
				}

				this.alertArrow.style.y = this.incomingNestable.style.y;
				this.alertIcon.style.y = this.incomingNestable.style.y - (ALERT_ICON_HEIGHT - ALERT_ARROW_HEIGHT) / 2;
			} else if (this.alertIcon.iconSet) {
				this.alertIcon.iconSet = false;
				this.alertArrow.style.visible = false;
				this.alertIcon.style.visible = false;
				this.alertFrame.style.visible = false;
			}

			// UPDATE MARKERS
			var queueMarkers = model.queueMarkers,
				marker = this.marker,
				markerStyle = marker.style,
				markerText = this.markerText,
				markerTextStyle = markerText.style,
				brmarker = this.bestRunMarker,
				brmarkerStyle = brmarker.style,
				brmarkerText = this.bestRunMarkerText,
				brmarkerTextStyle = brmarkerText.style,
				fmarker = this.friendMarker,
				fmarkerStyle = fmarker.style;

			if (queueMarkers.length) {
				var markerPlatform = queueMarkers[0];
				if (markerPlatform.bestRun) {
					brmarkerStyle.visible = true;
					brmarkerTextStyle.visible = true;
					brmarker.absoluteX = markerPlatform.endX + MARKER_OFFSET_X;
					brmarker.absoluteY = markerPlatform.y - brmarkerStyle.height + MARKER_OFFSET_Y;
					queueMarkers.shift();
				} else if (markerPlatform.markerNumber && markerTextStyle.x <= -markerTextStyle.width) {
					// wait until the current marker is off-screen
					markerStyle.visible = true;
					markerTextStyle.visible = true;
					marker.absoluteX = markerPlatform.endX + MARKER_OFFSET_X;
					marker.absoluteY = markerPlatform.y - markerStyle.height + MARKER_OFFSET_Y;
					markerText.setText(MARKER_SPACING * markerPlatform.markerNumber + DISTANCE_UNIT);
					queueMarkers.shift();
				} else if (markerPlatform.friendRun && fmarkerStyle.x <= -fmarkerStyle.width) {
					fmarker.style.visible = true;
					fmarker.absoluteX = markerPlatform.endX + MARKER_OFFSET_X;
					fmarker.absoluteY = markerPlatform.y - fmarkerStyle.height + MARKER_OFFSET_Y;
					queueMarkers.shift();
				}
			}

			var pX = player.x - pvs.x,
				markerX = marker.absoluteX - pX,
				markerTextX = markerX + (markerStyle.width - markerTextStyle.width) / 2,
				bestMarkerX = brmarker.absoluteX - pX,
				friendMarkerX = fmarker.absoluteX - pX;

			markerStyle.x = markerX;
			markerStyle.y = marker.absoluteY - camOff;
			markerTextStyle.x = markerTextX;
			markerTextStyle.y = markerStyle.y - markerTextStyle.height - 70;

			brmarkerStyle.x = bestMarkerX;
			brmarkerStyle.y = brmarker.absoluteY - camOff;
			brmarkerTextStyle.x = brmarkerStyle.x + (brmarkerStyle.width - brmarkerTextStyle.width) / 2;
			brmarkerTextStyle.y = brmarkerStyle.y - brmarkerTextStyle.height - 70;

			fmarkerStyle.x = friendMarkerX;
			fmarkerStyle.y = fmarker.absoluteY - camOff;



			// UPDATE SPECIAL VIEWS
			if (dt) {
				var tv = this.turtleView,
					tvs = tv.style,
					af = this.angelFish,
					afs = af.style;

				// emit angel particles
				if (this.angelFishAnimating) {
					var data = this.cameraParticles.obtainParticleArray(1),
						numOptions = RAINBOW_SPARKLES.length,
						width = random() * 16 + 16,
						height = width,
						ttl = 3000,
						pObj = data[0];

					if (random() < 0.4) {
						pObj.image = IMG_SPARKLE_WHITE;
					} else {
						pObj.image = RAINBOW_SPARKLES[~~(random() * numOptions)];
					}

					pObj.x = afs.x + random() * afs.width / 3;
					pObj.y = afs.y + afs.height / 3 + random() * afs.height / 3;
					pObj.dx = -player.vx / 10 + random() * 250 - 125;
					pObj.dy = random() * 250 - 125;
					pObj.ddy = GRAVITY / 2;
					pObj.r = random() * 6;
					pObj.anchorX = width / 2;
					pObj.anchorY = height / 2;
					pObj.width = width;
					pObj.height = height;
					pObj.ttl = ttl;

					this.cameraParticles.emitParticles(data);
				}

				if( this.playerView.shielded ){

					var sv = this.shieldView,
						svs = this.shieldView.style;

					if( !this.shieldActive ){
						this.shieldActive = true;
						this.shieldView.startAnimation('wind');
						svs.visible = true;
					}

					svs.x = pvs.x + pvs.width - SHIELD_INSET;

					sv.hover += SHIELD_HOVER_RATE * sv.hoverDirection;
					if (sv.hover <= SHIELD_HOVER_MIN) {
						sv.hoverDirection = 1;
					} else if (sv.hover >= SHIELD_HOVER_MAX) {
						sv.hoverDirection = -1;
					}

					svs.y = pvs.y + (pvs.height - svs.height) * 0.5 + sv.hover;

					if (model.shieldTimer <= SHIELD_WARNING_TIME) {
						svs.opacity += sv.blinkDirection * SHIELD_BLINK_RATE;
						if (svs.opacity <= SHIELD_MIN_OPACITY) {
							svs.opacity = SHIELD_MIN_OPACITY;
							sv.blinkDirection = -sv.blinkDirection;
						} else if (svs.opacity >= 1) {
							svs.opacity = 1;
							sv.blinkDirection = -sv.blinkDirection;
						}
					}

					this.shieldParticles.style.x = pvs.x + pvs.width * 0.5;
					this.shieldParticles.style.y = pvs.y + pvs.height * 0.5;
					this.shieldParticles.style.visible = true;

					this.shieldParticles.runTick(dt);

					if (random() < 0.3) {
						return;
					}
					else{
						var count = 1;
						var data = this.shieldParticles.obtainParticleArray(count);

						for (var i = 0; i < count; i++) {
							var width = 58,
							height = 10,
							ttl = 250,
							pObj = data[i],
							pvs = this.style,
							spawnData = shieldParticleSpawns[ ~~( random() * shieldParticleSpawns.length ) ],
							target = shieldParticleTarget,
							dx = spawnData[2];

							pObj.image = IMG_SHIELD_PARTICLE;

							pObj.x = spawnData[0];
							pObj.y = spawnData[1];
							pObj.dx = dx/ttl;
							pObj.ddx = 1000/ttl;

							pObj.anchorX = width * 0.5;
							pObj.anchorY = height * 0.5;
							pObj.width = width;
							pObj.height = height;
							pObj.dscale = -1000/ttl;
							pObj.ttl = ttl;
							pObj.opacity = 0.6;
							pObj.dopacity = -1000 / ttl;
						}

						this.shieldParticles.emitParticles(data);
					}
				}
				else {
					if( this.shieldActive ){
						this.shieldActive = false;
						this.shieldView.stopAnimation();
						this.shieldView.style.visible = false;
						this.shieldView.style.opacity = 1;
						this.shieldView.hover = 0;
						this.shieldParticles.style.visible = false;
					}
				}


				// trigger glider input and views
				if (model.kiwiGliderTimer > 0) {
					if (playerView.animState !== PlayerView.ANIM_STATES.GLIDING) {
						playerView.setGliding(true);
						this.onInputMove = this.boundInputMove;
					}
				} else if (this.onInputMove) {
					playerView.setGliding(false);
					this.onInputMove = false;
					model.deactivateKiwiGlider();
				}

				// angel fish ride
				if (model.queueAngelFish) {
					this.powerupDisplay.deactivateFish();
					model.queueAngelFish = false;

					playerView.style.r = 0;
					playerView.setFalling(true);
					this.bringBackBabies();

					af.startAnimation(ANIM_NAME_FLY);
					this.angelFishAnimating = true;
					afs.x = angelX;
					afs.y = pvs.y + playerHeight + 4 * ANGEL_HEIGHT / 5;
					afs.r = -ANGEL_ROTATION;
					afs.visible = true;

					this.animateSplash(af);
					setTimeout(this.boundAngelSound, 250);

					var angelData = this.cameraParticles.obtainObject();
					angelData.dy = ANGEL_VELOCITY;
					angelData.ddy = GRAVITY;
					angelData.dx = ANGEL_DX;
					angelData.dr = 2 * ANGEL_ROTATION;
					angelData.ttl = ANGEL_TTL;
					angelData.onDeath = this.boundAngelHide;
					this.cameraParticles.addExternalParticle(af, angelData);
				}

				// swimming turtle behavior
				if (model.turtleActive) {
					this.turtleFloat += this.turtleDirection;
					if (this.turtleFloat >= TURTLE_FLOAT_MAX) {
						this.turtleDirection = -0.5;
					} else if (this.turtleFloat <= TURTLE_FLOAT_MIN) {
						this.turtleDirection = 0.5;
					}

					if (tvs.x != pvs.x) {
						var diffX = pvs.x - tvs.x;
						tvs.x += diffX / 2 * dt / 1000;
					}

					if (!tv.animating) {
						tvs.y = this.turtleFloat + TURTLE_Y;
					}

					if (!this.turtleEntrance) {
						tvs.visible = true;

						this.powerupDisplay.activateTurtle();
						this.turtleEntrance = true;
						this.animateSplash(tv);

						tv.setImage(model.turtleBounces === 2
							? "resources/images/Game/turtleBestFriend.png"
							: "resources/images/Game/turtleFriend.png");
						tv.animating = true;
						tvs.y = TURTLE_Y + tvs.height;

						var emergeData = this.cameraParticles.obtainObject();
						emergeData.target = true;
						emergeData.ty = TURTLE_Y;
						emergeData.ttl = TURTLE_EMERGE_TIME;
						emergeData.transition = TRANSITION_EASE_OUT;
						emergeData.onDeath = this.boundTurtleShow;
						this.cameraParticles.addExternalParticle(tv, emergeData);
					}

					if (random() < 0.05) {
						var splashCount = 1;
						var splashImg = model.selectedLevel === LEVEL_PIRATE
							? IMG_SPLASH_PIRATE
							: IMG_SPLASH;
						var data = this.cameraParticles.obtainParticleArray(splashCount);

						var width, height;
						for (var i = 0; i < splashCount; i++) {
							if (model.selectedLevel === LEVEL_VALENTINE) {
								splashImg = IMG_SPLASH_CLOUDS[~~(random() * IMG_SPLASH_CLOUDS.length)];
								scale = random() * 0.2 + 0.1;
								width = splashImg.width * scale;
								height = splashImg.height * scale;
								splashImg = splashImg.img;
							} else {
								width = random() * 26 + 26;
								height = random() * 12 + 12;
							}

							var ttl = 600;
							var pObj = data[i];

							pObj.flipX = random() < 0.5;
							pObj.image = splashImg;
							pObj.x = tvs.x + tvs.width / 2;
							pObj.y = tvs.y + tvs.height - height;
							pObj.dx = -player.vx / 3;
							pObj.ddx = -player.vx;
							pObj.dy = -475;
							pObj.ddy = 1600;
							pObj.anchorX = width;
							pObj.anchorY = height;
							pObj.width = width;
							pObj.height = height;
							pObj.dscale = random() * 6 + 2;
							pObj.ttl = ttl;
							pObj.delay = random() * ttl;
						}

						this.cameraParticles.emitParticles(data);
					}
				} else {
					if (tvs.visible && !tv.animating) {
						this.powerupDisplay.deactivateTurtle();
						tv.animating = true;
						this.animateSplash(tv);

						var sinkData = this.cameraParticles.obtainObject();
						sinkData.target = true;
						sinkData.ty = tvs.y + tvs.height;
						sinkData.ttl = TURTLE_EMERGE_TIME;
						sinkData.transition = TRANSITION_EASE_IN;
						sinkData.onDeath = this.boundTurtleHide;
						this.cameraParticles.addExternalParticle(tv, sinkData);
					}
				}
			}



			// UPDATE AMBIENT VIEWS
			if (dt) {
				var level = model.selectedLevel;
				if (level === LEVEL_MOONFEST) {
					// spawn a firefly?
					var roll = random();
					if (roll < FIREFLY_SPAWN_CHANCE) {
						var spawn = this.fireflySpawns[~~(random() * this.fireflySpawns.length)];
						var opts = this.ambientPool.obtainObject();
						opts.parentView = spawn.parent;
						opts.x = BG_WIDTH;
						opts.y = random() * BG_HEIGHT;
						opts.anchorX = FIREFLY_WIDTH / 2;
						opts.anchorY = FIREFLY_HEIGHT / 2;
						opts.width = FIREFLY_WIDTH;
						opts.height = FIREFLY_HEIGHT;
						opts.scale = spawn.scale;
						opts.image = IMG_FIREFLY_LIT;
						opts.canHandleEvents = false;

						var firefly = this.ambientPool.obtainView(opts);
						spawn.parent.addSubview(firefly);
						firefly.setImage(IMG_FIREFLY_LIT);

						firefly.ambientType = AMB_TYPE_FIREFLY;
						firefly.relativeSpeed = spawn.scale;
						firefly.startY = firefly.style.y;
						firefly.hoverDirection = random() < 0.5 ? 1 : -1;
						firefly.hover = 0;
						firefly.litUp = true;

						this.ambientViews.push(firefly);
					}
				} else if (level === LEVEL_CHRISTMAS) {
					// spawn a snowflake?
					var roll = random();
					if (roll < SNOW_FLAKE_SPAWN_CHANCE) {
						var spawn = this.snowflakeSpawns[~~(random() * this.snowflakeSpawns.length)],
							engine = spawn.engine,
							data = engine.obtainParticleArray(1),
							pObj = data[0],
							ttl = 3500,
							width = random() * 30 + 15,
							height = width;

						pObj.x = random() * 3 * BG_WIDTH;
						pObj.y = 2 * spawn.scale * camOff - CAMERA_OFFSET_MAX;
						pObj.dx = spawn.scale * (-player.vx + random() * 40 - 20);
						pObj.dy = spawn.scale * (random() * 50 + 100);
						pObj.dr = random() * 10 - 5;
						pObj.ddx = spawn.scale * (random() * 120 - 60);
						pObj.ddy = spawn.scale * (GRAVITY / 10 + random() * 40 - 20);
						pObj.anchorX = width / 2;
						pObj.anchorY = height / 2;
						pObj.width = width;
						pObj.height = height;
						pObj.scale = spawn.scale;
						pObj.image = IMG_SNOW_FLAKE;
						pObj.ttl = ttl;

						engine.emitParticles(data);
					}

					for (var i = 0; i < this.snowflakeSpawns.length; i++) {
						var spn = this.snowflakeSpawns[i],
							eng = spn.engine;

						if (eng !== this.stationaryParticles) {
							eng.runTick(dt);
							eng.style.y = 2 * spn.scale * -camOff;
						}
					}
				}

				for (var i in this.ambientViews) {
					var view = this.ambientViews[i];
					if (view.ambientType === AMB_TYPE_FIREFLY) {
						// firefly blink on / off
						var toggle = random();
						if (toggle < FIREFLY_TOGGLE_CHANCE) {
							if (view.litUp) {
								view.litUp = false;
								view.setImage(IMG_FIREFLY_DARK);
							} else {
								view.litUp = true;
								view.setImage(IMG_FIREFLY_LIT);
							}
						}

						// firefly hover behavior
						view.hover += view.relativeSpeed * (view.hoverDirection * FIREFLY_HOVER_RATE);
						if (view.hover >= view.relativeSpeed * FIREFLY_HOVER_MAX) {
							view.hoverDirection = -view.hoverDirection;
						} else if (view.hover <= view.relativeSpeed * FIREFLY_HOVER_MIN) {
							view.hoverDirection = -view.hoverDirection;
						}

						// update firefly position
						view.style.x -= view.relativeSpeed * (player.vx + FIREFLY_SPEED) * dtPct;
						view.style.y = view.startY + view.hover - view.relativeSpeed * camOff;

						// recycle fireflies that get passed
						if (view.style.x < -FIREFLY_WIDTH) {
							view.removeFromSuperview();
							this.ambientPool.releaseView(view);
							this.ambientViews.splice(i, 1);
						}
					}
				}
			}



			// UPDATE TUTORIAL STUFF
			if (this.firstPlay) {
				this.updateTutorialViews(dt);
			} else if (this.tutHand) {
				this.tutHand.style.visible = false;
				this.tutHand.removeFromSuperview();
				this.tutHand = undefined;
			}



			// tick manual particle engines with our modified dt value
			this.cameraParticles.runTick(dt);
			this.cameraParticles.style.y = -camOff;
			this.closeParticles.runTick(dt);
			this.closeParticles.style.y = -camOff;
		} else if (model.finished && !this.deconstructed) {
			this.deconstructView(bind(controller, 'transitionToGameOver'));
		}

		// UI particles aren't part of the game proper, and should still tick
		this.stationaryParticles.runTick(dt || dtOrig);
	};
});
