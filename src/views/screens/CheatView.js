import animate;
import ui.View as View;
import ui.ImageView as ImageView;
import ...lib.TextView as TextView;
import ui.ScrollView as ScrollView;

import src.lib.ListView as ListView;
import src.lib.ButtonView as ButtonView;
import src.lib.ParticleEngine as ParticleEngine;

import src.views.helpers.LeaderboardNodeView as LeaderboardNodeView;
import src.views.helpers.InviteNodeView as InviteNodeView;

import src.controllers.Controller as Controller;

//import event.Callback as Callback;
import ff;

exports = Class(View, function(supr) {

	// CLASS CONSTANTS
	var BG_WIDTH,
		BG_HEIGHT,
		config,
		CELL_HEIGHT = 150;

	var cheatList = {
		'getBerries': {
			title: 'Get 100k Berries',
			desc: 'Gives you 100k Berries'
		},
		'getGems': {
			title: 'Get 1000 Gems',
			desc: 'Gives you 1000 Gems'
		},
		'maxUpgrades': {
			title: 'Full Upgrades',
			desc: 'Get All Upgrades'
		},
		'resetData': {
			title: 'Reset Game Data',
			desc: 'Resets your game data'
		},
		'maxRank': {
			title: 'Max Out Rank',
			desc: 'Maxes your Rank'
		},
		'topOffRank':{
			title: 'Top Off Rank',
			desc: 'Puts You One Point Away From Levelling Up'
		},
		'increaseRank': {
			title: 'Increase Rank',
			desc: 'Increase Rank By 1'
		},
		'decreaseRank': {
			title: 'Decrease Rank',
			desc: 'Decrease Rank By 1'
		}
	}

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = Controller.get();
		config = this.controller.config;
		BG_WIDTH = config.ui.maxWidth;
		BG_HEIGHT = config.ui.maxHeight;

		this.isMainView = true;

		this.addExperimentsToList();

		this.designView();
	};


	this.addExperimentsToList = function() {
		var expList = this.controller.experimentConfig.active,
			i,
			groups,
			group,
			functionName,
			unique;

		for(i in expList){
			groups = expList[i].groups;

			for(var j in groups){
				functionName = 'setExperiment_' + j;
				cheatList[functionName] = {
					title: j,
					desc: 'Set Experiment To: ' + j + '\n Re-load Game To See Results'
				};

				unique  = (function(i, j){
					return (function(){
							this.controller.trackExperiment(i, j);
							this.controller.setData('activeExperiment', this.controller.experiment);
					});
				})(i, j);


				this[functionName] = bind(this, unique);
			}
		}
	};

	// build the component subviews of the screen
	this.designView = function() {

		// center background horizontally, and support widest aspect ratio of 9:16
		this.background = new ImageView({
			parent: this,
			x: (this.style.width - BG_WIDTH) / 2,
			y: (this.style.height - BG_HEIGHT) / 2,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/Leaderboard/background.png",
			canHandleEvents: false
		});

		this.backButton = new ButtonView({
			parent: this,
			x: 0,
			y: 0,
			width: 137,
			height: 79,
			image: 'resources/images/Modals/btnGreen.png',
			onClick: bind(this, 'deconstructView', bind(this.controller, 'transitionToMainMenu'))
		});

		this.backText = new TextView({
			parent: this.backButton,
			centerHorizontal: true,
			centerVertical: true,
			width: this.backButton.style.width,
			height: this.backButton.style.height,
			text: GC.app.l.translate("Back"),
			textAlign: 'center',
			size: config.fonts.button,
			color: config.colors.button,
			multiline: false,
			canHandleEvents: false
		});

		this.cheatScroll = new ScrollView({
			parent: this,
			width: 600,
			height: this.style.height - 100,
			x: (this.style.width/2) - 300,
			y: 75,
			scrollX: false,
			scrollY: true,
			clip: true
		});

		var keys = Object.keys(cheatList);
		for(var i = 0; i < keys.length; i++){
			var item = cheatList[keys[i]];

			var itemWrapper = new View({
				parent: this.cheatScroll,
				centerHorizontal: true,
				y: i * CELL_HEIGHT,
				height: CELL_HEIGHT,
				width: this.cheatScroll.style.width,
				backgroundColor: '#999999'
			});

			var header = new TextView({
				parent: itemWrapper,
				x: 0,
				y: 0,
				width: itemWrapper.style.width,
				height: 50,
				text: item.title,
				textAlign: 'left',
				size: config.fonts.button,
				color: config.colors.button,
				multiline: false,
				canHandleEvents: false,
				backgroundColor: '#DDDDDD'
			});

			var desc = new TextView({
				parent: itemWrapper,
				x: 5,
				y: 50,
				width: itemWrapper.style.width - 142,
				height: 100,
				text: item.desc,
				textAlign: 'left',
				size: config.fonts.debug,
				color: config.colors.button,
				canHandleEvents: false
			});

			var btn = new ImageView({
				parent: itemWrapper,
				y: desc.style.y + desc.style.height/2 - 79/2,
				x: desc.style.width,
				width: 137,
				height: 79,
				image: 'resources/images/Modals/btnGreen.png'
			});

			var btnText = new TextView({
				parent: btn,
				y: 0,
				width: btn.style.width,
				height: btn.style.height,
				text: 'GO',
				textAlign: 'center',
				size: config.fonts.button,
				color: config.colors.button,
				multiline: false,
				canHandleEvents: false
			})

			btn.on('InputStart', bind(this, this[keys[i]]));
		}

		this.cheatScroll.setScrollBounds( { minY: 0, maxY: keys.length * CELL_HEIGHT  + 50 } )
	};

	this.getBerries = function(){
		this.controller.setData('berries', this.controller.getData('berries') + 100000);
	};

	this.getGems = function(){
		this.controller.setData('gems', this.controller.getData('gems') + 1000);
	};

	this.maxUpgrades = function(){
		var upgrades = this.controller.upgradesConfig;
		var purchased = this.controller.getData("upgradesPurchased");

		for( var i in purchased ){
			if( upgrades[i] ){
				purchased[i].level = upgrades[i].levels.length - 1;
			}
		}
		this.controller.setData("upgradesPurchased", purchased);
	}

	this.resetData = function(){
		this.controller.appModel.resetData();
		this.controller.appModel.initNewData();
		this.controller.setData('firstPlay', false);
		this.controller.updateAchievementState();
	};

	this.maxRank = function(){
		this.controller.setData("level", 50);
	};

	this.topOffRank = function(){
		var xp = this.controller.getLevelXP(this.controller.getData("level") + 1);
		this.controller.setData('XP', xp - 1);
	}

	this.decreaseRank = function(){
		this.controller.setData("level", this.controller.getData("level") - 1);
		this.topOffRank();
	};

	this.increaseRank = function(){
		this.controller.setData("level", this.controller.getData("level") + 1);
		this.topOffRank();
	};

	// ensures state and data get reset each time the view becomes active
	this.resetView = function() {
		this.controller.setHardwareBackButton(this.backButton, false);
	};

	// transition in; animate and position the component subviews of the screen
	this.constructView = function() {

	};

	// transition out; animate the component subviews of the screen out
	this.deconstructView = function(callback) {
		if (callback) {
			this.controller.blockInput();
			this.controller.unblockInput();
			callback();
		}
	};

});
