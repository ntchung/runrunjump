import device;
import event.Emitter as Emitter;

import src.views.modals.BasicModalView as BasicModalView;
import src.views.modals.BinaryModalView as BinaryModalView;

import billing;

/**
 * Product controller
 */
exports = Class(Emitter, function(supr) {

	var TYPE_UPGRADE = "UPGRADE",
		TYPE_AVATAR = "AVATAR",
		TYPE_CONSUMABLE = "CONSUMABLE";

	this.init = function(controller) {
		this.controller = controller;
		this.upgradeCconfig = this.controller.upgradeCconfig;
		this.modalStoreConfg = this.controller.modalStoreConfg;

		supr(this, 'init');
	};

	this.purchaseUpgrade = function(item, skipModal, skipSave, skipServer){
		var levels = item.levels,
		currentLevel = item.currentLevel,
		nextLevel = currentLevel + 1,
		totalLevels = levels.length,
		berries = item.currency === 'gems' ? this.controller.getData("gems") : this.controller.getData("berries"),
		money = false,
		type = item.type,
		canPurchase = false,
		productID = item.id || item.uid,
		berryCost, levelData, nextLevelData,
		modalType, modalMessage, modalOnAccept;

		if( type === TYPE_UPGRADE ){
			//If the upgrade cna still be leveled up
			if( nextLevel < totalLevels ){
				nextLevelData = levels[nextLevel];
				berryCost = nextLevelData.cost;
				canPurchase = true;
			}
		}
		else if( type === TYPE_AVATAR ){

			levelData = levels[currentLevel];

			//If not purchased
			if ( currentLevel < totalLevels ) {
				berryCost = levelData.cost;
				canPurchase = true;
			}
			else{
				return false; //(already purchased)
			}

			money = levelData.money;
		}
		else if( type === TYPE_CONSUMABLE ){
			levelData = levels[0];

			if( item.id === 'angelFish' ){
				berryCost = this.controller.getAngelSaveCost();
			}
			else{
				berryCost = levelData.cost;
			}

			canPurchase = true;
		}

		// update data and show modals as needed
		if ((berries >= berryCost || money) && canPurchase)
		{
			if (!money && type == TYPE_AVATAR) {
				this.controller.addAvatar(item.levels[currentLevel].value);
				modalType = "BASIC";
				modalMessage = "Change your kiwi on the Main Menu!";
			} else if (money && type == TYPE_AVATAR) { // REAL MONEY
				if (GC.isNative && device.isMobile) {
					billing.purchase(productID);
					return;
				} else { // browser purchase
					this.controller.addAvatar(item.levels[currentLevel].value);
					modalType = "BASIC";
					modalMessage = "Change your kiwi on the Main Menu!";
				}
			}

			var gamesPlayed = this.controller.getData("gamesPlayed") || 0;
			this.controller.trackEvent("UpgradePurchase", {
				gamesPlayed: gamesPlayed + "",
				upgradeID: productID,
				upgradeLevel: nextLevel + ""
			});

			if (type == TYPE_UPGRADE) {
				this.controller.trackEvent(productID + "Purchased", {
					upgradeLevel: nextLevel + ""
				});
			}

			if (this.controller.getData("firstPurchase")) {
				this.controller.setData("firstPurchase", false, skipServer);
				this.controller.trackEvent("FirstPurchase", {
					gamesPlayed: gamesPlayed + "",
					upgradeID: productID
				});
			}

			item.currentLevel = nextLevel;

			var purchased = this.controller.getData("upgradesPurchased");
			purchased[productID].level++;
			this.controller.setData("upgradesPurchased", purchased, skipSave, skipServer);

			berries -= berryCost;

			if ( item.currency === 'gems' ){

				var dataObj = {	'label': productID + '',
									'cost': berryCost + ''};

				if( productID === 'angelFish' ){
					dataObj.type = 'boost_angelFish';
				}
				else if( type === TYPE_AVATAR ){
					dataObj.type = 'adult_kiwi';
				}

				this.trackPurchase(dataObj, 'gems');

				this.controller.setData("gems", berries, skipSave, skipServer);
			}
			else{
				var dataObj = {	'label': productID + '',
								'cost': berryCost + ''};

				switch( item.type ){
					case TYPE_UPGRADE:
						dataObj.type = 'upgrade_' + item.spawnID;
						dataObj.level = (purchased[productID].level + 1) + '';
						break;
					case TYPE_AVATAR:
						dataObj.type = 'adult_kiwi';
						break;
					case TYPE_CONSUMABLE:
						dataObj.type = 'boost';
						break;
				}
				this.trackPurchase(dataObj, 'berries');

				this.controller.setData("berries", berries, skipSave, skipServer);
			}

		} else if (type == TYPE_UPGRADE && nextLevel >= totalLevels) {
			modalType = "BASIC";
			modalMessage = "That item is upgraded to the max!";
		} else if (type == TYPE_AVATAR && currentLevel >= totalLevels) {
			modalType = "BASIC";
			modalMessage = "You have already purchased that Runner!";
		} else if (type == TYPE_CONSUMABLE && currentLevel >= item.max) {
			modalType = "BASIC";
			modalMessage = "You are maxed out on that item!";
		} else if (item.currency === 'gems'){
			modalType = "BINARY";
			modalMessage = "You need more gems! Buy some more?";
			modalOnAccept = bind(this, this.goToGemStore);
		}
		else {
			modalType = "BINARY";
			modalMessage = "You need more berries! Buy some more?";
			modalOnAccept = bind(this, this.goToBerryStore);
		}

		// show a modal?
		if( modalType && skipModal ){
			return false;
		}
		else if (modalType == "BASIC") {
			new BasicModalView({
				parent: this.controller.activeView,
				message: modalMessage
			});
			return false;
		} else if (modalType == "BINARY") {
			new BinaryModalView({
				parent: this.controller.activeView,
				message: modalMessage,
				onAccept: modalOnAccept
			});

			return false;
		}

		return true;
	};

	this.purchaseModalStoreItem = function(item){
		var cur = item.currency,
			type = item.type,
			couldSpend = false;

		if (cur === 'bucks') {
			// spending real money

			// check to see if the user is in the middle of a game using angel save
			if (this.controller.midAngelSave) {
				this.controller.gemAngelModal = this.controller.activeModal;
				if (!GC.app.enableWebMode) {
					this.controller.basicAngelModal = new BasicModalView({
						parent: this.controller.activeView,
						message: GC.app.l.translate("Awaiting store response..."),
						onAccept: bind(this, function() {
							this.controller.basicAngelModal = undefined;
						})
					});
				}
			} else {
				this.controller.showPlayStoreNotification();
			}

			if (GC.isNative && device.isMobile) {
				billing.purchase(item[GC.app.storeProductID]);
			} else {
				// browser gems are free!
				couldSpend = true;
				this.controller.setData('gemFunnel', true);
				this.controller.trackEvent("GemFunnelStart", {'amount': item.amount + '' });
			}
		} else if (cur === 'gems') {
			// spending gems

			if (this.controller.spendGems(item.cost, true)) {
				couldSpend = true;
				this.controller.closeActiveModal(false);

				switch (item.type) {
					case 'berries':
						var dataObj = {	'label': 'berries_' + item.amount,
										'cost': item.cost + '',
										'type': 'berries'};

						this.trackPurchase(dataObj, 'gems');

						if( this.controller.getData('berryFunnel') ){
							this.controller.trackEvent("BerryFunnelEnd", {'amount': item.amount + ''});
							this.controller.resetBerryFunnel();
						} else {
							this.controller.setData('berryFunnel', true);
						}

						this.controller.trackEvent("BerryFunnelStart", {'amount': item.amount + '' });

						break;
					case 'hearts':
						var dataObj = {	'label': 'hearts_' + item.amount,
										'cost': item.cost + '',
										'type': 'hearts'};

						this.trackPurchase(dataObj, 'gems');
						break;
				}
			}
		} else if (cur === 'berries') {
			// spending berries

			if (this.controller.spendBerries(item.cost, true)) {
				couldSpend = true;
				this.controller.closeActiveModal(false);
			}
		}

		if (couldSpend) {
			var data = this.controller.getData(type);
			var earnings = item.amount;
			if (type === "hearts" && earnings === -1) {
				earnings = Number.MAX_VALUE;
				this.controller.setData("unlimitedHearts", true);
				this.controller.setData(type, earnings);
			} else {
				this.controller.setData(type, data + earnings);
			}
			return true;
		}

		return false;
	};

	this.purchasePlayStoreGems = function(item){
		if( this.controller.getData('gemFunnel') ){
			this.controller.trackEvent("GemFunnelEnd", {'amount': item.amount + ''});
			this.controller.resetGemFunnel();
		} else {
			this.controller.setData('gemFunnel', true);
		}

		this.controller.trackEvent("GemFunnelStart", {'amount': item.amount + '' });

		this.controller.addGems(item.amount);
		this.controller.showAds = false;
		this.controller.setData('ShowAds', false);

		var activeView = this.controller.activeView;
		var mainMenu = GC.app.screenViews["MainMenuView"];
		var gameOver = GC.app.screenViews["GameOverView"];
		if (!this.controller.transitioning
			&& (activeView === mainMenu || activeView === gameOver))
		{
			// show notification immediately if they are on a valid screen
			this.controller.showPurchaseSuccessNotification(true);
		} else {
			// show notification when they arrive on a valid screen
			this.controller.setData('purchaseSuccess', true);
		}

		if (this.controller.midAngelSave) {
			if (this.controller.basicAngelModal) {
				this.controller.basicAngelModal.closeModal();
				this.controller.basicAngelModal = undefined;
			}
			if (this.controller.gemAngelModal) {
				this.controller.gemAngelModal.closeModal();
			}
			this.controller.publish('acceptAngelPurchase');
		}
	};

	this.purchaseLevel = function(item){
		var id = item.id;
		if (item.money) {
			if (GC.isNative && device.isMobile) {
				billing.purchase(this.controller.mapConfig.levels[id].productID);
				return true;
			} else {
				this.controller.addLevel(id);
				this.finishPurchase(id);
				return true;
			}
		}
		else {
			var gems = this.controller.getData("gems");
			if (gems >= item.cost) {
				// TODO: particle fx?
				gems -= item.cost;

				this.controller.setData("gems", gems);
				this.controller.header.refresh();

				var purchaseObj = {
					'label': item.id + '',
					'cost': item.cost + '',
					'type': 'map_level'
				}

				this.trackPurchase(purchaseObj, 'gems');

				this.controller.addLevel(id);

				return true;
			}
			else {
				return false;

			}
		}
	};

	this.purchaseBabyKiwi = function(item){
		var gems = this.controller.getData("gems");

		if( item.cost <= gems ){
			var boughtBabies = this.controller.getData("babies");
			boughtBabies[boughtBabies.length] = item.id;

			this.controller.setData("babies", boughtBabies);
			this.controller.setData("gems", gems - item.cost);

			var dataObj = {'label': item.name + '',
							'cost': item.cost + '',
							'type': 'baby_kiwi'};

			this.trackPurchase(dataObj, 'gems');
			return true;
		}
		return false;
	};

	this.purchaseBabySlot = function(index){
		var availableSlots = this.controller.getData("babySlots");
		var slotCost = availableSlots * 50; //this.controller.checkExperiment('babySlotPrice');

		availableSlots += 1;

		this.controller.spendGems(slotCost);

		this.controller.setData("babySlots", availableSlots);
		this.controller.publish("babySlotPurchase");

		var dataObj = {	'label': 'slot_' + availableSlots + '',
						'cost': slotCost + '',
						'type': 'baby_kiwi_slot'};

		this.trackPurchase(dataObj, 'gems');
	};

	this.trackPurchase = function(data, currency){
		if( currency === 'berries' ){
			this.controller.trackBerryFunnel(data);
		}
		else if( currency === 'gems' ){
			this.controller.trackGemFunnel(data);
		}

	};

	this.goToBerryStore = function(){
		this.controller.showBerryStore();
		this.controller.showHeader(true);
	};

	this.goToGemStore = function(){
		this.controller.showGemStore();
		this.controller.showHeader(true);
	};
});
