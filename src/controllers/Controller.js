import device;
import Sound;

import animate;
import ui.View as View;
import ui.TextView as TextView;
import ui.ImageView as ImageView;

import src.controllers.SocialController as SocialController;
import src.controllers.ProductController as ProductController;
import src.controllers.PromoEffectController as PromoEffectController;
import src.models.AppModel as AppModel;
import src.views.helpers.GameInput as GameInput;
import src.views.modals.BinaryModalView as BinaryModalView;
import src.lib.Utils as Utils;
import src.views.drop_ins.Header as Header;

import src.lib.LocaleManager as LocaleManager;

import ui.resource.loader as loader

import event.Emitter as Emitter;

// this class is a singleton; see end of file
var Controller = Class(Emitter, function() {

	// CLASS CONSTANTS
	var MAX_VOLUME = 127,
		BASE_TIMEOUT = 5000,
		TRANS_TIME,
		BG_WIDTH = 1280,
		BG_HEIGHT = 720,
		MIN_TICK = 1,
		MAX_TICK = 100,
		TICK_MOD = 500,
		MODAL_WIDTH = 657,
		MODAL_HEIGHT = 646,
		STORE_MODAL_WIDTH = 893,
		STORE_MODAL_HEIGHT = 599,
		FRAME_WIDTH = 657,
		FRAME_HEIGHT = 477,
		TOAST_WIDTH = 552,
		TOAST_HEIGHT = 150,
		TOAST_SPACING = 25,
		TOAST_TIME = 500,
		ACHIEVEMENT_ICON_WIDTH = 73,
		ACHIEVEMENT_ICON_HEIGHT = 73,
		DEFAULT_LOAD_WAIT = 500,
		GAME_LOAD_WAIT = 3000,
		TYPE_OBJECT = "object",
		TYPE_BABY_KIWI = "Baby",
		ANGEL_SAVE_COST = 3;

	var Z_MODAL = 101;

	// some achievements
	var ACHIEVE_DATA_KEY = "achievements",
		ACHIEVE_TYPE_COUNTER = "COUNTER",
		ACHIEVE_TYPE_UNIQUE = "UNIQUE",
		ACHIEVE_TYPE_NESTABLES = "NESTABLES",

		ACHIEVE_ID_BERRY_MAGNET = "berryMagnet",
		ACHIEVE_ID_MEGA_BERRY = "megaBerry",
		ACHIEVE_ID_TURTLE_FRIEND = "turtleFriend",
		ACHIEVE_ID_PINK = "pinkNestables",
		ACHIEVE_ID_GREEN = "greenNestables",
		ACHIEVE_ID_BUGS = "bugs",
		ACHIEVE_ID_JAPAN = "japanese",

		ACHIEVE_TOAST_IMG,
		PASS_TOAST_IMG = "resources/images/Game/toasterFriend.png",

		NESTABLE_ID_PINK = "5000",
		NESTABLE_ID_GREEN = "6000",
		NESTABLE_ID_BUGS = ["7000", "11000"],
		NESTABLE_ID_JAPAN = ["13000", "14000"];

	this.init = function(opts) {
		// we are using this as a root parent only
		this.rootView = GC.app.view;
		this.inputBlocked = false;

		// scale to fit a normalized dimension; change this.landscape to false for portrait games
		this.landscape = true;
		if (this.landscape) { // LANDSCAPE APP
			// target dimensions define max aspect ratio as well as a value for the normalized dimension
			this.targetWidth = 960;
			this.targetHeight = 720;

			// base dimensions define actual values used; for landscape, expand width and normalize height
			this.baseWidth = device.screen.width * (this.targetHeight / device.screen.height);
			this.baseHeight = this.targetHeight;
			this.scale = device.screen.height / this.baseHeight;
		} else { // PORTRAIT APP
			this.targetWidth = 720;
			this.targetHeight = 960;

			this.baseWidth = this.targetWidth;
			this.baseHeight = device.screen.height * (this.targetWidth / device.screen.width);
			this.scale = device.screen.width / this.baseWidth;
		}

		// load JSON config files
		this.config = this.readJSON("resources/data/Config.json");
		this.avatarConfig = this.readJSON("resources/data/AvatarConfig.json");
		this.babyConfig = this.readJSON("resources/data/BabyConfig.json");
		this.babyOrder = this.babyConfig.ordering;
		this.babyConfig = this.babyConfig.babies;

		this.soundConfig = this.readJSON("resources/data/SoundConfig.json");
		this.nestablesConfig = this.readJSON("resources/data/NestablesConfig.json");

		this.upgradesConfig = this.readJSON("resources/data/UpgradesConfig.json");
		this.upgradesOrder = this.upgradesConfig.ordering;
		this.upgradesConfig = this.upgradesConfig.products;

		this.storeConfig = this.readJSON("resources/data/StoreConfig.json");
		this.modalStoreConfig = this.readJSON("resources/data/ModalStoreConfig.json");
		this.achievementsConfig = this.readJSON("resources/data/AchievementsConfig.json");
		this.levelConfig = this.readJSON("resources/data/LevelConfig.json");
		this.mapConfig = this.readJSON("resources/data/MapConfig.json");
		this.experimentConfig = this.readJSON("resources/data/ExperimentConfig.json");

		if (GLOBAL.NATIVE) {
			NATIVE.onBackButton = bind(this, 'fireBackButton');
		}

		// the app model holds the current state of saved data to reduce local storage access
		this.appModel = new AppModel({ controller: this });
		this.setTickManager();

		// other controllers
		this.productController = new ProductController(this);
		this.promoEffectController = new PromoEffectController(this);

		// preload effects then init effects audio API, init music immediately since it doesn't preload
		loader.preload("resources/sounds/effects", bind(this, 'initEffects'));
		this.initMusic();

		// stack of back button functions
		this.oldBackFuncs = [];
		this.transitioning = false;

		TRANS_TIME = this.config.ui.transitionTime;

		// we do not throw away screens, we save them and set style.visible = false
		this.screenViews = {};

		// init all achievement data to minimize look-up during gameplay
		this.updateAchievementState();
		this.initAchievementViews();

		// track total playtime
		this.totalPlaytime = this.getData("totalPlaytime") || 0;
		GC.app.engine.subscribe('Tick', this, 'onTick');

		// track Session M status if necessary
		this.trackSessionM("install");

		//reference for experiment group
		this.experiment = this.getData("activeExperiment");

		//EXPERIMENT LOGIC
		//if( this.experiment ){
			//this.configGemExperiment();
		//}

		this.splashOverlay = new View({
			parent: this.rootView,
			x: 0,
			y: 0,
			width: this.baseWidth,
			height: this.baseHeight,
			scale: this.scale,
			visible: false
		});
		this.splashImage = new ImageView({
			parent: this.splashOverlay,
			x: (this.splashOverlay.style.width - BG_WIDTH) / 2,
			y: (this.splashOverlay.style.height - BG_HEIGHT) / 2,
			width: BG_WIDTH,
			height: BG_HEIGHT,
			image: "resources/images/splash.png"
		});
		this.splashOverlay.removeFromSuperview();

		// ads can be disabled by purchases, so maintain a flag here
		this.showAds = this.getData("ShowAds") && device.isMobileNative;

		this.activeModal = null;

		this.modalStack = [];

		this.makeBerryModal = bind(this, this.makeBerryModal);
		this.makeGemModal = bind(this, this.makeGemModal);
		this.makeHeartModal = bind(this, this.makeHeartModal);
		this.makeEquipModal = bind(this, this.makeEquipModal);
		this.makeBabyKiwiModal = bind(this, this.makeBabyKiwiModal);

		this.buildHeader();
	};

	var playTimeEvents = [[1, "30s"], [2, "1m"], [3, "2m"], [4, "3m"], [5, "4m"],
		[6, "5m"], [7, "10m"], [8, "30m"], [9, "1h"]].map(function(pair) {
			return pair[0] + "Playtime" + pair[1];
		});
	this.onTick = function(dt) {
		this.totalPlaytime += dt;

		if (!this.getData("9Playtime1h") && this.totalPlaytime >= 3600000) {
			this.trackTimeEvent("9Playtime1h");
		} else if (!this.getData("8Playtime30m") && this.totalPlaytime >= 1800000) {
			this.trackTimeEvent("8Playtime30m");
		} else if (!this.getData("7Playtime10m") && this.totalPlaytime >= 600000) {
			this.trackTimeEvent("7Playtime10m");
		} else if (!this.getData("6Playtime5m") && this.totalPlaytime >= 300000) {
			this.trackTimeEvent("6Playtime5m");
		} else if (!this.getData("5Playtime4m") && this.totalPlaytime >= 240000) {
			this.trackTimeEvent("5Playtime4m");
		} else if (!this.getData("4Playtime3m") && this.totalPlaytime >= 180000) {
			this.trackTimeEvent("4Playtime3m");
		} else if (!this.getData("3Playtime2m") && this.totalPlaytime >= 120000) {
			this.trackTimeEvent("3Playtime2m");
		} else if (!this.getData("2Playtime1m") && this.totalPlaytime >= 60000) {
			this.trackTimeEvent("2Playtime1m");
		} else if (!this.getData("1Playtime30s") && this.totalPlaytime >= 30000) {
			this.trackTimeEvent("1Playtime30s");
		}
	};

	// this fn helps track smaller time increments for retention, which Flurry is lacking
	this.trackTimeEvent = function(name) {
		var events = playTimeEvents.slice(0, playTimeEvents.indexOf(name)+1);
		events.forEach(bind(this, function(name) {
			if (!this.getData(name)) {
				this.setData(name, true, true, true);
				this.trackEvent(name, {});
			}
		}));
	};


/* ANALYTICS WRAPPERS
* easy to patch if there is any trouble
*/

	this.trackEvent = function(name, params) {
		logger.log('TRACKING EVENT: ' + name);

		params.version = CONFIG.version;
		params.appStore = GC.app.appStore;
		params.locale = GC.app.l.locale;
		//gcsocial.track(name, params);
	};

	this.configGemExperiment = function(){
		var gemData = this.checkExperiment('gemPriceAdFrequency');
		this.modalStoreConfig.products.gems.items[5] = this.modalStoreConfig.products.gems.items[gemData['productIndex']];
		this.modalStoreConfig.products.gems.items.pop();
	};

	//Resets controller experiment data to new experiment name and group
	this.trackExperiment = function(name, group) {
		this.experiment = {"name": name, "group": group};
		this.trackEvent('Experiment_' + group, {});
	};

	this.checkExperiment = function(name) {
		var experiment = this.experimentConfig.active[name];

		if( !experiment ){
			//This should never happen. Don't bother checking against experiments we're not running
			logger.log('EXPERIMENT ERROR: Experiment name: ' + name + " doesn't exist.");
			return false;
		}

		if( this.experiment && this.experiment.name === name ){
			return experiment.groups[this.experiment.group].variableValue;
		}
		else{
			return experiment.control.variableValue;
		}
	};

	this.trackSessionM = function(name) {
		if (GC.app.enableSessionM && !this.getData("SM_" + name)) {
			this.setData("SM_" + name, true);
			//gcsocial.trackSessionM(name);
		}
	};

/* ~ ~ ~ VIEW FUNCTIONS ~ ~ ~ */

	this.transitionLockDown = function(lock) {
		this.transitioning = lock;
	};

	// swap active screens, called after a screen finishes deconstructing
	this.transitionToScreen = function(name, ctor, preloads) {
		this.blockInput();

		var oldView = this.activeView;
		var timestamp = Date.now();

		this.preloading = true;
		this.activeViewPreloads = preloads;
		this.nextScreen = name;

		var loadingScreen = this.screenViews["LoadingView"];
		if (!loadingScreen) {
			jsio("import src.views.screens.LoadingView as LoadingView");
			this.screenViews["LoadingView"] = loadingScreen = new LoadingView({
				parent: this.rootView,
				x: 0,
				y: 0,
				width: this.baseWidth,
				height: this.baseHeight,
				scale: this.scale,
				opacity: 0,
				visible: true
			});
		} else {
			loadingScreen.style.opacity = 0;
			loadingScreen.style.visible = true;
			this.rootView.addSubview(loadingScreen);
		}

		loadingScreen.resetView();

		if (name === "GameView") {
			var nestables = this.chooseNestableSet();
			if (preloads && nestables.nestableSet && nestables.nestableSet.preload) {
				preloads.push(nestables.nestableSet.preload);
			}

			this.nestableSet = nestables.nestableSet;
			this.collectedSet = nestables.collectedSet;
		}

		if (oldView) {
			var boundTransition = bind(this, function() {
				this.preloading = false;

				var savedScreen = this.screenViews[name];
				if (savedScreen) {
					this.activeView = savedScreen;
					this.rootView.addSubview(savedScreen);
				} else {
					this.screenViews[name] = this.activeView = new ctor({
						parent: this.rootView,
						x: 0,
						y: 0,
						width: this.baseWidth,
						height: this.baseHeight,
						scale: this.scale,
						visible: false
					});
				}

				// keep the loading screen on top
				loadingScreen.removeFromSuperview();
				this.rootView.addSubview(loadingScreen);

				this.activeView.resetView();
				this.activeView.style.visible = true;

				var delay = DEFAULT_LOAD_WAIT;
				if (name === "GameView") {
					delay = GAME_LOAD_WAIT;

					if (GLOBAL.NATIVE && NATIVE.gc) {
						NATIVE.gc.runGC();
					}
				}

				var elapsed = Date.now() - timestamp;
				if (elapsed < delay) {
					delay -= elapsed;
				} else {
					delay = 0;
				}

				this.loadingAnim = animate(loadingScreen).wait(delay)
				.then({ opacity: 0 }, TRANS_TIME, animate.linear)
				.then(bind(this, function() {
					loadingScreen.style.visible = false;
					loadingScreen.deconstructView();
					loadingScreen.removeFromSuperview();
					this.unblockInput();
					this.activeView.constructView();
					this.publish('viewChanged');
				}));
			});

			animate(loadingScreen).now({ opacity: 1 }, TRANS_TIME, animate.linear)
			.then(bind(this, function() {
				oldView.stopAnimations && oldView.stopAnimations();
				oldView.style.visible = false;
				oldView.removeFromSuperview();
				oldView.publish('removed');

				if (preloads) {
					loader.preload(preloads, boundTransition);
				} else {
					boundTransition();
				}
			}));
		} else {
			var boundTransition = bind(this, function() {
				this.preloading = false;

				var savedScreen = this.screenViews[name];
				if (savedScreen) {
					this.activeView = savedScreen;
					this.rootView.addSubview(savedScreen);
				} else {
					this.screenViews[name] = this.activeView = new ctor({
						parent: this.rootView,
						x: 0,
						y: 0,
						width: this.baseWidth,
						height: this.baseHeight,
						scale: this.scale,
						visible: false
					});
				}

				this.activeView.resetView();
				this.activeView.style.visible = true;
				this.unblockInput();
				this.activeView.constructView();
				this.publish('viewChanged');
			});

			loadingScreen.style.visible = false;
			loadingScreen.removeFromSuperview();

			if (preloads) {
				loader.preload(preloads, boundTransition);
			} else {
				boundTransition();
			}
		}


	};

	this.getLoadingOpts = function() {
		if (this.nextScreen === "GameView") {
			var tips = this.config.trainerTips,
				locale = LocaleManager.getLocale(),
				tip;

			if (!this.getData('koreanNYPromoShown') && locale === 'ko-kr') {
				tip = tips[0];
				this.setData('koreanNYPromoShown', true);
			} else {
				var minTipIndex = 0;
				if (locale !== 'ko-kr') {
					minTipIndex = 1;
				}
				if (GC.app.enableWebMode) {
					minTipIndex = 2;
				}

				tip = tips[~~(Math.random() * (tips.length - minTipIndex) + minTipIndex)];
			}

			return {
				showTip: true,
				title: tip.title,
				message: tip.tip
			};
		} else {
			return { showTip: false };
		}
	};

	this.transitionToTitle = function() {
		jsio("import src.views.screens.TitleView as TitleView");
		this.transitionToScreen("TitleView", TitleView, [
			GC.app.l.translate("resources/images/Locales/en") + '/Title'
		]);
	};

	this.transitionToMainMenu = function() {
		jsio("import src.views.screens.MainMenuView as MainMenuView");
		this.transitionToScreen("MainMenuView", MainMenuView, [
			"resources/images/MainMenu",
			this.getAvatarPreload(),
			GC.app.l.translate("resources/images/Locales/en") + "MainMenu",
			GC.app.l.translate("resources/images/Locales/en") + "Title"
		]);
	};

	this.transitionToGame = function() {
		jsio("import src.views.screens.GameView as GameView");
		var preloads = [
			"resources/images/Game",
			GC.app.l.translate("resources/images/Locales/en") + "/Game",
			this.getAvatarPreload(),
			this.getLevelPreload(),
			"resources/sounds/effects/Game",
			"resources/images/Enemies/Echidna"
		];

		this.angelSaveCount = 0;

		this.transitionToScreen("GameView", GameView, preloads);
	};

	this.transitionToGameOver = function() {
		jsio("import src.views.screens.GameOverView as GameOverView");
		this.transitionToScreen("GameOverView", GameOverView, ["resources/images/GameOver"]);
	};

	this.transitionToStats = function() {
		jsio("import src.views.screens.StatsView as StatsView");
		this.transitionToScreen("StatsView", StatsView, ["resources/images/Stats"]);
	};

	this.transitionToNestables = function() {
		jsio("import src.views.screens.NestablesView as NestablesView");
		this.transitionToScreen("NestablesView", NestablesView, [
			"resources/images/Nestables",
			GC.app.l.translate("resources/images/Locales/en") + "/Nestables"
		]);
	};

	this.transitionToUpgrades = function() {
		this.transitionToStore();
	};

	this.transitionToStore = function() {
		jsio("import src.views.screens.StoreView as StoreView");
		this.transitionToScreen("StoreView", StoreView, ["resources/images/Store"]);
	};

	this.transitionToAchievements = function() {
		jsio("import src.views.screens.AchievementsView as AchievementsView");
		this.transitionToScreen("AchievementsView", AchievementsView,
			["resources/images/Achievements", "resources/images/Game/Achievements"]);
	};

	this.transitionToLeaderboard = function() {
		jsio("import src.views.screens.LeaderboardView as LeaderboardView");
		this.transitionToScreen("LeaderboardView", LeaderboardView, [
			"resources/images/Leaderboard",
			"resources/images/Modals"
		]);
	};

	this.transitionToMap = function() {
		jsio("import src.views.screens.MapView as MapView");
		this.transitionToScreen("MapView", MapView, [
			"resources/images/Map"
		]);
	};

	this.transitionToCheatView = function() {
		jsio("import src.views.screens.CheatView as CheatView");
		this.transitionToScreen("CheatView", CheatView);
	};

	this.getAvatarPreload = function() {
		//var avatar = this.getData("selectedAvatar") || this.config.game.player.defaultAvatar;
		var avatar = this.config.game.player.defaultAvatar;
		var avatarConfig = this.avatarConfig[avatar];
		return avatarConfig.preload;
	};

	this.getLevelPreload = function() {
		var level = this.getData("selectedLevel");
		var levelConfig = this.levelConfig[level];
		return levelConfig.preload;
	};

	var eventTypeMapping = {
		'map_level': 'Maps',
		'baby_kiwi_slot': 'BabySlots',
		'boost_angelFish': 'AngelFish',
		'berries': 'Berries',
		'hearts': 'Hearts',
		'baby_kiwi': 'BabyKiwis',
		'adult_kiwi': 'AdultKiwis',
		'upgrade_berryMagnet': 'MagnetUpgrade',
		'upgrade_megaBerry': 'MegaBerryUpgrade',
		'upgrade_kiwiGlider': 'GliderUpgrade',
		'upgrade_shield': 'ShieldUpgrade',
		'boost': 'Boost'
	}

	//Flurry tracking of gem-based purchase events
	/* Data Object should contain:
	{
		"label": a product uID,
		"cost": cost in gems,
		"type": the product type, see options above (OR ADD YOUR OWN!)
	}
	*/
	this.trackGemFunnel = function(data) {
		//have they purchased gems with real world money?
		var evtName,
			playerLevel = this.getData("level"),
			totalGems = this.getData("gems");

			data.playerLevel = playerLevel + '';
			data.totalGems = ( totalGems + Number(data.cost) ) + '' ;

		if( this.getData('gemFunnel') ){
			if( !this.getData('firstBoughtAnything') ){
				evtName = "GemFunnelFirstBought" + eventTypeMapping[data.type];
				this.trackEvent(evtName, data);

				this.trackEvent("GemFunnelFirstBoughtAnything", data);
				this.setData('firstBoughtAnything', true);
			}

			evtName = "GemFunnelBought" + eventTypeMapping[data.type];
			this.trackEvent(evtName, data);

			this.trackEvent("GemFunnelBoughtAnything", data);
		}
		else{
			evtName = "NoFunnel_GemPurchase";
			this.trackEvent(evtName, data);
		}
	};

	//Flurry tracking of berry-based purchase events
	/* Data Object should contain:
	{
		"label": a product uID,
		"cost": cost in berries,
		"type": the product type, see options above (OR ADD YOUR OWN!)
	}
	*/
	this.trackBerryFunnel = function(data) {
		//have they purchased berries with gems?
		var evtName,
			playerLevel = this.getData("level"),
			totalBerries = this.getData("berries");

			data.playerLevel = playerLevel + '';
			data.totalBerries = ( totalBerries + Number(data.cost) ) + '';

		if( this.getData('berryFunnel') ){
			if( !this.getData('firstBoughtAnythingBerries') ){
				evtName = "BerryFunnelFirstBought" + eventTypeMapping[data.type];
				this.trackEvent(evtName, data);

				this.trackEvent("BerryFunnelFirstBoughtAnything", data);
				this.setData('firstBoughtAnythingBerries', true);
			}

			evtName = "BerryFunnelBought" + eventTypeMapping[data.type];
			this.trackEvent(evtName, data);

			this.trackEvent("BerryFunnelBoughtAnything", data);
		}
		else{
			evtName = "NoFunnel_BerryPurchase";
			this.trackEvent(evtName, data);
		}
	};

	this.resetGemFunnel = function(){
		this.setData('firstBoughtAnything', false);
	};

	this.resetBerryFunnel = function(){
		this.setData('firstBoughtAnythingBerries', false);
	};

	this.showPlayStoreNotification = function() {
		if (GC.app.enableStoreModals) {
			jsio("import src.views.modals.BasicModalView as BasicModalView");

			var message = GC.app.l.translate("Items from the Store may take some time to appear.");

			new BasicModalView({
				parent: this.activeView,
				message: message,
				onAccept: bind(this, 'closeActiveModal', false)
			});
		}
	};

	this.showPurchaseSuccessNotification = function(force) {
		if (GC.app.enableStoreModals && (this.getData('purchaseSuccess') || force)) {
			jsio("import src.views.modals.BasicModalView as BasicModalView");

			var message = GC.app.l.translate("Your items have arrived!");

			new BasicModalView({
				parent: this.activeView,
				message: message
			});

			this.setData('purchaseSuccess', false);
		}
	};

	var fnSort = function(setA, setB) {
		if (setA.order < setB.order) {
			return -1;
		} else if (setA.order > setB.order) {
			return 1;
		} else {
			return 0;
		}
	};

	this.chooseNestableSet = function() {
		var choices = this.nestablesConfig.sets;
		var collected = this.getData("nestablesCollected") || {};
		var available = [];

		for (var i in choices) {
			var choice = choices[i];
			var choiceCount = 0;
			for (var j in choice.pieces) {
				choiceCount++;
			}

			var collectedCount = 0;
			var collection = collected[i];
			if (collection) {
				for (var j in collection) {
					collectedCount++;
				}
			}

			if (choiceCount > collectedCount) {
				if( choice.id === '25000' && LocaleManager.getLocale() !== 'ko-kr' ){
					continue;
				}

				available.push(choice);
			}
		}

		// choose nestables in order of progression
		if (available.length) {
			available.sort(fnSort);

			// level specific nestables take precedent
			var level = this.getData("selectedLevel");
			var levelNestables = this.levelConfig[level].nestables;
			if (levelNestables) {
				for (var n in levelNestables) {
					var levelNestableID = levelNestables[n];
					for (var av in available) {
						if (levelNestableID === available[av].id) {
							return {
								nestableSet: available[av],
								collectedSet: collected[levelNestableID]
							};
						}
					}
				}
			}

			return {
				nestableSet: available[0],
				collectedSet: collected[available[0].id]
			};
		} else { // all sets have been collected!!!
			return {
				nestableSet: false,
				collectedSet: false
			};
		}
	};

	this.showModal = function(modalIn){
		var that = this;
		if( this.activeModal ){
			this.activeModal.closeModal( function(){
				that.bringUpNewModal(modalIn);
			});
		}
		else{
			return this.bringUpNewModal(modalIn);
		}
	};

	this.bringUpNewModal = function(modalIn){
		var modal,
			index,
			type,
			modal;

		index = this.modalStack.indexOf(modalIn);

		if( index === -1){
			modal = modalIn;
		}
		else{
			modal = this.modalStack[index];
			this.modalStack.splice(index, 1);
		}

		this.modalStack[this.modalStack.length] = modal;

		this.activeModal = this.modalStack[this.modalStack.length -1];
		this.activeModal.parent = this.activeView;
		if ( typeof this.activeModal.refresh === 'function' ){
			this.activeModal.refresh();
		}
		this.activeModal.openModal();
		this.publish('viewChanged');

		return this.activeModal;
	};

	this.closeActiveModal = function(closeAll, cb){
		var that = this;

		if( !this.activeModal ){
			cb && cb();
			return;
		}
		this.publish('modalClose');
		if( closeAll ){
			this.activeModal.closeModal(cb);
			this.publish('lastModalClose');
			this.modalStack = [];
			this.activeModal = undefined;
			this.publish('viewChanged');
		}
		else{
			this.activeModal.closeModal( function(){
				that.modalStack.splice(that.modalStack.length-1, 1);
				if( that.modalStack.length > 0 ){
					that.showModal(that.modalStack[that.modalStack.length-1]);
				}
				else{
					that.activeModal = undefined;
					that.publish('lastModalClose');
					that.publish('viewChanged');
				}
				cb && cb();
			});
		}
	};

	this.initModal = function(modalClass, extraOpts){
		var defaultOpts = {
			parent: this.activeView,
			width: this.activeView.style.width,
			height: this.activeView.style.height,
			noAutoOpen: true
		},
		opts = this.mergeRecursively(defaultOpts, extraOpts);

		var modal = new modalClass(opts);

		return modal;
	};

	this.showBerryStore = function(){
		if( !this.berryStoreModal ){
			jsio("import src.views.modals.BerryStoreModal as BerryStoreModal");

			this.berryStoreModal = this.initModal(BerryStoreModal);
		}

		this.showModal(this.berryStoreModal);
	};

	this.showGemStore = function(){
		if( !this.gemStoreModal ){
			jsio("import src.views.modals.GemStoreModal as GemStoreModal");

			this.gemStoreModal = this.initModal(GemStoreModal);
		}

		this.showModal(this.gemStoreModal);
	};

	this.showNoHeartsModal = function() {
		var message = GC.app.l.translate("Oh no, you ran out of hearts! Buy more now?");
		new BinaryModalView({
			parent: this.activeView,
			message: message,
			acceptButtonText: 'BUY!',
			onAccept: bind(this, function() {
				this.showHeartStore();
			})
		});
	}

	this.showHeartStore = function(){
		if( this.getData('unlimitedHearts') ) {
			return;
		}

		if( !this.heartStoreModal ){
			jsio("import src.views.modals.HeartStoreModal as HeartStoreModal");

			this.heartStoreModal = this.initModal(HeartStoreModal);
		}

		this.showModal(this.heartStoreModal);

	};

	this.showEquipModal = function() {
		if( !this.equipModalView ){
			jsio("import src.views.modals.EquipModalView as EquipModalView");

			this.equipModalView = this.initModal(EquipModalView);
		}

		this.equipModalView.refresh();
		this.showModal(this.equipModalView);

	};

	this.showBabyKiwiModal = function( index ) {
		index = index || 0;
		var availableSlots = this.getData("babySlots");

		// purchase baby slot?
		if (index >= availableSlots) {

			//EXPERIMENT LOGIC
			var slotCost = availableSlots * 50;//this.checkExperiment('babySlotPrice');
			var gems = this.getData("gems");
			if (slotCost <= gems) {
				jsio('import src.views.modals.BinaryModalView as BinaryModalView');
				var message = GC.app.l.translate("Do you want to buy a baby slot for $[1] gems?", slotCost);
				new BinaryModalView({
					parent: this.activeView,
					message: message,
					onAccept: bind(this, function() {
						this.productController.purchaseBabySlot(index);
						this.header.refresh();
					})
				});
			} else {
				this.showGemStore();
			}
		} else {
			this.makeBabyKiwiModal(index);
		}
	};

	this.makeBabyKiwiModal = function(index){
		if( !this.babyKiwiModal ){
			jsio("import src.views.modals.BabyKiwiStoreModal as BabyKiwiStoreModal");

			this.babyKiwiModal = this.initModal(BabyKiwiStoreModal, {index: index});
		}

		this.babyKiwiModal.setIndex(index);
		this.showModal(this.babyKiwiModal);
		this.setData('babyPromoShown', true);
	};

	this.getAngelSaveCost = function() {
		return this.angelSaveCount ? ANGEL_SAVE_COST * this.angelSaveCount : 1;
	};

	this.activateGameInput = function(gameView) {
		if (!this.gameInput) {
			this.gameInput = new GameInput({
				parent: this.rootView,
				x: 0,
				y: 0,
				width: this.baseWidth,
				height: this.baseHeight,
				scale: this.scale,
				realView: gameView
			});
		}
	};

	this.deactivateGameInput = function() {
		if (this.gameInput) {
			this.gameInput.removeFromSuperview();
			this.gameInput = null;
		}
	};

	this.blockInput = function() {
		this.rootView.getInput().blockEvents = true;
	};

	this.unblockInput = function() {
		this.rootView.getInput().blockEvents = false;
	};

	this.isInputBlocked = function() {
		return this.rootView.getInput().blockEvents;
	};

	this.overlaySplash = function() {
		if (this.splashAnim || this.splashOverlay.style.visible) {
			return; // don't do anything if removeSplash is already in action
		}

		this.rootView.addSubview(this.splashOverlay);
		this.splashOverlay.style.visible = true;
		this.splashOverlay.style.opacity = 1;

		this.overlayActive = true;
	};

	this.resumeApp = function() {
		this.setData('showedPromoThisSession', false);
		if (this.activeViewPreloads && !this.preloading) {
			loader.preload(this.activeViewPreloads, bind(this, 'removeSplash'));
		} else {
			this.removeSplash();
		}
	};

	this.removeSplash = function() {
		if (this.overlayActive) {
			this.overlayActive = false;

			this.splashAnim = animate(this.splashOverlay)
			.now({ opacity: 0 }, TRANS_TIME, animate.linear)
			.then(bind(this, function() {
				this.splashAnim = null;
				this.splashOverlay.style.visible = false;
				this.splashOverlay.removeFromSuperview();

				this.pauseGame();
			}));
		}
	};

	this.getActiveView = function() {
		return this.activeView;
	};

	this.getGameView = function() {
		return this.screenViews['GameView'];
	};

	this.getStoreView = function() {
		return this.screenViews['StoreView'];
	};

	this.getMapView = function() {
		return this.screenViews['MapView'];
	};

	this.getNestablesView = function() {
		return this.screenViews['NestablesView'];
	};

	this.removeView = function(viewName) {
		delete this.screenViews[viewName];
	};

	this.buildHeader = function(){
		this.header = new Header({
			superview: this.rootView,
			x: (this.rootView.style.width - STORE_MODAL_WIDTH*this.scale)/2,
			y: 0,
			scale: this.scale,
			width: STORE_MODAL_WIDTH,
			height: 75,
			controller: this,
			zIndex: Z_MODAL
		});

		this.header.makeInvisible();
	};

	this.showHeader = function(doAnimate, cb, dontRefresh){
		!dontRefresh && this.header.refresh();
		this.header.showHeader(doAnimate, cb);
	}

	this.hideHeader = function(doAnimate, cb){
		this.header.hideHeader(doAnimate, cb);
	}

/* ~ ~ ~ MODEL FUNCTIONS ~ ~ ~ */

	// old users may have earned achievements that cannot be attained again
	this.accountForOldUsers = function() {
		this.achieveCheck(ACHIEVE_ID_PINK, NESTABLE_ID_PINK);
		this.achieveCheck(ACHIEVE_ID_GREEN, NESTABLE_ID_GREEN);
		this.achieveCheck(ACHIEVE_ID_BUGS, NESTABLE_ID_BUGS);
		this.achieveCheck(ACHIEVE_ID_JAPAN, NESTABLE_ID_JAPAN);
	};

	this.updateAchievementState = function() {
		var achievements = this.getData(ACHIEVE_DATA_KEY);
		var conf = this.achievementsConfig;

		// init
		this.achieveLevels = {};
		this.achCounterTriggers = {};

		// special cases
		this.berriesCollected = this.getData("berriesCollected");

		// behavior by type
		for (var id in conf) {
			var achieveConf = conf[id];
			var achieveData = achievements[id];
			switch (achieveConf.type) {
				case ACHIEVE_TYPE_COUNTER:
					var level = this.achieveLevels[id] = achieveData.level;
					var triggers = achieveConf.triggers;

					if (level >= triggers.length) {
					 	this.achCounterTriggers[id] = false;
					} else {
						this.achCounterTriggers[id] = triggers[level];
					}
					break;

				case ACHIEVE_TYPE_UNIQUE:
				case ACHIEVE_TYPE_NESTABLES:
					this.achieveLevels[id] = achieveData.level;
					break;
			}
		}
	};

	// check achievement status, which is pre-calc'd by the fn above, this.updateAchievementState
	this.achieveCheck = function(id, testValue) {
		var achievements = this.getData(ACHIEVE_DATA_KEY);
		var conf = this.achievementsConfig;
		var achieveConf = conf[id];
		var achieveData = achievements[id];

		if (!achieveConf || !achieveData) {
			return;
		}

		switch (achieveConf.type) {
			case ACHIEVE_TYPE_COUNTER:
				var trigger = this.achCounterTriggers[id];
				if (trigger && trigger <= testValue) {
					achieveData.level++;
					var level = achieveData.level;
					var triggers = achieveConf.triggers;

					if (level >= triggers.length) {
						this.achCounterTriggers[id] = false;
					} else {
						this.achCounterTriggers[id] = triggers[level];
					}

					this.animateAchievement(achieveConf, level - 1);
					this.setData(ACHIEVE_DATA_KEY, achievements, true, true);
				}
				break;

			case ACHIEVE_TYPE_UNIQUE:
				if (!this.achieveLevels[id]) {
					achievements[id].level = this.achieveLevels[id] = 1;
					this.setData(ACHIEVE_DATA_KEY, achievements, true, true);
					this.animateAchievement(achieveConf);
				}
				break;

			case ACHIEVE_TYPE_NESTABLES:
				if (!this.achieveLevels[id] && this.checkNestableSetCompletion(testValue)) {
					achievements[id].level = this.achieveLevels[id] = 1;
					this.setData(ACHIEVE_DATA_KEY, achievements, true, true);
					this.animateAchievement(achieveConf);
				}
				break;
		}
	};

	this.checkNestableSetCompletion = function(setIDs) {
		var collected = this.getData("nestablesCollected") || {};

		if (!isArray(setIDs)) {
			setIDs = [setIDs];
		}

		var complete = true;
		for (var i = 0; i < setIDs.length; i++) {
			var id = setIDs[i];
			var set = this.nestablesConfig.sets[id];
			var setCollection = collected[set.id];

			var count = 0, ownedCount = 0;
			for (var p in set.pieces) {
				var piece = set.pieces[p];
				var owned = setCollection && setCollection[piece.id];

				count++;
				if (owned) {
					ownedCount++;
				}
			}

			complete = count == ownedCount && complete;
		}

		return complete;
	};

	this.animateAchievement = function(achievo, level) {
		var parent = this.activeView,
			ps = parent.style;

		if (level !== undefined) {
			achievo = achievo.levels[level];
		}

		animate(this.achievementToast)
		.then(bind(this, function() {
			this.achievementToast.style.visible = true;
			this.achievementIcon.style.visible = true;

			parent.addSubview(this.achievementToast);
			this.achievementToast.style.x = (ps.width - TOAST_WIDTH) / 2;
			this.achievementToast.style.y = ps.height;
			this.achievementIcon.setImage(achievo.icon);

			if ( achievo.type === "pass" ) {
				this.achievementToast.setImage(PASS_TOAST_IMG);
				this.passText.style.visible = true;
				this.nameText.style.visible = true;
				this.achievementIcon.style.visible = false;
				this.nameText.setText(achievo.name);
			} else {
				this.achievementToast.setImage(ACHIEVE_TOAST_IMG);
				this.passText.style.visible = false;
				this.nameText.style.visible = false;
				this.achievementIcon.style.visible = true;
			}

			parent.emitAchieveParticles && parent.emitAchieveParticles(TOAST_TIME);
		}))
		.then({ y: ps.height - TOAST_HEIGHT }, TOAST_TIME, animate.easeOut)
		.wait(4 * TOAST_TIME)
		.then({ y: ps.height }, TOAST_TIME, animate.easeIn)
		.then(this.boundHideToast);
	};

	this.initAchievementViews = function() {
		var config = this.config;

		ACHIEVE_TOAST_IMG = GC.app.l.translate('resources/images/Locales/en') + '/Game/modal_achievement_unlocked.png';

		this.achievementToast = new ImageView({
			x: 0,
			y: 0,
			width: TOAST_WIDTH,
			height: TOAST_HEIGHT,
			zIndex: Z_MODAL,
			canHandleEvents: false
		});

		this.achievementIcon = new ImageView({
			parent: this.achievementToast,
			x: 2 * TOAST_SPACING,
			y: TOAST_SPACING,
			width: ACHIEVEMENT_ICON_WIDTH,
			height: ACHIEVEMENT_ICON_HEIGHT,
			zIndex: Z_MODAL,
			canHandleEvents: false
		});

		this.passText = new TextView({
			parent: this.achievementToast,
			x: (TOAST_WIDTH - 250) / 2,
			y: 18,
			width: 250,
			height: 50,
			size: config.fonts.achievementInfo,
			color: config.colors.achievementToast,
			text: "You Passed",
			multiline: false,
			buffer: true
		});

		this.nameText = new TextView({
			parent: this.achievementToast,
			x: (TOAST_WIDTH - 500) / 2,
			y: 40,
			width: 500,
			height: 100,
			size: config.fonts.achievementToast,
			color: config.colors.achievementToast,
			multiline: false,
			buffer: true
		});

		this.boundHideToast = bind(this, function() {
			this.achievementToast.style.visible = false;
			this.achievementIcon.style.visible = false;
			this.achievementToast.removeFromSuperview();
		});
	};

	this.pauseGame = function() {
		var view = this.getGameView();
		if (view && !view.gamePaused && !view.firstPlay) {
			var model = view.model;
			if (model && !model.finished && (!model.gameoverCountdown || model.gameoverCountdown <= 0)) {
				view.pause();
			}
		}
	};

	this.setTickManager = function() {
		var min = Math.min;

		var history = [];
		var historySize = 1;
		var historyIndex = 0;

		this.tickManager = function(dt) {
			if (dt > MAX_TICK) {
				dt = MAX_TICK;
			} else if (dt < MIN_TICK) {
				dt = MIN_TICK;
			}

			historySize = ~~(TICK_MOD / dt);

			if (historyIndex >= historySize) {
				historyIndex = 0;
			}

			history[historyIndex++] = dt;

			var sum = 0;
			var count = min(history.length, historySize);
			for (var i = 0; i < count; i++) {
				sum += history[i];
			}

			return ~~(sum / count) || MIN_TICK;
		};
	};

	this.canPurchaseUpgrade = function() {
		var upgrades = this.upgradesConfig;
		var purchased = this.getData("upgradesPurchased");
		var berries = this.getData("berries");

		for (var i in upgrades) {
			var upgrade = upgrades[i];
			var purchase = purchased[upgrade.id];

			if (upgrade.levels.length == 1) {
				// ignore real money purchases
				if (upgrade.levels[0].money) {
					var nextLevel = upgrade.levels.length;
				} else {
					var nextLevel = purchase ? purchase.level : 0;
				}
			} else {
				var nextLevel = purchase ? purchase.level + 1 : 1;
			}

			var data = false;
			if (nextLevel < upgrade.levels.length) {
				data = upgrade.levels[nextLevel];
			}

			if (data && data.cost <= berries) {
				return true;
			}
		}

		return false;
	};

	this.addAvatar = function(avatar) {
		var avatars = this.getData("avatars");
		if (avatars.indexOf(avatar) < 0) {
			avatars.push(avatar);
			this.setData("avatars", avatars);
		}
	};

	this.addLevel = function(level) {
		var levels = this.getData("levels");
		if (levels.indexOf(level) < 0) {
			levels.push(level);
			this.setData("levels", levels);
		}
	};

	this.fireBackButton = function() {
		if (!this.transitioning) {
			if (this.activeView === this.screenViews["GameView"]) {
				this.pauseGame();
			} else {
				this.showExitGameModal();
			}
		}
		return true;
	};

	this.showExitGameModal = function() {
		if (!this.exitModal && this.activeView && GC.isNative && device.isMobile) {
			this.exitModal = new BinaryModalView({
				parent: this.activeView,
				message: "Are you sure you want to exit?",
				onAccept: bind(this, function() {
					this.exitModal = undefined;
					GLOBAL.NATIVE && NATIVE.sendActivityToBack && NATIVE.sendActivityToBack();
				}),
				onCancel: bind(this, function() {
					this.exitModal = undefined;
				})
			});
		}
	};

	this.setHardwareBackButton = function(fn, isModal) {
		/*if (isModal) { // modals create a stack of back functions
			this.oldBackFuncs.push(this.backFn);
		} else {
			this.oldBackFuncs = [];
		}

		if (fn && typeof fn === 'function') {
			this.backFn = fn;
		} else {
			this.backFn = function() {};
		}*/
	};

	// used by modals to pop their back function stack
	this.updateModalBackButton = function() {
		if (this.oldBackFuncs.length) {
			this.backFn = this.oldBackFuncs.pop();
		} else {
			this.backFn = function() {};
		}
	};

	this.enableBackButton = function(val) {
		this.isBackEnabled = val;
	};

	this.getGameModel = function() {
		return this.screenViews['GameView'].model;
	};

/* ~ ~ ~ DATA FUNCTIONS ~ ~ ~ */

	this.getData = function(key) {
		return this.appModel.getData(key);
	};

	this.setData = function(key, value, skipSave, skipServer) {
		this.appModel.setData(key, value, skipSave, skipServer);
	};

/* ~ ~ ~ SOUND FUNCTIONS ~ ~ ~ */

	this.initMusic = function() {
		this.music = new Sound({
			path: "resources/sounds/music",
			map: {
				forest_music: { background: true },
				night_music: { background: true },
				spooky_music: { background: true },
				pirate_music: { background: true },
				run_win_music: { background: true },
				run_menu_music: { background: true },
				achieves_music: { background: true },
				store_music: { background: true },
				christmas_music: { background: true },
				valentines_music: { background: true }
			}
		});

		this.setMusicVolume(this.soundConfig.globals.master);
	};

	this.initEffects = function() {
		this.effects = new Sound({
			path: "resources/sounds/effects/",
			map: {
				bee_buzz: { path: "Game" },
				combo_lost: { path: "Game" },
				echidna_quills: { path: "Game" },
				cage_break: { path: "Game" },
				dive: { path: "Game" },
				jump: { path: "Game" },
				kiwi_land: { path: "Game" },
				kiwi_roll: { path: "Game" },
				special_get: { path: "Game" },
				flapping: { path: "Game" },
				struggle_flight: { path: "Game" },
				glider_wind1: { path: "Game" },
				glider_wind2: { path: "Game" },
				splash: { path: "Game" },
				cloud_splash: { path: "Game" },
				angel_fish: { path: "Game" },
				fruit_get: {
					path: "Game",
					sources: [
						"fruit_get_a",
						"fruit_get_b",
						"fruit_get_c"
					]
				},
				ui_click: {},
				berry_count_ind: {
					sources: [
						"berry_count_ind_a",
						"berry_count_ind_b",
						"berry_count_ind_c",
						"berry_count_ind_d"
					]
				},
				berry_count_med: {},
				berry_count_large: {}
			}
		});

		this.setEffectVolume(this.soundConfig.globals.master);

		this.effectsLoaded = true;
	};

	this.setMusicVolume = function(value) {
		for (var key in this.music._map) {
			this.music.setVolume(key, this.calcVolume(value, "music", key));
		}
	};

	this.setEffectVolume = function(value) {
		for (var key in this.effects._map) {
			this.effects.setVolume(key, this.calcVolume(value, "effects", key));
		}
	};

	this.calcVolume = function(volume, type, key) {
		var baseVolume;
		if (type === "music") {
			baseVolume = this.soundConfig.volume.music[key];
		} else {
			baseVolume = this.soundConfig.volume.effects[key];
		}

		if (!baseVolume && baseVolume !== 0) {
			baseVolume = MAX_VOLUME;
		}

		return volume * baseVolume / MAX_VOLUME;
	};

	this.pauseSound = function(key) {
		this.effectsLoaded && this.effects.pause(key);
	};

	this.playSound = function(sname) {
		if (this.effectsLoaded && this.getData("sfxEnabled")) {
			this.effects.play(sname);
		}
	};

	this.playSong = function(sname) {
		if (this.getData("musicEnabled")) {
			this.playingSong = sname;
			this.music.playBackgroundMusic(sname);
		}
	};

	this.resumeSong = function() {
		if (this.playingSong) {
			this.playSong(this.playingSong);
		}
	};

	this.pauseSong = function() {
		this.music.pauseBackgroundMusic();
	};

/* ~ ~ ~ UTILITY FUNCTIONS ~ ~ ~ */

	this.readJSON = function(url) {
		try {
			if (typeof window.CACHE[url] === 'string') {
				window.CACHE[url] = JSON.parse(window.CACHE[url]);
			}

			if (window.CACHE[url] === undefined) {
				logger.error('Did you embed the file: ' + url + '? Try restarting Tealeaf.');
				throw new Error('cannot find embedded JSON file');
			}

			return window.CACHE[url];
		} catch (e) {
			logger.error('Invalid JSON file format.');
			throw e;
		}
	};

	this.mergeRecursively = function(base, fresh) {
		for (var f in fresh) {
			var freshProp = fresh[f];
			var baseProp = base[f];

			if (baseProp) {
				if (typeof baseProp === TYPE_OBJECT && typeof freshProp === TYPE_OBJECT) {
					baseProp = this.mergeRecursively(baseProp, freshProp)
				} else {
					baseProp = freshProp;
				}
			} else {
				base[f] = freshProp;
			}
		}

		return base;
	};

	this.getParentScreenView = function(view) {
		while (view && !view.isMainView) {
			view = view.getSuperview();
		}

		return view;
	};

	// only works if screen views are flagged with isMainView = true
	this.getAbsolutePosition = function(view) {
		var pos = { x: 0, y: 0 };

		while (view && !view.isMainView) {
			pos.x += view.style.x;
			pos.y += view.style.y;
			view = view.getSuperview();
		}

		return pos;
	};

	// get the XP required to reach lvl
	this.getLevelXP = function(lvl) {
		if (lvl <= 1) {
			return 0;
		} else {
			var xp = 100;
			while (lvl > 2) {
				xp += 300 + 800 * Math.floor(lvl / 5);
				lvl--;
			}

			return xp;
		}
	};

	// save any rewards for leveling up
	this.rewardLevelUp = function(startLvl, endLvl) {
		var rewards = {
			berries: 0,
			gems: 0,
			prizes: []
		};

		for (var l = startLvl + 1; l <= endLvl; l++) {
			rewards.berries += l * 50;
			rewards.gems += 5;
			for (var b in this.babyConfig) {
				var baby = this.babyConfig[b];
				if (baby.unlock === l) {
					var reward = {
						name: baby.name,
						type: TYPE_BABY_KIWI,
						value: baby.id,
						width: baby.width,
						height: baby.height,
						image: baby.selectionIcon
					};
					rewards.prizes.push(reward);
				}
			}
		}

		return rewards;
	};

	this.spendBerries = function(amount, showStore){
		var cur = this.getData('berries');

		if( amount > cur ){
			if( showStore ){
				this.showBerryStore();
			}
			return false;
		}
		else{
			this.setData('berries', cur - amount);
			return true;
		}
	};

	this.addBerries = function(count) {
		if (count !== 0) {
			var berries = this.getData("berries");
			this.setData("berries", berries + count);
			var storeView = this.getStoreView();
			var activeView = this.getActiveView();
			if (storeView && activeView == storeView) {
				storeView.berryCounter.addBerries(count);
			}
		}
	};

	this.spendGems = function(amount, showStore){
		var cur = this.getData('gems');

		if( amount > cur ){
			if( showStore ){
				this.showGemStore();
			}
			return false;
		}
		else{
			this.setData('gems', cur - amount);
			return true;
		}
	}

	this.addGems = function(count) {
		if (count !== 0) {
			var gems = this.getData("gems");
			this.setData("gems", gems + count);
			this.header.refresh();
		}
	};

	this.removeRefundedGems = function(count) {
		if (count) {
			var gems = this.getData("gems"),
				finalGems = gems;

			if (gems <= count) {
				finalGems = 0;
			} else {
				finalGems -= count;
			}

			this.trackEvent("Refund", {
				currentGems: gems + "",
				lostGems: count + "",
				finalGems: finalGems + ""
			});

			this.setData("gems", finalGems);
			this.header.refresh();
		}
	};

	this.spendHearts = function(amount, showStore){
		var cur = this.getData('hearts');

		if( this.getData('unlimitedHearts') ){
			return true;
		}

		if( amount > cur ){
			if( showStore ){
				this.showNoHeartsModal();
				//this.showHeartStore();
			}
			return false;
		}
		else{
			this.setData('hearts', cur - amount);
			this.header.updateHeartData();
			return true;
		}
	}

	this.addHearts = function(count) {
		if (count !== 0) {
			var hearts = this.getData("hearts");
			this.setData("hearts", hearts + count);
		}
	};

	this.showOfferWall = function(){
		return (GLOBAL.NATIVE && NATIVE.ads && NATIVE.ads.showOfferWall());
	};
});

// singleton behavior
var instance;
exports = {
	get: function() {
		if (!instance) {
			instance = new Controller();
		}
		return instance;
	}
}
