import event.Emitter as Emitter;
import ff;

/**
 * Social controller
 */
exports = Class(Emitter, function(supr) {

	this.init = function(controller) {

		this.appController = controller;
		this.scores = null;
		this.scoresReady = false;
		this.contactsReady = false;
		this.serverUsersLoaded = false;
		this.nameData = this.appController.getData("username") || {
			name: null,
			confirmed: false
		};

		supr(this, 'init');	

		if (this.nameData.name != null && this.nameData.confirmed === false) {
			this.setUsername(this.nameData.name);		
		}
	};

	this.isScoresReady = function() {
		return this.scoresReady;		
	};

	this.isContactsReady = function() {
		return this.contactsReady;	
	};

	this.postScore = function(score, dist) {
		//gcsocial.user.postScore({
		//	score: score,
		//	data: {
		//		distance: dist		
		//	}
		//});		
	};

	/**
	 * Accepts a user object and returns the correct username
	 * @method getUsername
	 */
	this.getUsername = function(user) {
		var 
			service = user.getService("phone"),
			name;

		if (user.getNameSource() === "manual" || service == null) {
			name = user.getName();	
		} else {
			name = service.getName();	
		}

		return name;
	};

	this.setUsername = function(name, cb) {
		var that = this;
	
		// always store name in localStorage so user only has to set it once,
		// even if call fails.
		this.nameData.name = name;
		this.appController.setData("username", this.nameData);		
		logger.log("===Attempt to set username!");
		gcsocial.user.setName(name, function(err) {
			logger.log("===Receive username cb err: " + err);

			if (!err) {
				that.nameData.confirmed = true;		
				that.appController.setData("username", that.nameData);
			}

			if (typeof cb === 'function') {
				cb(err);		
			}
		});
	};

	this.isUsernameSet = function() {
		return this.nameData.name != null;		
	};

	this.getContacts = function(cb) {
		var that = this;
		
		gcsocial.user.getConnections(gcsocial.user.PHONE, function(err, connections) {
			if (err) {
				if ( cb != null ) cb(err);
				return;	
			}

			logger.log("=== RECEIVED CONTACTS: " + JSON.stringify(connections));
			logger.log("received contacts num: " + connections.length);
			
			that.contacts = connections;
			that.contactsReady = true;
			that.publish("contactsReceived");
			
			cb(err, connections);
		});		
	};

	/**
	 * getFriendsScores
	 * Loads scores objects from server. 
	 * @param - optional cb (function(err){}) triggered on finish
	 * @param - optional timeout for callback.
	 */
	this.getFriendsScores = function(cb, timeout) {
		var 
			that = this,
			fired = false,
			finishCB;

		var f = ff(function() {
			if ( !that.serverUsersLoaded ) {
				//gcsocial.user.getConnections(f());
			}
		}, function(connections) {
			that.serverUsersLoaded = true;		
			//gcsocial.user.getScoresForFriends(f());		
		}, function(scores) {

			scores = scores.filterLive(function(score) {
				return score.user._nativeContact == null ? false : true;
			});

			scores.forEach(function(score) {
				// if (gcsocial.isNative) {
				// 	score.user.refresh(f.wait());
				// }
			});

			f()(false, scores);
		}, function(scores) {
			that.scores = scores;
			logger.log("=== RECEIVED SCORES: " + JSON.stringify(scores));

			if ( !that.scoresReady ) {
				that.scoresReady = true;
			}

			that.publish("friendsReceived");
			f(scores);
		}).cb(cb);

		if ( timeout != null ) {
			f.timeout(timeout);	
		}
	};

	/**
	 * getFriendRunList
	 * Returns 5-7 friends that you can run by during the level sorted
	 * by distance.
	 */
	this.getFriendRunList = function() {

		// local configurables.
		var
			numBelow = 2,
			numAbove = 5,
			yourScore,
			ret;

		if ( this.scoresReady !== true ) {
			return [];	
		}

		yourScore = (this.appController.getData("topMeters") || [])[0] || 0;
		
		ret = this.scores.setSorter(function(item) {
			return item.getData().distance;			
		}).reduce(function(values, score) {
			var 
				friendDist = score.getData().distance,
				valueArr,
				maxLength,
				compA,
				compB,
				iDist,
				replaceIdx = -1,
				replaceDist = -1,
				minReplace,
				i;

			// place elements in different value array depending on
			// score
			if ( friendDist <= yourScore ) {
				valueArr = values.lower;	
				maxValue = numBelow;
			} else {
				valueArr = values.higher;	
				maxValue = numAbove;
			}
				
			// place elements in correct value bin if space is left,
			// or iterate to see if can replace any of them.
			if ( valueArr.length < maxValue ) {
				valueArr.push(score);		
			} else {
				for ( i = 0; i < maxValue; i++ ) {
					iDist = valueArr[i].getData().distance;		
					
					if ((friendDist <= yourScore && 
						friendDist > iDist && (iDist < minReplace || replaceDist === -1) )
						||
						(friendDist > yourScore && 
						friendDist < iDist && (iDist > minReplace || replaceDist === -1) )) {
						minReplace = iDist;
						replaceDist = friendDist;	
						replaceIdx = i;
					}
				}

				if ( replaceIdx !== -1 ) {
					valueArr[replaceIdx] = score;		
				}
			}
	
			return values;		
		}, { lower: [], higher: [] });

		return ret.lower.concat(ret.higher).sort(function(a, b) { 
			return a.getData().distance - b.getData().distance; 
		});
	};

});
