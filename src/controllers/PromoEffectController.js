import event.Emitter as Emitter;
import src.lib.Animation as W.Animation;
import src.lib.Utils as W.Utils;
import src.config.promoEffectMap as promoEffectMap;


exports = Class(Emitter, function() {

	this.init = function(controller) {

		this.activeAnimations = [];
		this.anims =[];

		this.controller = controller;
		this.controller.on( 'viewChanged', bind( this, this.updateAnimations) );
	};

	/*
	 *	doUnlock
	 *	- begins a 'NEW' effect chain based on a passed in unlock name,
	 *	final views is an object with the final top-level view and its child buttons to animate
	 */

	this.doUnlock = function( unlockType, finalViews, doUpdate ){
		var i , anim;

		for( i in this.anims ){
			anim = this.anims[i];
			if( anim.name === unlockType ){
				return;
			}
		}

		this.anims.push(
			{
				name: unlockType,
				lastView: finalViews
			}
		);

		if( doUpdate ){
			this.updateAnimations();
		}
	};

	/*
	 *	updateAnimations
	 *	- after a view change, checks if any views within GC.app's top view need to be animated.
	 *	- disables animations on the now unseen view
	 */
	this.updateAnimations = function(){
		var name, i, j, k, anim, views, allViews, v, top, buttons, button;

		if( this.activeAnimations.length > 0){
			this.clearAll();
		}

		top =  this.controller.activeModal || this.controller.activeView;

		for ( i = 0; i < this.anims.length; i++ ){
			anim = this.anims[i];
			views = promoEffectMap[anim.name].views;
			allViews = views;
			if(anim.lastView){
				allViews = views.concat(anim.lastView);
			}

			for( j = 0; j < allViews.length; j++ ){
				v = W.Utils.selectEvalFromApp(allViews[j].view);

				if( v && v === top ){
					if( v.newEffectCat == null || (v.newEffectCat === anim.name) ){
						buttons = allViews[j].buttons;
						for( k in buttons ){
							button = W.Utils.selectEvalFromApp(buttons[k]);
							this.animateView( button );
						}

						if( j === allViews.length - 1 ){
							this.anims.splice(i, 1);
						}
					}
					break;
				}
			}
		}
	};

	this.animateView = function( view ){
		W.Animation.addVisualPulse(view, {r:200, g:220, b: 50, a: 0}, 0.25, 2000);
		this.activeAnimations.push(view);
	};

	/*
	 *	clearAll
	 * 	- removes all view animations from the active views array
	 */

	this.clearAll = function( ){
		var anim, i;

		while(this.activeAnimations.length > 0){
			anim = this.activeAnimations[0];
			W.Animation.removeVisualPulse(anim);
			this.activeAnimations.splice(0, 1);
		};
	};

	this.newMap = function(mapID){
		this.doUnlock('newMap', {
									view: 'controller.screenViews.MapView',
									buttons: [
										'controller.screenViews.MapView.levelViews.' + mapID + '.icon'
									]
								}, true);
	};

	this.newBabyKiwis = function(kiwiIDS){
		if( !kiwiIDS ){ return; };

		var buttons = [],
			order = this.controller.babyOrder;


		for( var i = 0; i < kiwiIDS.length; i ++ ){
			buttons[i] = 'controller.babyKiwiModal.slots.' + order.indexOf(kiwiIDS[i]);
		}

		this.doUnlock('newBabyKiwi', {
									view: 'controller.babyKiwiModal',
									buttons: buttons
								}, true);
	};
});