import animate;
import event.Emitter as Emitter;

import src.controllers.Controller as Controller;
import src.lib.Utils as Utils;
import src.lib.LocaleManager as LocaleManager;

exports = Class(Emitter, function(supr) {

	var random = Math.random;

	var controller = Controller.get(),
		config = controller.config,
		babyConfig = controller.babyConfig,

		// CLASS CONSTANTS
		MAX_TICK = config.game.model.maxTick,
		BG_WIDTH = config.ui.maxWidth,
		BG_HEIGHT = config.ui.maxHeight,
		COIN_HOVER_MIN = -10,
		COIN_HOVER_MAX = 10,
		COIN_HOVER_RATE = 0.5,
		JUMP_COUNT = config.game.player.jumps.length,
		SPEED_MIN = config.game.model.speed.min,
		SPEED_MAX = config.game.model.speed.max,
		ACCELERATION_PERCENT = config.game.model.speed.accelerationPercent,
		BEAK_REACH = config.game.player.beakReach,
		GRAVITY = config.game.model.gravity,
		FLOATING_VELOCITY = config.game.player.floatingVelocity,
		FLOATING_BUFF_RATE = 0.9 * FLOATING_VELOCITY / SPEED_MAX, // asymptotically approach 90% of flying straight!
		FLOATING_END_Y = config.game.player.floatingEndY,
		DIVING_VELOCITY = config.game.player.divingVelocity,
		CAMERA_OFFSET_MAX = config.game.camera.maxY,
		PLATFORM_BUFFER_SIZE = config.game.model.platformBufferSize,
		SPEED_TIER_1 = config.game.speedTiers[0],
		SPEED_TIER_2 = config.game.speedTiers[1],
		SPEED_TIER_3 = config.game.speedTiers[2],
		PIXELS_TO_METERS = config.game.model.pixelsToMeters,
		COLLECTION_TIME = config.game.coins.collectionTime,
		TURTLE_OFFSET = config.game.player.powers.turtle.offset,
		TURTLE_BOUNCE_VELOCITY = config.game.player.powers.turtle.velocity,
		MAX_GLIDER_Y = config.game.player.maxGliderY,
		TOP_SCORE_COUNT = config.game.scores.topCount,
		DEFAULT_AVATAR = config.game.player.defaultAvatar,
		MARKER_SPACING = config.game.markers.spacing,
		ANGEL_VELOCITY = config.game.player.powers.angelFish.velocity,
		GAMEOVER_COUNTDOWN = 3250, // slightly over angel save cooldown
		TURTLE_DIVE_COOLDOWN = 250,
		PLAYER_HISTORY_SIZE = 120,
		BABY_OFFSET_X = 110,
		BABY_GAP = -10,
		BABY_OFFSET_SIZE = 6,
		BABY_INDEX_SPACING = 18,
		BABY_RADIUS = 10,
		BABY_VY_MAX = 1200,

		ECHIDNA_SPAWN_CHANCE = 0.25,
		BEE_SPAWN_CHANCE = 0.175,
		POO_SPAWN_CHANCE = 0.25,

		BABY_MARGIN = 6,
		MAX_BABIES = 3,
		MIN_ECHIDNA_METERS = 25,
		MIN_BEE_METERS = 100,
		MIN_POO_METERS = 50,

		ECHIDNA_WIDTH = 150,
		ECHIDNA_HEIGHT = 150,

		POO_WIDTH = 95,
		POO_HEIGHT = 112,
		POO_HITBOX_Y = 30,
		POO_HITBOX_H = 80,

		ECH_HIT_BOX_X = 40,
		ECH_HIT_BOX_Y = 100,
		ECH_HIT_BOX_W = 70,
		ECH_HIT_BOX_H = 50,
		BEE_WIDTH = 64,
		BEE_HEIGHT = 64,
		TURTLE_SPAWN_CHANCE = 0.05,
		MAX_TURTLE_FRIENDS = 3,
		ANGEL_SAVE_COST = 3;

	var BERRY_VAL = 100,
		BERRY_EXP = 1.03,
		BERRY_SCALE_INC = 0.015,
		BERRY_CHAIN_MAX = 235;

	// width and y values for platform tutorial script
	// w { 960 } 125 { 960 } 900 { 1280 }
	// y { 545 }     { 270 }     { 545 }

	var PAUSE_JUMP = 860,
		PAUSE_FLOAT = 1945,
		PAUSE_DIVE = 3000,
		PAUSE_TUTORIAL_END = 3800;

	// achievements
	var BERRY_ALLERGY_METERS = 200,
		SURE_FOOTED_LANDINGS = 50,
		HIGH_AND_DRY_FLOATINGS = 3,
		BERRY_STREAK = 100;

	// power-ups and berry spawns
	var ITEM_TYPE_BASIC = 1,
		ITEM_TYPE_BLUE = 2,
		ITEM_TYPE_MAGNET = 3,
		ITEM_TYPE_TURTLE_BAIT = 4,
		ITEM_TYPE_NESTABLE = 5,
		ITEM_TYPE_GLIDER = 6,
		ITEM_TYPE_SHIELD = 7;

	// STRINGS
	var BERRY_ID = "berry",
		MEGA_BERRY_ID = "megaBerry",
		BERRY_MAGNET_ID = "berryMagnet",
		TURTLE_FRIEND_ID = "turtleFriend",
		SHIELD_ID = "shield",
		TURTLE_BEST_FRIEND_ID = "turtleBestFriend",
		KIWI_GLIDER_ID = "kiwiGlider",
		MEGA_GLIDER_ID = "megaGlider",
		SUPER_GLIDER_ID = "superGlider",
		ANGEL_FISH_ID = "angelFish",
		CALLOUT_NEW_HISCORE = "NEW HISCORE!",
		CALLOUT_BEST_RUN = "BEST RUN!",
		TYPE_CONSUMABLE = "CONSUMABLE",
		TYPE_UPGRADE = "UPGRADE",
		ACHIEVE_ID_BERRY_COUNT = "berryCount",
		ACHIEVE_ID_SCORE_COUNT = "scoreCount",
		ACHIEVE_ID_DISTANCE_COUNT = "distanceCount",
		ACHIEVE_ID_FAIL_BOUNCE = "failBounce",
		ACHIEVE_ID_KIWI_GLIDER = "kiwiGlider",
		ACHIEVE_ID_BERRY_ALLERGY = "berryAllergy",
		ACHIEVE_ID_SURE_FOOTED = "sureFooted",
		ACHIEVE_ID_BERRY_STREAK = "berryStreak",
		ACHIEVE_ID_FLOAT_STREAK = "floatStreak",
		ACHIEVE_ID_ANGEL_FISH = "angelFish",
		ACHIEVE_ID_MEGA_GLIDER = "megaGlider",
		ACHIEVE_ID_SUPER_GLIDER = "superGlider";

	var GLIDER_BERRY_CHANCE = 0.75;
		GLIDER_BERRY_SPACING = 160;
		GLIDER_BERRY_AMPLITUDE = 90;
		GLIDER_BERRY_AMPLITUDE_VARIANCE = 5;
		GLIDER_BERRY_AMPLITUDE_MAX_VARIANCE = 50;
		GLIDER_BERRY_FREQUENCY = 450;
		GLIDER_BERRY_FREQUENCY_VARIANCE = 5;
		GLIDER_BERRY_FREQUENCY_MAX_VARIANCE = 50;
		MIN_GLIDER_BERRY_START_Y = 0;
		MAX_GLIDER_BERRY_START_Y = 40;
		GLIDER_BERRY_SPAWN_STOP = 1500;

	var NESTABLE_DENSITY = 25,
		lastNestablePlatform = 0;

	// class counters
	var platformID = 0,
		coinID = 0;

	// mechanic variables
	var megaBerryValue,
		megaBerrySpawn,
		berryMagnetDuration,
		berryMagnetMultiplier,
		berryMagnetSpawn,
		berryMagnetBonus,
		kiwiGliderDuration,
		kiwiGliderSpawn,
		kiwiGliderSpeed,
		shieldSpawn,
		shieldDuration,
		runningSpawns,
		gliderSpawns,
		spacingMin,
		spacingMax,
		platformStartHeight,
		platformShortening,
		playerX,
		playerWidth,
		playerHeight,
		gliderBerryChance,
		gliderBerryFrequency,
		gliderBerryFrequencyVel,
		gliderBerryFrequencyAcc,
		gliderBerryAmplitude,
		gliderBerryAmplitudeVel,
		gliderBerryAmplitudeAcc,
		gliderBerrySpacing,
		gliderBerryDelay,
		gliderBerryStartX,
		gliderBerryStartY,
		gliderBerryLastX;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		playerX = config.game.player.x;

		this.controller = controller;
		this.firstPlay = controller.getData("firstPlay");

		if (this.firstPlay) {
			controller.trackEvent("TutorialStart", {});
			this.teachJump = true;
			this.teachFloat = true;
			this.teachDive = true;
		}

		this.babyBoosts = {
			"itemSpawnBonus": 0,
			"berrySpawnBonus": 0,
			"berryDive": 0,
			"magnetReach": 0,
			"magnetDuration": 0,
			"scoreBonus": 0,
			"jumpVelocity": 0,
			"gliderDuration": 0,
			"shieldDuration": 0,
			"freeTurtle": 0,
			"freeGlider": 0
		};

		this.loadPowerupData();

		// increment games played and track level start
		var gamesPlayed = controller.getData("gamesPlayed") || 0;
		gamesPlayed++;
		controller.setData("gamesPlayed", gamesPlayed, true, true);

		controller.trackEvent("GameStart", { gamesPlayed: gamesPlayed + "" });

		this.selectedLevel = controller.getData("selectedLevel");
		this.levelConfig = controller.levelConfig[this.selectedLevel];
		spacingMin = this.levelConfig.platforms.spacing.min;
		spacingMax = this.levelConfig.platforms.spacing.max;
		platformStartHeight = this.levelConfig.platforms.startHeight;
		platformShortening = this.levelConfig.platforms.shortening;

		this.platformChoices = this.levelConfig.platforms.choices;
		this.platformJoints = this.levelConfig.platforms.joints;
		this.spacing = this.levelConfig.platforms.spacing;

		this.nestablesConfig = controller.nestablesConfig;
		this.chooseNestableSet();

		this.coinConfig = config.game.coins;
		this.activeCoins = [];
		this.freeCoins = [];
		this.coinsCollected = 0;
		this.coinsMissed = 0;

		// model = producer, view = consumer
		this.queueCollections = [];
		this.queueCallouts = [];
		this.queueFriendCallouts = [];
		this.queueMarkers = [];
		this.queueItemPoolReleases = [];
		this.queueEffectPoolReleases = [];

		this.uniqueSpawns = {};

		this.finished = false;
		this.firstPlatform = true;
		this.bestRunReached = false;
		this.highScoreReached = false;
		this.dtLast = 0;
		this.elapsed = 0;
		this.score = 0;
		this.distance = 0;
		this.bestRun = controller.getData("bestRun") || 0;
		this.highScore = (controller.getData("topScores") || [0])[0];
		this.friendRuns = null;
		this.markerCount = 1;
		this.markerLastX = 0;
		this.friendMarkerLastX = 0;
		this.friendSpawnIdx = 0;

		this.lastNestablePlatform = 0;

		// special powers
		this.berryMagnetTimer = 0;
		this.kiwiGliderTimer = 0;
		this.kiwiGliderStartTime = 0;

		gliderBerryChance = GLIDER_BERRY_CHANCE;
		gliderBerrySpacing = GLIDER_BERRY_SPACING;
		gliderBerryAmplitude = GLIDER_BERRY_AMPLITUDE;
		gliderBerryFrequency = GLIDER_BERRY_FREQUENCY;
		gliderBerryStartX = 0;
		gliderBerryStartY = 0;
		gliderBerryLastX = 0;
		gliderBerryFrequencyVel = 0;
		gliderBerryFrequencyAcc = 0;
		gliderBerryAmplitudeVel = 0;
		gliderBerryAmplitudeAcc = 0;

		this.turtleDiveCooldown = 0;
		this.shieldTimer = 0;

		// the avatar currently selected and player model init
		var avatarConfig = controller.avatarConfig;
		var selectedAvatar = controller.getData("selectedAvatar") || DEFAULT_AVATAR;
		var selectedAvatarConfig = avatarConfig[selectedAvatar];
		playerWidth = selectedAvatarConfig.width;
		playerHeight = selectedAvatarConfig.height;
		this.playerHitbox = selectedAvatarConfig.hitbox;
		this.playerJumps = config.game.player.jumps;

		// these should execute in this order
		this.initPlayer();
		this.initBabies();

		var babyItemBoost = 1 + this.babyBoosts["itemSpawnBonus"],
			babyBerryBoost = 1 + this.babyBoosts["berrySpawnBonus"];

		runningSpawns = {
			'spawnBerries': this.coinConfig.chance,
			'spawnMegaBerry': megaBerrySpawn * babyBerryBoost,
			'spawnBerryMagnet': berryMagnetSpawn * babyItemBoost,
			'spawnTurtleFriend': TURTLE_SPAWN_CHANCE * babyItemBoost,
			'spawnShield': shieldSpawn * babyItemBoost,
			'spawnKiwiGlider': kiwiGliderSpawn * babyItemBoost,
			'spawnEchidna': ECHIDNA_SPAWN_CHANCE,
			'spawnBee': BEE_SPAWN_CHANCE,
			'spawnPoo': POO_SPAWN_CHANCE
		};

		gliderSpawns = {
			'spawnBerryMagnet': berryMagnetSpawn * babyItemBoost,
			'spawnTurtleFriend': TURTLE_SPAWN_CHANCE * babyItemBoost,
			'spawnEchidna': ECHIDNA_SPAWN_CHANCE,
			'spawnBee': BEE_SPAWN_CHANCE,
			'spawnPoo': POO_SPAWN_CHANCE
		};

		if( ! (LocaleManager.getLocale() === 'ko-kr') ){
			delete runningSpawns.spawnPoo;
			delete gliderSpawns.spawnPoo;
		}

		this.prepConsumables();
		this.initPlatforms();

		// update player y now that platforms are ready
		this.player.x = playerX;
		this.player.y = this.currentPlatform.y - playerHeight;
		this.player.gliderX = selectedAvatarConfig.gliderX;
		this.player.gliderY = selectedAvatarConfig.gliderY;
		this.player.selectedAvatar = selectedAvatar;
		merge(this.player, selectedAvatarConfig);

		// update babies' y
		for (var i = 0; i < this.babies.length; i++) {
			this.babies[i].y = this.currentPlatform.y - this.babies[i].height;
		}

		this.playerHistoryTimestamp = 0;
		this.playerHistoryIndex = 0;
		this.playerHistory = [];
		for (var i = 0; i < PLAYER_HISTORY_SIZE; i++) {
			this.playerHistory[i] = {
				feetY: this.currentPlatform.y
			}
		}

		this.berryStreak = 0;
		this.landingStreak = 0;
		this.floatingStreak = 0;
		this.lastLandingY = 0;
		this.lastPlatformY = 0;
		this.lastFingerY = 0;

		// angel fish save
		this.beforeAngelVx = 0;
		this.angelModalActive = false;
		this.divingStreak = 0;
		this.lastDiveY = 0;
		this.lastFloatY = 0;

		// set up screen X, varies as babies are picked up
		this.player.screenX = playerX;

		this.POO_STATE_ACTIVE = 0;
		this.POO_STATE_PASSED = 1;
		this.POO_STATE_DEAD = 2;
		this.POO_STATE_EMPTY = 3;
		this.POO_STATE_NEW = 4;
		this.MAX_POOS = 4;
	};
/*
	this.setFriendRuns = function() {
		this.friendRuns = controller.socialController.getFriendRunList();
		logger.log("=== CHECKING FRIEND RUNS LIST: " + JSON.stringify(this.friendRuns));
	};

	this.isFriendsActive = function() {
		return this.friendRuns && this.friendRuns.length > 0;
	};

	this.postAsyncCheckMarkerSpawn = function() {
		for (var i = 0, len = this.platforms.length; i < len; i++) {
			this.checkMarkerSpawn(this.platforms[i]);
		}
	};
*/
	this.loadPowerupData = function() {
		this.powerUps = {};
		this.powerUps[MEGA_BERRY_ID] = {};
		this.powerUps[BERRY_MAGNET_ID] = {};
		this.powerUps[KIWI_GLIDER_ID] = {};
		this.powerUps[SHIELD_ID] = {};

		var purchased = controller.getData("upgradesPurchased");
		var upgrades = controller.upgradesConfig;
		for (var p in purchased) {
			var purchase = purchased[p];
			var power = upgrades[p];
			if (power) {
				var spawnID = power.spawnID;
				var levels = power.levels;
				if (power.type === TYPE_CONSUMABLE) {
					if (purchase.level) {
						this.powerUps[p] = levels[0];
					}
				} else if (power.type === TYPE_UPGRADE) {
					var data = levels[purchase.level];
					for (var d in data) {
						if (d !== "text" && d !== "cost") {
							this.powerUps[spawnID][d] = data[d];
						}
					}
				}
			}
		}

		megaBerryValue = this.powerUps[MEGA_BERRY_ID].value;
		megaBerrySpawn = this.powerUps[MEGA_BERRY_ID].spawn;

		berryMagnetDuration = this.powerUps[BERRY_MAGNET_ID].duration;
		berryMagnetMultiplier = this.powerUps[BERRY_MAGNET_ID].multiplier;
		berryMagnetSpawn = this.powerUps[BERRY_MAGNET_ID].spawn;
		berryMagnetBonus = this.powerUps[BERRY_MAGNET_ID].bonus;

		shieldSpawn = this.powerUps[SHIELD_ID].spawn;
		shieldDuration = this.powerUps[SHIELD_ID].duration;

		this.turtlesAvailable = purchased[TURTLE_FRIEND_ID].level;
		this.bestTurtlesAvailable = purchased[TURTLE_BEST_FRIEND_ID].level;
		this.turtleCount = 0;

		kiwiGliderDuration = this.powerUps[KIWI_GLIDER_ID].duration;
		kiwiGliderSpawn = this.powerUps[KIWI_GLIDER_ID].spawn;
		kiwiGliderSpeed = this.powerUps[KIWI_GLIDER_ID].speed;
	};

	this.prepConsumables = function() {
		var superGlider = this.powerUps[SUPER_GLIDER_ID],
			megaGlider = this.powerUps[MEGA_GLIDER_ID],
			angelFish = this.powerUps[ANGEL_FISH_ID];

		if (this.turtlesAvailable || this.bestTurtlesAvailable || this.babyBoosts['freeTurtle']) {

			if( !this.bestTurtlesAvailable && this.babyBoosts['freeTurtle'] ){
				this.turtlesAvailable ++;
			}

			this.activateTurtleBait();
		}

		if (superGlider) {
			this.superGlider = superGlider;
			this.useConsumable(SUPER_GLIDER_ID);

			this.gliderSpeed = superGlider.speed;
			this.kiwiGliderTimer = superGlider.duration * (1 + this.babyBoosts['gliderDuration']);
			this.setFingerDown(true);

			this.vxOriginal = this.player.vx;
			this.player.vx += superGlider.speed;
			this.player.vy = 0;
			this.initGliderBerries();
		} else if (megaGlider ) {
			this.megaGlider = megaGlider;

			this.useConsumable(MEGA_GLIDER_ID);

			var megaGliderUses = controller.getData("megaGliderUses");
			controller.setData("megaGliderUses", megaGliderUses + 1, true, true);

			this.gliderSpeed = megaGlider.speed;
			this.kiwiGliderTimer = megaGlider.duration * (1 + this.babyBoosts['gliderDuration']);
			this.setFingerDown(true);

			this.vxOriginal = this.player.vx;
			this.player.vx += megaGlider.speed;
			this.player.vy = 0;
			this.initGliderBerries();
		}

		if (angelFish) {
			this.angelFish = angelFish;
			this.useConsumable(ANGEL_FISH_ID);
		}
	};

	this.useConsumable = function(id) {
		var purchased = controller.getData("upgradesPurchased");
		var purchase = purchased[id];
		purchase.level -= 1;
		controller.setData("upgradesPurchased", purchased, true, true);
	};

	// choose a nestable set to spawn this play session
	// and only choose sets that are incomplete
	this.chooseNestableSet = function() {
		this.nestableSet = controller.nestableSet;
		this.collectedSet = controller.collectedSet;
	};

	this.initPlatforms = function() {
		// initialize platform buffer
		this.platformX = playerX - (BABY_GAP + BABY_OFFSET_X) * this.babies.length;
		this.platforms = [];
		for (var i = 0; i < PLATFORM_BUFFER_SIZE; i++) {
			this.generatePlatform();
		}

		this.platformIndex = 0;
		this.currentPlatform = this.platforms[this.platformIndex];
	};

	this.initPlayer = function() {
		this.player = {
			x: playerX,
			y: 9999, // placeholder
			w: playerWidth,
			h: playerHeight,
			vx: SPEED_MIN,
			vy: 0,
			jumping: false,
			diving: false,
			didDive: false,
			invincible: false
		};
	};

	this.initBabies = function() {
		this.babies = [];

		var selectedBabies = controller.getData("selectedBabies");
		for (var b in selectedBabies) {
			var babyID = selectedBabies[b];
			if (babyID) {
				this.addBaby(babyID);
			}
		}

		if( this.babyBoosts['freeGlider']  && !this.powerUps[SUPER_GLIDER_ID]){
			var purchased = controller.getData("upgradesPurchased");
			var purchase = purchased[MEGA_GLIDER_ID];
			purchase.level += 1;
			controller.setData("upgradesPurchased", purchased, true, true);

			if( !this.powerUps[MEGA_GLIDER_ID] ){
				var upgrades = controller.upgradesConfig,
				power = upgrades[MEGA_GLIDER_ID],
				levels = power.levels;

				this.powerUps[MEGA_GLIDER_ID] = levels[0];
			}
		}

		shieldDuration = shieldDuration * (1 + this.babyBoosts['shieldDuration']);
		kiwiGliderDuration = kiwiGliderDuration * (1 + this.babyBoosts['gliderDuration']);
	};

	this.addBaby = function(babyID, spawn) {
		var babies = this.babies;

		for (var i = 0; i < this.babies.length; i++) {
			this.babies[i].x += this.babies[i].width + BABY_GAP;
			this.babies[i].origX += this.babies[i].width + BABY_GAP;
		}

		var babyData = babyConfig[babyID];
		var boost = babyData.boost;
		this.babyBoosts[boost.id] += boost.value;

		var newBabyData = {
			id: babyID,
			x: config.game.player.x - (babyData.width - BABY_MARGIN),
			origX: config.game.player.x - (babyData.width - BABY_MARGIN),
			y: spawn ? spawn.y : 9999, // placeholder
			vy: 0,
			indexOffset: BABY_INDEX_SPACING * (babies.length + 1),
			organicOffset: ~~(random() * (5 * BABY_OFFSET_SIZE / 4 + 1) - BABY_OFFSET_SIZE),
			horzOffset: 0,
			boost: boost,
			babyIndex: 0,
			dive: boost.id === "berryDive" ? boost.value : 0
		};
		newBabyData = merge(newBabyData, babyData);
		this.babies.push(newBabyData);

		if (this.babies.length > 1) {
			playerX += babyData.width + BABY_GAP;
			this.player.screenX = playerX;
		}
	};

	this.removeBaby = function() {
		var babies = this.babies;
		var baby = babies.shift();
		var boost = baby.boost;
		this.babyBoosts[boost.id] -= boost.value;
		if (babies.length > 0) {
			playerX -= baby.width + BABY_GAP;
			this.player.screenX = playerX;
		}
		this.queueBabyRemoval = true;
	};

	// platforms must be sorted by width and be multiples of the smallest platform
	var platformCount = 0;
	var platformArray = [];
	var recycledPlatforms = [];
	this.generatePlatform = function(recyclable) {
		// if the platform wasn't the starting "main" platform we don't need to generate another
		// otherwise expanding platforms would lead to exponential platform generation
		if (recyclable && !recyclable.generator) {
			recycledPlatforms.push(recyclable);
			return;
		}

		var isDingy = false;
		var shorten = ~~(random() * platformShortening);
		var vx = SPEED_MIN;
		if (this.player) {
			vx = this.player.vx;
			if (this.kiwiGliderTimer > 0) {
				if (this.superGlider) {
					vx -= this.superGlider.speed;
				} else if (this.megaGlider) {
					vx -= this.megaGlider.speed;
				} else {
					vx -= kiwiGliderSpeed;
				}
			}
		}

		var minWidth, maxWidth, setY, setSpacing, firstChoice;
		if (this.firstPlay) {
			// width and y values for platform tutorial script
			// w { 960 } 125 { 960 } 900 { 1280 } ...
			// y { 545 }     { 270 }     { 545 } ...
			if (platformCount === 0) {
				minWidth = 3;
				maxWidth = 3;
				setY = 545;
				setSpacing = 125;
				firstChoice = 1;
			} else if (platformCount === 1) {
				minWidth = 3;
				maxWidth = 3;
				setY = 270;
				setSpacing = 900;
				firstChoice = 2;
			} else if (platformCount === 2) {
				minWidth = 4;
				maxWidth = 4;
				setY = 545;
				firstChoice = 3;
			} else if (vx < SPEED_TIER_1) {
				minWidth = 1;
				maxWidth = 4;
			} else if (vx < SPEED_TIER_2) {
				minWidth = 2;
				maxWidth = 6;
			} else if (vx < SPEED_TIER_3) {
				minWidth = 3;
				maxWidth = 8;
			} else {
				minWidth = 4;
				maxWidth = 10;
			}
		} else {
			if (this.firstPlatform) { // first platform is big
				minWidth = 3;
				maxWidth = 4;
			} else if (!this.player) { // first 5 platforms can't be the smallest
				minWidth = 2;
				maxWidth = 4;
			} else if (vx < SPEED_TIER_1) {
				minWidth = 1;
				maxWidth = 4;
			} else if (vx < SPEED_TIER_2) {
				minWidth = 2;
				maxWidth = 6;
			} else if (vx < SPEED_TIER_3) {
				minWidth = 3;
				maxWidth = 8;
			} else {
				minWidth = 4;
				maxWidth = 10;
			}
		}

		platformArray.length = 0;

		// pirate level-specific logic -- TODO: generalize
		if (this.selectedLevel == "pirate") {
			if (minWidth == 1 && random() < 0.3) {
				isDingy = true;
				platformArray.push(this.levelConfig.platforms.dingy);
			} else {
				var subMax = maxWidth - 1, subMin = minWidth - 1;
				platformArray.push(this.levelConfig.platforms.back);
				while (platformArray.length < subMin || (platformArray.length < subMax && random() < 0.5)) {
					platformArray.push(this.levelConfig.platforms.mid);
				}
				platformArray.push(this.levelConfig.platforms.front);
			}
		} else { // default level logic
			var choices = this.platformChoices;
			var choiceCount = choices.length;

			var base = minWidth;
			var range = maxWidth - minWidth;
			var choice = base + ~~(random() * range + 0.5);

			while (choice > 0) {
				var piece;
				if (firstChoice) {
					piece = firstChoice;
					firstChoice = false;
				} else {
					piece = ~~((choice % (choiceCount + 1)) * random());
				}

				choice -= piece + 1;
				platformArray.push(this.platformChoices[piece]);
			}
		}

		var platform = platformArray[0];

		if (recyclable) {
			recyclable.x = this.platformX;
			recyclable.y = setY || (BG_HEIGHT - platformStartHeight + shorten + CAMERA_OFFSET_MAX);
			recyclable.w = platform.w;
			recyclable.h = platform.h;
			recyclable.endX = this.platformX + platform.w;
			recyclable.image = platform.image;
			recyclable.id = platformID++;
			recyclable.friendRun = false;
			recyclable.generator = true;
			recyclable.dingy = isDingy;
			recyclable.railing = (!isDingy && this.selectedLevel == "pirate") ? this.levelConfig.extras.railBack : undefined;
			recyclable.mast = false;

			this.checkMarkerSpawn(recyclable);
			this.platforms.push(recyclable);
			this.generatePlatformEventsNew(recyclable);
			//this.generatePlatformEvents(recyclable);
		} else {
			var newPlatform = {
				x: this.platformX,
				y: setY || (BG_HEIGHT - platformStartHeight + shorten + CAMERA_OFFSET_MAX),
				w: platform.w,
				h: platform.h,
				endX: this.platformX + platform.w,
				image: platform.image,
				id: platformID++,
				generator: true, // used to determine when to generate new platforms or not
				dingy: isDingy,
				railing: (!isDingy && this.selectedLevel == "pirate") ? this.levelConfig.extras.railBack : undefined,
				mast: false
			};

			this.checkMarkerSpawn(newPlatform);
			this.platforms.push(newPlatform);

			if (!this.firstPlatform) {
				this.generatePlatformEventsNew(newPlatform);
				//this.generatePlatformEvents(newPlatform);
			}
		}

		this.platformX += platform.w;

		for (var i = 1; i < platformArray.length; i++) {
			var extensionPlatform = platformArray[i];

			if (recycledPlatforms.length) {
				var extension = recycledPlatforms.pop();
			} else {
				var extension = {};
			}

			extension.x = this.platformX;
			extension.y = setY || (BG_HEIGHT - platformStartHeight + shorten + CAMERA_OFFSET_MAX);
			extension.w = extensionPlatform.w;
			extension.h = extensionPlatform.h;
			extension.endX = extension.x + extension.w;
			extension.image = extensionPlatform.image;
			extension.id = platformID++;
			extension.friendRun = false;
			extension.generator = false; // removing this will cause an exponential platform explosion!
			extension.joint = this.selectedLevel == "pirate" ? undefined : Utils.choose(this.platformJoints);
			extension.dingy = false;
			extension.railing = (this.selectedLevel == "pirate" && extension.image == this.levelConfig.platforms.front.image)
				? this.levelConfig.extras.railFront : undefined;
			extension.mast = ! (((platformArray.length - 1) - i) % 2);

			this.checkMarkerSpawn(extension);
			this.platforms.push(extension);

			this.platformX += extension.w;

			if ( !this.firstPlatform || i > 2 ) {
				this.generatePlatformEventsNew(extension);
			}
		}

		var spacing = ~~(random() * (spacingMax - spacingMin) + spacingMin);
		var multiplier = this.player ? vx / SPEED_MIN : 1;
		this.platformX += setSpacing || (spacing * multiplier);

		this.firstPlatform = false;
		platformCount++;
	};

	this.generatePlatformEventsNew = function(platform) {
		var roll,
			rollWinners = [],
			spawns,
			spawnKeys,
			spawnChance,
			i;

			var dist = this.getDisplayDistance();

			// tutorial scripting
			if (this.firstPlay) {
				if (platformCount === 0) {
					this.spawnBerries(platform);
					return;
				} else if (platformCount === 1) {
					this.spawnBerries(platform);
					return;
				} else if (platformCount === 2) {
					this.spawnEchidna(platform);
					this.forceBeeSpawn = true;
					this.spawnBee(platform);
					return;
				}
			}

			if( !this.gliderSpawned && !(this.kiwiGliderTimer > 0) ){
				spawns = runningSpawns;
			}
			else{
				spawns = gliderSpawns;
			}

			spawnKeys = Object.keys(spawns);

			for( i = 0; i < spawnKeys.length; i++ ) {
				roll = random();
				spawnChance = spawns[spawnKeys[i]];

				if( roll < spawnChance )
				{
					rollWinners.push(spawnKeys[i]);

					if( i === 'spawnBerries' ){
						this.spawnBerries(platform);
						return;
					}
				}
			}

			if( rollWinners.length ){
				var winner = rollWinners[~~(random() * rollWinners.length)];

				switch( winner ){
					case 'spawnBerries':
						this.spawnBerries(platform);
						break;
					case 'spawnMegaBerry':
						this.spawnMegaBerry(platform);
						break;
					case 'spawnBerryMagnet':
						this.spawnBerryMagnet(platform);
						break;
					case 'spawnTurtleFriend':
						this.spawnTurtleFriend(platform);
						break;
					case 'spawnShield':
						this.spawnShield(platform);
						break;
					case 'spawnKiwiGlider':
						this.spawnKiwiGlider(platform);
						break;
					case 'spawnEchidna':
						this.getDisplayDistance() > MIN_ECHIDNA_METERS && this.spawnEchidna(platform);
						break;
					case 'spawnBee':
						this.getDisplayDistance() > MIN_BEE_METERS && this.spawnBee(platform);
						break;
					case 'spawnPoo':
						this.getDisplayDistance() > MIN_POO_METERS && this.spawnPoo(platform);
						break;
				}

			}
			else{
				this.generateNestable(platform);
			}
	}

	this.checkMarkerSpawn = function(platform) {
		var friendRuns = this.friendRuns;

		// first check for distance markers, then check for best run
		if (platform.endX - this.markerLastX > MARKER_SPACING * PIXELS_TO_METERS) {
			this.markerLastX = this.markerLastX + MARKER_SPACING * PIXELS_TO_METERS;

			platform.bestRun = false;
			platform.markerNumber = this.markerCount++;
			this.queueMarkers.push(platform); // platform object is already managed
		} else if (!this.bestRunMarkerPlaced && this.bestRun && platform.endX / PIXELS_TO_METERS >= this.bestRun) {
			this.bestRunMarkerPlaced = true;
			platform.bestRun = true;
			this.queueMarkers.push(platform); // platform object is already managed
		} else if (friendRuns != null && this.friendSpawnIdx < friendRuns.length &&
				platform.endX > this.friendMarkerLastX + BG_WIDTH &&
				platform.endX / PIXELS_TO_METERS >= friendRuns[this.friendSpawnIdx].getData().distance) {
			platform.friendRun = true;
			platform.friendName = controller.socialController.getUsername(friendRuns[this.friendSpawnIdx].getUser());

			this.friendMarkerLastX = platform.endX;
			this.queueMarkers.push(platform);
			this.friendSpawnIdx++;
		}
	};

	this.spawnItems = function(choice, formation, platform) {
		var pattern = formation.pattern,
			coinSpacing = formation.spacing,
			widthPerCoin = formation.widthPerCoin,
			count = this.forceBerryCount,
			x, maxCoins;

		if (widthPerCoin === 0) {
			count = count || (formation.base + ~~((formation.range + 1) * random()));
			x = formation.offsetX || platform.x + platform.w / 2 - choice.width / 2;
		} else {
			maxCoins = ~~(platform.w / widthPerCoin);
			count = count || (Math.min(maxCoins, formation.base + ~~((formation.range + 1) * random())));
			x = formation.offsetX || platform.x + platform.w / 2 - count * widthPerCoin / 2;
		}

		var offsetYData = formation.offsetY;
		var minMult = offsetYData.minMult;
		var maxMult = offsetYData.maxMult;
		var offsetYMult = minMult + ~~(random() * (1 + maxMult - minMult));
		var offsetY = offsetYData.base * offsetYMult;
		var y = platform.y + offsetY - coinSpacing - choice.height;
		for (var i = 0; i < count; i++) {
			var index = i % pattern.length;
			var pos = pattern[index];

			// grab a free coin from the back if we can to avoid object creation
			if (this.freeCoins.length) {
				var coin = this.freeCoins.pop();
			} else {
				var coin = {};
			}

			if (i > 0) {
				x += pos.dx || pos.repeatX || 0;
				y += pos.dy || pos.repeatY || 0;
			}

			coin.x = x;
			coin.y = y;
			coin.width = choice.width;
			coin.height = choice.height;
			coin.endX = x + choice.width;
			coin.endY = y + choice.height;
			coin.id = coinID++;
			coin.hover = COIN_HOVER_MAX - 1.5 * i;
			coin.hoverDirection = -1;
			coin.callout = choice.callout;
			coin.image = choice.image;
			coin.type = choice.type;
			coin.value = choice.value;
			coin.view = false;
			coin.radial = false;
			coin.collectionTime = COLLECTION_TIME;
			coin.startX = false;
			coin.startY = false;
			coin.sparkles = false;
			coin.alreadyCollected = false;
			coin.babyDiveRoll = false;

			this.activeCoins.push(coin);
		}
	};

	this.spawnEchidna = function(platform) {
		if (this.echidnaSpawn) {
			return;
		}

		var x, y;
		if (this.firstPlay) {
			x = platform.x + ECHIDNA_WIDTH;
		} else {
			x = platform.endX - ECHIDNA_WIDTH - ~~(random() * (platform.w - ECHIDNA_WIDTH));
		}

		y = platform.y - ECHIDNA_HEIGHT;

		this.echidnaSpawn = {
			x: x,
			y: y,
			width: ECHIDNA_WIDTH,
			height: ECHIDNA_HEIGHT,
			endX: x + ECHIDNA_WIDTH,
			endY: y + ECHIDNA_HEIGHT,
			hitX: x + ECH_HIT_BOX_X,
			hitY: y + ECH_HIT_BOX_Y,
			hitEndX: x + ECH_HIT_BOX_X + ECH_HIT_BOX_W,
			hitEndY: y + ECH_HIT_BOX_Y + ECH_HIT_BOX_H
		};
	};

	this.spawnBee = function(platform) {
		if (this.beeSpawn) {
			return;
		}

		// handle tutorial bee spawn?
		var x = platform.endX;
		var y = platform.y - (random() * 2 * BEE_HEIGHT + 2 * BEE_HEIGHT);
		if (this.forceBeeSpawn) {
			this.forceBeeSpawn = false;
			x = platform.endX - 5.5 * BEE_HEIGHT;
			y = platform.y - 3.25 * BEE_HEIGHT;
		}

		this.beeSpawn = {
			x: x,
			y: y,
			vx: -125,
			time: 0,
			amplitude: -9,
			width: BEE_WIDTH,
			height: BEE_HEIGHT,
			hitbox: {
				x: 8,
				y: 8,
				w: BEE_WIDTH - 16,
				h: BEE_HEIGHT - 16
			}
		};
	};

	this.spawnPoo = function(platform) {
		if (this.pooSpawn) {
			return;
		}

		var numPoos = ~~(Math.random() * this.MAX_POOS) + 1;

		var x, y;

		x = platform.endX - POO_WIDTH*numPoos - ~~(random() * (platform.w - POO_WIDTH * numPoos)) + 10;

		y = platform.y - POO_HEIGHT;

		this.pooSpawn = {
			x: x,
			y: y,
			width: POO_WIDTH * numPoos,
			height: POO_HEIGHT,
			hitbox: {
				x: 8,
				y: 8,
				w: POO_WIDTH - 16,
				h: POO_HEIGHT - 16
			},
			count: numPoos,
			pooStates: []
		};

		for( var i = 0; i < this.MAX_POOS; i++ ){
			this.pooSpawn.pooStates[i] = ( i < numPoos ? this.POO_STATE_NEW : this.POO_STATE_EMPTY );
		}
	};

	this.spawnBerries = function(platform) {
		var coinConfig = this.coinConfig,
		choice = coinConfig.choices[BERRY_ID],
		formation = Utils.choose(choice.formations);

		if (this.firstPlay) {
			if (platformCount === 0) {
				formation = choice.formations[3];
				this.forceBerryCount = 2;
			} else if (platformCount === 1) {
				formation = choice.formations[1];
				this.forceBerryCount = 5;
			} else {
				this.forceBerryCount = 0; // falsey, disabled
			}
		}

		this.spawnItems(choice, formation, platform);
	};

	this.spawnShield = function(platform) {
		var coinConfig = this.coinConfig;
		var choice = coinConfig.choices[SHIELD_ID];
		var formation = Utils.choose(choice.formations);

		if (!this.shieldSpawned && this.shieldTimer <= 0) {
			this.shieldSpawned = true;
			this.spawnItems(choice, formation, platform);
		}
	};

	//Called when a glider is picked up,
	//removes off-screen berries to prepare for glider flight berry spawning
	this.initGliderBerries = function() {
		var coins = this.activeCoins,
		player = this.player,
		rightX = (player.x + BG_WIDTH) - playerX,
		i = 0, coin;

		gliderBerryStartX = rightX;

		if( player.y > MAX_GLIDER_BERRY_START_Y ){
			gliderBerryStartY = MAX_GLIDER_BERRY_START_Y;
		}
		else if( player.y < MIN_GLIDER_BERRY_START_Y ){
			gliderBerryStartY = MIN_GLIDER_BERRY_START_Y;
		}
		else{
			gliderBerryStartY = player.y;
		}

		this.kiwiGliderStartTime = this.elapsed;

		//Remove offscreen berries
		while( i < coins.length ){
			coin = coins[i];
			if( coin.type === ITEM_TYPE_BASIC && coin.x > rightX){
				coins.splice(i, 1);
				this.freeCoins.push(coin);
				this.queueItemPoolReleases.push(coin.view);
			}
			else{
				i ++;
			}
		}

		gliderBerryLastX = gliderBerryStartX;
	};

	this.finishGliderRun = function() {
		var coins = this.activeCoins,
			player = this.player,
			rightX = (player.x + BG_WIDTH) - playerX,
			i = 0, coin;

		var platforms = this.platforms,
			platform;

		//remove offscreen berries
		while( i < coins.length ){
			coin = coins[i];
			if( coin.type === ITEM_TYPE_BASIC && coin.x > rightX){
				coins.splice(i, 1);
				this.freeCoins.push(coin);
				this.queueItemPoolReleases.push(coin.view);
			}
			else{
				i ++;
			}
		}

		for( i = 0; i<platforms.length; i++ ){
			platform = platforms[i];

			if( platform.x > rightX ){
				this.generatePlatformEventsNew(platform);
			}
		}

		gliderBerryAmplitude = GLIDER_BERRY_AMPLITUDE;
		gliderBerryAmplitudeVel = 0;
		gliderBerryFrequencyAcc = 0;

		gliderBerryFrequency = GLIDER_BERRY_FREQUENCY;
		gliderBerryFrequencyVel = 0;
		gliderBerryAmplitudeAcc = 0;
	}

	this.spawnGliderBerry = function(ignoreRightX){
		var rightX = (this.player.x + BG_WIDTH) - playerX;

		if (ignoreRightX || (rightX - gliderBerryLastX) > gliderBerrySpacing) {
			var berryX = gliderBerryLastX + gliderBerrySpacing;
			gliderBerryLastX = berryX;
			if (random() < gliderBerryChance) {
				var berryY = gliderBerryStartY + gliderBerryAmplitude * Math.sin(berryX / gliderBerryFrequency);
				this.spawnBerry(berryX, berryY);
			}

			gliderBerryFrequencyAcc += random() * GLIDER_BERRY_FREQUENCY_VARIANCE - GLIDER_BERRY_FREQUENCY_VARIANCE / 2;
			gliderBerryFrequencyVel += gliderBerryFrequencyAcc;
			gliderBerryFrequency += gliderBerryFrequencyVel;

			var diff = gliderBerryFrequency - GLIDER_BERRY_FREQUENCY;
			if (diff > GLIDER_BERRY_FREQUENCY_MAX_VARIANCE) {
				gliderBerryFrequency = GLIDER_BERRY_FREQUENCY + GLIDER_BERRY_FREQUENCY_MAX_VARIANCE;
			} else if (diff < -GLIDER_BERRY_FREQUENCY_MAX_VARIANCE) {
				gliderBerryFrequency = GLIDER_BERRY_FREQUENCY - GLIDER_BERRY_FREQUENCY_MAX_VARIANCE;
			}

			gliderBerryAmplitudeAcc += random() * GLIDER_BERRY_AMPLITUDE_VARIANCE - GLIDER_BERRY_AMPLITUDE_VARIANCE / 2;
			gliderBerryAmplitudeVel += gliderBerryAmplitudeAcc;
			gliderBerryAmplitude += gliderBerryAmplitudeVel;

			diff = gliderBerryAmplitude - GLIDER_BERRY_AMPLITUDE;
			if (diff > GLIDER_BERRY_AMPLITUDE_MAX_VARIANCE) {
				gliderBerryAmplitude = GLIDER_BERRY_AMPLITUDE + GLIDER_BERRY_AMPLITUDE_MAX_VARIANCE;
			} else if (diff < -GLIDER_BERRY_AMPLITUDE_MAX_VARIANCE) {
				gliderBerryAmplitude = GLIDER_BERRY_AMPLITUDE - GLIDER_BERRY_AMPLITUDE_MAX_VARIANCE;
			}
		}
	};

	this.spawnBerry = function(x, y) {
		var choice = this.coinConfig.choices[BERRY_ID];

		if (this.freeCoins.length) {
			var coin = this.freeCoins.pop();
		} else {
			var coin = {};
		}

		coin.x = x;
		coin.y = y;
		coin.width = choice.width;
		coin.height = choice.height;
		coin.endX = x + choice.width;
		coin.endY = y + choice.height;
		coin.id = coinID++;
		coin.hover = COIN_HOVER_MAX;
		coin.hoverDirection = -1;
		coin.callout = choice.callout;
		coin.image = choice.image;
		coin.type = choice.type;
		coin.value = choice.value;
		coin.view = false;
		coin.radial = false;
		coin.collectionTime = COLLECTION_TIME;
		coin.startX = false;
		coin.startY = false;
		coin.sparkles = false;
		coin.alreadyCollected = false;
		coin.babyDiveRoll = false;

		this.activeCoins.push(coin);
	};

	this.spawnMegaBerry = function(platform) {
		var coinConfig = this.coinConfig;
		var choice = coinConfig.choices[MEGA_BERRY_ID];
		var formation = Utils.choose(choice.formations);

		choice.value = megaBerryValue;
		this.spawnItems(choice, formation, platform);
	};

	this.spawnBerryMagnet = function(platform) {
		var coinConfig = this.coinConfig;
		var choice = coinConfig.choices[BERRY_MAGNET_ID];
		var formation = Utils.choose(choice.formations);

		if (!this.magnetSpawned && this.berryMagnetTimer <= 0) {
			this.magnetSpawned = true;
			this.spawnItems(choice, formation, platform);
		}
	};

	this.spawnTurtleFriend = function(platform) {
		var coinConfig = this.coinConfig;
		var choice;
		if (this.bestTurtlesAvailable) {
			choice = coinConfig.choices[TURTLE_BEST_FRIEND_ID];
		} else {
			choice = coinConfig.choices[TURTLE_FRIEND_ID];
		}
		var formation = Utils.choose(choice.formations);

		if (!this.turtleSpawned && !this.turtleActive
			&& (this.turtlesAvailable + this.bestTurtlesAvailable)
			&& this.turtleCount < MAX_TURTLE_FRIENDS)
		{
			this.turtleCount++
			this.turtleSpawned = true;
			this.spawnItems(choice, formation, platform);
		}
	};

	this.spawnKiwiGlider = function(platform) {
		var coinConfig = this.coinConfig;
		var choice = coinConfig.choices[KIWI_GLIDER_ID];
		var formation = Utils.choose(choice.formations);

		if (!this.gliderSpawned && this.kiwiGliderTimer <= 0) {
			this.gliderSpawned = true;
			this.spawnItems(choice, formation, platform);
		}
	};

	this.generateNestable = function(platform) {
		if (!this.nestableSet || (lastNestablePlatform && (platformCount - lastNestablePlatform) < NESTABLE_DENSITY)) {
			return;
		}

		var available = [];
		for (var i in this.nestableSet.pieces) {
			var nestable = this.nestableSet.pieces[i];

			// all nestables are unique per play
			if (this.uniqueSpawns[nestable.id]) {
				continue;
			}

			if (this.collectedSet) {
				var collected = this.collectedSet[i];
				if (!collected) {
					nestable.collected = false;
					available.push(nestable);
				}
			} else {
				nestable.collected = false;
				available.push(nestable);
			}
		}

		if (available.length) {
			var choice = Utils.choose(available);
			var config = this.nestablesConfig;
			var width = config.width;
			var height = config.height;

			// don't spawn if the individual piece chance fails
			if (random() >= choice.chance) {
				return;
			}

			// save unique spawn event
			this.uniqueSpawns[choice.id] = true;

			if (this.freeCoins.length) {
				var coin = this.freeCoins.pop();
			} else {
				var coin = {};
			}

			coin.x = platform.x + platform.w / 2 - width / 2;
			coin.y = platform.y + config.offsetY - 10 - height;
			coin.width = width;
			coin.height = height;
			coin.endX = coin.x + width;
			coin.endY = coin.y + height;
			coin.id = coinID++;
			coin.hover = COIN_HOVER_MAX;
			coin.hoverDirection = -1;
			coin.image = choice.image;
			coin.shadow = choice.shadow;
			coin.type = choice.type;
			coin.value = 0;
			coin.view = false;
			coin.radial = false;
			coin.collectionTime = COLLECTION_TIME;
			coin.startX = false;
			coin.startY = false;
			coin.sparkles = false;
			coin.nestableID = choice.id;
			coin.alreadyCollected = choice.collected;
			coin.babyDiveRoll = false;

			this.activeCoins.push(coin);

			lastNestablePlatform = platformCount;
		}
	};

	this.activateBerryMagnet = function() {
		this.berryMagnetTimer = berryMagnetDuration * (1 + this.babyBoosts["magnetDuration"]);
		this.magnetSpawned = false; // flag for power-up spawning
	};

	this.activateTurtleBait = function() {
		var purchased = controller.getData("upgradesPurchased");
		if (this.bestTurtlesAvailable) {
			this.bestTurtlesAvailable--;
			this.turtleBounces = 2;
			purchased[TURTLE_BEST_FRIEND_ID].level = this.bestTurtlesAvailable;
		} else {
			this.turtlesAvailable--;
			this.turtleBounces = 1;
			purchased[TURTLE_FRIEND_ID].level = this.turtlesAvailable;
		}
		controller.setData("upgradesPurchased", purchased);

		this.turtleActive = true;
		this.turtleSpawned = false;
	};

	this.activateKiwiGlider = function() {
		controller.achieveCheck(ACHIEVE_ID_KIWI_GLIDER);
		this.floatingStreak = 0;

		this.kiwiGliderTimer = kiwiGliderDuration;
		this.gliderSpeed = kiwiGliderSpeed;

		this.vxOriginal = this.player.vx;
		this.player.vx += kiwiGliderSpeed;
		this.player.vy = 0;

		this.gliderSpawned = false;

		this.initGliderBerries();
	};

	this.activateAngelFish = function() {
		var player = this.player;

		controller.achieveCheck(ACHIEVE_ID_ANGEL_FISH);

		var angelFishUses = controller.getData("angelFishUses");
		controller.setData("angelFishUses", angelFishUses + 1, true, true);

		player.spiked = false;
		this.queueAngelFish = true;

		player.y = FLOATING_END_Y - 1; // move our player out of the dead zone
		player.vy = ANGEL_VELOCITY;
		player.floating = false;
		player.diving = false;
		player.jumping = false;
		player.invincible = true;

		animate(this).wait(750).then(bind(this, function(){
			this.player.invincible = false;
		}));

		this.playerSplashed = false; // reset this flag so we splash again
	};

	this.activateShield = function() {
		this.shieldTimer = shieldDuration * (1 + this.babyBoosts["magnetDuration"]);
		this.shieldSpawned = false;
	};

	this.deactivateKiwiGlider = function() {
		if (this.superGlider) {
			controller.achieveCheck(ACHIEVE_ID_SUPER_GLIDER);
			this.player.vx -= this.superGlider.speed;
			this.superGlider = false;
		} else if (this.megaGlider) {
			controller.achieveCheck(ACHIEVE_ID_MEGA_GLIDER);
			this.player.vx -= this.megaGlider.speed;
			this.megaGlider = false;
		} else {
			this.player.vx -= kiwiGliderSpeed;
		}

		this.vxOriginal = false;

		this.player.vy = -900;
		this.player.jumping = true;
		this.player.floating = true;
		this.player.diving = false;

		this.finishGliderRun();
	};

	this.setFingerDown = function(fingerDown) {
		// tutorial scripting
		if (this.firstPlay) {
			if (this.teachJump && !fingerDown) {
				return;
			} else if (!this.teachJump && this.teachFloat && this.pauseFloat && !fingerDown) {
				this.failFloat = true;
			}
		}

		this.fingerDown = fingerDown;
		if (fingerDown && !this.player.jumping) {
			this.holdStart = this.elapsed;
		}
	};

	var scoreSort = function(a, b) {
		if (a.score < b.score) {
			return 1;
		} else if (a.score > b.score) {
			return -1;
		} else if (a.meters < b.meters) {
			return 1;
		} else {
			return -1;
		}
	};

	this.gameover = function() {
		// push this game's score on this high scores, sort, and remove any extra
		var topScores = controller.getData('topScores') || [];
		var topMeters = controller.getData('topMeters') || [];
		var meterage = this.getDisplayDistance();
		var topPairs = [];

		for (var i = 0; i < topScores.length; i++) {
			topPairs.push({
				score: topScores[i],
				meters: i < topMeters.length ? topMeters[i] : 0
			});
		}

		topPairs.push({
			score: ~~this.score,
			meters: meterage
		})

		topPairs.sort(scoreSort);
		topPairs = topPairs.slice(0, TOP_SCORE_COUNT);

		for (i = 0; i < topPairs.length; i++) {
			var pair = topPairs[i];
			topScores[i] = pair.score;
			topMeters[i] = pair.meters;
		}

		controller.setData('topScores', topScores, true, true);
		controller.setData('topMeters', topMeters, true, true);

		if (meterage > this.bestRun) {
			controller.setData('bestRun', meterage, true, true);
		}

		this.finished = true;
	};

	this.getDisplayDistance = function() {
		return ~~(this.distance / PIXELS_TO_METERS);
	};

	this.testLanding = function(platform, player, playerInitialEndY, playerEndY) {
		if (this.kiwiGliderTimer > 0) {
			return;
		}

		// are we landing on the platform?
		if (playerInitialEndY <= platform.y && playerEndY > platform.y) {
			if (this.lastLandingY != platform.y && (player.diving || player.jumping)) {
				this.lastLandingY = platform.y;

				if (this.floatingStreak) {
					this.floatingStreak = 0;
				} else {
					this.landingStreak++;
				}
			}

			if (player.diving) {
				this.lastDiveY = platform.y;
				this.queueRoll = true;
			} else {
				this.queueLanding = true;
			}

			this.failBounceTrigger = false;

			player.y = platform.y - player.h;
			player.vy = 0;
			player.jumping = false;
			player.floating = false;

			if (player.diving) {
				player.rolling = true;
				player.diving = false;
			}
		} else if (playerInitialEndY > platform.y && playerEndY > platform.y) {
			player.jumping = true; // beneath the platform
		} else if (playerInitialEndY < platform.y && playerEndY < platform.y) {
			player.jumping = true; // above the platform
		}
	};

	this.angelSave = function() {
		var success = false;
		if (!this.angelModalActive) {
			this.angelModalActive = true;
			this.beforeAngelVx = this.player.vx;
			this.player.vx = this.player.vx / 10;
			this.publish("angelSaveModal");
			success = true;
		}

		return success;
	};

	this.angelSaveResolve = function(accept) {
		if (accept) {
			var result = controller.productController.purchaseUpgrade(controller.upgradesConfig['angelFish'], true, true, true);
			if (result) {
				this.player.vx = this.beforeAngelVx;
				this.useConsumable(ANGEL_FISH_ID);
				this.angelFish = true;
				this.gameoverCountdown = undefined;
				controller.angelSaveCount++;
			}
		}

		this.pauseGameoverCountdown = false;
		this.angelModalActive = false;
	};

	this.pauseGameover = function() {
		this.pauseGameoverCountdown = true;
	};

	// step the game model forward
	this.step = function(dt, everyOther) {
		if (this.finished) {
			return;
		}

		// if the player falls, countdown during splash
		if (this.gameoverCountdown > 0) {
			!this.pauseGameoverCountdown && (this.gameoverCountdown -= dt);

			if (!this.playerSplashed) {
				this.playerSplashed = true;
				this.queuePlayerSplash = true;
			}

			if (this.gameoverCountdown <= 0) {
				this.gameover();
			}

			return;
		}

		var min = Math.min,
			max = Math.max;

		this.dtLast = dt;
		this.elapsed += dt;

		var elapsedPct = dt / 1000,
			player = this.player,
			leftPlatform = this.platforms[0];

		this.distance = ~~player.x;

		// score / distance achievements
		if ( everyOther === 0 ) {
			var dist = this.getDisplayDistance();
			controller.achieveCheck(ACHIEVE_ID_SCORE_COUNT, this.score);
			controller.achieveCheck(ACHIEVE_ID_DISTANCE_COUNT, dist);

			if (!this.berryAllergyFailed && dist >= BERRY_ALLERGY_METERS) {
				controller.achieveCheck(ACHIEVE_ID_BERRY_ALLERGY);
			}

			if (!this.bestRunReached && this.bestRun && this.getDisplayDistance() > this.bestRun) {
				this.bestRunReached = true;
				this.queueCallouts.push(CALLOUT_BEST_RUN);
			}

			if (!this.highScoreReached && this.highScore && this.score > this.highScore) {
				this.highScoreReached = true;
				this.queueCallouts.push(CALLOUT_NEW_HISCORE);
			}
		}



		// generate next platform when one is fully off-screen
		if (player.x - playerX > leftPlatform.endX) {
			var recyclePlatform = this.platforms.shift();

			if ( recyclePlatform.friendRun === true ) {
				this.queueFriendCallouts.push(recyclePlatform.friendName);

				this.friendRuns.shift();
				this.friendSpawnIdx--;
			}

			this.generatePlatform(recyclePlatform);
			this.platformIndex--; // decrement to match the shift
		}

		this.turtleDiveCooldown -= dt;



		if (this.kiwiGliderTimer > 0) {
			this.kiwiGliderTimer -= dt;

			this.kiwiGliderTimer > GLIDER_BERRY_SPAWN_STOP && this.spawnGliderBerry(false);

		} else { // jump and dive logic if we aren't gliding
			if (this.fingerDown) {
				var elapsedHold = this.elapsed - this.holdStart,
					doJump = false;

				for (var i = 0; i < JUMP_COUNT; i++) {
					var jump = this.playerJumps[i];
					if (elapsedHold <= jump.hold) {
						doJump = true;
						break;
					}
				}

				if (doJump) { // jump!
					this.player.jumping = true;
					this.player.vy = jump.velocity * (1 + this.babyBoosts["jumpVelocity"]);
				}
			}

			if (player.diving) {
				this.player.vy = DIVING_VELOCITY;
			}
		}



		// tutorial pausing flow
		if (this.firstPlay) {
			if (this.teachJump) {
				if (!this.pauseJump && player.x >= PAUSE_JUMP) {
					this.pauseJump = true;
					this.queuePause = true;
				}
			} else if (this.teachFloat) {
				if (!this.pauseFloat && player.x >= PAUSE_FLOAT) {
					this.pauseFloat = true;
					this.queuePause = true;
				} else if (player.x >= PAUSE_DIVE) {
					this.teachFloat = false;
					this.failFloat = false;
				}
			} else if (this.teachDive) {
				if (!this.pauseDive && player.x >= PAUSE_DIVE) {
					this.pauseDive = true;
					this.queuePause = true;
				}
			} else if (!this.pauseTutorialEnd && player.x >= PAUSE_TUTORIAL_END) {
				this.pauseTutorialEnd = true;
				this.queuePause = true;
				this.firstPlay = false;
				this.forceBerryCount = 0; // falsey, disabled
				controller.setData("firstPlay", false);
			}
		}



		// hit detection values
		var hitbox = this.playerHitbox,
			platform = this.currentPlatform,
			playerInitialEndX = player.x + hitbox.x + hitbox.w,
			playerInitialEndY = player.y + hitbox.y + hitbox.h;

		// ~~~ Note:
		// Step player velocity by acceleration, one half at a time.
		// This more closely approximates the real curve despite bad FPS.

		// VX
			// scaleSpeed is used so that gliders don't reduce the
			// acceleration of progression through the level
		var currSpeed = player.vx,
			origSpeed = currSpeed - (this.kiwiGliderTimer > 0 ? this.gliderSpeed : 0),
			speedScale = origSpeed ? currSpeed / origSpeed : 1,
			accelPct = ACCELERATION_PERCENT,
			ddxHalved = speedScale * elapsedPct * accelPct * (SPEED_MAX - origSpeed) / 2;

		player.vx += ddxHalved;

		// VY
		var currSpeedY = player.vy,
			ddyHalved = 0;

		// default player behavior while not gliding
		if (this.kiwiGliderTimer <= 0) {
			// is the player bouncing off the turtle? or is he floating? else in free-fall
			if (player.y > FLOATING_END_Y - TURTLE_OFFSET && this.turtleActive) {
				this.turtleBounces--;
				if (this.turtleBounces <= 0) {
					this.turtleActive = false;
				}

				this.failBounceTrigger = true;

				this.turtleDiveCooldown = TURTLE_DIVE_COOLDOWN;

				player.spiked = false;
				this.queueTurtleBounce = true;

				player.y = FLOATING_END_Y - TURTLE_OFFSET; // move our player out of the dead zone
				player.vy = TURTLE_BOUNCE_VELOCITY;
				player.floating = false;
				player.diving = false;
				player.jumping = false;
				player.invincible = true;
				animate(this).wait(750).then(bind(this, function(){
					this.player.invincible = false;
				}));
			} else if (player.vy > 0
				&& player.jumping
				&& player.y < FLOATING_END_Y
				&& this.fingerDown
				&& !player.diving)
			{
				// tutorial scripting
				if (this.firstPlay && this.teachJump) {
					this.teachJump = false;
					this.queueJumpFinish = true;
					this.setFingerDown(false);
					controller.blockInput();
				} else {
					player.floating = true;
				}
			} else if (player.floating) {
				player.floating = false;
			}

			// acceleration due to gravity and floating
			if (player.jumping && player.floating) {
				ddyHalved = ((FLOATING_VELOCITY - FLOATING_BUFF_RATE * this.player.vx) - currSpeedY) / 2;
				player.vy += ddyHalved;
			} else if (player.jumping) {
				ddyHalved = elapsedPct * GRAVITY / 2;
				player.vy += ddyHalved;
			}
		}

		// Now step player position by velocity.
		if (!this.angelModalActive && !this.gameoverCountdown) {
			player.x += elapsedPct * player.vx;
		}
		player.y += elapsedPct * player.vy;

		// Lastly add the other halves of the change in velocity
		player.vx += ddxHalved;
		player.vy += ddyHalved;



		// track player history so that babies can follow
		var history = this.playerHistory,
			historyIndex = this.playerHistoryIndex,
			historyNode = history[historyIndex++];

		historyNode.feetY = player.y + hitbox.y + hitbox.h;
		historyNode.dt = dt;
		historyNode.platformY = platform.y;

		if (historyIndex >= PLAYER_HISTORY_SIZE) {
			historyIndex = this.playerHistoryIndex = 0;
		} else {
			this.playerHistoryIndex = historyIndex;
		}

		// update babies based on player history
		if (player.vx >= SPEED_MIN) {
			for (var b in this.babies) {
				var baby = this.babies[b];

				if (baby.diving) {
					continue;
				}

				if (random() < 0.1) {
					baby.organicOffset += random() < 0.5 ? 1 : -1;
					if (baby.organicOffset < -BABY_OFFSET_SIZE) {
						baby.organicOffset = -BABY_OFFSET_SIZE;
					} else if (baby.organicOffset > BABY_OFFSET_SIZE / 4) {
						baby.organicOffset = BABY_OFFSET_SIZE / 4;
					}
				}

				var playerSpeed = player.vx;
				if (this.kiwiGliderTimer > 0) {
					playerSpeed = this.vxOriginal;
				} else if (playerSpeed > SPEED_MAX) {
					playerSpeed = SPEED_MAX;
				}

				// reduce index offset as player gets faster
				var indexOffset = -baby.indexOffset + baby.organicOffset;
				var denominator = (playerSpeed / SPEED_MIN) * (0.25 + 0.75 * (SPEED_MAX - playerSpeed) / (SPEED_MAX - SPEED_MIN));
				baby.babyIndex = historyIndex + ~~(indexOffset / denominator);
				if (baby.babyIndex < 0) {
					baby.babyIndex += history.length;
				}

				var historyData = history[baby.babyIndex];
				var diffY = historyData.feetY - (baby.y + baby.height);
				baby.vy = min(max(historyData.dt ? diffY / (historyData.dt / 1000) : 0, -BABY_VY_MAX), BABY_VY_MAX);

				if (baby.horzOffset < baby.organicOffset) {
					baby.horzOffset += 0.02;
				} else if (baby.horzOffset > baby.organicOffset) {
					baby.horzOffset -= 0.02;
				}

				baby.y += elapsedPct * baby.vy;
			}
		}



		var playerStartX = player.x + hitbox.x,
			playerStartY = player.y + hitbox.y,
			playerEndX = playerStartX + hitbox.w,
			playerEndY = playerStartY + hitbox.h,
			centerX = playerStartX + hitbox.x / 2,
			centerY = playerStartY + hitbox.y / 2,
			radiusMult = this.kiwiGliderTimer > 0 ? 1.2 : 1,
			defaultRadius = radiusMult * (hitbox.x / 2 + BEAK_REACH),
			collectionRadius = defaultRadius;

		// is the player on the platform?
		if (platform.x <= playerEndX && playerStartX <= platform.endX) {
			!player.spiked && this.testLanding(platform, player, playerInitialEndY, playerEndY);
		} else if (playerStartX > platform.endX) { // get the next platform
			if (this.lastPlatformY != platform.y && this.kiwiGliderTimer <= 0 && !player.spiked) {
				this.lastPlatformY = platform.y; // has to update each platform

				if (this.lastDiveY != this.lastPlatformY) {
					this.divingStreak = 0;
				} else {
					this.divingStreak++;
					if (this.divingStreak == SURE_FOOTED_LANDINGS) {
						controller.achieveCheck(ACHIEVE_ID_SURE_FOOTED);
					}
				}

				if (this.lastLandingY != platform.y) {
					this.landingStreak = 0;
				}

				if (!this.landingStreak && player.y < platform.y && this.lastFloatY !== platform.y ) {
					this.lastFloatY = platform.y;
					this.floatingStreak++;
				}
				else if( player.y > platform.y ){
					this.floatingStreak = 0;
				}

				if (this.floatingStreak == HIGH_AND_DRY_FLOATINGS) {
					controller.achieveCheck(ACHIEVE_ID_FLOAT_STREAK);
				}
			}

			platform = this.currentPlatform = this.platforms[++this.platformIndex];
			!player.spiked && this.testLanding(platform, player, playerInitialEndY, playerEndY);
		} else { // player is off the platform
			player.jumping = true;
		}



		var activeCoins = this.activeCoins,
			freeCoins = this.freeCoins,
			magnetMultiplier = 1;

		if (this.berryMagnetTimer > 0) {
			this.berryMagnetTimer -= dt;
			collectionRadius *= berryMagnetMultiplier * (1 + this.babyBoosts["magnetReach"]);
		}

		var berryScaleBonus = 1 + BERRY_SCALE_INC * (this.berrySize || 0);

		this.collectionRadius = collectionRadius;

		// coin hit detection and recycling
		for (var i = 0; i < activeCoins.length; i++) {
			var coin = activeCoins[i];

			// coin hover
			coin.hover += COIN_HOVER_RATE * coin.hoverDirection;
			if (coin.hover <= COIN_HOVER_MIN) {
				coin.hoverDirection = 1;
			} else if (coin.hover >= COIN_HOVER_MAX) {
				coin.hoverDirection = -1;
			}

			// recycle off-screen coins
			if (player.x - playerX > coin.x + coin.width) {
				if (coin.type === ITEM_TYPE_BASIC) {
					this.coinsMissed++;
					this.berryStreak = 0;
					this.queueUpdateMultiplier = true;
				}

				// flags for power-up spawning
				if (coin.type == ITEM_TYPE_TURTLE_BAIT) {
					this.turtleSpawned = false;
				} else if (coin.type == ITEM_TYPE_MAGNET) {
					this.magnetSpawned = false;
				} else if (coin.type == ITEM_TYPE_GLIDER) {
					this.gliderSpawned = false;
				}
				else if (coin.type == ITEM_TYPE_SHIELD) {
					this.shieldSpawned = false;
				}

				// produce view references for the view to consume and release
				this.queueItemPoolReleases.push(coin.view);
				coin.radial && this.queueEffectPoolReleases.push(coin.radial);

				freeCoins.push(activeCoins.splice(i, 1)[0]);
				i--;

				continue;
			}

			var babyGrab = false;
			var babyGrabRadius = coin.type === ITEM_TYPE_BASIC ? berryScaleBonus * BABY_RADIUS : BABY_RADIUS;
			for (var b in this.babies) {
				var baby = this.babies[b];
				var babyX = player.x - playerX + baby.x;
				if (!(coin.x - (babyX + baby.width) > babyGrabRadius
					|| coin.y - (baby.y + baby.height) > babyGrabRadius
					|| babyX - coin.endX > babyGrabRadius
					|| baby.y - coin.endY > babyGrabRadius))
				{
					babyGrab = b;
					break;
				}
			}

			// check if we are not NOT colliding, therefore we are ;)
			var playerRadius = coin.type === ITEM_TYPE_BASIC ? berryScaleBonus * collectionRadius : collectionRadius;
			if (!((coin.x - playerEndX > playerRadius
				|| coin.y - playerEndY > playerRadius
				|| playerStartX - coin.endX > playerRadius
				|| playerStartY - coin.endY > playerRadius)
				&& babyGrab === false))
			{
				if (this.berryMagnetTimer > 0) {
					coin.value *= berryMagnetBonus;
				}

				if (coin.type === ITEM_TYPE_BASIC) {
					this.berryStreak += coin.value;
					if (this.berryStreak == BERRY_STREAK) {
						controller.achieveCheck(ACHIEVE_ID_BERRY_STREAK);
					}
				}

				var scoreMult = 1 + this.babyBoosts["scoreBonus"];
				this.score += coin.value * scoreMult * BERRY_VAL * Math.pow(BERRY_EXP, min(this.berryStreak, BERRY_CHAIN_MAX));

				this.coinsCollected += coin.value;
				this.queueUpdateMultiplier = true;

				this.berryAllergyFailed = true;
				controller.achieveCheck(ACHIEVE_ID_BERRY_COUNT, controller.berriesCollected + this.coinsCollected);

				activeCoins.splice(i, 1);
				i--;

				coin.babyGrab = babyGrab;
				this.queueCollections.push(coin);

				this.updateScore = true;
			} else {
				// baby dives to get missed berries
				if (coin.x < playerEndX + playerRadius && !coin.babyDiveRoll) {
					coin.babyDiveRoll = true;
					if (random() < this.babyBoosts["berryDive"]) {
						var diveBaby;
						for (var b = 0; b < this.babies.length; b++) {
							var testBaby = this.babies[b];
							if (testBaby.dive && !testBaby.diving) {
								diveBaby = this.babies[b];
							}
						}

						if (diveBaby) {
							diveBaby.diving = true;
							var destX = playerX - player.x + coin.x + (coin.width - diveBaby.width) / 2;
							var destY = coin.y + (coin.height - diveBaby.height) / 2;
							var dx = destX - diveBaby.x;
							var dy = destY - diveBaby.y;
							var dist = Math.sqrt(dx * dx + dy * dy);

							if (dy > 0) {
								diveBaby.rot = 0;
							} else {
								diveBaby.rot = -2.2;
							}

							animate(diveBaby)
							.now({
								x: destX + 0.1 * dx,
								y: destY + 0.1 * dy
							}, 1.1 * dist, animate.easeOut)
							.then(bind(this, function() {
								diveBaby.diving = false;
							}))
							.then({
								x: diveBaby.origX
							}, 1.5 * dist, animate.easeIn);
						}
					}
				}
			}
		}


		if (this.shieldTimer > 0) {
			this.shieldTimer -= dt;
		}

		// echidna hit detection and recycling
		var echidnaSpawn = this.echidnaSpawn;
		if (echidnaSpawn) {
			if (player.x - playerX > echidnaSpawn.x + echidnaSpawn.width) {
				echidnaSpawn.passed = true;
			} else {
				// check if we are not colliding first
				var spikeRadius = defaultRadius - BEAK_REACH;
				if (echidnaSpawn.hitX - playerEndX > spikeRadius
					|| echidnaSpawn.hitY - playerEndY > spikeRadius
					|| playerStartX - echidnaSpawn.hitEndX > spikeRadius
					|| playerStartY - echidnaSpawn.hitEndY > spikeRadius)
				{
					echidnaSpawn.passed = false;
				} else { // otherwise we have a collision
					// spiked by echidna or killed him?
					if (!player.rolling
						&& !player.diving
						&& !player.invincible
						&& this.kiwiGliderTimer <= 0
						&& this.shieldTimer <= 0)
					{
						// sacrifice a baby to the echidna!
						if (this.babies.length) {
							this.removeBaby();
							echidnaSpawn.killed = true;
						} else if (!player.spiked) {
							player.spiked = true;
							player.vy = -600;
							this.setFingerDown(false);
						}
					} else {
						echidnaSpawn.killed = true;
					}
				}
			}
		}



		// bee hit detection and recycling
		var beeSpawn = this.beeSpawn;
		if (beeSpawn) {
			beeSpawn.time += dt;
			beeSpawn.x += beeSpawn.vx * elapsedPct;
			beeSpawn.y += beeSpawn.amplitude * Math.sin(beeSpawn.time / 200);

			var beeBox = beeSpawn.hitbox;
			if (player.x - playerX > beeSpawn.x + beeSpawn.width) {
				beeSpawn.passed = true;
			} else {
				// check if we are not colliding first
				var spikeRadius = defaultRadius - BEAK_REACH;
				if (beeSpawn.x + beeBox.x - playerEndX > spikeRadius
					|| beeSpawn.y + beeBox.y - playerEndY > spikeRadius
					|| playerStartX - (beeSpawn.x + beeBox.x + beeBox.w) > spikeRadius
					|| playerStartY - (beeSpawn.y + beeBox.y + beeBox.h) > spikeRadius)
				{
					beeSpawn.passed = false;
				} else { // otherwise we have a collision
					// spiked by echidna or killed him?
					if (!player.rolling
						&& !player.diving
						&& !player.invincible
						&& this.kiwiGliderTimer <= 0
						&& this.shieldTimer <= 0)
					{
						// sacrifice a baby to the bee!
						if (this.babies.length) {
							this.removeBaby();
							beeSpawn.killed = true;
						} else if (!player.spiked) {
							player.spiked = true;
							player.vy = -600;

							this.floatingStreak = 0;
							this.landingStreak = 0;

							this.setFingerDown(false);
						}
					} else {
						beeSpawn.killed = true;
					}
				}
			}
		}

		var pooSpawn = this.pooSpawn;
		if (pooSpawn) {

			var pooViews = pooSpawn.view;

			if( !pooViews ){
				return;
			}

			for(var p = 0; p < pooSpawn.count; p++){

				var view = pooViews[p],
					pooStates = pooSpawn.pooStates,
					pooWidth = pooSpawn.width/pooSpawn.count,
					curX = pooSpawn.x + pooWidth * p,
					pooBox = pooSpawn.hitbox;

				if( pooStates[p] === this.POO_STATE_ACTIVE ){

					if ( player.x - playerX > curX + pooWidth ) {
						pooStates[p] = this.POO_STATE_PASSED;
					} else {
						// check if we are not colliding first
						var spikeRadius = defaultRadius - BEAK_REACH;
						if ( !(curX + pooBox.x - playerEndX > spikeRadius
							|| pooSpawn.y + pooBox.y - playerEndY > spikeRadius
							|| playerStartX - (curX + pooBox.x + pooBox.w) > spikeRadius
							|| playerStartY - (pooSpawn.y + pooBox.y + pooBox.h) > spikeRadius) )
						{
						 // otherwise we have a collision
							// spiked by poo or killed him?
							if (
								!player.rolling
								&& !player.diving
								&& !player.invincible
								&& this.kiwiGliderTimer <= 0
								&& this.shieldTimer <= 0
								&& !pooSpawn.collided)
							{
								// sacrifice a baby to the poo!
								if (this.babies.length) {
									this.removeBaby();
								} else if (!player.spiked) {
									player.spiked = true;
									player.vy = -600;
									this.setFingerDown(false);
								}

								pooSpawn.collided = true;
							}
							pooStates[p] = this.POO_STATE_DEAD;
						}
					}
				}
			}
		}

		// sink or swim?
		if (this.kiwiGliderTimer <= 0) {
			// player dies when they fall
			if (player.y >= FLOATING_END_Y) {
				if (!this.angelFish && !this.angelSave()) {
					if (this.failBounceTrigger) {
						controller.achieveCheck(ACHIEVE_ID_FAIL_BOUNCE);
					}
					this.gameoverCountdown = GAMEOVER_COUNTDOWN;
					this.player.vx /= 10; // cut player velocity to slow water parallax
				} else if (this.angelFish) {
					this.angelFish = false;
					this.activateAngelFish();
				} else {
					if (!this.playerSplashed) {
						this.playerSplashed = true;
						this.queuePlayerSplash = true;
						player.y = min(player.y + player.h, FLOATING_END_Y + player.h);
					}
				}
			}
		} else if (dt) { // special gliding behavior
			// player can't fly beneath this level
			if (player.y >= MAX_GLIDER_Y) {
				player.y = MAX_GLIDER_Y - 1;
			}
		}
	};
});
