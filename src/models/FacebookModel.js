import event.Emitter as Emitter;
import util.ajax as ajax;

var facebookActive = true;

var Facebook = exports = Class(Emitter, function(supr) {
	var defaults = {
		serverURL: "http://54.235.165.143/",
		authKey: null
	};

	var controller,
		leaderboard = "default";

	this.init = function(opts) {
		opts = merge(opts, defaults);
		supr(this, 'init', [opts]);

		controller = GC.app.controller;

		this._serverURL = opts.serverURL;
		this._authkey = opts.authkey;
		this._isConnected = false;
		this._friendsByID = {};
		this._gifts = controller.getData('gifts') || [];
		if (this._gifts.length) {
			this._inviteID = this._gifts[this._gifts.length - 1]._uniqueID;
		} else {
			this._inviteID = 0;
		}
		this.deadline = controller.getData('fbDeadline');
		controller.myFacebookID = controller.getData('fbId');
		if (GLOBAL.NATIVE && NATIVE.facebook) {
			NATIVE.facebook.on('Auth', bind(this, '_onAuth'));
			NATIVE.facebook.on('Data:me', bind(this, '_gotUserData'));
			NATIVE.facebook.on('Data:me/friends', bind(this, '_gotFriendData'));
			NATIVE.facebook.on("Data:me/friendlists", bind(this, '_gotCloseFriendData'));
			NATIVE.facebook.on('Error', bind(this, '_onError'));
			this._isConnected = NATIVE.facebook.connectIfAuthed();
		} else {
			logger.log("~~~ Couldn't find NATIVE.facebook");
			this._isConnected = false;
			facebookActive = false;
		}

		//gcsocial.on('online', bind(this, this._onOnline));
	};

	this.login = function() {
		facebookActive && NATIVE.facebook.connect();
	};

	this.getMe = function() {
		facebookActive && this._isConnected && NATIVE.facebook.getMe();
	};

	this.getFriends = function() {
		facebookActive && this._isConnected && NATIVE.facebook.getFriends();
	};

	this.getFriend = function(id) {
		return this._friendsByID[id];
	};

	this.sendRequests = function(ids, message) {
		facebookActive && this._isConnected && NATIVE.facebook.sendRequest(ids, message);

		controller.trackEvent("FacebookInvite", {
			id: (ids && ids.length && ids[0]) + ""
		});
	};

	this.isConnected = function() {
		return this._isConnected;
	};

	this.getUserData = function() {
		return this._userData;
	};

	this.getFriendData = function() {
		return this._friendData;
	};

	this.getCloseFriendData = function() {
		return this._closeFriendData;
	};

	this.setLeaderboard = function(lb) {
		leaderboard = lb;
	};

	this.getFriendsWithScores = function() {
		var friendsWithScores = [];
		if (this._friendData) {
			for (var i = 0; i < this._friendData.length; i++) {
				var friend = this._friendData[i];
				var score = this._scores[friend.id];
				if (score !== undefined) {
					friend.score = score;
					friendsWithScores.push(friend);
				}
			}
		}

		if (this._userData) {
			var topScores = controller.getData("topScores") || [0],
				currentHighScore = topScores[0],
				currentUserScore = this._scores[this._userData.id];

			if (currentUserScore !== undefined) {
				if (currentHighScore > currentUserScore) {
					currentUserScore = currentHighScore;
					this.postScore(currentUserScore);
				}
			} else {
				currentUserScore = currentHighScore;
				this.postScore(currentUserScore || 0);
			}

			this._userData.score = currentUserScore;
			friendsWithScores.push(this._userData);
		}

		return friendsWithScores;
	};

	this.onScores = function(cb) {
		if (this._scores) {
			cb(this.getFriendsWithScores());
		} else {
			this._onScoresCallback = cb;
		}
	};

	this.postScore = function(score) {
		if(!this._scoreSubmissionInProgress) {	
			if (!this._isConnected || !this._userData) {
				logger.warn('Facebook not connected! Cannot post a score');
				this._pendingScoreData = { score: score, leaderboard: leaderboard };
				if (score > 0) {
					this.publish('PostScoreFailure', score);
				}
			} else {
				var id = this._userData.id;
				var url = this._serverURL + 'scores/' + id + '/';
				logger.log('posting score to fb server!', url, score);
				this._scoreSubmissionInProgress = true;
				ajax.post({
					url: url, 
					data: { "score": score, leaderboard: leaderboard },
					headers: { "Content-Type": "application/json" }
				}, bind(this, function(e) {
					this._scoreSubmissionInProgress = false;
					if (e) {
						logger.log("Score posting failed. Maybe we should try again");
					} else {
						this._pendingScoreData = null;
					}
				}));
			}
		}
	};

	this.getContestEnd = function(cb) {
		cb(null, this.deadline);

		var url = this._serverURL + 'leaderboards/' + leaderboard + '/end/';

		this._getData(url, function(e, response) {
			try {
				var dl = Date.now() + JSON.parse(response).timeleft;
				if (dl) {
					this.deadline = dl;
					controller.setData("fbDeadline", this.deadline);
					cb(null, this.deadline);
				}
				else {
					cb('no deadline returned');
				}
			} catch (e) {
				cb('error parsing contest end response');
			}
		});
	};

	this.getScores = function() {
		if (!this._isConnected || !this._friendData) {
			logger.warn('Facebook not connected! Cannot get scores');
		} else {
			var ids = this._friendData.map(function(friend) {
				return friend.id;
			}).join(',');

			//Also request our own current score
			//If we are logged in with multiple devices, we may have
			//posted a new high score to the server from another device
			if(this._userData) {
				ids = ids + ',' + this._userData.id;
			}

			var url = this._serverURL + 'scores/';
			this._postData(url, {ids: ids, leaderboard: leaderboard}, bind(this, function(e, scores) {
				if (e) {
					logger.warn('error getting friends scores');
				} else {
					try {
						scores = JSON.parse(scores);
						logger.log('got scores!', scores);
						this._scores = scores.reduce(bind(this, function(memo, s) {
							memo[s.id] = s.score;
							return memo;
						}), {});

						this._onScoresCallback && this._onScoresCallback(this.getFriendsWithScores());
						this._onScoresCallback = null;
					} catch (e) {
						logger.warn('Error trying to parse scores data');
					}
				}
			}));
		}
	};

	this.sendGift = function(id) {
		if (this._isConnected) {
			var url = this._serverURL + 'gifts/send/';
			var fromID = this._userData ? this._userData.id : 0;
			var data = {from: fromID, to: id};
			this._postData(url, data, function(e, response) {
				if (e) {
					logger.log('error sending gift', e);
				}
			});
		}
	};

	this.checkForGifts = function() {
		this._getGifts(bind(this, 'onGifts'));
	};

	this._getGifts = function(cb) {
		if (this._isConnected && this._userData) {
			var url = this._serverURL + 'gifts/' + this._userData.id + '/';
			this._getData(url, function(e, data) {
				if (!e) {
					try {
						data = JSON.parse(data);
					} catch (e) {
						logger.log('erroro parsing gifts response');
					}
				}
				cb(e, data);
			});
		}
	};

	var clone = function(o) {
		var clone = null;
		if (o) {
			clone = JSON.parse(JSON.stringify(o));
		}
		return clone;
	};

	this.getGifts = function() {
		this._newGifts = false;
		return this._gifts;
	};

	this._transformGifts = function(gifts) {
		var i = 0;
		var gifters = gifts.map(bind(this, function(gift) {
			var gifter = clone(controller.fb.getFriend(gift.from));
			if (gifter) {
				gifter._uniqueID = this._inviteID++;
			}
			return gifter;
		}));
		gifters = gifters.filter(function(gifter) {
			return gifter != null;
		});
		return gifters;
	};

	this.onGifts = function(e, gifts) {
		if (!e && gifts && gifts.length) {
			this._gifts = this._gifts.concat(this._transformGifts(gifts));
			this._persistGifts(this._gifts);
			if (this._gifts.length) {
				this._newGifts = true;
			}
		}
	};

	this.acceptGift = function(id) {
		var giftsLeft = [];
		var cleared = false;
		for (var i = 0; i < this._gifts.length; i++) {
			if (this._gifts[i].id == id && !cleared) {
				cleared = true;
			} else {
				giftsLeft.push(this._gifts[i]);
			}
		}
		this._gifts = giftsLeft;
		this._persistGifts();
	};

	this.hasNewGifts = function() {
		return this._newGifts;
	};

	this._persistGifts = function() {
		controller.setData('gifts', this._gifts);
	};

	this._getData = function(url, cb) {
		ajax.get({
			url: url,
			headers: {'Content-Type': 'application/json'}
		}, cb);
	};

	this._postData = function(url, data, cb) {
		ajax.post({
			url: url,
			data: data,
			headers: {'Content-Type': 'application/json'}
		}, cb);
	};

	this._onOnline = function() {
		if(this._pendingScoreData) {
			this.postScore(this._pendingScoreData.score, this._pendingScoreData.leaderboard);
		}
	};

	this._onAuth = function(status) {
		logger.log("~~~ Facebook Auth Status:", JSON.stringify(status));
		this._isConnected = status.success;
		this.publish("Auth", status);

		controller.trackEvent("FacebookLogin", {
			status: status.success + ""
		});
	};

	this._gotUserData = function(data) {
		logger.log("~~~ Facebook Current User Data:", data);
		if (this._isConnected) {
			this._userData = data.data && data.data.length && data.data[0];
			if (this._userData) {
				this._userData.id = this._userData.uid;
				delete this._userData.uid;
				controller.myFacebookID = this._userData.id;
				controller.setData('fbId', controller.myFacebookID);
				this.publish("User", this._userData);
			} else {
				logger.log("~~~ Error loading user data");
			}
		}
	};

	this._gotFriendData = function(data) {
		logger.log("~~~ Facebook Friend Data:", data);
		if (this._isConnected) {
			if (!data.error) {
				if(data.data) {
					this._friendData = data.data;
					this._friendData.map(bind(this, function(f) {
						f.id = f.uid;
						delete f.uid;
						this._friendsByID[f.id] = f;
					}));
					this.publish("Friends", this._friendData);
					this.getScores();
				} else {
					logger.log('warning, facebook response had no data property', JSON.stringify(data.data));
				}
			} else {
				logger.log('warning, facebook error', JSON.stringify(data.error));
			}
		}
	};

	this._gotCloseFriendData = function(data) {
		logger.log("~~~ Facebook Close Friend Data:", data);
		if (this._isConnected) {
			this._closeFriendData = data;
			this.publish("CloseFriends", data);
		}
	};

	this._onError = function(err) {
		logger.log("~~~ Facebook Error:", err);
		this._isConnected = false;
		this.publish("Error", err);
	};
});
