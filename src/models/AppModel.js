import device;
import event.Emitter as Emitter;

import src.lib.Utils as Utils;

exports = Class(Emitter, function(supr) {

	var BASE_TIMEOUT = 5000,
		SERVER_COOLDOWN = 60000;

	this.init = function(opts) {
		supr(this, 'init', [opts]);

		this.controller = opts.controller;

		this._data = {};
		this._key = CONFIG.appID + "_data";
		this._serverTimeout = BASE_TIMEOUT;

		// init download handlers for server-side storage
		/*this.serverDataReady = false;
		this.boundDownloader = bind(this, 'downloadServerData');
		this.boundDownloadHandler = bind(this, function(err, response) {
			if (!err) {
				response = response || {};
				this.serverDataReady = true;
				this._data = this.controller.mergeRecursively(this._data, response);
				this.controller.updateAchievementState();
			} else {
				setTimeout(this.boundDownloader, this._serverTimeout);
				this._serverTimeout *= 2;
			}
		});
		this.boundUploadHandler = bind(this, function(err, response) {});*/

		// init or load data
		var storedData = localStorage.getItem(this._key);
		if (storedData != null) {
			this._data = JSON.parse(storedData);
			this.controller.trackEvent("LaunchReturning", {});
		} else { // init data
			this.resetData();
			this.controller.trackEvent("LaunchFirst", {});
		}

		this.saveData();

		// data init for old users
		this.initNewData();

		// start trying to download server data
		//this.downloadServerData();
	};

	// download data from server for this user
	/*this.downloadServerData = function() {
		gcsocial.user.getPublicData(this.boundDownloadHandler);
	};

	// upload this user's data to the server
	this.uploadServerData = function() {
		gcsocial.user.setPublicData(this._data, this.boundUploadHandler);
	};*/

	this.resetData = function() {
		this._data = {};

		//this.setNewUserExperiment();

		// NEW USER FLAG FOR MIGRATION
		this._data["newUser"] = true;

		// tutorial flags
		this._data["firstPlay"] = true;

		// first session flag for tracking metrics
		this._data["firstSession"] = true;

		// first berry purchase
		this._data["firstPurchase"] = true;

		// currency
		this._data["berries"] = 0;
		this._data["gems"] = 80;
		this._data["hearts"] = 5;

		// xp + level
		this._data["XP"] = 0;
		this._data["level"] = 1;
		this._data["babySlots"] = 1;

		// available avatars
		this._data["avatars"] = ["avatarKiwiTeal/kiwiTeal"];
		this._data["selectedAvatar"] = "avatarKiwiTeal/kiwiTeal";

		// available levels
		this._data["levels"] = ["default"];
		this._data["selectedLevel"] = "default";

		// available babies
		this._data["babies"] = [];
		this._data["selectedBabies"] = [];

		// progress and achievements
		this._data["topScores"] = [];
		this._data["topMeters"] = [];
		this._data["bestRun"] = 0;
		this._data["nestablesCollected"] = {};
		this._data["achievementsEarned"] = {};
		this._data["upgradesPurchased"] = {};

		// tracking
		this._data["gamesPlayed"] = 0;

		// settings
		this._data["sfxEnabled"] = true;
		this._data["musicEnabled"] = true;

		this._data["appRated"] = false;

		this._data["ShowAds"] = true;
	};

	// ensure new data is added for users who reinstall the game
	this.initNewData = function() {
		// add gems and hearts if they don't already exist
		var gems = this.getData("gems");
		if (gems === undefined) {
			this.setData("gems", 80);
		}
		var hearts = this.getData("hearts");
		if (hearts === undefined) {
			this.setData("hearts", 5);
		}
		var babySlots = this.getData("babySlots");
		if (!babySlots) {
			this.setData("babySlots", 1);
		}

		// add XP and level if they don't already exist
		var XP = this.getData("XP") || 0;
		this.setData("XP", XP);
		var level = this.getData("level") || 1;
		this.setData("level", level);

		// init angel / equip experiment counts
		var angelFishUses = this.getData("angelFishUses") || 0;
		this.setData("angelFishUses", angelFishUses);
		var megaGliderUses = this.getData("megaGliderUses") || 0;
		this.setData("megaGliderUses", megaGliderUses);

		// init rating req
		var req = this.getData("RatingReq") || 20;
		this.setData("RatingReq", req);

		// init achievements
		var achievements = this.getData("achievements") || {};
		var achievementsConfig = this.controller.achievementsConfig;
		for (var a in achievementsConfig) {
			var achievo = achievementsConfig[a];
			achievements[achievo.id] = achievements[achievo.id] || { level: 0 };
		}
		this.setData("achievements", achievements);

		var berriesCollected = this.getData("berriesCollected") || 0;
		this.setData("berriesCollected", berriesCollected);

		// init topMeters if we haven't already
		var topMeters = this.getData("topMeters") || [];
		this.setData("topMeters", topMeters);

		// init avatars if we haven't already
		var avatars = this.getData("avatars") || ["avatarKiwiTeal/kiwiTeal"];
		this.setData("avatars", avatars);

		// init levels if we haven't already
		var levels = this.getData("levels") || ["default"];
		this.setData("levels", levels);

		var selectedLevel = this.getData("selectedLevel") || "default";
		this.setData("selectedLevel", selectedLevel);

		// init upgrades purchased
		var purchased = this._data["upgradesPurchased"] || {};
		var upgrades = this.controller.upgradesConfig;
		for (var u in upgrades) {
			var upgrade = upgrades[u];

			// if they already have a character, don't let them buy it
			if (upgrade.type === "AVATAR") {
				var value = upgrade.levels[0].value;
				if (avatars.indexOf(value) >= 0) {
					purchased[upgrade.id] = {
						level: 1
					};
				}
			}

			// init upgrades to level 0
			if (!purchased[upgrade.id]) {
				var initLevel = 0;
				if (upgrade.id === "turtleFriend") {
					initLevel = 1;
				} else if (upgrade.id === "turtleBestFriend") {
					initLevel = 4;
				}

				purchased[upgrade.id] = {
					level: initLevel
				};
			}
		}

		this.setData("upgradesPurchased", purchased);

		if (!this.getData("newUser")) {
			this.migrateOldUser();
		}
	};

	// convert old upgrades into new upgrades
	this.migrateOldUser = function() {
		var purchased = this._data["upgradesPurchased"] || {};
		var upgrades = this.controller.upgradesConfig;
		for (var p in purchased) {
			if (!upgrades[p]) {
				var oldPurchase = purchased[p];
				var level = oldPurchase.level;
				for (var u in upgrades) {
					var upgrade = upgrades[u];
					if (upgrade.spawnID === p) {
						purchased[u].level = level;
					}
				}
				delete purchased[p];
			}
		}
		this.setData("upgradesPurchased", purchased);
	};



	// new users are placed in only 1 group of only 1 experiment
	this.setNewUserExperiment = function() {
		var experimentData = this.controller.experimentConfig.active,
			experiment, groupKey;

		if( experimentData ) {
			experiment = Utils.getWeightedRandomObject(experimentData);


			groupKey = Utils.getRandomObjectKey(experiment.groups);

			this.controller.trackExperiment(experiment.id, groupKey);
			this.setData("activeExperiment", this.controller.experiment);
		}
	};

	this.getData = function(key) {
		return this._data[key];
	};

	this.setData = function(key, value, skipSave, skipServer) {
		this._data[key] = value;

		if (!skipSave) {
			this.saveData(skipServer);
		}
	};

	this.saveData = function(skipServer) {
		localStorage.setItem(this._key, JSON.stringify(this._data));

		/*if (!skipServer && this.serverDataReady) {
			if (!this.serverTimestamp || Date.now() - this.serverTimestamp >= SERVER_COOLDOWN) {
				this.uploadServerData();
				this.serverTimestamp = Date.now();
			}
		}*/
	};
});
