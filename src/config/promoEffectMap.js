exports = {

	newMap: {
		views: [
			{
				view: 'controller.screenViews.MainMenuView',
				buttons: [ 'controller.screenViews.MainMenuView.levelPic' ]
			}
		]
	},
	newBabyKiwi: {
		views: [
			{
				view: 'controller.equipModalView',
				buttons: [ 'controller.equipModalView.kiwiSlots.2' ]
			}
		]

	}
};
