import device as device;
device.defaultFontFamily = "Patrick";

import ui.View as View;

import src.controllers.Controller as Controller;
import src.lib.LocaleManager as LocaleManager;
import src.models.FacebookModel as FacebookModel;

import billing;

exports = Class(GC.Application, function(supr) {

/* ~ ~ ~ ~ GAME VERSION INFO ~ ~ ~ ~ *
	GAME v ~~~~~~~ 2.2.4
	ANDROID v ~~~~ 2.5.0
	BASIL v ~~~~~~ beta-0.0.9
* ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ */

	// app store IDs
	var APP_STORE_ID_GOOGLE = "GOOGLE",
		APP_STORE_ID_APPLE = "APPLE",
		APP_STORE_ID_AMAZON = "AMAZON",
		APP_STORE_ID_TSTORE = "TSTORE",
		APP_STORE_ID_ICEPOP = "ICEPOP";

	// in-app purchase constants
	var PURCHASE_STATUS_SUCCESS = 0,
		PURCHASE_STATUS_REFUND = 1,
		PURCHASE_STATUS_CANCEL = 2,
		statusStrings = ["Success", "Refund", "Cancel"];

	// SDK GC.Application settings
	this._settings = {
		alwaysRepaint: true,
		logsEnabled: true,
		noTimestep: false,
		showFPS: false,
		clearEachFrame: false,
		preload: ["resources/images/MainMenu"],
		disableNativeViews: true
	};

/* ~ ~ ~ MAIN ENTRY POINTS ~ ~ ~ */

	this.initUI = function() {

		// NOTES ABOUT RELEASING:
		//     * this.appStore must be set appropriately
		//     * any manifest keys still need to be changed manually
		//         * i.e. Flurry Key

		this.appStore = APP_STORE_ID_GOOGLE;

		// set up app store specific flags
		switch(this.appStore) {
			case APP_STORE_ID_GOOGLE:
				this.enableOfferwalls = false;
				this.enableAds = false;
				this.enableStoreModals = true;
				this.enableSessionM = false;
				this.enableWebMode = false;
				this.enableSocial = false;
				this.storeProductID = "id";
				// NATIVE store defaults to Google Play on Android
				break;

			case APP_STORE_ID_APPLE:
				this.enableOfferwalls = false;
				this.enableAds = false;
				this.enableStoreModals = false;
				this.enableSessionM = false;
				this.enableWebMode = false;
				this.enableSocial = false;
				this.storeProductID = "id";
				// NATIVE store defaults to Apple App Store on iOS
				break;

			case APP_STORE_ID_AMAZON:
				this.enableOfferwalls = false;
				this.enableAds = false;
				this.enableStoreModals = false;
				this.enableSessionM = false;
				this.enableWebMode = false;
				this.enableSocial = false;
				this.storeProductID = "id";

				// point NATIVE to the appropriate app store
				//if (GLOBAL.NATIVE && NATIVE.purchase && NATIVE.purchase.AMAZON_STORE) {
					//NATIVE.purchase.setStore(NATIVE.purchase.AMAZON_STORE);
				//}
				break;

			case APP_STORE_ID_TSTORE:
				this.enableOfferwalls = false;
				this.enableAds = false;
				this.enableStoreModals = false;
				this.enableSessionM = false;
				this.enableWebMode = false;
				this.enableSocial = false;
				this.storeProductID = "tstore";

				// point NATIVE to the appropriate app store
				//if (GLOBAL.NATIVE && NATIVE.purchase && NATIVE.purchase.T_STORE) {
					//NATIVE.purchase.setStore(NATIVE.purchase.T_STORE);
				//}
				break;

			case APP_STORE_ID_ICEPOP:
				this.enableOfferwalls = false;
				this.enableAds = false;
				this.enableStoreModals = false;
				this.enableSessionM = false;
				this.enableWebMode = true;
				this.enableSocial = false;
				this.storeProductID = "id";
				// this version is intended for the IcePopBeat website, all things are free!
				break;
		}



		// set up localization
		this.l = LocaleManager.getLocalizeInstance();



		// useful code to catch view initialization during gameplay
		/*var initOrig = View.prototype.init;
		View.prototype.init = function(opts) {
			if (GC.app.controller
				&& GC.app.controller.screenViews
				&& GC.app.controller.screenViews['GameView'])
			{
				var gameView = GC.app.controller.screenViews['GameView'];
				if (gameView.gameInitialized
					&& !gameView.gamePaused
					&& !gameView.model.finished
					&& gameView.gameStarted)
				{
					debugger;
					logger.warn('~~~ View Instantiation During Level');
				}
			}

			initOrig.call(this, opts);
		};*/
	};

	this.launchUI = function() {
		// init our controller
		this.controller = Controller.get();

		// init facebook model
		if (this.enableSocial) {
			this.controller.fb = new FacebookModel();
		}

		// start on the Title Screen
		this.controller.transitionToTitle();
		if (this.controller.showAds && this.enableAds) {
			NATIVE.ads.enableInterstitial('a150ad80c56f07f');
			NATIVE.ads.subscribe('AdShow', bind(this, function(network) {
				this.controller.trackEvent('AdShow', {
					network: network
				});
			}));
		}



		// organize product data and order history
		this.processedOrders = this.controller.getData("processedOrders") || {};
		this.processedRefunds = this.controller.getData("processedRefunds") || {};

		var items = this.controller.modalStoreConfig.products.gems.items;
		var productItems = {}; // populate item list referenced by product ID
		for (var i in items) {
			var item = items[i];
			productItems[item[this.storeProductID]] = item;
		}

    //NATIVE.purchase.onResult = bind(this, function(status, productID, orderID) {
    billing.onPurchase = function onBillingSuccess (productID, transactionInfo) {
      // this.controller.trackEvent("PurchaseCallback", {
      //   status: statusStrings[status],
      //   productID: productID + "",
      //   orderID: orderID + ""
      // });
      this.controller.trackEvent("PurchaseSuccess", {
        productID: item + ""
      });

      if (!this.processedOrders[orderID]) {
        this.processedOrders[orderID] = true;
        this.controller.setData("processedOrders", this.processedOrders);
      } else {
        return true;
      }

      var purchase = productItems[productID];
      if (purchase) {
        this.controller.productController.purchasePlayStoreGems(purchase);
        return true;
      }
    };

    billing.onFailure = function onBillingFailure (reason, item) {
      if (reason !== 'cancel') {
        // Market unavailable?
      }

      // cancelled
      // if (this.processedOrders[orderID]
      //   && !this.processedRefunds[orderID]
      //   && productID)
      // {
      //   this.processedRefunds[orderID] = true;
      //   this.controller.setData("processedRefunds", this.processedRefunds);

      //   // take gems back if we can
      //   var refund = productItems[productID];
      //   if (refund) {
      //     this.controller.removeRefundedGems(refund.amount);
      //   }
      // }

      return true;
    }.bind(this);

		GLOBAL.NATIVE && NATIVE.currency && NATIVE.currency.on('Add', bind(this.controller, 'addGems'));
		window.addEventListener('onfocus', bind(this, 'onAppFocus'), false);
		window.addEventListener('onblur', bind(this, 'onAppBlur'), false);
		window.addEventListener('pageshow', bind(this, 'onAppShow'), false);
		window.addEventListener('pagehide', bind(this, 'onAppHide'), false);
	};

	this.onAppFocus = function() {
		logger.log("ON APP FOCUS CALLED");
	};

	this.onAppBlur = function() {
		logger.log("ON APP BLUR CALLED");
		this.controller.pauseGame();
	};

	this.onAppShow = function() {
		logger.log("ON APP SHOW CALLED");

		var c = this.controller;
		c.resumeSong();
		c.resumeApp(); // handles preloading and splash screen

		// resubscribe controller's tick on resume
		GC.app.engine.subscribe('Tick', c, 'onTick');
	};

	this.onAppHide = function() {
		logger.log("ON APP HIDE CALLED");

		var c = this.controller;
		c.pauseSong();
		c.pauseGame();
		c.overlaySplash();

		// first session tracking
		if (c.getData("firstSession")) {
			c.setData("firstSession", false);

			var playtime = c.totalPlaytime;
			var gamesPlayed = c.getData("gamesPlayed") || 0;

			if (playtime < 30000) {
				playtime = "30 seconds or less";
			} else if (playtime < 60000) {
				playtime = "30 - 60 seconds";
			} else if (playtime < 120000) {
				playtime = "1 - 2 minutes";
			} else if (playtime < 180000) {
				playtime = "2 - 3 minutes";
			} else if (playtime < 240000) {
				playtime = "3 - 4 minutes";
			} else if (playtime < 300000) {
				playtime = "4 - 5 minutes";
			} else if (playtime < 600000) {
				playtime = "5 - 10 minutes";
			} else if (playtime < 1800000) {
				playtime = "10 - 30 minutes";
			} else if (playtime < 3600000) {
				playtime = "30 - 60 minutes";
			} else {
				playtime = "more than 1 hour";
			}

			c.trackEvent("FirstSession", {
				gamesPlayed: gamesPlayed + "",
				playtime: playtime
			});
		}

		// ensure our playtime gets saved and doesn't tick after pausing
		c.setData("totalPlaytime", c.totalPlaytime);
		GC.app.engine.unsubscribe('Tick', c, 'onTick');
	};
});
